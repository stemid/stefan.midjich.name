# stefan.midjich.name

This is the personal website of Stefan Midjich.

It is also the old wiki from wiki.sydit.se converted from dokuwiki format into mkdocs markdown format.

## Local testing

    $ pip3 install -r requirements.txt
    $ mkdocs serve

## Deployment

Requires the following variables set in CI/CD environment.

* ``AWS_ACCESS_KEY_ID`` - Create one in IAM for this purpose with an inline policy, see below for example.
* ``AWS_SECRET_ACCESS_KEY`` - The secret token.
* ``AWS_DEFAULT_REGION`` - Wherever you create your S3 bucket, mine was eu-north-1 of course.
* ``S3_BUCKET_URI`` - Used in .gitlab-ci.yml like ``s3://stefan.midjich.name/``.
* ``CLOUDFRONT_DISTRIBUTION`` - Used in CI job to invalidate caches when site is updated.

### Inline JSON policy

This is for both deploying to S3 bucket and invalidating Cloudfront distribution cache, which is done by the CI job.

```JSON
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "StefanMidjichNameS3DeploymentPolicy",
            "Effect": "Allow",
            "Action": [
              "s3:PutObject",
              "s3:GetObject",
              "s3:ListObjects"
            ],
            "Resource": [
                "arn:aws:s3:::stefan.midjich.name",
                "arn:aws:s3:::stefan.midjich.name/*"
            ]
        },
        {
            "Action": [
                "cloudfront:CreateInvalidation"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:cloudfront::<your cloudfront distribution arn>"
        }
    ]
}
```

## Puppys

The puppy pictures are uploaded manually to AWS.

### Building the gallery

    sigal build -c valpar.conf.py

### Uploading the gallery

    aws s3 sync --metadata-directive REPLACE pictures "s3://stefan.midjich.name/pictures/" --delete

### Invalidating Cloudfront cache to update the page quicker

    aws cloudfront create-invalidation --distribution-id "<cloudfront distribution ID>" --paths '/pictures/*'

### Sigal theme mods

I'm using the [sigal-justlight theme with some modifications](https://github.com/stemid/sigal-justlight) to make all external JS and CSS local.
