#!/usr/bin/env python3

import sys
from datetime import datetime
from csv import DictReader
from matplotlib import pyplot

INPUT_CSV_FILE = 'puppys.csv'
OUTPUT_FILE = 'docs/files/puppy_weight.png'
plots = {}

with open(INPUT_CSV_FILE, 'r') as csvfile:
    # Only use last 16 metric values (4 puppies so 4*16)
    lines = list(DictReader(csvfile, delimiter=','))[-4*16:]

    for row in lines:
        # Init dict once if it does not exist
        if (name := row['Name']) not in plots:
            plots[name] = {
                'x': [],
                'y': [],
                'Gender': row['Gender']
            }
        dt = datetime.fromisoformat(row['Date'])
        plots[name]['x'].append(dt.strftime('%d/%m %H:%M'))
        plots[name]['y'].append(int(row['Weight']))

plot_colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']

for name, data in plots.items():
    pyplot.plot(
        data['x'], data['y'],
        color=plot_colors.pop(0),
        linestyle='dashed',
        marker='o',
        label='{name} {gender}'.format(
            name=name,
            gender=data['Gender']
        )
    )

pyplot.xticks(rotation=25)
pyplot.xlabel('Datum')
pyplot.ylabel('Vikt (gram)')
pyplot.title('Valparnas vikt', fontsize=18)
pyplot.grid()
pyplot.legend()

try:
    show = sys.argv.pop(1)
except IndexError:
    with open(OUTPUT_FILE, 'wb') as output:
        pyplot.savefig(output, bbox_inches='tight', dpi=100)
        sys.exit(0)

pyplot.show()
