# Nybörjarguide till Dwarf Fortress

**Dokumentet är ett pågående arbete.**

Det har tagit över ett år för mig att lära mig spela DF, men det har varit en underbar resa. Så jag vill gärna dela med mig av lite tips som jag plockat upp på vägen. 

## Vad är DF?

[Dwarf Fortress](http://www.bay12games.com/dwarves/) är ett hemmagjort spel som utvecklas av två bröder i USA. Det är extremt simpelt grafiskt, men nästan otroligt djupt spelmässigt. 

Den bästa beskrivningen jag kan tänka mig är spelstilen strategi, specifikt drift av en dvärgfästning. 

Det tar tid att lära sig till fullo, men tycker man om att lära sig nya saker så blir det en del av tjusningen på resan genom världen som är DF. I min egen åsikt så blir spelet roligare och mer ombytligt för varje ny sak man lär sig. 

## Spellägen

Det finns tre olika spellägen i DF, men alla tar plats i en och samma värld som man genererar procedurellt från huvudmenyn genom att trycka på *Create new world*. 

### Fortress

*Fortress mode* är drift av en dvärgfästning, detta spelar jag mest och dokumentet kommer fokusera mest på detta läget tills vidare. 

I detta läget ska man ta med sig 7 dvärgar, ett par kreatur, mat, frön och annan utrustning ut i vildmarken för att bygga sig en ny civilisation. 

Först och främst väljer man vart i den stora vida världen man ska bege sig. Det är bra att välja ett litet område, 4x4 t.ex. för att undvika lagg senare i spelet på grund av stora kartor. 

Sedan får man ge sina dvärgar poäng i olika kunskaper som är viktiga för överlevnad, samt ange vilken utrustning de ska ha med sig på sin resa. Allt är baserat på att kunskap och utrustning kostar poäng och när poängen är slut måste man se till att ha med sig allt nödvändigt för att bygga sin befästning. 

### Adventure

I *Adventure mode* antar man rollen av en ensam hjälte som i en av de procedurellt skapade världarna ska göra sin egen lycka med svärd, yxa eller båge. :)

### Legends

Det tredje spelläget talar lite för hur komplex värld som skapas av DF, för i *Legends* ska man enbart läsa igenom historian av världen, legenderna. 

## Gränssnitt

Nästan allt i DF styrs med tangentbordet, det går att måla med musmarkören men eftersom jag aldrig har försökt mig på det så tänker jag inte gå in på det. 

Det gäller att se sig omkring i fönstret efter instruktioner på vad man ska göra, för spelet skiftar ofta mellan att använda piltangenter, bokstäver och siffror för att navigera i menyer. 

För nybörjar rekommenderar jag starkt att välja ett paket som [Lazy Newb Pack](http://www.bay12forums.com/smf/index.php?topic=59026.0) eller [Mac Newbie](http://www.bay12forums.com/smf/index.php?topic=106790.0). Där man med enkla klick av musen kan slippa standardgrafiken och få något som påminner mer om gamla DOS-spel. 

### Vanliga styrtangenter

I kartan kan man flytta runt sig med piltangenter, det kan även fungera i många menyer. __TAB__ skiftar mellan kartan och huvudmenyn. 

Ofta kommer man dock se bokstäver bredvid menyalternativ, vilket betyder att en bokstav på tangentbordet motsvarar ett alternativ i menyn. 

När det inte är piltangenter som flyttar en markering upp, ner eller åt sidan, så är det __+__ och __-__, eller någon siffertangent. 

Siffertangenterna används även som standard för att flytta sin karaktär i *Adventure mode*, så en numpad fungerar som en sorts ersättare för joystick. 

Det finns olika lägen man kan äntra från huvudmenyn, som t.ex. när man trycker på tangenterna __d__ och __d__ igen vilket betyder att man ska ange områden för att gräva ut (//mining//). I detta läget flyttas en __X__ markör runt och när man trycker ner Enter så börjar markeringen av området som ska grävas ut. Vid nästa tillfälle som Enter trycks ner kommer hela området mellan de två punkterna markeras för utgrävning av dina dvärgar. 

# Se också


*  [DwarfFortress Wiki](http://dwarffortresswiki.org/index.php/Main_Page)


