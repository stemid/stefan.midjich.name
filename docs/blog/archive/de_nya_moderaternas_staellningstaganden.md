# De "Nya" Moderaternas ställningstaganden

Detta är ett gammalt inlägg som hängt med min personliga webbsida sedan 2007-2008 kanske, då de "nya" moderaterna var mer aktuella i nyheterna. Jag tyckte namnet var skrattretande och stal denna listan som var väldigt slående.

[Källa](http://www.dalademokraten.se/Opinion--Kultur/Kultur/En-liten-moderat-historielektion-lista-over-vilka-reformer-hogerpartietmoderaterna-/).

## Moderaternas ställningstaganden genom åren som gått

*  1904-1918: Nej till allmän rösträtt.
*  1916: Nej till allmän olycksförsäkring i arbetet.
*  1919: Nej till åtta timmars arbetsdag.
*  1919: Nej till kvinnlig rösträtt.
*  1921: Nej till avskaffandet av dödsstraff i Sverige.
*  1923: Nej till åtta timmars arbetsdag.
*  1923: Nej till avskaffandet av dödsstraff i Sverige.
*  1927: Nej till folkskolereform.
*  1931: Nej till sjukkassan.
*  1933: Nej till beredskapsarbete.
*  1934: Nej till a-kassa.
*  1935: Nej till höjda folkpensioner.
*  1938: Nej till två veckors semester.
*  1941: Nej till sänkt rösträttsålder.
*  1946: Nej till fria skolmåltider.
*  1946: Nej till allmän sjukvårdsförsäkring.
*  1947: Nej till allmänna barnbidrag.
*  1951: Nej till tre veckors semester.
*  1953: Nej till fri sjukvård.
*  1959: Nej till ATP.
*  1960-talet: Ja till apartheid. Moderaterna tog avstånd från alla sanktioner mot apartheidregimen i Sydafrika och var emot det svenska stödet till ANC.
*  1963: Nej till fyra veckors semester.
*  1970: Nej till 40-timmars arbetsvecka.
*  1973: Nej till möjligheten till förtidspensionering vid 63.
*  1974: Nej till fri abort. Den 29 maj 1974 röstade riksdagen ja till fri abort, vilket resulterade i den svenska abortlagen som låter kvinnan själv besluta om abort upp till den 18 graviditetsveckan. Moderaterna röstade nej.
*  1976: Nej till femte semesterveckan.
*  1983: Nej till löntagarfonderna.
*  1994: Nej till partnerskapslag för homosexuella.
*  1998: Nej till erkännande av homosexuellas rättigheter inom EU. EU-parlamentet röstade för ett erkännande av homosexuellas rättigheter, men de moderata ledamöterna röstade nej.
*  2003: Ja till Irakkriget. Alla riksdagspartier demonstrerade mot och kritiserade Irakkriget utom moderaterna.
*  2004: Ja till sänkt a-kassa och sjukpenning.
*  2006: Nej till gröna jobb.
*  2006: Nej till sex timmars arbetsdag.
*  2006: Nej till upprustning av offentliga sektorn.
*  2006: Nej till höjd a-kassa.
*  2006: Ja till sänkt a-kassa.
*  2006: Nej till höjd sjukersättning.
*  2007: Ja till sänkt sjukersättning.

Nu är jag den första som anser att både socarna och moderåttorna går och putsar skorna av storföretagarna, men en sak är säker, en röst på moderaterna är en bortkastad röst. 

