# Öppen Källkod

Öppen Källkod betyder att du kan ladda ner programkoden för ett program. Oftast gäller detta under olika licenser, eller villkor. 

Fri mjukvara kan ses på olika sätt, enligt [FSF](https://en.wikipedia.org/wiki/Free_Software_Foundation) är det mjukvara vars licens inför så lite restriktioner på användandet av koden som möjligt. Det kan även vara kod vars licens tvingar användare att hålla sig till vissa regler, som t.ex. att inte få använda koden på hårdvara som inte anses vara gästvänlig för hobbyarbete och hacking. 
## Gemenskap

Eftersom öppen källkod i grunden är ideellt så finns det naturligt en frivillig gemenskap runt hela konceptet. En gemenskap av vanliga personer som antingen tycker om att göra eller använda öppen källkod. 

Det är upp till gemenskapen att hjälpa varandra så gott de kan eftersom det sällan finns en central organisation som kan tala för all öppen källkod som produceras. 

Så när man talar om gemenskapen kan det betyda alla inblandade i öppen källkod, eller specifikt forum, epostlistor, IRC-kanaler och andra sätt att diskutera öppen källkod med andra. 
