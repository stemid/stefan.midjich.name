---
title: Smulans dräktighet
date: 2021-08-02
summary: Dagbok och sammanfattning av Smulans dräktighet, en Jack Russel Terrier
---

# Om dagboken

Vad händer under en hunds dräktighet? Här har jag skrivit ner lite saker i efterhand och lite saker medans de hände. På internet hittar du många olika sidor om ämnet som antingen är skrivna ur perspektivet av en hundras, eller i allmänhet, men detta är enbart från mitt perspektiv.

Jag började skriva dagboken måndag den andra augusti när värkar hade börjat, så mycket av informationen är i efterhand.

# Smulan

Smulan är en väldigt aktiv Jack Russel Terrier född 2017-09-12, hon är ganska lågställd men inte så pass att hennes korta ben blir ett handikapp. Renrasig, vaccinerad och undersökt för patella luxation bland annat.

Jag tycker hon är fantastisk och väl uppfostrad med en god instinkt.

# Löp

Innan jag uppskattade att hon skulle löpa gav jag henne mer foder än vanligt, så hennes vikt gick upp från standardvikt 4.5kg till 4.8kg. Jag råkade ge lite för mycket ett tag så den gick upp till 5kg men stabiliserade på 4.8 innan parning.

För att upptäcka att löpet börjat använde jag mig av ett par okastrerade hanar i grannskapet, genom att observera deras reaktion när de mötte henne.

Måndagen för första försöket till befruktning stötte jag av en slump på en hane och Smulan reagerade väldigt starkt genom att vända sin stjärt mot hanen, och vända ut slidan så den stack ut längre. Innan dess hade den även börjat svullna lite så att den kommit fram ur pälsen.

Normalt är Smulan väldigt avvisande mot hanar.

# Befruktning

Man brukar räkna dräktigheten från dagen för befruktning, i Smulans fall åkte vi torsdag 2021-06-03 upp till Jackflash Kennel utanför Ängelholm och parade henne med Calle som är en Jack Russel av liknande storlek.

Det tog två försök, första försöket var måndagen innan men Calle var inte sugen nog. Han blev inte hård nog eller tryckte inte med tillräcklig kraft för att komma in i Smulan.

Uppfödaren rekommenderade att gå till veterinären och mäta Smulans nivåer av progesteron för att fastställa hur långt fram i löpet hon var. Detta borde gjorts tidigare i löpet, vi var ute i sista minuten.

Andra fösöket var mer lyckat så vi blev sittandes på golvet i 35 minuter innan Smulan släppte Calle.

Vi höll i var sin hund så de inte skulle skada varandra genom att dra.

Calles ägare tog omedelbart efter lite kallt vatten på fingrarna och vidrörde Smulans snippa snabbt för att få den att dra ihop sig.

Det tar upp till 17 dagar för äggen att "fästa", så omedelbart efter befruktning ska man ta det lugnt med promenad och andra aktiviteter. Hunden kan bokstavligt talat skaka ur sperman.

# Dräktighet

Under det mesta av dräktigheten har Smulan varit väldigt pigg och velat mycket, så jag fick ofta bromsa henne från att överanstränga sig.

Man ska inte ge hunden mycket proteinrik mat i början eftersom valparna kan bli så stora att de kräver kejsarsnitt. Fortsätt helst med vanlig kost.

Smulan fick mot halva dräktigheten ganska dålig aptit, så pass att jag var orolig. Särskilt på dag 45-50 och framåt då hennes mage var enorm men hon åt nästan inget.

Jag använde alla möjliga knep som att blanda ut torrfoder med leverpastej, hundglass osv..

Runt dag 50-55 började man känna valpar sparka utåt i magen, då började aptiten återvända också.

# Förlossning

## Aggressiv under förvärkar

Fick intrycket att Smulan var lite aggressiv under förvärkar men blev mycket lugnare under de senare värkarna. 

Detta observerade jag två gånger, först några timmar innan vattnet gick då hon morrade åt mig när jag ville klappa henne.

Sedan var hon lugn innan vattnet gick, efter, och under de första tre valparnas förlossning. Då kunde man massera henne och röra henne.

Den fjärde valpen var väldigt sen så ca. 6 timmar efter den tredje började hon få dessa förvärkar igen och blev åter aggressiv och ville inte ha någon kontakt med oss. Fram till hon började klämma ut den, samma beteende som sist.

## Tog lååång tid

Totalt från första tecknet på att något hände (oro, flämtande och kanske värk) till sista valpen var ute tog 35 timmar!

Första tecknet på att något var på gång kom runt 23:00 den första augusti, vilket är dag #59. Vi gick och la oss så hon fick oroa sig ensam hela natten.

Hon flämtade med viss frekvens och var orolig, letade ofta efter en bekväm ställning. Vilade lite mellan orosmomenten.

Hela dagen den andra augusti lät vi henne vara då hon ständigt letade efter en bättre ställning, denna oro blev värre och värre mot natten.

En blåsa med fostervatten kom ut först runt 04 på natten den tredje augusti, vattnet gick strax därefter när vi gick ut för att kissa och bajsa.

De första tre var ute till kl. 08 samma dag, och den fjärde kom inte förrän klockan 15 samma dag.

## Oroliga över blåsan

Eftersom blåsan med fostervatten stack ut så lång tid blev vi oroliga och ringde jour-veterinär på Evidensia. De rådde oss att gå en runda med henne. Så jag passade på att gå ut så hon fick kissa och på vägen sprack blåsan med vattnet.

Efter det fortsatte processen utan någon märkvärdig oro.

Blåsan är väldigt tydligt inte en valp, den är mycket mörk, rödaktig. Valparna när de väl sticker ut är den färg som pälsen blir, med en tunn transparent hinna över. I vissa fall kan man även se dem röra på sig.

## Kräktes

Hon kräktes ett par gånger tidigt under förlossningen, stackarn försökte klättra ut ur sin hundbädd bara för att inte spy på filtarna.

## Kissa och bajsa

Hon kunde utan problem gå ut fram till att vattnet gick, efter det stannade hon inne fram till nästa dag.

Sista promenaden var en uppmaning från Evidensias jour-veterinär. Det var ett bra förslag för då sprack bubblan och vattnet gick.

Jag hade en ficklampa och en handduk med på den sista promenaden om något skulle trilla ut.

Innan förlossningen gick hon ut fyra gånger totalt och det var alltid business, rakt till första gräsplätten, kissa, bajsa, sen rusa hem.

Jag gjorde vår vanliga ritual; klädde på mig, tog mobilen, hörlurar, allt medan jag frågade om hon ville ut och kissa&bajsa. Till slut satte jag på skorna och skramlade i hennes koppel och hon kom till dörren.

# Efter förlossning

Första promenaden efter förlossningen var lite kul. Det var förmiddag den fjärde augusti.

Hon skyndade sig ut, stapplade lite, gick som vanligt till första biten gräs, kissade, bajsade, sen följde hon mig ut ur skuggan in i solen. Där sprang hon en bit, bajsade en gång till, sen sprang hon full gallopp hem! Gnällde vid varje dörr jag skulle öppna åt henne.

Annars har hon återhämtat sig snabbt. I skrivande stund är det kväll den fjärde augusti och hon har redan varit ute 3 gånger. Varje gång lite längre än innan.

Men det är fortfarande fokus på att göra sina behov och springa hem. På eftermiddagen har hon även varit runt i hemmet och utforskat, hoppat upp på soffa och vanliga saker som hon annars brukar göra. Så hon verkar vara tillbaka.

# Återhämtad

Det är nu eftermiddag 5:e augusti och Smulan är tillbaka till sitt vanliga jag. Hon går lite längre promenader och hälsade på en granne som hjälpte till med förlossningen t.ex..

Men vid halva biten hem sprang hon fortfarande till sina valpar. Så hennes beteende i allmänhet är mer moget och sakligt.

Hon är inte lika intresserad av sina leksaker när hon kommer nära valparna.

## Aptit

Hennes aptit är enorm och vi hade nog kunnat mata henne varannan timme om vi hade disciplinen för det.

Direkt efter förlossning matar vi henna minst 5 gånger om dagen och varje matning äter hon mer än en vanlig skål av sin hundskål.

Jag ger henne Canagan våtfoder men allteftersom ska jag nog blanda ut det med hennes torrfoder för att få lite fastare avföring.

Den är ok nu men jag vet att torrfodret gör den extra fast och lätt att plocka upp ur gräset.

Vid varje stor portion hon äter gör jag mig redo för hon vill oftast ut och bajsa direkt efter.

När valparna nu är 4-5 dagar gamla matar vi henne 2-3 gånger om dagen istället. Har börjat blanda ut fodret med lite torrfoder.

# Ängslig

I dagarna efter förlossning har hon ibland uppvisat ängsligt beteende utan tydlig förklaring. En gång om dagen, ibland två, börjar hon gnälla och leta runt efter valparna trots att de är framför henne.

Ibland vet hon inte vad hon ska göra av sig, hoppar upp på soffa, ner igen, gräver sig under kuddar och filtar, sen ner igen. Detta kan pågå 10 minuter innan hon lugnar sig.

Jag provade att sätta hennes valpar i en mindre korg med hög kant på soffan och hela den dagen fick hon ingen "panikångest".

Vi spekulerade att hundbädden vi lånade kanske var för stor så hon var rädd att de skulle springa iväg över kanten.

# Valparna sjunger och dansar

Valparna kan rent ut sagt skrika för ingen anledning, antar att de testar sina förmågor. Ibland gnyr de högt och utdraget medans de ammar, ibland bara ligger de och gnyr tills man lyfter dem mot mamman.

De drömmer också livligt, kan ibland skälla högt i sömnen. Eller vakna med ett skrik!

Från slutet av vecka 2 har de börjat gå mer självständigt och bråka med varandra. De kan klättra ut ur lådan så jag har satt dem i valphagen där de kan springa runt och leka.

# Smulan har gaser

Från början av vecka 3 har Smulan haft gaser konstant. Det luktar ständigt fis i varje rum hon är i, och hon måste alltid vara i samma rum som mig.

Jag gissar att det har med ökad mjölkproduktion att göra. Det är vecka 3-4 så valparna ska börja få riktig mat nu snart.

# Tips

## Smulans hundbädd

Insåg att filtar och pläd i hennes hundbädd var inte lika hygieniskt som att bara använda vanliga gamla handdukar. Är mycket lättare att snabbt rengöra handdukar från bajs och kräk.

## Varning för godis

Min bror kom och hälsade på med lite presenter till Smulan, bland annat ett godisben. Det var ett misstag att ge henne det för hon blev territorial och morrade åt en valp som råkade krypa mot godiset.

Vi fick ta ifrån henne det tillsvidare. Bäst att hålla alla distraherande saker som godis och leksaker borta tills hon är redo.

## Ögonen öppnas

Måndag 16:e augusti, dag 13 efter födsel, har Första och Fjärde öppnat ögonen lite. Första började redan dagen innan men nu verkar två stycken sakta öppna ögonen lite.

# Valpar

De är döpta efter ordningen som de föddes i.

## Första

Var störst i början, bruna markeringar. Markeringar runt huvud och nacke, annars är kroppen helt vit.

## Andra

Minst vid födsel, och även i skrivande stund dagarna efter födseln. Svart markeringar vid huvud, bara en svart fläck som går från roten av svansen till höger lår, annars bara vit.

## Tredje

Denna hade både svart och brun markering på huvudet. En svart fläck ovanför vänster höft och en svart fläck vid roten av svansen.

## Fjärde

Denna verkar ha flest fläckar, bland annat två svarta prickar vid svansens rot som nästan sitter ihop som en åtta. Den var också ganska stor redan vid födsel.

# Valparnas vikt

Grafen visar bara senaste veckan ungefär, så den inte blir för stor.

![Graf över valparnas vikt](/files/puppy_weight.png)

## Valparnas vikt i CSV

