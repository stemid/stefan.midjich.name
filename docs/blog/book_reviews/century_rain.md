# Century Rain

By Alastair Reynolds, published in 2004.

## Synopsis

Future earth is destroyed by nano-tech gone awry when our protagonist discovers a portal to an alternative timeline of earth in 1959 where the nazis were defeated before they could conquer France.

## Review

* 4.5/5

I really liked this book, even though the story is all over the place. It's hard to write a short synopsis without making it sound insane.

As always, Alastair Reynolds excels in imaginative world building. Just like [Terminal World](terminal_world.md) this one is more fast paced than I'm used to from this author. Another adventure story where you're kept on the edge of your seat, but less epic travel and more noir detective story mixed with scifi.

One of my favorite scifi concepts is when people from a high tech future encounter primitive versions of themselves, perhaps from the past. This book manages to do that, without the paradox of time travel.

Of course it involves god-like technology from a mysterious precursor race that have vanished from the galaxy and left their toys behind.

Besides those concepts the book also delivers concepts like space travel, portals and alternative history.
