# Earth Abides

By George R. Stewart and published in 1949.

## Synopsis

A survivor of a massively deadly pandemic explores the United States shortly before settling down and breeding new humans with the first attractive female survivor he finds.

## Review

* 2/5

It's an OK post-apoc book but there's not enough exploration. The protagonist almost immediately settles down and then just talks about their daily life re-building a small community of polygamists and idiots.

Much sooner than later the story becomes very slow and drawn out. Their new founded community only makes one expedition away from their settlement and that part isn't even talked about so much, or experienced 1st hand by the reader. The few things you do learn about this expedition only makes you thirsty for more material in that vein.

The ending, or rather the last 3rd of the book, is almost excruciatingly slow. Eventually the protagonist just details himself growing old and slipping away. Could not be more depressing.

You can almost tell that this book was forward thinking when published, and yet it's still extremely prejudiced. The ideas of the 40s really come through, like mentally challenged people, blacks and women barely being considered as human.
