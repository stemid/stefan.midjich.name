# Terminal World

By Alastair Reynolds, published in 2010.

## Synopsis

A pathologist goes on an epic adventure on earth of a distant future, after a mysterious cataclysm has left the world twisted and complex.

## Review

* 4/5

Alastair Reynolds always has the most amazing world building, this is no exception. It's hard to define this novel in a short synopsis, and that's partly why I like it.

The pace might be described as a bit quicker than other books from the same author, the adventure is ongoing and exciting events often follow each other back to back.

The technology is a mix of relatable near-future or present-time tech, to god-like "magic" technology from a distant hypothetical future.

That's really the only negative I can think of, that the cataclysm affecting earth is so fantastical you have a hard time digesting it. But once you consider nano-tech, GM and how such knowledge can be lost during technological dark ages you become more accepting.
