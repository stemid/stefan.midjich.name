# Station Eleven

By Emily St. John Mandel, published in 2015.

## Synopsis

The stories of several different people and how their fates all revolve around a single actor who passes away from a heartattack the day that the world descends into a pandemic apocalypse.

## Review

* 4/5

This is an exciting and well paced post-apoc book that left me wanting more stories in the same universe. One of the protagonists is almost a comic book like super hero, reminiscent of Eli in *The Book of Eli*.

During the last 3rd of the book I could not put it down, it was literally gripping.

The only negative was that some of the story feels rushed and condensed. The author creates a lot of interesting premises that don't go anywhere, or are cut short. Which is why I hope and believe there are sequels coming. It sure has the basis of a good series with the female protagonist called Kirsten.
