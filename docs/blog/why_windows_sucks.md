# Why Windows Sucks

I've decided to maintain a list of all the things that annoy me with Microsoft Windows. Real original huh? I'm just tired of people looking at me like I'm insane for criticizing Windows these days.

I'll just summarize the main issue here, Microsoft is making money off of Windows. It's their main product. And yet it sucks. So most of these bullet points could be prefaced with;

>With all that money rolling in, all those Windows taxes and all those PCs being shipped with Windows I can't believe it's still like this.

Most of these are for Windows 10 and onwards, I started the page in **2020-01-31** and you can see the last update at the bottom.

##  Where are my routes/NICs? 

!!! note "This issue was promoted from Random bugs 2020-06-24"
    This is literally the biggest issue to me that affects me most regularly.

Any sane power user would expect static routes and network interface IDs to remain static.

Not in Windows!

Granted I could attribute this issue to Juniper Pulse VPN client. But even with external influence a normal OS like Unix or Linux does not change the ID of its NICs.

That is exactly what happens intermittently on my Windows 10. My static route disappears, likely because the NIC ID changes. Sometimes it's 3, sometimes it's 4. And then suddenly it's 18.

Another reason why Windows is not suitable for Power users.

Even if the NICs stay the same routes still stop working and I have to run both route delete and route add -p to re-enable them. I suspect it might have to do with moving my host laptop between networks (home, café, vpn) and the guest VM not keeping up. But it's still attributed to Windows not supporting power users or giving them proper facilities to use their OS in unsupported ways.

## Sends your password to any remote UNC path you click

This blew my mind in [the news lately](https://www.bleepingcomputer.com/news/security/zoom-client-leaks-windows-login-credentials-to-attackers/) when I read about criticism towards the Zoom meeting app because you can send UNC paths to users that are converted to clickable links.

But the real criticism should be aimed towards Windows because in the current climate, in 2020, they still naively send your password to any remote UNC path you might click on.

That is almost like broadcasting your password to the internet. Amazing.

## Privacy nightmare

Despite the fact that Microsoft sell products and services they still want to catch up to Google and own your data. When setting up a newly installed Windows 10 system I have to answer no to 11 questions about sharing my data with Microsoft.

As of last install of Windows 10 late 2019.

## Why two passwords?

Lately Microsoft have introduced PIN numbers for devices. Of course I understand their motivation but for power users who want to setup their own local accounts this now means you have two "passwords" to keep track of.

And even worse, they follow the same poor UX design as the rest of Windows so last I checked the PIN number had different password complexity requirements than regular local passwords. I couldn't use Swedish characters in PIN numbers for example.

## PIN "number" is not a mature implementation

I had a PIN "number" set of course but I chose to use a long regular password as PIN number because I have no portable devices.

The last blunder from MS when I was going to login to my Azure connected work VM I got a message stating that my Azure org required me to change my PIN. That's fine with me, except now it requires me to set a numerical PIN with no special characters, or whitespaces that I normally use in my passphrases.

So Microsoft rules just demoted my long and strong passphrase to a short and weak PIN number. Thanks a lot, Microsoft. Clearly this PIN number thing isn't a mature implementation, ugh... WHY START OVER WHEN YOU ALREADY HAD A GOOD PASSWORD SYSTEM?!

## The installer looks like it's 1999

The installer looks like a student made it for a school project. Just a couple examples;

Did a reinstall and it found my old files so it offered to do an upgrade to keep all my files, I jumped on this offer and clicked. But then was faced with a message saying "no you can't do an upgrade from here, only from inside the OS", the OS was already broken and wouldn't start which is why I needed the reinstall.

And so the whole process had to start over and I had to enter the CD key again because of this. It's like they don't even care about UX design in the installer because they don't expect normal users to see it.

And in the end it still managed to save my files in a Windows.old folder, real nice of them but how come they couldn't just use those files to make an upgrade?

## Error messages across Windows are often shit

Connecting to an SMB share I enter \\nas\Downloads and nothing happens for a while to indicate that it's connecting but that's normal for this OS. After a few seconds it wants to start the network troubleshooter.

But it never tells me that the name nas is not resolvable. I have to go into CMD and check ping or ipconfig /all to see that I don't have a suffix set for my LAN DNS. Something the network troubleshooter was unable to discover was that I had to write the full domain name.

This is typical of Windows errors. They try to coddle the user when in fact some users are more able to figure out the solution if they're given the actual error message.

Another example is that Windows loves to spit out error codes and links to their knowledge base, as if that has ever helped anyone but an engineer. 

So it's this massive contradiction that Windows is easy to use (as long as you use it within their frame of mind) but we give you error messages that only a trained engineer can interpret.

## Magic disks

I had a setup with a Bare metal Windows OS dual booting with a bare metal Fedora hypervisor. And in the Fedora hypervisor I had a Windows VM running on its own physical SSD.

Both Bare Metal Windows and VM Windows could see a disk that was empty, it was only used for Steam games by Bare Metal Windows.

So in VM Windows I created a folder on it and moved some files to the folder. No problem.

Then I booted into Bare Metal Windows and saw the same disk, wanted to copy the folder to the Bare Metal Windows OS disk. The folder disappeared before my eyes. And errors came up, strange errors of course because Windows never tells you what is wrong.

So I tried browsing the OS disk of VM Windows, which was also visible from Bare Metal Windows, since it was a physical SSD. 

No problem browsing it but my user directories were of course off limits due to permissions. So I went no further.

**Keep in mind that so far I've only done what I would consider READ operations.**

Of course Windows OS doesn't care what you consider Reading. Because it had done something to those disks, it had written something to them. Because when I then rebooted into Fedora Hypervisor and started the VM Windows, it wouldn't boot. It was corrupt.

I ended up having to reinstall the VM Windows because I had been "reading" the VM disks from bare metal Windows. 

This is a core issue with Windows, they do things behind the scenes that you don't know about.

## Random bugs

This section is just random bugs I encounter day to day and remember to write down. Its purpose is to further show that Windows which costs 2300kr in stores is no better than Linux which costs 0kr.

### 1px wide vertical line

Appears across multiple parts of Outlooks UI. Is erased by wiping it with the mouse pointer. This is a common issue in Windows where parts of the UI are left like ghost traces, and then erased by the pointer.

Except in this case it was a 1px wide line which spanned several UI components.

### Installing an app in MS Store

I'm logged in with my work account through Azure cloud AD, so it should just use that account.

In fact, if I click the user icon in MS Store that account shows up.

And yet when I click Download Check Point Capsule VPN I'm asked for my Microsoft login.

After several failed attempts to login with my Azure Cloud AD account I just close that box and then the download starts. Wtf.

So I never had to login. This type of amateurish design riddles this so called "Enterprise OS" for 2300kr/copy.

### Errors are so random, Skype works and then doesn't work

I don't bother much with Skype for Business on my gaming PC so for the last 6 months at least Skype for business has started with errors on my gaming PC.

Suddenly, without any recent update, it starts and "logs in", showing my contacts from work and no error. Wtf.

Then the next boot the errors are back and the Skype for business contacts window turns white and loses all its controls, which is actually a known issue to me.

Microsoft can't even sort out the simplest behaviour in their own applications.

And it's so typical of Windows issues that things just happen without any explanation or change on your part. That's why I can never trust that OS.

### You're logged in but the OS is not ready to use

This is an old pet peeve that I notice most when I also use Fedora alongside Windows.

When logging into Fedora and you see the desktop you can be sure that Fedora is now ready for use. You can start apps, tmux, whatever you want to do.

When logging into Windows the OS is not ready to be used. The blue circle is spinning because Windows is doing a bunch of shit in the background and clicking on anything takes several minutes for it to start.

Of course this is made worse on a work laptop with awful things like Skype for Business and MS Teams starting up automatically. But even when I've set both of those to not start on login in the settings, it still takes a minute or so before the OS is ready to launch apps like Explorer.

## Office365

This service has been bugging me so much lately that I must write my grievances down. Maybe this post should be called Why Microsoft sucks?

### Session timeout

When your session times out after too long inactivity, and you re-visit outlook.office.com in your browser, it flips you between domains a couple times until you land on a page that says "You're now signed out, please close the browser". Completely unhelpful, you literally have to go back to the address bar and re-type outlook.office.com, that you just typed to get there.

### License error trap

This is clearly a bug that will be resolved, in time. I am a member of two different office365 organisations, but one of them hasn't been fully migrated to o365 yet. In particular my account has not been migrated yet.

This means I can login, and go through the entire security procedure for my account in this org. But when the login completes I am stuck on an error page saying my account has no license for o365. No logout link, no way to get out without going into browser settings and deleting cookies.

Or so I thought, mysteriously after I tried deleting cookies for this domain, and other microsoft domains, FIVE TIMES, it just suddenly started working and logged me out so I could log back in with my other org. Such a shitty product.

## Disclaimer about all this

### 2020-06-24

Some of the issues I'm having are because 1) I'm running Windows as a guest VM under a KVM Fedora host only to do certain job-related tasks. 2) My company has made their own image, with their own mistakes that might not be compatible with running as a guest VM. 3) Like for example their AV starts consuming all CPU for hours sometimes.

So I realize MS is not completely to blame but partly, just like Apple is to blame for issues experienced by power users of their OS who want to use it outside of its regular frame of reference. 

Any power user will try to take the thing they're using and "hack" it in various unsupported ways. This is why Linux is simply best for power users in the long run and Windows/Apple Macintosh OS will always be a penned in experience suitable mostly for one-sided users.

At least Macintosh OS allowed for more freedom through Homebrew, we'll see what happens to that after they switch to their own CPU arch. I'm sure enthusiasts will still keep building packages. Or just use source packages, with all the hassle from 15 years ago on Gentoo and FreeBSD. 🤯

!!! note "About Linux and unsupported use"
    The entire point of Linux is DIY, to use it as you wish. Therefore its facilities are much more robust and ready for unsupported use.

### 2020-01-31

I understand why Windows is a bigger seller than an alternative like Fedora Linux for example. It's because Microsoft has made deals with all the major HW manufacturers so that they get drivers for all the good hardware.

You only have to look at the drivers download page of any popular hardware to see how Microsoft dominates the market. 10 years ago the options would be mostly Windows, Windows and Windows. Today you might see Linux drivers from major GPU manufacturers but they're not as polished or maintained as the Windows driver.

As long as Windows dominates that gaming market Linux will never break into the consumer desktop.

People try to tell me it's also because of DirectX but I don't put much weight on that. I believe the open source community could also write an amazing graphics API if only they had the motivation. But it's impossible to have such motivation without the support of the HW vendors. Without their drivers and regular updates for their drivers any graphics API would be pointless.

>And of course these days we have Vulkan so now all Linux is missing is HW vendor support.

And on top of that they've made deals with PC vendors to ship pre-installed Windows OS of course but I consider that to be a minor point, the main point is having HW drivers readily available directly from the vendors.

But all that said, I still reserve the right to point out that Microsoft is not living up to the standard of a major player in the PC OS market. They in fact lack healthy competition imho.
