# Why iPhone sucks

Around november 2019 I ordered a company phone so I could separate it from my private phone, which I had been using so far. So I then had my old Google Nexus Android and an iPhone 8.

This page will be used to summarize all the issues I've been having with the iPhone 8, in contrast to the Android I had before. Showing ultimately that iPhone is an overpriced piece of shit.

This page will grow as I am reminded of more daily issues. Started on 2020-02-03, last updated is on the bottom of the page.

## We don't need more junk!

iPhone is the only device in my life right now that requires its own charger.

With a Thinkpad X1 Yoga I can charge my personal phone, tablet and laptop with the same laptop charger.

We're already manufacturing so much junk that ends up in the ocean, Apple is only making it worse. Had they used usb-c I could buy one set of headphones for multiple devices [that got rid of the 3.5mm jack](#of-course-the-headphone-thing) too.

## Screen keeps turning on

This is a nightmare with any phone, android too. But at least in Android I could disable it easily.

It should go without saying that turning the screen on at unexpected times for a touch device is stupid. But it also draws battery, screen being one of the most battery hungry components on the device.

In certain pants the volume suddenly goes up violently in my headphones because the screen turned on in my pocket and they rubbed against the volume slider. As long as the phone is in my jacket or my fanny pack this doesn't happen.

## No signal

I used to be able to do Skype meetings on the lawn outside my house, over 3G, using my Android phone. With the iPhone it's not only impossible to do over 3G because the sound keeps cutting out. It's also often very hard to get started.

You'll pick up your phone and it'll say "3G" in the top left corner, full bars. But when you start dialing or trying to connect with Skype for Business it just takes forever and after 10-15 seconds the bars actually disappear and it says "no service". WTF?

Sometimes I've had to restart it just to get service again, when I'm out. This never happens on wifi of course.

>Keep in mind here that I live centrally in a city of 320k inhabitants, it's Swedens 3rd largest city. And my operator is Telia.

## The UI is confusing (bad)

I'm not just saying this because I came from Android. I've had the iPhone for 3 months now and the UI is just terrible.

Here is a list of issues related to UI/UX.

The saddest thing is that I had an iPhone back in 2011 so I can see that Apple have been adding things from Android because they make sense. But they haven't done it in a good way so it just makes no sense anymore.

### When charging it doesn't show the notifications

If I press the home button I see a clock and notifications, but if I stick in a charging cable I just see a big green battery. Waste of space. Android just turns on the screen when charging so you see what you'd normally see by waking the screen up. Two birds, one cable. Simplify, Apple, simplifyyyy!

### The top bar is not constant

Speaking of the clock, it would be nice to see it at all times. So that expensive pocket computer can at least serve a useful function. But Apples UI makes the top bar disappear in some contexts, making your expensive phone even more useless.

The Android top bar or taskbar, whatever you wanna call it, is always there. It always shows useful information like coverage, wifi and clock. THE BASICS, APPLE. REMEMBER THE BASICS?!

### Obtrusive prompts

There are two obtrusive prompts I can remember from these past ~6 months. First, because I used to have a Macbook (still do but it's at work, unused) Apple wants me to login on the Macbook and do something to verify my Apple ID on the iPhone. Apparently it's not enough to have access to my iCloud.com account and everything other than that damn Macbook.

So they keep prompting me about this, reminding me to go into settings with a prompt that slides in from the top and covers essential UI features at bad times. And with a little notification number badge.

This notification pops up at random times, sometimes twice in a row if I swipe it away. It has often placed itself over something I want to see, or in the way of my finger so I tap it by mistake.

The second obtrusive prompt lately is because my company did something to our Office365, or maybe because my AD password has expired, either way it's generating a prompt to update my Exchange password for my Exchange account.

Just now I was writing a text message and during the text message had to click away that prompt twice. When I was done with the text message I couldn't remember where in the settings this was set so I waited for the prompt to come back, of course it didn't. God damnit Apple, god damn it all to hell.

This 2nd prompt eventually went away when I resolved my office365 issues at work.

### The fake back button

Like for example Android has the back button which lets you go back to any app you were using, for example if one app opens another to request an OTP/MFA code.

Apple has added that now but it's squeezed into the top bar, which is like 2 pixels high.

### Where is the phone app?

Apple wants you to use the double press on the home button to switch between tasks, EXCEPT for when you want to see your current call. Then it's squeezed into the top bar which changes color to indicate you're in a call.

So I've ended up listening to a phone operator and pressing the buttons in the phone app, wondering why the operator isn't picking my commands up when I press. "Oh I have to press the tiny top bar to get back to my call, fuck you Steve".

### Apps don't have a consistent UI

This isn't all Apples fault but I remember hearing about strict UI developer guidelines on iOS so I'm a bit surprised that I still see major apps in 2019 that completely deviate from them.

Even Apple's own Safari deviates from any set guidelines when the back button moves to the bottom of the screen, while in most other apps it's in the top left.

In contrast, Android has much better consistency. Which is a strange thing to say seeing as their market platform is notoriously more open and accepting. I'm only speaking from personal experience.

Speaking of Safari it's quite annoying to get the toolbar out. It hides to use more screen space and you have to scroll around for a while before it decides to pop back up.

Bottom line; if you're going to have a sleek minimal phone with no back button then you should enforce some UI guidelines in your app developers.

### It's a button, but not really

The home button is not actually a button. It feels like a button because when you touch it a little vibrating motor starts inside your phone that delivers so called haptic feedback. So it basically vibrates to fool you into thinking you pressed a button.

Very cute, awesome idea. I would love to see this at a science conference but not in my own phone that I have to use every day. Because once the battery goes you're not getting any feedback anymore.

And speaking of battery, WHY WOULD YOU START A VIBRATING MOTOR EVERY TIME I USE THE MOST USED BUTTON?!

I don't see the practical motive behind this button. Were people lining up in droves at the Apple store to get their worn out Home button replaced? I doubt it. More likely their screen was cracked.

## Toggling services

I think having location services on when you only need it like 0.1% of your time is unnecessary. So being able to swipe down on Android and enable them quickly is great. Not so much on Apple though where my only option is going to the home screen, starting the Settings app, going down two levels to the location services and then enabling them.

And when you disable them you have to rinse and repeat, but then it also asks you for confirmation. Fuck you Steve.

Same goes for stuff like Bluetooth, where on Android you just long press on the Bluetooth toggle to see your Bluetooth settings. On Apple it's the same song and dance as for location services.

These are things I use pretty often due to Bluetooth speakers. And also apps requiring location services for one specific feature.

## Of course, the headphone thing

This is a general gripe but not being able to use my own headphones sucks donkey balls.

To avoid the shitty Apple headphones I've decided to carry an adapter instead. It's a better solution but it's also funny that I'm now carrying around an adapter at all times.

It's a better solution because the adapter won't tangle. The default iPhone headphones don't make use of any anti-tangling technique. It's like Apple are in denial that other headphone manufacturers have mitigated the tangling issue.

And don't tell me to use AirPods because having a 70USD (a piece) earring that isn't even attached to your ear lobe must be the dumbest 1st world issue I've seen in my life. And on top of losing them, you also have to charge them. Fuck off Steve.

This really boggles my mind. We've had headsets since the 90s and they've always been attached to your head or ear somehow because they're expensive and no two ears are the same. But nooooo, Apple have to be unique and different.

## Summary

There is absolutely no reason at all to buy an iPhone, unless you for some reason trust Apple more than Google because at least Apple have made a business of selling products and not your personal data. But who knows how long that will last.

The vanilla Android UI you get with a 3rd party Android OS like LineageOS is actually good enough and miles better than Apple iOS.
