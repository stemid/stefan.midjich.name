# The Death of CentOS

My thoughts on the recent [death of CentOS](https://blog.centos.org/2020/12/future-is-centos-stream/).

## What happened?

RedHat took ownership of the CentOS project in 2014.

RedHat, inc. has now decided three major things.

1. The planned End Of Life support period for CentOS 8 is cut short from 2029, to December 31st 2021.
2. No new CentOS releases will be made.
3. CentOS Stream will be the only available official CentOS distro.

## WTF is CentOS Stream?

CentOS Stream is essentially the development branch of RHEL.

A [rolling release](https://en.wikipedia.org/wiki/Rolling_release) always ahead of RHEL in development, except in one key area: security patches will still be rolled out to RHEL customers before CentOS.

So strangely CentOS Stream will get bugfixes before RHEL but security patches after RHEL. How would that even work? I'm not sure, but it seems like a roundabout way of saying _Fuck you, pay me_.

This of course means that CentOS can no longer claim to be binary compatible with any current RHEL release.

## Is this bad?

It is sad because I've relied on the stability of CentOS and I still have a lot of CentOS 7 systems deployed.

As far as the binary compatibility goes, doesn't really matter to me that much.

I believe it mostly matters to people who were saving costs by using CentOS instead of the recommended RHEL. And in those cases people were already shooting themselves in the foot because if a vendor recommends you to use RHEL or SUSE and you use CentOS because it's binary compatible, that same vendor can also deny you support for using an unsupported OS.

!!! note "Vendor support"
    Vendors like Dell, HP or Alcatel might recommend certain operating systems that work with their products. Usually the list goes Windows Server, RHEL and SUSE Enterprise Linux. At least back when I was working in this field, many years ago. You might also see Oracle Linux or Scientific Linux. Both RHEL-derivatives.

## So what do we do now?

1. First of all we have until 2024 to replace our CentOS 7 systems.
2. CentOS 8 can be upgraded to CentOS stream easily, see below.
3. Let's wait and see how this develops.

I am not hostile to using CentOS Stream, but I haven't decided yet if CentOS Stream will replace all my CentOS servers.

* Even if it's not as stable as RHEL it will still be in the direction that RHEL is headed.
* Only update exposed services and vulnerable packages when necessary to allow the community time to mature each new update.
* According to [this blog post](https://blog.centos.org/2020/12/centos-stream-is-continuous-delivery/) it seems like CentOS Stream will match a RHEL release at certain points.
* So if the above interpretation is correct I would simply update my CentOS Stream at the same pace as new RHEL updates come out. That would bring me as close as possible to the current RHEL update.

Another option for me is Fedora server.

The main reason I'm sad about the loss of a stable system is because my philosophy used to be put stable systems facing the internet. So for anything exposed to the internet like an MTA, TLS terminating web proxy or Tor node I would use CentOS. When I need modern technology like modern container implementation, modern languages and such I would use Fedora on an intranet, shielded from the internet by the stable servers. This is just one layer of many to give you some peace of mind.

So one option is to use Fedora Server but stay one release behind the latest to use more mature packages.

Even so, automatic patching is going to be even more important than it already has been. Unless you're running a carefully balanced cluster you should most likely do automatic security patching AND rebooting.

### How to upgrade CentOS 8 to CentOS Stream

I have tried this in a VM and it was indeed very quick and painless. Have not tried it in production yet but I assume you'd have to keep an eye on any 3rd party repos or apps you've installed outside of the CentOS repos.

    $ sudo dnf install centos-release-stream
    $ sudo dnf distro-sync
    $ hostnamectl

It simply changes from CentOS Linux 8 to CentOS Stream 8.

## What about Ubuntu and Debian?

Both fine distros but I am firmly a RHEL person.

I've taken the time to learn SElinux because I believe it to be the best and most mature Mandatory Access Control implementation in Linux today.

Also I truly enjoy the features of yum/dnf like history, rollback and rpm.

Both Fedora and CentOS Stream promise a much easier release upgrade process than CentOS and RHEL have had historically. Which would put it on par with the excellent release upgrade process of Debian systems.

But really I need to write [a whole different blog post comparing Debian to RHEL](./why_i_prefer_redhat.md) because I have a lot of firm opinions there too that I often find myself repeating on IRC or message boards.

