# Den bortglömda sagan

    En saga som är glömd
    om rikedom att få
    Kunskap noga gömd
    uti ett murarskrå

    En mytisk skatt så tung
    från stora tider forna
    Kan göra man till kung
    en like bland välborna

    Ritualerna beskriver
    vad man måste göra
    Den som sigillen river
    får andars visdom höra

    Jord och luft och vatten
    har samlats här ikväll
    Eldens sken i natten
    förtär på offerhäll

    Verkligheten rämnar
    offret öppnar upp en port
    Väsen hemmet lämnar
    passerar gränsland fort

    Vad har väl här släppts lös
    i hopp om snöd profit
    Demoner i en drös
    med glupande aptit

    Din skräck är deras bäring
    de vägleds av sitt hat
    Ur lidande fås näring
    förtär din själ likt mat

    Nu härskar de på jorden
    från troner utav guld
    Och från syd till norden
    vi satts i evig skuld

*(C) 2010 by Ivan *

