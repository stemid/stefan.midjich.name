# Why I prefer RedHat-based distros

With the recent [death of CentOS](./death_of_centos.md) I thought I'd write down exactly why I prefer RedHat-based distros over Debian-based distros, and the rest.

## My background

* I started using Linux in the late 90s as a 14 year old child.
* My first IT job in 2004 used FreeBSD so my boss converted me into a FreeBSD fanboy without actually knowing what it meant.
* 2011 I was hired partially because of my background in FreeBSD, on recommendation from my first boss.
* During my time at this job I was converted back to (Debian) Linux based on actual experience and facts this time, instead of just blind faith.
* 6 years ago in 2014 I was sent to get RedHat certified and my instructor converted me to a RedHat fanboy. I spent that night at the hotel re-installing my laptop from Debian to Fedora 21.

## What converted me from Debian to RedHat?

### Issues with Debian at the time

I had spent years managing Debian and Ubuntu systems and was a bit tired of some issues they were having consistently at the time. So I was ready to try something new.

* Around this time Ubuntu was still new and even frowned upon by some Debian purists.
* Ubuntu also made some highly publicized mistakes like storing the root password in cleartext during install.
* They tried to re-invent a wheel by launching AppArmor as an alternative to SElinux. It was still immature and I saw it be worked around due to issues in packaging a few times, last I remember was probably 16.04.
* Debian had issues with /boot volumes filling up with new kernels.
* Several times I had to boot a rescue ISO and restore the grub config because of a persistent issue in the Debian update process. That's regular update, not release upgrade.
* Debian-based systems start services by default, which also means one MTA package replaces another since they can't both listen on the same port.
* The above point makes the system less secure and feels like you give up control over something, which as a control-freak I absolutely hate.
* There are probably more little annoyances that I haven't remembered.
* There are also things I only realized were bad when I saw how RedHat handled it. Read on for that list.

### Why I liked RedHat better

* Very passionate about open source my RedHat instructor taught me exactly what RedHat, inc. had been doing in regards to the open source movement.

I was completely uninformed and based most of my decisions on intuition. So I viewed RedHat as *Big Linux* who don't care about open source and only about profit.

My instructor told me that RedHat consistently buy and release new projects as open source. Meaning they purchase a small company and their product, clean up the code so it's ready for release and simply **give it away** to the public.

This had been a regular process for them to do, every year, for years at the time I discovered it.

I had been exposed to RHEL and CentOS before this so I didn't just blindly re-install my work laptop.

After this I was forced to learn how SElinux works, because I'm not a [quitter. ;)](https://stopdisablingselinux.com/)

The more I learned, the more I liked. Let me summarize some of it.

* RedHat backing CentOS meant you always had the option to upgrade to RHEL if the requirement arose.
* RedHat have always been very involved in Linux kernel development and Linux standards development. I started to understand the issue of fragmentation in the Linux ecosystem back then.
* At the time no major vendor officially supported Debian, it was always RHEL or SUSE EL.
* Some certifications or major clients require you to have support contracts on your OS, so the choice was between an immature Ubuntu with a Canonical contract or a very mature RHEL and a RedHat license.
* I have written entire articles on SElinux but to summarize it's a 20 year old mature Mandatory Access Control implementation with origins at the NSA. Say what you want about their spying on citizens, if the US government uses RHEL with SElinux then they clearly trust it. And it's open source, it's not useless like DES3 or any of their other backdoor attempts over the years.
* And in my own experience SElinux is so secure that most lazy sysadmins simply disable it. That told me SElinux will most likely prevent zero day exploits from working, because it's already preventing my legitimate programs from working!
* yum had features like history and rollback that were actually client requirements in some government contracts we had.
* There is still no simple equivalent to ``yum provides '*bin/munpack'`` to find which package contains a certain binary. This is a very common use pattern where you read something online about a tool and you want to find where it is and how to install it. I always had to open [packages.ubuntu.com](https://packages.ubuntu.com) or [packages.debian.org](https://packages.debian.org) to find out.
* Most operations that dealt with packages, listing them, searching their contents, seeing which repo they belonged to, were better in RHEL.
* But first and foremost was probably the stability. Almost immediately I realized the advantage of having something stable and proven facing the internet/users, in a DMZ for example.
* I never had any of the issues with patching RHEL/CentOS that had scared me away from Debian.
* Lately RedHat backing of systemd and a common dbus has resulted in much less fragmentation in Linux and definitely elevated the entire ecosystem. Making it more available to other computer users.
* Over the years I've also discovered more and more RedHat employees out in various git repos contributing back to the open source ecosystem on both their work and free time.
* I put nmcli in the same vein as systemd, it might be more complex but it's practical in a lot of cases like automation, scripting for example. I mean Debian has netplan now...

#### It's not all roses

There were some things I disliked though, or rather liked better in Debian.

* The Debian/Ubuntu release upgrade process was unmatched, until I found Fedoras upgrade process. Unfortunately that was never available in CentOS/RHEL.
* Stability meant old packages. You had to be strategic about where to place RHEL based distros in your setup.
* apt is fast but only because they split their operations into two commands, apt-get update and apt-get install.
* Yum seems slow because it runs the equivalent of apt update every time you want to do something like search for packages or install something.
* Learning that RHEL had essentially crippled SElinux by not enforcing it on services launched from systemd was disappoint. You can of course fix that pretty easy, and go even further with user roles if you want to harden your system and feel comfortable writing policy modules.
