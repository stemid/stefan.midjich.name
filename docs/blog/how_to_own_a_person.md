# How to legally own a person

En kul [artikel](/files/employee.pdf).

# Sammanfattning

 1.  Employees are willing to trade "freedom" for job stability and increased average returns. Contracters will more easily defect for sporadic higher paying opportunities, which makes them less reliable.
 2.  Employers can overpay employees while ensuring that they know that they are overpaid, to 'domesticate' their employees and improve reliability of outcomes.
 3.  Decades ago, employers could also promise long-term job security to achieve similar effects, however rapidly changing markets mean their future existence (and ability to grant stability) is in question.
 4.  Instead, employees have fixated on cross-company careers, optimizing for general employability , which means that they must still conform to the requirements of a domesticated employeeship.
 5.  However, when employees have significant, direct, measurable profit impact skill, and can make or break the company, they can not be owned.
