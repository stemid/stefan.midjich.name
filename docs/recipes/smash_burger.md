# Smash burger med potatis

Detta är hur jag gör en snabb och enkel smash burgare med potatisklyftor.

## Ingredienser

* 500g högrevsfärs (för fyra burgare)
* 1 stor mjölig potatis
* 1 rödlök
* cosmopolitan salladshuvud
* salt
* hamburgerost
* hamburgerdressing
* bacon

## Metod

Tvätta händerna med kallt vatten, ta en fjärdedel av färsen och forma till en boll. Kläm bollen ordentligt och forma den till en rund boll i 1 minut ungefär.

Ställ in färsbollen i kylen.

Koka vatten, skala potatis, klyfta potatisen och lägg i vattnet. Den ska koka ca. 10-15 minuter. Den brukar vara färdig tills jag steker hamburgaren.

Skär skivor av rödlök och förbered lite saker för att låta färsen vila i kylen. Egentligen ska den vila 1 timme men det har funkat med bara 20 minuter för mig.

Stek bacon, lägg undan på papper och låt den torka lite. Skölj av pannan innan vi steker hamburgaren.

Värm upp pannan till en bit över medelvärme på spisen. Lägg i en rejäl klick smör men låt inte smöret brännas, sänk värmen till medelvärme.

Lägg i färsbollen och mosa den platt med en stekspade, se till att den blir jämnt tillplattad.

Låt den stekas ca. 1 minut på första sidan, eller tills du ser köttsaft komma upp på ovansidan. Salta ovansidan, vänd på hamburgaren.

På med osten direkt och låt den andra sidan stekas ungefär lika länge, eller längre på eftervärme. Nu kan man hälla av vattnet från potatisen, sänka värmen till en fjärdedel ungefär och ha i klyftorna i stekpannan för att stekas lite i smöret.

Låt burgaren vila på en tallrik i ett par minuter.

Bygg hamburgaren så som du önskar. Jag brukar ha hamburgarebröna i brödrosten på låg värme först.

Gott!
