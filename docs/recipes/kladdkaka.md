# Kladdkaka

Samlar alla mina recept för kladdkaka här. Kladdkaka är underbart, kladdkaka är livet. ;P

# Julekladdkaka

Denna kaka har en smått julig fudge som så klart passar hela året, eftersom kladdkaka passar alltid till allt. Jag tycker om att garnera den med kanderade nötter som beskrivs [här](#garnering). 

## Smeten


*  2 st ägg

*  3 dl socker

*  1.5 dl mjöl

*  1 dl kakao

*  1 tesked vaniljsocker

*  knappt en halv tesked salt

*  100 gram smält smör

Vispa ägg och socker och blanda de torra ingredienserna för sig i en annan skål. Vänd ner äggvispet i de torra ingredienserna. Rör sist ner det smälta smöret. 

Grädda mitt i ugnen i en smord och bröad form på 175 grader i cirka 20 minuter. Låt svalna. 

## Glasyr


*  1.5 dl vispgrädde

*  1 tesked honung

*  2 st malda kanelstänger (eller 1 tesked malen kanel)

*  1 tesked kardemummakärnor (eller en halv tesked malen kardemumma)

*  200 gram mörk choklad (går även med mjölkchoklad)

Koka upp grädde, honung och kryddor i en kastrull. Ta bort kastrullen från värmen och låt det dra i 10 minuter. Koka upp glasyren igen och sila över den finhackade chokladen. Rör glasyren tills den blir slät som fudge och bred sedan ut på den svala kakan. 


# Stewarts kladdkaka

Detta recept fick jag från Stewart. 

## Smeten


*  3dl socker

*  3 ägg

*  3dl mjöl

*  2 matskedar vaniljsocker

*  6 matskedar kakao

*  50 gram mörk choklad

*  150 gram smält smör

*  ½ tesked salt

Börja med att smälta chokladen och smöret över vattenbad. 

Vispa ägg och socker. Rör i mjöl och till sist den smälta chokladen tillsammans med smöret. Grädda mitt i ugnen i en smord och bröad form på 175 grader i cirka 20 minuter. Låt svalna. 

## Glasyr


*  100 gram mörk choklad

*  25 gram smör

*  ½ dl vispgrädde

Smält choklad och smör i en kastrull på låg värme, rör i grädden och se till att det håller sig ljummet medans du gör klart garneringen. 

## Garnering


*  50 gram krossad mandel eller krossade pistagekärnor

*  ½ dl brunt farinsocker (går bra med vanligt socker också)

*  ½ tesked salt

*  ½ tesked bikarbonat

Rör ihop allt i en stekpanna och stek tills sockret karameliseras och stelnar runt nötterna. Strö garneringen över glasyren direkt efter att du penslat kakan med den. 

# Helenas kladdkaka


*  250 g osaltat smör

*  250 g mörk choklad

*  4 dl florsocker

*  2 dl mjöl

*  4 äggvitor

*  4 äggulor

*  2 matskedar Grand Marniér eller rivet skal från en apelsin och en lime

Sätt ugnen på 200C. Smält smör och choklad över vattenbad. Blanda det med florsocker. Låt smeten svalna lite till rumstemperatur, blanda ner mjöl, äggulor och citrussmaksättning. Vispa äggvitorna till hårt skum. Vänd försiktigt ner äggvitorna i smeten. Häll upp i osmord form och grädda i 13 minuter. 
