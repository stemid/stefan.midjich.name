---
title: Småländsk ostkaka
date: 2021-11-06
summary: Äkta småländsk ostkaka med ostlöpe och hemmagjord färskost
---

# Småländsk ostkaka

![Bild av nygräddade ostkakor i folieformar](/files/ostkaka.jpg)

## Ingredienser

* 5 l lågpastöriserad 3% mjölk
* 5 dl vispgrädde
* 2.25 dl vetemjöl
* 1.5 matsked ostlöpe från butik
* 5 ägg
* 1 äggula
* 1.5 dl strösocker
* 50 g sötmandel
* 1 bittermandel
* ostduk eller liknande för att krama ur vätskan från osten
* termometer så att mjölken inte är över 37 grader celsius när ostlöpen rörs i

## Metod

Värm mjölken i en stor kastrull till ca. 35 grader celsius, rör i vetemjöl under tiden.

Värm mjölken till så nära 37 grader du kan. Går du över, låt den bara stå utan värme tills temperaturen går ner till 37.

Rör i ostlöpe och låt kastrullen stå under en duk i ca. 30 minuter, eller tills det bildats en klump av ostmassa i mitten.

Hacka mandeln fint.

Värm ugnen till 200 grader celsius (175 för varmluftsugn).

Häll av så mycket av vätskan från kastrullen som du kan.

Krama ur ostmassan med en ostduk, eller en vanlig kökshandduk om du saknar ostduk.

Dela sönder ostmassan och blanda den med socker, grädde, ägg och mandel.

Grädda i små eller mellanstora formar i mitten av ugnen 30-35 minuter eller tills du är nöjd med den gyllenbruna ytan.

## Ytterligare info

Mjölken måste vara lågpastöriserad eller opastöriserad, högpastöriserad mjölk kommer inte fungera. Laktosfri mjölk är oftast högpastöriserad. Läs på innehållsförteckningen.

Det finns olika sorters ostlöpe, detta receptet är för vanlig "butikslöpe", läs gärna på flaskan också. Så kallad mejerilöpe ska tydligen vara mer potent.

Jag köper folieformar som jag både gräddar och förvarar ostkakan i, den går jättebra att frysa in.
