# Goulash

Detta är ett eget recept av en relativt simpel rätt. 

>//Parentes//: Enligt en österrikisk vän (som var gift med en ungrare;) är hemligheten till en bra gulash att inte använda vatten. All vätska ska komma från gullök enligt henne. Jag har dock paprika och kalvfond i men mycket riktigt är det inget annat tillsatt vatten.

**Tid**: ca. 2-3 timmar.

# Ingredienser


*  ca. halvt till 1 kg gullök - mellan 3-5 stora lökar. *Tänk på att det ska utgöra det mesta av vätskan. Jag gör ibland grytor med 1kg gullök för 500g kött.*

*  Ett par klyftor pressad vitlök

*  Kalvfond

*  Paprikapulver

*  3-6 paprikor tärnade - *Jag använder en av varje färg; gul, grön och röd.*

*  ca. 500g Grytbitar - *Kan vara nöt, älg, vildsvin, eller blandat.*

*  halvt kg tärnad fast potatis.

# Recept

Värm matolja i en tjock och stor kastrull.

Hacka gullöken fint, en matberedare eller mixer funkar bäst med sådana stora mängder.

I med löken i kastrullen och få upp värmen på ca. 3/6 tills den börjar koka och ge ifrån sig vätska. Sänk värmen till 1/6 och låt stå i en timme med lock på.

Tillsätt ett par teskedar paprikapulver, vitlök och en matsked kalvfond. Plus andra kryddor efter behag som ett kryddmått chiliflingor t.ex..

Löken ska vara relativt upplöst redan innan du fortsätter, det ska ligga ett par dl vätska i botten. Man kan låta den stå och puttra på låg värme längre om man vill.

Passa på och tärna paprikan, skala potatisen och tärna den med.

När där ser ut att vara 2-3 dl vätska i kastrullen kan du röra i den tärnade paprikan.

Se till att det puttrar och låt det stå i 10-15 minuter medan du skär köttet.

I med köttet också och rör om. Låt det stå i 25 minuter.

Hur länge köttet kokar beror mycket på hur mört det blir men tänk på att potatisen i sin tärnade form kräver 20-25 minuter på slutet.

I med potatisen när du känner dig redo.

Låt det stå 25 minuter till och känn efter på konsistensen av potatisen och köttet.

Smaka av med kalvfond och/eller salt om det saknar sälta.

Detta är en gryta som så klart kan puttra lite extra medans man smakar av.

Den brukar släppa sin smak dagen efter när den stått en natt. Utsökt.
