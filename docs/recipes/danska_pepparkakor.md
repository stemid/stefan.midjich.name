# Danska pepparkakor (länk)

Har gjort dessa ett par gånger nu och blir samma bra resultat varje gång.

Degen räcker länge, kan stå i kylen eller frysas in. Varje sats du gör i ugnen använder kanske en tiondel av degen.

# Se också

*  [Originalrecept](https://www.dr.dk/mad/opskrift/blomsterbergs-brunkager-med-pistaciekerner)
