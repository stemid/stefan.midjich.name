# Stefans pankisar

Pannkakor görs på många olika sätt, mitt recept är ganska standard men det är ändå mitt sätt. 

För ungefär 12 pannkakor beroende på storlek och tjocklek. 


*  4dl grädde

*  2dl mjölk

*  3-4 ägg

*  bourbon vaniljextrakt (eller vanligt vanlijsocker)

*  1krm salt

*  2.5dl mjöl

Rör ihop vaniljextrakt, salt och mjöl i en bunke. 

Vispa i mjölk och 1dl grädde tills du har en jämn smet. 

Rör i resten av ingredienserna, grädde och ägg. Använd 4 ägg om du aldrig gjort pannkakor förut men man kan klara sig på 3. Beror på storleken av äggen. 

Låt smeten vila ett tag medans stekpannan värms upp, jag värmer den alltid på styrka 7 av 12. 

Lägg i en liten klick smör och låt den glida runt medan den smälter, när smöret slutar fräsa ska du hälla i smeten. 


