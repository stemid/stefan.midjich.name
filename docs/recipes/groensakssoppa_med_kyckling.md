# Grönsakssoppa med kyckling

Persiljestjälkar och blad ger en god smak. Skal av gullök ger en fin färg men har du för mycket skal så blir soppan brun. 


*  ca. 3 liter vatten

*  två kycklinglår med ben

*  Palsternacka

*  Morot

*  Tomat

*  Persiljestjälkar och blad (eller rotpersilja om du inte hittar stjälkar och blad)

*  Halv gullök (plus lite av skalet för färg)

*  Purjolök

*  två tärningar kycklingbuljong

*  salt och hela korn svartpeppar

Skär grönsakerna i stora bitar, ungefär lika stora som kycklingen. 

Koka upp alla ingredienser utom buljong och svartpeppar.

Det bildas skum på ytan från köttet när du kokar kyckling, ta bort det med sked tills det inte kommer mer skum.

I med svartpepparkorn. 

Koka lite till, smaka av med kycklingbuljong och salt om det behövs mer smak.


