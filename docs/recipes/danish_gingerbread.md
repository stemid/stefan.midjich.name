# Danish gingerbread

!!! warning "This recipe requires a full day before baking!"
    You should let this dough sit at room temperature until the next day before you put it into an oven for baking.

* 250 grams butter
* 125 grams light colored sugar syrup
* 250 grams brown sugar
* 2 teaspoons potash (potassium)
* 1 tablespoon water
* 3 teaspoons ground cinnamon
* 1 teaspoon ground cloves
* 2 teaspoons ground ginger
* 1 teaspoon allspice pepper
* 60 grams diced bits of preserved orange fruit (optional)
* 25 grams unsalted pistachio kernels
* 150 grams almonds
* 500 grams wheat flour

!!! note "WTF is potash?"
    It's also known as potassium and yes it was used as a cleaning agent in the olden times, but it also gives gingerbread its crunchiness. I'm commenting on it because it's almost impossible to find in Sweden. Baking soda is not a good replacement. Natron could work as a replacement for potassium.

# Procedure

Mix butter, syrup and brown sugar in a pot on the stove at medium heat, or until it reaches about 70 celsius (158 F). Take the pot off the stove.

Mix all the spices, cinnamon, cloves, ginger and allspice with the flour.

Add the diced oranges, almonds and pistachios to the flour mix.

Mix potash with 1 tablespoon of water in a cup.

Add all the components, flour mixture, potash and sugar into a kitchen assistant where the dough can be stirred until it's consistently even.

Place the dough into a container of about 7x7", I personally used a regular bread pan for this stage. Protect the dough from sticking to the container with oven paper (parchment paper).

Really press the dough into the container so it fills out all the space. Finally cover it with some oven paper. Let it sit at room temperature until the next day.

Before baking you cut thin squares from the loaf of dough and place these on oven paper.

Bake for about 9-12 minutes at 180 C (356 F). Let the cookies cool down before devouring.

# See also

* [Original recipe in danish](https://www.dr.dk/mad/opskrift/blomsterbergs-brunkager-med-pistaciekerner)
