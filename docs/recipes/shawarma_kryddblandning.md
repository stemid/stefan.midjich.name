# Shawarma kryddblandning

Måtten här passar för att gnidas in i ungefär 1kg brynt kött. 


*  1 matsked malen kummin

*  1 matsked malen koriander

*  1 matsked vitlökspulver

*  ½ tesked malen kardemumma

*  ½ tesked malen kryddnejlika

*  1 tesked malen svartpeppar

*  1 tesked malen kanel
