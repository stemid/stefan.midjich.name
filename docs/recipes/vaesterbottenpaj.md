# Västerbottenpaj

En ostpaj med västerbottenost. Ska helst serveras ljummen, till kräftor. 


*  3 dl riven västerbottenost
*  3 ägg
*  3 dl mjölk eller grädde
*  salt
*  peppar
*  smördeg

Tryck ut smördegen i en pajform. Riv osten och fyll pajformen. 

## Äggstanning

Rör ihop ägg och grädde, krydda efter eget behag. Häll över äggstanning i pajskalet över osten. 

Grädda mitt i ugnen på 175 grader i 15 minuter eller tills pajen ser gräddad ut. 

