# Omvandling av mått

Samlar lite info här om att omvandla brittiska måttenheter till de mer civiliserade metriska måtten vi använder i sverige. 

För många mått och temperaturer kan man bara söka på google, [till exempel](http://lmgtfy.com/?q=170+C+in+F). 
# Mjöl och konfektyrsocker/florsocker

Cups to Grams


*  1/8 cup=15 grams

*  1/4 cup=30 grams

*  1/3 cup=40 grams

*  3/8 cup=45 grams

*  1/2 cup=60 grams

*  5/8 cup=70 grams

*  2/3 cup=75 grams

*  3/4 cup=85 grams

*  7/8 cup=100 grams

*  1 cup=110 grams

# Strösocker

Cups to Grams


*  1/8 cup=30 grams

*  1/4 cup=55 grams

*  1/3 cup=75 grams

*  3/8 cup=85 grams

*  1/2 cup=115 grams

*  5/8 cup=140 grams

*  2/3 cup=150 grams

*  3/4 cup=170 grams

*  7/8 cup=200 grams

*  1 cup=225 grams

# Smör och margarin

Cups to Grams


*  1/8 cup=30 grams

*  1/4 cup=55 grams

*  1/3 cup=75 grams

*  3/8 cup=85 grams

*  1/2 cup=115 grams

*  5/8 cup=140 grams

*  2/3 cup=150 grams

*  3/4 cup=170 grams

*  7/8 cup=200 grams

*  1 cup=225 grams

# Brunt socker/brun farin

Cups to Grams


*  1/8 cup=25 grams

*  1/4 cup=50 grams

*  1/3 cup=65 grams

*  3/8 cup=75 grams

*  1/2 cup=100 grams

*  5/8 cup=125 grams

*  2/3 cup=135 grams

*  3/4 cup=150 grams

*  7/8 cup=175 grams

*  1 cup=200 grams

# Osötat kakaopulver

Cups to Grams


*  1/8 cup=15 grams

*  1/4 cup=30 grams

*  1/3 cup=40 grams

*  3/8 cup=45 grams

*  1/2 cup=60 grams

*  5/8 cup=70 grams

*  2/3 cup=75 grams

*  3/4 cup=85 grams

*  7/8 cup=100 grams

*  1 cup=125 grams

Cake Flour
Cups to Grams
1/8 cup=10 grams
1/4 cup=20 grams
1/3 cup=25 grams
3/8 cup=30 grams
1/2 cup=50 grams
5/8 cup=60 grams
2/3 cup=65 grams
3/4 cup=70 grams
7/8 cup=85 grams
1 cup=95 grams

Sliced Almonds
Cups to Grams
1/8 cup=10 grams
1/4 cup=20 grams
1/3 cup=25 grams
3/8 cup=30 grams
1/2 cup=40 grams
5/8 cup=50 grams
2/3 cup=55 grams
3/4 cup=60 grams
7/8 cup=70 grams
1 cup=80 grams

Ground Almonds
Cups to Grams
1/8 cup=25 grams
1/4 cup=50 grams
1/3 cup=65 grams
3/8 cup=75 grams
1/2 cup=100 grams
5/8 cup=125 grams
2/3 cup=135 grams
3/4 cup=150 grams
7/8 cup=175 grams
1 cup=200 grams

Flaked Coconut
Cups to Grams
1/8 cup=10 grams
1/4 cup=20 grams
1/3 cup=25 grams
3/8 cup=30 grams
1/2 cup=40 grams
5/8 cup=45 grams
2/3 cup=50 grams
3/4 cup=60 grams
7/8 cup=65 grams
1 cup=75 grams

Grated Coconut
Cups to Grams
1/8 cup=10 grams
1/4 cup=25 grams
1/3 cup=35 grams
3/8 cup=40 grams
1/2 cup=50 grams
5/8 cup=60 grams
2/3 cup=65 grams
3/4 cup=75 grams
7/8 cup=85 grams
1 cup=100 grams

# Se också


*  [Conversion Calculators](http://www.dianasdesserts.com/index.cfm/fuseaction/tools.measures/measures.cfm) 
