# Pastasås med parmesan och mozarella

FIXME work in progress.

Detta är en pastasås man gör som tillbehör till stekt kyckling t.ex..


*  tärnad stekt bacon

*  stekta bitar kyckling, innerlårfilé passar bra eller tärnad bröstfilé

*  1.5dl mjölk

*  1.5dl grädde

*  broccoli

*  pasta

*  ca. 1dl riven parmesan

*  ca. 1dl riven mozarella

Förstek bacon och kyckling, lägg åt sidan och knapra inte på baconen. 

Koka pasta och lägg åt sidan. 

Värm upp lika delar grädde och mjölk i en kastrull eller stekpanna. För smakens skull tar jag stekpannan som jag sktekte bacon i. 

Häll i osten i såsen och låt den smälta. Rör om regelbundet så ost inte bränns fast i botten. 

Häll i broccoli och låt sjuda i några minuter, broccoli ska inte koka länge. 

Vänd i den kokta pastan i såsen, vänd försiktigt runt allt tills pastan suger upp det mesta av såsen. Vänd i bacon och kyckling tills den värms upp igen. Servera. 
