FIXME översätt


*  1 1/2 cups graham cracker crumbs 

*  1 lb confectioners’ sugar (3 to 3 1/2 cups) 

*  1 1/2 cups peanut butter 

*  1 cup butter, melted 

*  1 (12 ounce) bag milk 

*  chocolate chips 

 1.  Combine graham crumbs, sugar and peanut butter and mix well. 
 2.  Blend in melted butter until well combined. 
 3.  Press mixture evenly into a 9 x 13 inch pan. 
 4.  Melt chocolate chips in microwave or in double boiler. 
 5.  Spread over peanut butter mixture. 
 6.  Chill until just set and cut into bars (these are very hard to cut if the chocolate gets “rock hard”.
