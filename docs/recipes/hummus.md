# Hummus


*  1 burk blötlagda kikärter på burk (ca. 400g)

*  2 pressade vitlöksklyftor

*  0.5dl olivolja

*  4 msk tahini (sesampasta)

*  4 msk citronsaft

*  0.5 tsk mald spiskummin

*  salt och vitpeppar

Skölj kikärter från burk under vatten med ett durkslag eller en sil så att all vätska från burken tvättas bort.

Kasta ner allt utom salt och vitpeppar i en matberedare och kör. 

Smaka av med salt och vitpeppar. Är den för torr kan man prova lite mer olivolja och lite mer citronsaft så länge citronsaften inte tar över smaken för mycket.

Andra tillägg är färsk koriander och en halv röd chili för extra smak.

Enda tahini jag hittat är av märket Kung Markatta.
