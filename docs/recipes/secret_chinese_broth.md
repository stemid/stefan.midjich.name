# Hao SiFu’s pork bone broth

From https://np.reddit.com/r/ProRevenge/comments/7v42dq/reuse_unfinished_soups_for_the_next_customer/dtpi3xj/?context=3

Here it is folks! I’ve had to modify this recipe as we used to make in a freaken humongous pot. This makes about 1.5 gallons of bone broth so please modify to your requirements.

## Ingredients

*     4-5 pounds of pork leg bones and knuckles
*     1 pound of chicken bones (neck or wings work best)
*     2 large onions – chopped in half
*     6 cloves of garlic – lightly crushed
*     1/2 Chinese white radish – chopped roughly (replace with carrots if unavailable in your area)
*     1 thumb piece of ginger
*     Handful of dried shitake mushroom (pre-soaked)
*     Handful of conpoy (dried scallop)
*     1 tpsn of crushed black pepper
*     Several dashes of Chinese rice wine

## Method

 1.      Boil the bones in a separate pot for at least 10 minutes. Remove from water and rinse carefully under tap water.
 2.      Crack the leg bones along the middle (we used a giant cleaver) Place washed and cracked bones in clean pot with cold water and bring to boil.
 3.      Add all other ingredients.
 4.      Gently boil with lid slightly ajar for at least 8 hours (the longer the better) stirring occasionally. It will smell terrible for the first 2 hours for some reason – this is normal. I've also used pressure cooker which reduces the cooking time to about 2 hours. It's still very good but unfortunately you don't get the nice emulsification of the fats.
 5.      The finished product will look like a rich, opaque, creamy white colour. Strain before serving.


This broth is served with fresh noodles with vegetables and protein of your choice. Ensure you season with soy sauce or salt before serving. For example, I like it served with roast pork, bok choy and mushrooms with egg noodles.

