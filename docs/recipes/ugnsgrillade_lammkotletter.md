# Ugnsgrillade lammkotletter

Grundrecept för lammkotletter grillade i ugn, eller på vanlig grill. 


*  2 teskedar olivolja

*  4 klyftor pressad vitlök

*  1 tesked krossad timjan

*  1 tesked krossad rosmarin

*  1 tesked flingsalt

Marinera kotletterna i blandningen mellan 30 till 60 minuter. Bryn 3 minuter på varje sida i en het panna med olivolja. Släng in i ugnen med fettkanten uppåt på 200C i ungefär 10 minuter. Låt vila i 5 minuter. 
