# Krispig Toffee


*  450g smör

*  drygt 4.5 dl strösocker

*  halv tesked salt

*  150g skalade mandlar

Blöt mandlarna med äggvita och strö över flingsalt. Äggvitan ska bara agera klister för saltet. Sprid ut mandlarna på en plåt med bakplåtspapper, rosta i ugnen på 200 grader i ca. 10 minuter. Låt mandlarna svalna och hacka dem sedan. 

Smält smör, strösocker och salt i en tjockbottnad kastrull. Koka tills du når 137 grader och gör då ett kulprov. 

Rör ner mandlarna och häll ut smeten på bakplåtspapper i en form. Smörj formens kanter med vegetabilisk olja om du tror att smeten kommer nudda kanten. 
