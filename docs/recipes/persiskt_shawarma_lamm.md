# Persiskt shawarma lamm

Du behöver. 


*  Shawarma kryddblandning

*  1 kg grytbitar av lamm

*  1 liter labneh yoghurt (en syrligare yoghut som finns bl.a. på Özen allfrukt)

*  salt

Bryn köttet i olivolja. Låt det svalna tillräckligt för att gnida in kryddblandningen. Gnid in grytbitarna med kryddblandningen och salt. 

Släng i grytbitarna och yoghurt i en gjutjärnspanna med lock och in i ugnen på 140°C i 2 timmar. Efter den tiden vänd lammet och låt det stå i 2 timmar till i ugnen. 

Jag brukar köpa lammstek och skära bort svål men behålla några benbitar för smakens skull. Det går bra med vilket lammkött som helst, så länge man kan skära bort jämna grytbitar. 

