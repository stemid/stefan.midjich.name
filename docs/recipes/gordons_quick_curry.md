# Gordon's quick curry

Saw Gordon Ramsay cook this on the F word in 20 minutes.

The four first vegetables in the list are up to you but I've written some estimated measurements in parentheses. 


*  Grated ginger (1 thumb sized piece)

*  Chopped chilis (2 whole red chilis)

*  Chopped garlic (2 cloves)

*  Chopped lemongrass (4-5 stalks or 50 gram packages sold at ICA)

*  Oil for paste (1 tbsp)

*  Diced chicken

*  A couple Lime leaves for flavor

*  100ml Soy sauce

*  1 cinnamon stick

*  1 cup of coconut milk

*  Green beans

*  Chopped coriander

Throw fresh veggies into a food processor with some oil and run it until you have a paste.

Throw paste into hot pan with a little butter and diced chicken bits.

Fry chicken for a few minutes while stiring. Lower to medium heat.

Pour in a couple tablespoons soy sauce and some lime leaves. Fry for another 4 minutes.

Put in the cinnamon stick and pour in the coconut milk. Let simmer on low/medium heat and put in the green beans.

Pour all the coriander into the pan, or save a few leaves for garnish. Plate it immediately after.
