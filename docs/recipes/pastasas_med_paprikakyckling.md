# Pastasås med paprikakyckling

En pastasås du kan göra en mindre sats av direkt i pannan eller en större sats i kastrull. 

## Ingredienser


*  1 grön paprika

*  1 gullök

*  1 vitlök solo eller 4 klyftor

*  solrosolja eller rapsolja

*  kycklingfond

*  0.5dl mjöl

*  ca. 1krm salt

*  ca. 1krm nymalen svartpeppar

*  ca. 2 teskedar paprikapulver

*  ca. 1/2 tesked chiliflingor

*  ca. 500g innerlårfilé av kyckling, annan del går oftast också bra

*  2dl créme fraiche

## Tillagning

Hacka paprika, gullök och vitlök. Värm stekpannan till medelvärme och häll i olja. Bryn kycklingen med rum mellan bitarna för att undvika kokeffekt. Bryn i flera omgångar och lägg den brynta kycklingen åt sidan om du har mycket kött. 

Häll alla ingredienser utom créme fraiche i en kastrull med 1/3 ledig plats. Krydda efter eget behag. 

>//Gör du en mindre sats kan du göra allt i stekpannan och glömma kastrullen.//

Lägg på lock och låt grytan koka i ca. 40 minuter på låg värme, eller tills all lök löses upp av värmen. 

>//Gör du med quorn, eller en snabb sats direkt i pannan, kan du bara steka allt tillsammans under lock i mycket mindre tid.//

När löken har smält helt och bildat vätska kan du ta ett par matskedar av vätskan och blanda med créme fraiche i en skål, rör i mjöl och rör om ordentligt. Blir det torrt kan du ha i mer av vätskan från kastrullen. 

Häll i blandningen i grytan och rör om, låt puttra 15 minuter till under lock. 

## Tips

Smaka av sista minuterna, använd kycklingfond om det behövs mer salt. 

Löken blir vätskan i såsen så använd mer gullök till mer kött. 

