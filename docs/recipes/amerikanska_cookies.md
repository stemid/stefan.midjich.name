# Amerikanska Cookies

Till detta recept bör du ha en köksassistent, allt blir lättare om du har en >=4.5l skål till den också.

Receptet gör ca. 24 kakor.

## Innehåll

* 2dl strösocker
* 2dl packad brun farinsocker (170g enligt originalrecept)
* 226g rumstempererat och mjukt smör
* 2 ägg
* 1 tesked vaniljextrakt
* 0.5 tesked nypressad citronjuice
* 5dl mjöl
* 1dl havregryn
* 1 tesked bikarbonatpulver
* 1 tesked finkornigt salt
* 1 nypa kanel
* 300g mörka chokladknappar
* 150g hackade valnötskärnor

## Tillvägagångssätt

Rör om smör, socker och brun farin i ca. 2 minuter.

Rör i ägg, vaniljextrakt och citronjuice. Mixa på låg hastighet i 30 sekunder innan du höjer till medelhastighet i ca. 2 minuter, eller tills degen ser fluffig ut.

Skrapa skålen efterhand så degen inte fastnar på sidorna.

På låg hastighet lägg till mjöl, havregryn, bikarbonat, salt och kanel. Mixa i ca. 45 sekunder till men mixa inte för länge.

Har du en stor (>=4.5l) skål till din köksassistent kan du fortsätta mixa på låg hastighet, annars ta av skålen och rör i resten för hand.

Rör i chokladknappar och hackad valnöt.

Bäst resultat får du om degen kyls i 1 timme nu.

Värm ugnen till 150C (300F).

Gör bollar ungefär 3 matskedar stora och lägg dem med några cm mellan på bakplåtspapper.

In i ugnen i 20-23 minuter. Tills kanterna börjar bli lite gyllenbruna men mitten fortfarande är lite mjuk. Låt svalna i 1 timme ungefär och servera eller frys in.

## Anteckningar

Originalreceptet sa 450g chokladknappar och 170g valnötskärnor men mitt första försök resulterade i bruna chokladkakor. Jag tror 300g chokladknappar räcker och de säljs i 150g paket så det passar bättre.

Valnötskärnorna tog över smaken också, 100g kanske hade räckt där.

Prova ett par nypor kanel nästa gång.

## Länkar

* [BETTER than Doubletree Chocolate Chip Cookies](https://www.crazyforcrust.com/doubletree-chocolate-chip-cookies-recipe/)
* [5-Star Chocolate Chip Cookies](https://eatmovemake.com/5-star-chocolate-chip-cookies/)
