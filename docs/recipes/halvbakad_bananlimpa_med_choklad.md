# Halvbakad Bananlimpa med Choklad

Detta är baserat på ett grundrecept som jag fick av en vän utomlands så receptet var i Amerikanska imperiala mått. Jag har gjort om det till svenska mått, och ändrat lite. 

## Detta behövs


*  100g smör (rumsvarmt)

*  3 matskedar kakao

*  1 tesked vaniljsocker

*  4.5dl mjöl (fullkorn funkar bra)

*  2dl socker

*  1 tesked bakpulver

*  4 mosade bananer

*  3 matskedar sur mjölk

*  vinäger (till mjölken)

## Sur mjölk

Detta är något som tydligen går att köpa i staterna under titeln "sour milk" men här har jag inte lyckats hitta en motsvarighet så vad jag gör är att jag blandar mjölken med bara några droppar vinäger. Har använt vitvinsvinäger och eftersom jag inte kan vetenskapen bakom den här typen av matlagning vet jag inte om det gör någon skillnad men om mjölken får stå 5 minuter med vinäger så ändrar den konsistens. 

Det är alltså inte filmjölk, det är vanlig mjölk men surare, lägre pH kanske, vinäger fixar biffen iaf. 

Till 3 matskedar mjölk har jag använt knappt en tesked av vinäger. 

## Så här gör du

Värm ugnen till 175°C. 

Mosa övermogna bananer för det är mycket lättare att mosa ut till en jämn smet då. 

Blanda ihop allt i en bunke och rör väl till en smet som du sedan häller ut i en smord och brödad ugnsform. Jag använder en brödform för limpor så det ser ut som en limpa bröd och det funkar bra. 

Nu ska den gräddas på 175 grader i 45 minuter, minst 40 minuter men originalreceptet hade 1 timme så det beror på hur kladdig den ska vara. Jag tycker om att den är lite halvbakad inuti. ;)

