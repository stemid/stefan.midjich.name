# Norwegian Batter Waffles

This is apparently the recipe of Stine Aasland.


* 3 eggs
* 1/2 cup (8 tablespoons) sugar
* 1/2 teaspoon salt
* 4 cups (1 liter) whole milk
* 4 cups (1 liter) unbleached all-purpose flour
* 3/4 cup (2 dl) dairy sour cream
* 1 1/2 stick (150 grams) butter
* 2 teaspoons cardamom

Beat eggs and sugar until light and fluffy. Add wet and dry ingredients alternately. Beat around 3 minutes to make a smooth batter. Melt butter and beat slowly into the batter. Let the batter rest for at least 15 minutes before cooking.
