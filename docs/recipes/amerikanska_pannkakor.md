# Amerikanska fluffiga pannkakor

Detta räcker för två proppmätta personer.

## Ingredienser

* 2dl mjölk
* 2 matskedar vit vinäger
* 2.5dl (1 cup) mjöl
* 2 matskedar socker
* 1 tesked bakpulver
* halv tesked salt
* 1 ägg
* 2 matskedar smält smör
* smör till stekning

## Metod

1. Blanda mjölk, vinäger och låt stå i 5 minuter för att surna.

    Detta skapar "sur mjölk" som jag har hört är en vanlig ingrediens i USA.

2. Blanda mjöl, socker, bakpulver. 
3. Vispa ägg, salt och smält smör in i den sura mjölken.
4. Vik ner torra ingredienser i mjölken och rör om till en slät smet.
5. Använd en sked för att ploppa klumpar av deg i en panna och vrid på pannan lite för att få degen att rinna ut åt sidorna.

    Ca. 2 matskedar räcker för en bra storlek.

6. Vänd på pannkakan när små bubblor börjar dyka upp i ytan.
    
    Dessa pannkakor blir små men fluffiga och matiga jämfört med traditionella "svenska pannkakor".

!!! note "Tips"
    Använd en halv tesked bikarbonat för extra fluffighet och krispighet.

# Se också

* [Originalrecept (varning för annonser)](https://www.allrecipes.com/recipe/162760/fluffy-pancakes/)
