# Gratinering med västerbottenost

Detta är en superlätt gratinering som passar till flera olika fisksorter. 


*  1.5 - 1.8 kg fisk

*  en halv riven westerbottenost (ungefär 3 dl)

*  5 dl créme fraiche

*  salt

*  peppar

*  citronpeppar

Rör ihop créme fraiche och riven ost i en skål, krydda på efter eget behag. Smeta röran över fisken i en lång ugnspanna. 

Grädda mitt i ugnen på cirka 175 grader. 

Till detta passar potatis, ugnsgrillade rotfrukter, grönsaker eller mycket annat. 
