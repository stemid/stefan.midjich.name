# HellyBellys Köttfärspaj


*  3-400 gram kalvfärs eller nötfärs

*  1 finhackad gullök

*  1 pressad vitlök solo

*  1 hackad röd paprika

*  1 hackad grön paprika

*  kalvfond

*  salt och peppar

*  torkad timjan

*  3 ägg

*  3 dl grädde

*  riven ost

*  skivad tomat

Bryn köttfärs, sänk värmen och lägg till löken. Stek tills löken blir mjuk. 

Smaka av med salt, peppar och lite kalvfond. Lägg därefter ner paprikan i pannan tillsammans med timjan. Stek i 5-10 minuter till. 

Lägg fyllningen i ett förgräddat pajskal. Dekorera med skivor av tomat och riven ost. Häll över äggstanning. Grädda tills den får fin färg, ungefär 15-20 minuter. 

## Äggstanning

Rör ihop ägg och grädde, krydda efter eget behag. Häll över äggstanning i pajskalet över osten. 

