# Afrikansk gryta

Kan göras med lamm, kalv eller oxkött. Passar utmärkt för långkok och kan serveras med ris.  


*  600 g av något ovannämnt kött

*  4 bananschalottenlökar eller 2 gula lökar

*  2 vitlöksklyftor

*  2 gröna chilifrukter

*  1 matsked smör

*  salt och svartpeppar

*  3 matskedar rapsolja

*  2 matskedar riven färsk ingefära

*  1 burk kokosmjölk

*  2 kanelstänger

*  10-15 hela kardemummakärnor

*  1 matsked hel spiskummin

*  1 matsked hela kryddnejlikor

*  1 kruka färsk koriander

Skär köttet i lagom stora grytbitar. Skala och hacka lök och vitlök, hacka chilin. Bryn köttet i smör några minuter på alla sidor. Lägg över det i en gryta och strö på med salt och peppar. Bryn lök, vitlök, chili och ingefära i olja någon minut. Häll över allt i grytan tillsammans med kokosmjölken, lägg i alla kryddor utom koriandern. 

Låt lammgrytan koka upp och sjuda i minst 1 ½ timme. Strö över hackad koriander och servera grytan med matlagningsyoghurt och mango chutney. 

## Vid större sats

Vid tre gånger större sats så tar bara 50% av angiven mängd kryddnejlika, och ca. två tredjedelar kummin. 
