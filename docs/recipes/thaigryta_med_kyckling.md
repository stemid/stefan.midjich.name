# Thaigryta med kyckling

En snabb och enkel rätt, du behöver. 


*  Ca. 500 g lårfilé av kyckling

*  3 vitlöksklyftor

*  ½ purjolök

*  1 matsked solrosolja eller rapsolja

*  3-4 teskedar röd eller grön currypasta

*  ½ burk konserverad ananas (ca 300 gram)

*  ½ dl ananasspad

*  en burk kokosmjölk

*  saft från en halv lime

*  1 tesked Kycklingfond

*  150 g sugarsnaps färska eller frysta

*  2 matskedar hackad färsk mynta

Rengör och hacka vitlök och purjon, strimla kycklingbitarna. Bryn kycklingen i panna med oljan. Tillsätt vitlök, currypasta och låt det fräsa tillsammans en halv minut. 

Låt spadet rinna av från ananasen och spara det i en skål, tillsätt ananasen i stekpannan tillsammans med kokosmjölk, ananasspad, limesaft och kycklingfond. Låt det koka i 7-8 minuter. Tillsätt sedan purjon och sugarsnaps och låt det koka i 2-3 minuter till. Strö över hackad mynta innan servering. 

Servera gärna med lite sallad, roman eller isberg, och jasminris. 
