# HellyBellys Dahl

## Ingredienser


*  2 matskedar raps eller solrosolja

*  1 pressad vitlök solo

*  1 finhackad gullök

*  2 finhackade morötter

*  1 hel röd chilifrukt

*  2 matskedar finriven färsk ingefära

*  1 matsked mald spiskummin

*  1 matsked mald koriander

*  ½ matsked gurkmeja

*  ½ matsked chilipulver

*  1 kryddmått kanel

*  1 tärning grönsaksbuljong

*  1 burk krossade tomater

*  1 burk kokosmjölk

*  4 dl röda linser

*  2 dl vatten

*  salt

*  lime

*  hackad färsk koriander
 
## Tillagning

 1.  Bryn lök och morötter lätt i olja på medelvärme
 2.  Lägg i ingefära, kummin, koriander, gurkmeja, chilipulver och kanel, stek i ca. 10 minuter till
 3.  Häll över allt i kastrull tillsammans med resterande ingredienser
 4.  Låt puttra under lock i ca 45-60 minuter, tills linserna kokar sönder. Vid behov lägg till vatten för konsistens

Servera gärna med ris, chapati, yoghurt, sambal oelek, mango chutney, lime och färsk hackad koriander. 

**ENJOY!!**
