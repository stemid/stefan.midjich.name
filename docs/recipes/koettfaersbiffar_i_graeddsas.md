# HellyBellys kalvfärsbiffar i gräddsås

## Biffar


*  ca. 500g köttfärs, 100% nöt eller kalv

*  1 dl grädde

*  1 ägg

*  1 dl ströbröd

*  1 matsked kalvfond

*  1 tesked sambal oelek

*  1 matsked torkad mejram

*  1 finhackad gullök

*  1 pressad vitlök solo

Bryn löken lätt i panna med smör. Vispa grädde och ägg, häll sedan i ströbröd och rör runt så det inte klumpar. Häll i kalvfond, sambal och kryddor.  

Rör i den brynda löken i degblandningen. Häll i köttet i degen och rör med händerna. Forma biffar och bryn dem i panna så de får en fin stekyta men inte så att de tillagas helt. 

## Gräddsås


*  ½ l grädde

*  1 matsked kalvfond

*  ½ tesked soja

*  några varv nymalen svartpeppar från kvarnen

*  2 teskedar svartvinbärsgelé eller mango chutney

*  salt

*  brun maizena

Koka upp alla ingredienser utom maizena i en kastrull och smaka sedan av med salt, peppar, gelé eller chutney och soja efter eget behov. 

Använd sedan maizena för att få en tjockare konsistens. 

Lägg biffarna i ett lager och låt dem tillagas i puttrande sås på låg värme. 
