# Österrikiska klimpar för soppa

# Spaetzle


*  2 matskedar mjöl

*  1 ägg

*  salt

Rör om till en jämn smet. Ta en tesked stora bitar och lägg ner i kokande vatten, t.ex. kokande soppa.

# Griessnockerl


*  1 ägg

*  Lika mycket mycket smör (eller annat fett) som ägg

*  ca. 1 matsked mannagryn

Blanda ägg och smör med salt. 

Rör i mannagryn tills du får en degig konsistens. Låt den svälla ett tag. Degen ska vara fast och påminna om gröt. 

Ta teskeds stora bitar av degen och lägg i kokande vatten. Lägger du i kokande soppa så suger den upp soppans buljong och smak.

Dosering av mannagrynen är lite konst, för mycket och degen blir för fast så blir det stenkakor.
