# Amerikansk Körsbärspaj

I skrivande stund har jag inte bakat denna utan skriver bara upp och översätter receptet här för att baka den hemma i helgen. 

Till detta bak behövs:


*  1 bakpensel

*  1 pajform ca. 24cm i diameter

## Degen


*  2 matskedar strösocker

*  2.5 dl vetemjöl

*  3 matskedar malen mandel

*  nypa salt

*  170 gram osaltat kallt smör i kuber

*  1 stort vispat ägg

*  1 matsked brun farin strösocker

## Fyllning


*  100g körsbärskompott eller sylt

*  1 matsked Kirsch Likör, ej obligatoriskt (eller körsbärsbrandy eller amaretto)

*  halv tesked malen muskot

*  halv tesked vaniljextrakt

*  1 matsked majsmjöl blandat till en massa med 2 teskeder vatten

*  500g färska svarta eller röda körsbärs, kärnade och delade

## Bak

For the pastry, put the sugar, flour, ground almonds, salt and butter in a food processor, and blitz until the mixture resembles fine breadcrumbs. With the motor still running, add about three tablespoons of the beaten egg and two tablespoons of ice-cold water, and pulse until the mixture starts to clump together into a dough. You need to be cautious at this stage as you don’t want sticky pastry. Add a little more water if necessary.

Remove the dough from the food processor, divide into two, flatten each portion into discs, wrap each disc in cling film and chill in the fridge for at least one hour.

Preheat the oven to 200C/400F/Gas 6 and grease the pie dish. Remove a disc of pastry from the fridge, unwrap it and roll it out on a generously floured work surface to 3mm/¼in thick and about 2½cm/1in wider than the pie dish. Transfer to a floured baking sheet and chill for about 10 minutes. Repeat this process with the remaining disc of pastry.

Heat the jam for the filling in a saucepan with 100ml (3½ fl oz) water, the alcohol (if using), nutmeg and the vanilla extract. When it’s all melted together, add the diluted cornflour and stir together until smooth and thickened. Add the cherries and gently coat them in the mixture, being careful not to mush them up, so you preserve their shape. Remove from the heat and set aside.

Using a floured rolling pin, carefully transfer one of the chilled pastry sheets to the greased pie dish and drape it across the dish. Let it sink into the dish and, holding on to the edges, lift and tuck the pastry into the edges of the dish, all the way around, to line it. Trim off any excess pastry and lightly prick the base with a fork. Fill the dish with the cherry filling. Use a pastry cutter to cut holes in the remaining pastry sheet, covering an area just smaller than the diameter of the pie dish, leaving a large border intact. Place it over the pie filling and fold the edge of the top crust over the edge of the bottom crust, crimping it together with your fingers to seal.

Brush the pastry with the remains of the beaten egg and sprinkle over the demerara sugar. Bake for 20 minutes, until the crust is golden, then reduce the oven temperature to 180C/350F/Gas 4, covering the top of the pastry with foil if you need to, to avoid it burning, and bake for a further 35–40 minutes, until the filling is bubbling and the pastry is golden, firm and lightly puffed.

Allow the pie to cool for about one hour before serving with cream. 

# Se också


*  [.:omvandling](./omvandling)
