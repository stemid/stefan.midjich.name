# CV

## Personal intro

A full-time Linux and BSD user since 1999, born in 1985 and currently residing in Malmö (Sweden). Full-time employee of the same company since 2011.

My roots are in Croatia so I speak those languages relatively fluently for someone who has lived in Sweden all their life. Along with a very strong grasp of English and Swedish. I've studied Danish and worked with Danish clients for a year, but it's a very difficult language to speak confidently.

At this point in my life I want a 100% remote position.

## Job history

### may 2011 - currently

Was hired by Cygate AB in Malmö to take over after a former Linux specialist who was leaving. At first under their consulting department working full time with their operations department, and later directly under the operations department. 

One of my first projects needed both Linux and BSD knowledge, and I had been recommended to them by my first boss on the base of my FreeBSD experience. Ended up also working with a few Solaris servers in another client contract.

Within a year I was hired by the operations department but still did consultancy jobs throughout the company and for other clients, because my expertise in Linux was highly valued in an organization otherwise mostly staffed by Microsoft and network specialists.

We did a lot of project based work using ITIL roles and life cycles.

In the beginning I worked with TSIC customers who were based all over Europe, but eventually my work was solely with Swedish organizations.

Everyone has a Linux server somewhere giving them trouble so I've been fortunate to work with the most well known names in Sweden, as well as regional governments and the health care sector.

I have a skill of being able to solve problems using open source software without any license fees and this lead me to design and build all sorts of services for the most interesting clients, including Cygate themselves.

Sometimes project managers have even weighed my open source solution against an Enterprise solution and saved hundreds of thousands SEK each year by choosing open source.

It's safe to say my work at Cygate has been the most formative of my career so far.

My first project at Cygate involved Puppet and when that project was eventually completed and handed over to the client I was looking for a new configuration management tool. By happenstance Ansible had just released version 0.2 and I just stumbled into their IRC channel with Michael deHaan and about 20 other users at the time.

Now of course Ansible has become not only my favorite automation tool but also a world leader in its field.

Since 2019 I have been part of the Architecture & Development department where I do mostly Devops in Gitlab and Kubernetes. But still use my Linux sysadmin expertise to help manage RHEL servers in a highly secure PKI environment.

### 2009 - may 2011

Worked at Stream Global Services in Helsingborg doing phone support for Dell PowerEdge servers. Did pretty well at this job, was often top ranked in our statistics.

This is where I got to work with Danish Dell customers on the phone. It was a struggle but I learned enough to know when even the most hardcore Jutlander needed their hard drives replaced.

### 2004-2008

First IT job was at Swebase AB, a small web hosting company run by a few guys out of Helsingborg. We used primarily FreeBSD but we'd use anything the customer required, even Windows Server 2003.

I was 19 when I started, a child by my standards today but looking back I still got a lot of things done and it was an invaluable learning experience.

For example I wrote a customer control panel in PHP where they could manage their own services. Very crudely stored DNS zones in PostgreSQL and used scripts to update, so no modern job management like I would have used today.

You could find me taking phone calls from elderly end users needing help with Outlook, troubleshooting FreeBSD firewalls, DIY spam filters, configuring IIS extensions or working on some code that could be Perl, PHP or even C.

## Education history

I never finished my secondary education (gymnasiet) in Sweden and went straight to working in Helsingborg.

### 2008-2009

Tried to complement my secondary school grades at Helsinborgs Folkuniversitet by taking a course called Tourism in the Øresund region. Among other classes did Danish 1 and Danish 2 for points.

The nerd in me shone through here when we were required to hand in work assignments in MS Word format. All I had was a FreeBSD laptop so I created a static HTML generator that would generate CSS to match the format of MS Word so that even our teachers were fooled.

## Current skills

When you're focused on Linux you get to work with so many different technologies. Which is why I've condensed this list into the most relevant ones today, and the ones I enjoy working with the most.

* AWS (Lambda, Route53, Cloudfront, ACM, IAM, S3, EC2)
* Ansible
* Bash
* Bind
* DOKS (DigitalOcean Kubernetes)
* Docker
* ELK
* Fedora CoreOS
* Galera3
* Gitlab CI/CD
* Grafana
* Keepalived
* Kubernetes
* K8s operators such as kube-prometheus-stack, CrunchyData PostgreSQL and others
* LKE (Linode Kubernetes Engine)
* MariaDB/PostgreSQL
* Maxscale/pgpool-II
* Nextcloud
* Podman/Buildah
* Prometheus
* Python
* RHEL/CentOS/Fedora
* SElinux
* Unbound
* VMware
* git
* vim

Even if I've never had the chance to work with some popular tech like HAproxy for example, I could pick it up within an hour based on my previous experience. And there are other technologies that I have worked in extensively like Postfix, Apache httpd and nginx but they seem a bit superfluous to mention these days when it's so simple to get an nginx container up and running for most purposes.

I also have experience working with some other cloud providers and their APIs.

* Digitalocean
* Glesys
* Linode

Here is also a list of some technologies I've worked in lately that I did not enjoy but nonetheless had to learn.

* Azure Devops
* Debian/Ubuntu
* Microsoft Teams
* Microsoft Windows Server 2016/2019
* Powershell
* ServiceNow

## Strengths and weaknesses

Learned in my mid 30s that I am on the autism spectrum, which explains why I can hyperfocus on things. It also means I have trouble working in teams due to a difficulty with interpreting social cues.

Definitely prefer communicating with people online, on Slack, IRC, Discord, Teams and similar services.

I'm honest to a fault, straightforward and solution-driven. Working as a consultant for over a decade I have a good way with clients, despite what people might think, clients often enjoy speaking to me because I cut through the bullshit while keeping it professional.

Very meticulous when performing tasks, which adds stability, security and accessibility for other techs, but also means I can spend more time on tasks than other techs.

Good visualizer when it comes to clusters, distributed systems, granular permissions and other complex systems.

I can basically solve most problems in the Linux, or open-source, ecosystem.

## Highlighted projects

These are some personal open source projects I want to highlight. Most of these have primarily found use within my work duties but are also available publicly.

* [This website stefan.midjich.name](https://gitlab.com/stemid/stefan.midjich.name), where I have a Wiki-like section of documentation I have been writing for over 10 years.
* [kubeadm-cluster](https://gitlab.com/stemid-ansible/playbooks/kubeadm-cluster) Gitlab repo that I use to setup and manage both on-prem k8s clusters and cloud based k8s.
* [Boilerplate project of a Python Tornado AMQP consumer](https://gitlab.com/stemid/tornado-boilerplate).
* [postgres-s3backup](https://gitlab.com/stemid/postgres-s3backup) is a simple but clever way to perform encrypted backups of PostgreSQL databases into S3 compatible object storage.
* [Prometheus dead man's switch in Python](https://gitlab.com/stemid/python-deadmansswitch) using AWS Lambda to send a notification if your Prometheus setup goes down and is unable to send alerts.
* [Prometheus exporter using Python Twisted](https://gitlab.com/stemid/halon-exporter), originally made to export Halon metrics from Elasticsearch.
* [Password pusher](https://gitlab.com/stemid/pwfrank) that is still in use today.
* [The rest of my Gitlab.com profile](https://gitlab.com/users/stemid/projects) where I host most of my projects.
* [My old Github.com profile](https://github.com/stemid) which I have stopped using in favor of Gitlab.
* [The stemid-ansible namespace at Gitlab.com](https://gitlab.com/stemid-ansible) where I host all my Ansible playbooks and roles.

