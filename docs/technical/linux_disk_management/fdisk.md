# Fdisk

## Utöka diskpartition

Först och främst måste systemet startas om efter att hårdvara förändrats, annars förstår inte Linux detta. VMware rekommenderar till och med att stänga ner en virtuell maskin innan diskar utökas men detta har jag aldrig gjort. 

I vilket fall som helst startas fdisk på disken som ska utökas. 

    sudo fdisk /dev/sda

I fdisk ska man sedan radera den sista partitionen och skapa om den med en större storlek. **Observera** att det är *mycket* viktigt att partitionen skapas om med samma startsektor. Går detta inte så får man avbryta genom att trycka Ctrl+d eller q för att avsluta fdisk. 

 1.  Tryck p för att visa partitionerna och notera sektorvärden (vi låtsas att partition 2 ska utökas)
 2.  Tryck d för att radera och välj 2
 3.  Tryck n för att skapa ny och välj nummer 2
 4.  Välj standardvärden, eller försök ange startsektorvärdet om standard inte stämmer
 5.  När partitionen är skapad tryck t för att ange typen av partitionen
 6.  Välj 8e för Linux LVM typen, eller L för att lista alla typer
 7.  p igen för att kontrollera ändringarna
 8.  w för att skriva ändringarna till disk

Nu kan man behöva starta om igen. Alternativt försök med ''sudo kpartx -a /dev/sda'' men det finns i min erfarenhet ingen garanti att en partition kan läsas in igen från MBR. 

Följ sedan [LVM guiden](./LVM). 

# Se också


*  [VMware KB: Extending a logical volume in a virtual machine running Red Hat or Cent OS (1006371)](http://kb.vmware.com/kb/1006371)
