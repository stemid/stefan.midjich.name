# Expand virtual disk with GPT partition

>**Note:** I consider it a mistake made by me a long time ago to create a GPT partition for a very large volume. Had I done the same today I would skip the partition and write an LVM PV directly to the disk.

If you've expanded a virtual disk with a very large GPT partition in your hypervisor then this is how you finish the expansion in Linux. There are a few ways to skin this cat, here's one using sgdisk.

First install the gdisk package for the sgdisk binary.

Print the partition table with sgdisk to see what you have to work with.

```
$ sudo sgdisk -p /dev/sdc
Disk /dev/sdc: 6920601600 sectors, 3.2 TiB
Logical sector size: 512 bytes
Disk identifier (GUID): 9DF2B6DF-4ABE-4DBD-A853-60C5511C14D8
Partition table holds up to 128 entries
First usable sector is 34, last usable sector is 6920601566
Partitions will be aligned on 8-sector boundaries
Total free space is 29 sectors (14.5 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1              63      6920601566   3.2 TiB     8300  

```

Note the start and end sector. 

>Also note if you have more than one partition because this guide only covers expanding a single partition. If you have more than one partition I would recommend making an offline expansion with the GNU Parted Live CD.

Move the GPT partition backup data to the end of disk, because after you've discovered the new disk space in OS this data is not at the end anymore.

    sudo sgdisk -e /dev/sdc

Delete the first partition.

    sudo sgdisk -d 1 /dev/sdc

Re-create the first partition with the exact start sector as before.

    sudo sgdisk -a 1 -n 1:63:0 /dev/sdc

>You might not need a start sector of 63, the default is 2048. Read the manual about -a and -n because they are related. I used ''-a 1'' as a last resort to get a very non-standard starting sector.

Now most systems will require a reboot for the kernel to register the new partition table. This depends on if the volume is in use or not.

Partprobe should otherwise discover the new partition table for the kernel.

    sudo partprobe /dev/sdc

Now you should be able to see the new space in pvs as free space.

    sudo pvs

So go through the normal process of expanding the logical volume.

    sudo pvresize /dev/sdc1
    sudo lvresize -l+100%FREE /dev/mapper/your_vg-your_lv
    sudo resize2fs /dev/mapper/your_vg-your_lv
