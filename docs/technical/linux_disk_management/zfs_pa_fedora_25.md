# ZFS på Fedora 25

En lathund för hur jag hanterar volymerna på mitt NAS.

# Installation

Följ [instruktionerna på deras wiki](https://github.com/zfsonlinux/zfs/wiki/Fedora).

## Aktivera tjänster

    $ sudo systemctl enable zfs && sudo systemctl start zfs
    $ sudo systemctl enable zfs-import-cache && sudo systemctl start zfs-import-cache
    $ sudo systemctl enable zfs-mount && sudo systemctl start zfs-mount

# Skapa RAIDZ

Jag har fyra diskar så jag kör RAIDZ1 vilket påminner om RAID5. 

    $ sudo zpool create pool1 sda sdb sdc sdd

# Skapa zvol

Zvol dyker upp som block-enheter och kan därför exporteras med iSCSI. Utan ''-V'' argumentet så blir det ingen block-enhet skapad under ''/dev/zvol''.

    $ sudo zfs create -V 25G pool1/vm-db01-disk1

# Exportera ZFS med iSCSI

Installera tgtd.

    $ sudo dnf install scsi-target-utils

Precis som att exportera vilen block-enhet som helst. Se länken längst ner på sidan.

Ett exempel på ''/etc/tgt/conf.d/nas02.conf''.

```
`<target iqn.2017-18.local.swehack:nas02.target1>`
 # Allow connections from:
 initiator-address 192.168.22.80

 `<backing-store /dev/zvol/pool1/vm-db01-disk1>`
    write-cache on
    lun 1
 `</backing-store>`

 `<backing-store /dev/zvol/pool1/vm-db01-disk2>`
    write-cache on
    lun 2
 `</backing-store>`

 `<backing-store /dev/zvol/pool1/vm-web01-disk1>`
    write-cache on
    lun 3
 `</backing-store>`

 `<backing-store /dev/zvol/pool1/vm-web01-disk2>`
    write-cache on
    lun 4
 `</backing-store>`

`</target>`

```
# Fallgropar

Om din zpool inte dyker upp efter omstart så kanske tjänsterna inte är startade och aktiverade. T.ex. zfs-import-cache ska se till att gamla importerade pooler importeras igen efter omstart. 

Prova detta. 

    $ sudo zpool import pool1

Lägg till ''-f'' om det inte fungerar. 

    $ sudo zpool import -f pool1

Funkar det så är det troligtvis något fel i systemd som gör att poolen antingen inte avmonteras rent vid nedstängning, eller inte monteras vid uppstart. 

# Se också


*  [..:..:projekt:Hemmabyggt NAS](../../projekt/Hemmabyggt NAS)

*  [RedHat dokumentation om iSCSI storage pool med virsh](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Virtualization_Administration_Guide/ch12s05s04.html)

*  [ZFS on Linux wiki om Fedora](https://github.com/zfsonlinux/zfs/wiki/Fedora)

*  [Fedora wiki om iscsi targets](https://fedoraproject.org/wiki/Scsi-target-utils_Quickstart_Guide)
