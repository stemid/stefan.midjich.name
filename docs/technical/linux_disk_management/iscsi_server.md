# iSCSI server

# tgtd configuration

Example configuration in ''/etc/tgt/conf.d/nas02.conf''.

```

`<target iqn.2017-18.local.mydomain:nas02.target1>`                      
 # Allow connections from:                                            
 initiator-address 192.168.22.80                                      

 `<backing-store /dev/zvol/pool1/vm-db01-disk1>`                        
    write-cache on                                                      
    lun 1                                                               
 `</backing-store>`                                                     
`</target>`

```

Add new LUNs in there and restart tgtd service.

    $ sudo systemctl restart tgtd

# iSCSI client

If you're not using chap auth then it's very simple.

## Login to a target

    $ sudo iscsiadm --mode node --targetname iqn.2017-18.local.mydomain:nas02.target1 --portal 192.168.22.22:3260,1 --login

## Show iscsi session

    $ sudo iscsiadm -m session -P 3

## Re-discover new LUNs on a session

    $ sudo iscsiadm -m session --sid=4 --rescan

## Logout of all iSCSI targets

    $ sudo iscsiadm -m node -u
# Some tgt-admin commands

    $ sudo tgt-admin --dump
    $ sudo tgt-admin --update ALL
