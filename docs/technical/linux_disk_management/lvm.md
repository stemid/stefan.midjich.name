# Logisk Volymhantering i Linux

I denna guiden har vi en fysisk disk som heter */dev/sda*. 

# Begrepp i LVM


*  PV - Physical Volume är en representation av fysiska diskar i LVM
    * ''sudo pvcreate /dev/sda'' förbereder en fysisk disk för logisk volymhantering

*  VG - Volume Group är grupper av logiska volymer
    * ''sudo vgcreate diskgrupp /dev/sda'' skapar en volymgrupp som din PV tillhör

*  LV - Logical Volume är en logisk volym som kan formateras med ett filsystem
    * ''sudo lvcreate ...'' skapar din logiska volym

# Information om logiska volymer

Här ovan skapades en logisk volym som kommer ha följande sökvägar i filsystemet. 


*  /dev/mapper/diskgrupp-disk1

*  /dev/diskgrupp/disk1

Linux stödjer olika sökvägar till logiska volymer, bäst är att använda ''/dev/mapper'' sökvägen.


*  sudo pvs - lista fysiska volymer

*  sudo vgs - lista volymgrupper

*  sudo lvs - lista logiska volymer

# Skapa volym

## Skapa PV (fysisk disk)

    sudo pvcreate /dev/sda

## Skapa VG (volymgrupp)

    sudo vgcreate diskgrupp /dev/sda

Volymgruppen heter *diskgrupp*. 

## Skapa LV (logisk volym)

    sudo lvcreate -l+100%FREE -n disk1 diskgrupp

Skapar en logisk volym som heter *disk1* i volymgruppen *diskgrupp*. Storleken blir 100% av ledigt utrymme i volymgruppen. 

Alternativt kan du ange storleken så här. 

    sudo lvcreate -L50g -n disk1 diskgrupp

### Montera logisk volym

En logisk volym kan sedan formateras med ett filsystem.

    sudo mkfs.ext4 /dev/diskgrupp/disk1

Och till sist monteras genom att läggas in i filen ''/etc/fstab''.

    /dev/mapper/diskgrupp-disk1 /opt ext4 defaults 0 0

Testa monteringen genom att köra ''sudo mount -a'' som monterar allt i /etc/fstab.
# Utöka volym

## Skapa ny PV (fysisk volym)

Efter att ha lagt till en ny disk kan man göra så här. 

    sudo pvcreate /dev/sdb

Ska man utöka en befintlig diskpartition måste man starta om systemet. Metoden för det är att radera partitionen i fdisk och skapa om den med samma startsektor fast en ny slutsektor. Detta täcks inte här, se wiki-sidan för [.:fdisk](./fdisk). 

## Utöka VG (volymgrupp)

    sudo vgextend diskgrupp /dev/sdb

## Utöka LV (logisk volym)

    sudo lvextend -l+100%FREE /dev/mapper/diskgrupp-disk1

Den kan bara utökas om volymgruppen har ledigt utrymme, kontrollera med vgs. 

# Fallgrop

Med ''-L'' argumentet anges storlek i suffixenheter som g eller m. Detta är dock missvisande för det är nästan omöjligt att få med sig allt ledigt utrymme med sådana enheter då LVM räknar utrymme i *extents*. 

Därför är ''-l'' argumentet lite smidigare för då anger man antal extents som ska utökas, eller procent. Jag använder alltid procent. 

    sudo lvextend -l+100%FREE ...

Garanterat att få med allt ledigt utrymme. 

# Se också


*  [.:fdisk](./fdisk)

*  [RHEL7 LVM manual](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Logical_Volume_Manager_Administration/)
