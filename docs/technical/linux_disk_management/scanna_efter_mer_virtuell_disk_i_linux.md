# Scanna efter mer virtuell disk i Linux

Se två andra guider längst ner på sidan för hur man använder fdisk och LVM. 

Denna guide visar bara hur man scannar om en disk i Linux för att upptäcka att storleken ändrats. 

## Scanna efter ny disk

Ett knep jag brukar köra som alltid funkat för mig. 

    for host in /sys/class/scsi_host/host?; do echo '- - - ' | sudo tee $host/scan; done

En enkel loop som loopar igenom innehållet av ''/sys/class/scsi_host'' som börjar på host, alltså olika host-kontroller i systemet. Sedan måste man använda root för att skriva tre streck till scan-filen i varje kontrollers katalog för att få den att scanna efter nya diskar.

Lista alla upptäckta diskar.

    sudo fdisk -l

## Scanna efter utökad befintlig disk

I modernare Linux varianter räcker det att göra ``echo '1' | sudo tee /sys/block/sda/device/rescan``, här nedanför går jag igenom lite mer detaljer som var relevanta i äldre versioner av Linux.

Leta i ''/sys/class/scsi_disk'' och i ''/sys/block/sd*'' för att matcha SCSI ID med disknamn. 

T.ex. kan det se ut så här. 

    # ls -l /sys/block/sd*
    lrwxrwxrwx 1 root root 0 apr 12 12:13 /sys/block/sda -> ../devices/pci0000:00/0000:00:10.0/host2/target2:0:0/2:0:0:0/block/sda
    lrwxrwxrwx 1 root root 0 apr 12 12:13 /sys/block/sdb -> ../devices/pci0000:00/0000:00:10.0/host2/target2:0:1/2:0:1:0/block/sdb

    # ls -l /sys/class/scsi_disk/
    lrwxrwxrwx 1 root root 0 apr 12 12:13 2:0:0:0 -> ../../devices/pci0000:00/0000:00:10.0/host2/target2:0:0/2:0:0:0/scsi_disk/2:0:0:0
    lrwxrwxrwx 1 root root 0 apr 12 12:13 2:0:1:0 -> ../../devices/pci0000:00/0000:00:10.0/host2/target2:0:1/2:0:1:0/scsi_disk/2:0:1:0

Då ser vi på target2:0:0 och target2:0:1 vilken disk vi vill scanna om. 

Sedan kör vi följande kommando för att scanna om /dev/sdb, detta måste göras som root. 

    echo '1' > /sys/devices/pci0000:00/0000:00:10.0/host2/target2:0:1/2:0:1:0/block/sdb/device/rescan

>Notera att tack vare symlänkar finns det flera olika sökvägar till den filen, men det viktiga är att använda filen rescan och för rätt SCSI enhets ID. 

# Se också

*  [Utöka disk i fdisk](./fdisk)
*  [Utöka disk i LVM](./lvm)

