# Flytta /var på CentOS 7

CentOS 7 installationsprogram är handikappat och kan inte skapa en volym på en annan disk utan att ha / där. Så om du inte använder kickstart är detta en bra metod för att flytta /var efter installation.

Du har följande fysiska diskar.


*  /dev/sda - Systemdisk

*  /dev/sdb - Tom disk

## Steg 1: Installera på Systemdisk

Följ de vanliga stegen i CentOS för att skapa /, /home och swap på systemdisken.

Den andra disken rör du inte under installationen.

## Steg 2: Init 1

Efter start av systemet logga in i lokal konsoll och starta ett root-skal.

Se till att du har följande kommandon installerade, rsync och lsof. Har du inte det måste du ansluta till internet och installera dem först.

    $ yum install lsof rsync

När programmen har installerats ska du gå till init 1 läge med följande kommando. **Detta kan kräva root-lösenord**, så se till att ha ett root-lösenord om du t.ex. installerat utan att sätta root-lösenordet under installation.

    $ init 1

Kontrollera att ingen process har filer öppna på /var med lsof kommandot. 

    $ lsof | grep '/var'

I ''init 1'' läge ska det inte visas något alls från det kommandot. 

## Steg 3: Formatera och montera ny disk

Nu ska du formatera och montera /dev/sdb under /mnt tillfälligt. 

    $ pvcreate /dev/sdb
    $ vgcreate vg_data /dev/sdb
    $ lvcreate -l+100%FREE -n lv_var vg_data
    $ mkfs.xfs /dev/mapper/vg_data-lv_var
    $ mount /dev/mapper/vg_data-lv_var /mnt

## Steg 4: Synka filer till ny disk

Nu synkar du filerna från gamla /var till nya disken som är monterad på /mnt.

    $ rsync -av /var/* /mnt/

>**Obs**. att om du flyttar /var på en befintlig server som kanske är ansluten till ett stort AD så har du en mycket stor "sparse" fil som heter /var/log/lastlog. I det fallet är kommandot ''cp -ar /var/* /mnt/'' bättre än rsync.

Kontrollera att filerna ser likadana ut. 

    $ ls -lat /mnt /var

Är du orolig kan du även kontrollera storleken. 

    $ du -ch /var /mnt

## Steg 5: Radera gamla /var och ersätt med ny

    $ rm -rf /var/*
    $ umount /mnt

Redigera ''/etc/fstab'' så detta sker vid uppstart. Raden du lägger till kan se ut så här. 

    /dev/mapper/vg_data-lv_var /var xfs defaults 0 0

Du kan lägga till den raden med följande kommando utan att starta en text-editor.

    $ echo '/dev/mapper/vg_data-lv_var /var xfs defaults 0 0' >> /etc/fstab

Testa din fstab genom att montera /var igen med följande kommando. 

    $ mount -a

''mount -a'' ska montera alla rader i ''/etc/fstab'' om de stämmer. 

Kontrollera att /var är monterad och ser bra ut innan du startar om.

    $ df -h
