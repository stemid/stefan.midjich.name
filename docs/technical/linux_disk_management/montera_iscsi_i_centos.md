# Mounting iSCSI in CentOS 7

Tested on CentOS 7 and Fedora 25/26.

Install package ''iscsi-initiator-utils''.


*  iSCSI server is 192.168.22.22

# Discovery

Find out the target names. I'll assume there is only one target.

    $ sudo iscsiadm -m discovery -t sendtargets -p nas02.swehack.local
    192.168.22.22:3260,1 iqn.2017-18.local.swehack:nas02.target1

# Login

Basic login without CHAP. Just copy most of the values from the discovery step.

    $ sudo iscsiadm --mode node --targetname iqn.2017-18.local.swehack:nas02.target1 --portal 192.168.22.22:3260,1 --login

## Basic initiator authentication

This requires no login info and is usually restricted by client IP. Just edit ''/etc/iscsid/initiatorname.iscsi'' and add the InitiatorName.

```

InitiatorName=iqn.2017-18.local.swehack:nas02.target1

```
## CHAP Authentication

In ''/etc/iscsi/iscsid.conf'' you can see the following options for CHAP authentication.


*  node.session.auth.username

*  node.session.auth.password

*  discovery.sendtargets.auth.username

*  discovery.sendtargets.auth.password

# Using devices

To find out which devices are being exposed to your client. This will list all your targets so you'll have to scroll to the one you want.

    $ sudo iscsiadm -m session -P 3
    ...
                Attached SCSI devices:                                                                                                                                                                             

                ************************                                                                                                                                                                           
                Host Number: 11 State: running                                                                                                                                                                     
                scsi11 Channel 00 Id 0 Lun: 0                                                                                                                                                                      
                scsi11 Channel 00 Id 0 Lun: 1                                                                                                                                                                      
                        Attached scsi disk sdf          State: running                                                                                                                                             
                scsi11 Channel 00 Id 0 Lun: 2                                                                                                                                                                      
                        Attached scsi disk sdh          State: running
                                 

## WWN for devices

In my experience those names aren't reliable (/dev/sdg) so to find out the WWN that will not change across reboots/session changes you can do this.

    $ ls -l /dev/disk/by-id/wwn-*
    ...
    /dev/disk/by-id/wwn-0x60000000000000000e00000000010001 -> ../../sdg
    /dev/disk/by-id/wwn-0x60000000000000000e00000000010002 -> ../../sdh

These won't change and can be used in virtual machines as storage for example.

You can also use by-path which should also remain unchanged as long as you don't change your NAS or target.

    /dev/disk/by-path/ip-192.168.22.22:3260-iscsi-iqn.2017-18.local.swehack:nas02.target1-lun-1

# Rescan for new LUNs

Find out your session ID.

    $ sudo iscsiadm -m session
    tcp: [6] ...

Rescan that session ID.

    $ sudo iscsiadm -m session --sid=6 --rescan
# See also


*  [Find WWN of FC/iSCSI disk](https://access.redhat.com/articles/17054)
