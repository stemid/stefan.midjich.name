# Linux I/O köhantering

Detta är ingen tekniskt djup guide, jag vill bara visa vad jag ofta ändrar på mina Linux servrar. 

Linux har oftast tre olika köhanteringsmetoder för IO-data, du kan se vilka och vilken du använder med följande kommando. 

    sudo cat /sys/block/sda/queue/scheduler

Detta är köhanterare för diskenheten /dev/sda. Som standard visas ''[cfq]'' inom klamrar, vilket betyder att den används just nu. 

## cfq

Är optimerad för arbetsstationer, främst snurrande magnetiska diskar. 

Står för Completely Fair Queuing. 

##  deadline 

Jag vet inte mycket om den här förutom att i de flesta debatter jag har läst online så föredrar folk deadline framför noop eftersom deadline applicerar ändå vissa optimeringar till skrivningarna utan att vara lika begränsad till vissa typer av medium som cfq är. 

## noop

Står för no operation och är i stort sett bara en FIFO kö. First in, first out. Allt skrivs direkt till diskstyrenheten. 

Denna kan verka optimal om man vill lita på sin diskstyrenhet, t.ex. SSD, RAID eller VMware. Dock är det deadline som vinner popularitetsomröstningen på nätet eftersom den har fler optimeringar än noop. 

# Byt köhanterare

    echo 'deadline' | sudo tee /sys/block/sda/queue/scheduler
