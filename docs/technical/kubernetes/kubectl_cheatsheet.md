# kubectl cheatsheet

There are many cheatsheets like this online but this one is from my perspective.

## kubectl explain

This shows all the object specifications available and you can drill down each object to see what key values they take.

    $ kubectl explain deployment.spec.template.spec.containers

## Run command inside pod/container

    $ kubectl exec -n namespace -it pod-name -- nslookup my-service

## Contexts and files

* I tend to avoid using .kube/config and instead specify individual files like ``$HOME/.kube/my-environment.yaml``.
* And then use the KUBECONFIG environment variable to add them like this ``export KUBECONFIG=$KUBECONFIG:$HOME/.kube/my-environment.yaml``.

## Connecting through Socks proxy

If you only have SSH access to a k8s cluster you can [setup a tunnel using DynamicForward](../../security/ssh_tunnel_with_systemd_service) and connect to it using the ``https_proxy`` environment variable.

    $ export https_proxy=socks5://localhost:56443
    $ kubectl get nodes

## Get configmap or secret values from keys with dashes

Retrieve a value with dashes instead of underscores.

``kubectl get secret mysecret --template='{{index .data "key-name-with-dashes"}}'``

## Get Secret environment variables

``kubectl -n mastodon get secret/mastodon-env -o json | jq '.data | map_values(@base64d)'``

# See also

* [Official Kubernetes kubectl cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-context-and-configuration)
