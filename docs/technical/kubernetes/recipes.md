# Kubernetes recipes

Here I gather bookmarks, or little tips that I come across in my journey through k8s.

## Using a certificate from a CA in ingress

The only difference between dynamically signing a cert with cert-manager is that you must create your own secret, and update it at renewal.

* [Kubernetes docs](https://kubernetes.io/docs/concepts/services-networking/ingress/#tls)

## Assorted homelab services

* [Gitlab.com repo of my homelab services](https://gitlab.com/stemid-ansible/playbooks/homelab-services.git)
