# OpenVAS i CentOS 7

## Kompilering

Paketet openvas-smb går inte att kompilera på CentOS 7 i sitt nuvarande tillstånd, 1.0.1. Denna patch är nödvändig. Har att göra med att CentOS är så hårt knutet till krb5-libs men openvas-smb kräver heimdal varianten.

```
--- CMakeLists.txt.orig 2016-09-28 00:09:08.736154546 +0200
+++ CMakeLists.txt      2016-09-28 00:09:12.170140914 +0200
@@ -32,6 +32,14 @@
 find_package(Perl REQUIRED)
 find_package(Popt REQUIRED)
 
+set(GSSAPI_LIBRARIES /usr/lib64/heimdal)
+set(GSSAPI_INCLUDE_DIRS=/usr/include/heimdal)
+set(KRB5_CONFIG /usr/bin/heimdal-krb5-config)
+
+if (EXISTS "/etc/redhat-release")
+ set(CMAKE_PREFIX_PATH "/usr/lib64/heimdal")
+endif()
+
 find_package(GSSAPI)
 if (NOT "${GSSAPI_FLAVOR_HEIMDAL}" STREQUAL "TRUE")
   message (FATAL_ERROR "The heimdal GSS library could not be found. Please make sure the heimdal development package is installed.")
```

FIXME Slutför guiden.
