# Smartkort med SSH

Detta är en guide för att uppnå följande mål. 


*  Autentisera med ett smartkort för SSH.

*  Använda Omnikey CardMan PCMCIA kortläsare med Fedora 22.

För att göra detta kommer jag byta till GPG för SSH-autentisering, detta är lite av en livsstilsomställning för vissa så bra att vara redo. Istället för ssh-agent används gpg-agent. 

Till detta behövs följande saker. 


*  Någon typ av smartkortläsare för datorn, t.ex. Omnikey CardMan eller Gemalto Gemplus GemPC.

*  Ett GnuPG smartkort som går att beställa från [kernelconcepts.de](http://shop.kernelconcepts.de/).

*  En grundläggande kännedom av [GnuPG CLI-gränssnitt och nycklar](./gnupg för nybörjare). 

*  En egen privat nyckel. 
# Omnikey CardMan

{{ .:lenovo-omnikey_cardman.jpg?300x200}}

Först och främst går jag igenom hur jag fick igång min Omnikey CardMan PCMCIA kortläsare i min Lenovo X230 med fedora 22. Detta är inte obligatorisk läsning eftersom en extern Gemalto USB-kortläsare fungerar utan problem och finns att köpa på Kjell o co..

Omnikey CardMan har USB enhetsnummer 076B:4321. 

Problemet med Omnikey är att den kräver en speciell drivrutin för att hantera stora paket, som i sin tur leder till att kunna hantera nycklar över 1024-bitar i storlek. 

Ladda ner drivrutinen [härifrån](http://www.hidglobal.com/drivers). Jag kom till slut till den här [nedladdningssidan](http://www.hidglobal.com/Download-EULA?headless=&regionfree=&nid=20084). 

Packa upp filen och läs install-skriptet eller README-filen för mer information. 

Sammanfattat så är det en drivrutin för pcscd, i Fedora krävs paketen pscs-lite och pscs-tools. 

# Konfa GnuPG med Gnome

Kör man Fedora 22, eller någon annan modern Linuxdistro med Gnome, så händer det att gnome-keyring försöker kapa gpg-agentens funktionalitet. Det är möjligt att även KDE gör något liknande. 

Tyvärr fungerar inte gpg-agenten i gnome-keyring bra nog för att använda med smartkort. 

Så man måste stänga av GPG-agenten i gnome-keyring med följande kommandon som jag vet fungerar i Fedora 22 och Ubuntu 14.04. 

    cp /etc/xdg/autostart/gnome-keyring-gpg.desktop ~/.config/autostart/
    cp /etc/xdg/autostart/gnome-keyring-ssh.desktop ~/.config/autostart/
    echo 'Hidden=true' >> ~/.config/autostart/gnome-keyring-ssh.desktop
    echo 'Hidden=true' >> ~/.config/autostart/gnome-keyring-gpg.desktop

Aktivera den traditionella gpg-agenten. 

    echo 'use-agent' >> ~/.gnupg/gpg.conf
    echo 'enable-ssh-support' >> ~/.gnupg/gpg-agent.conf

Till sist använder jag [ett skript](https://gist.github.com/stemid/1ae5544dc3fc242842e0) för att starta gpg-agenten vid inloggning, installera det i ''~/.gnupg/gpg-wrapper''. 

[gist stemid/1ae5544dc3fc242842e0]

Använd skriptet i din .bashrc med ''source ~/.gnupg/gpg-agent-wrapper'' t.ex., i din globala xsession som är ''/etc/X11/xinit/xinitrc.d/01-xsession'' eller din lokala ~/.xsession. 

Logga sedan ut eller starta om datorn. 

# Skapa nyckel på smartkort

Kortet måste gå att läsa av annars måste det lösas först innan nyckel kan skapas. 

```
$ gpg --card-status
gpg: detected reader `OMNIKEY CardMan (076B:4321) 4321 00 00'
Application ID ...: D2XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Version ..........: 2.1
Manufacturer .....: ZeitControl
Serial number ....: 0000XXXX
Name of cardholder: Stefan Midjich
Language prefs ...: se
Sex ..............: man
...
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]
```

FIXME Work in progress.

# Felsökning

Ett vanligt fel är att gpg-agent inte får kontakt med kortläsaren. Se då till att pcscd är igång. 

    sudo systemctl start pcscd

Bra att tänka på är att gpg-agent normalt sett kan starta scdaemon, vilket fungerar helt ok för Gemalto Gemplus kortläsare t.ex., men sämre för Omnikey. Därför föredrar jag pcscd eftersom den kan ladda in extra drivrutiner enklare. 

Scdaemon har normalt sett inget startskript utan anropas från gpg-agent. 
# Se också


*  [.:GnuPG för nybörjare](./GnuPG för nybörjare)

*  [Linux Foundation guide för SSH med Smartcard](https://github.com/lfit/ssh-gpg-smartcard-config/blob/master/Gemalto_USB_Shell_Token.rst)
