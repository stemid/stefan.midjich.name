# Socks proxy over SSH

This is my preferred method of setting up a permanent proxy solution over ssh.

# Step 1: Configure the SSH server

First of all ensure the server is configured to only accept public key authentication.

I'll create a special user account for this proxy. I don't want this account to login, I only want it to establish SSH connection.

    $ sudo useradd -r -m -a -G ssh sshproxy
    $ sudo passwd -l sshproxy

> **Note** that I use a special group called ''ssh'' for all users that need ssh access.

Add your public ssh key to the account, ''~sshproxy/.ssh/authorized_keys''. Prepend the following to the key to further restrict access.

    no-agent-forwarding,no-X11-forwarding,command="/bin/bash -c 'read a; exit'"

# Step 2: Configure your SSH client

This is from where you connect to the newly configured sshproxy account. It might be a client system you need a proxy for, or it might be a server behind a firewall that you need to access through a reverse proxy.

Only thing that matters on the client is how to persistently keep this connection going.

My tip is to use -C and -T arguments. This makes a Dynamic Socks5 proxy listen on localhost:8081.

    $ ssh -CTx -Dlocalhost:8081 -l sshproxy your.ssh.server

## Connecting through tor

Assuming your tor proxy is running on localhost:9050. This example starts a reverse proxy on the ssh server's localhost:65432 port that connects back to the client machine on port 22.

    $ ssh -o "ProxyCommand=ncat --proxy 127.0.0.1:9050 --proxy-type socks5 %h %p" -Rlocalhost:65432:localhost:22 your.ssh.server
