# Misslyckade login-försök i CentOS 7

Kan se detta meddelandet ibland.

    There was 1 failed login attempt since the last successful login.

Visa alla försök med aureport. 

```
$ sudo aureport -au

Authentication Report
###### 

# date time acct host term exe success event
###### 

1. 2016-10-03 09:37:55 administrator 192.168.xx.xx ssh /usr/sbin/sshd no 138

```

Visa misslyckade försök så här. 

    $ sudo aureport -au --failed
