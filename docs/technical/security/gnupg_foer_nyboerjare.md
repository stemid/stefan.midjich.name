# GnuPG för nybörjare

[New quickstart guide in English](../gnupg_quickstart).

En kort genomgång om två användningsområden för [GnuPG](http://www.gnupg.org/). 

Först och främst så är GnuPG ett program som krypterar och signerar information. Det betyder kortfattat att du kan kryptera information så att bara vissa personer kan läsa den, och du kan signera information så att den som läser vet vem informationen kommer ifrån. 

Här beskrivs en hypotetisk situation där två personer kommunicerar via e-post, jag kallar den ena personen Stefan och den andra för Vladimir. 

Båda parterna har gått igenom följande instruktioner som visar hur man skapar en personlig GPG-nyckel. 

## Skapa personlig GPG nyckel

Installera först GnuPG2. På Macintosh använder jag [Homebrew](http://brew.sh/) och skriver ''brew install gnupg2'', och på Debian skriver jag ''sudo apt-get install gnupg2''. 

Skapa nu en personlig nyckel. 

    $ gpg2 --gen-key

Du kan ge standardsvar på alla frågor fram tills den frågar om ditt namn. Fyll då i ett passande namn, en e-postadress och möjligtvis en kommentar. Tänk på att en sådan här nyckel oftast är personlig. 

Det kan se ut så här när du svarat på alla frågor och ska ange ditt personliga lösenord. 

    Välj vilken typ av nyckel du vill ha:
     (1) RSA and RSA (default)
     (2) DSA and Elgamal
     (3) DSA (endast signering)
     (4) RSA (endast signering)
    Vad väljer du?
    RSA-nycklar kan vara mellan 1024 och 4096 bitar långa.
    Vilken nyckelstorlek vill du ha? (2048)
    Den efterfrågade nyckelstorleken är 2048 bitar
    Specificera hur länge nyckeln skall vara giltig.
           0 = nyckeln blir aldrig ogiltig
        `<n>`  = nyckeln blir ogiltig efter n dagar
        `<n>`w = nyckeln blir ogiltig efter n veckor
        `<n>`m = nyckeln blir ogiltig efter n månader
        `<n>`y = nyckeln blir ogiltig efter n år
    För hur lång tid ska nyckeln vara giltig? (0)
    Nyckeln går aldrig ut
    Stämmer detta? (j/N) j
    
    GnuPG behöver konstruera en användaridentitet för att identifiera din nyckel.
    
    Namn: Vladimir Petrov
    E-postadress: vlad@gmail.com
    Kommentar:
    Du valde följande användaridentitet:
      "Vladimir Petrov `<vlad@gmail.com>`"

Efter det stadiet anger du ditt lösenord och nyckeln skapas, då kan du behöva använda datorn mycket för att det ska gå snabbare att generera slumptal. 

Nu har vi en nyckel, som vi kan se med kommandot ''gpg2 --list-keys''. 

### Skapa ett spärrcertifikat

En bra idé är att skapa ett spärrcertifikat som kan återkalla din nyckel omedelbart och lagra det någonstans säkert så att du kan återkalla nyckeln i nödfall. 

    gpg --armor --output .gnupg/stemid_revocation.asc --gen-revoke `<nyckel id>`

## Signering

### Utbyte av nycklar

Ett kritiskt steg är att skicka sin publika nyckel till en annan person. Man kan ha ett nyckelparty t.ex. där folk träffas och utbyter nycklar. Det går även att ha nyckeln på en webbsida eller i en [..:..:nyckelserver](../../nyckelserver). 

Hur man än väljer att göra det så är steget kritiskt för två anledningar. 


*  Man måste se till att man får rätt nyckel

*  Man ska helst verifiera personens identitet samtidigt

Kan båda stegen uppfyllas så går det att signera en annan persons nyckel med sin egen. Då litar man fullständigt på allt som signeras av den nyckeln. 

Stefan har kört följande kommando för att exportera sin publika nyckel. 

    $ gpg2 --export --armor 'Stefan'

Det exporterar nyckeln i ett format som lätt kan skickas via e-post till andra. Namnet på slutet ska finnas i listan av ''gpg2 --list-keys''. 

### Stefan ska skicka ett signerat meddelande till Vlad

Nu vill Stefan signera ett meddelande med sin nyckel så att Vlad kan vara säker på att meddelandet är från Stefan. 

Så han skriver ett meddelande i gpg2, och signerar textfilen med en ascii-signatur som kan skickas i e-post. 

    $ gpg2 --clearsign
    ...
    Hej Vlad
    
    Jag mår fint, hur mår du?
    
    / Stefan (utan tvekan)

>Här kan även en fil användas så här ''gpg2 --clearsign meddelande.txt''. 

När Stefan trycker Ctrl+d så avslutas meddelandet och det skrivs ut på skärmen med sin signatur. Det ser ut så här. 

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Hej Vlad

Jag mår fint, hur mår du?

/ Stefan (utan tvekan)
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.20 (Darwin)

iQEcBAEBAgAGBQJSDDVfAAoJECU/FhaummWZEmYIAJRme8aNCze+UGAzaCbh+nxb
1lFSFiwUH7m8PhngJ02js6bs96QbrvuIJlN1kWKXQHGWpX3aWvQv+OLdgZza2Eez
GLY8Xb4u3bipV4Ywu0oDnLaCPMHM2zEa132RrDvYn00iMgp+xRaT8eXtRwKGl29n
sCNIK0WSyDwYZRFZl5WcEJGM80x3XQsgdUuil2QmOKeXta+KXnTh2mIn1QQ/jy3K
vYcqdd1/ufXyqyHcXrOixHvJJFPj6n5rN6RdBVhMByXqmFep/AkrBBd0o4soygQe
HYtCY9zIf9MO5CI2nc9L9UsweFTo6NHyGdHZZG3qmWhbTYfomlmCRvVmyz3S078=
###### dnQ1

-----END PGP SIGNATURE-----
```

Det är bara innehållet mellan raden ''BEGIN PGP SIGNED MESSAGE'' och raden ''BEGIN PGP SIGNATURE'' som är signerat, om det innehållet ändras så stämmer inte signaturen längre. 

### Vlad tar emot ett signerat meddelande från Stefan

Stefan har tidigare gett Vlad sin exporterade publika nyckel på ett personligt sätt som gör att Vlad är säker på vems nyckel det är. 

När Vlad tog emot Stefans publika nyckel så importerade han den i sin nyckelring på det här viset. 

    $ gpg2 --import
    ...

>Där kan man antingen klistra in nyckeln direkt i terminalen och avsluta med en ny rad och Ctrl+d. Eller så kan man köra ''gpg2 --import stefans_publika_nyckel.txt'' mot en textfil som innehåller nyckeln. 

Nu har Vlad tagit emot ett signerat meddelande från Stefan och vill med hjälp av signaturen, och Stefans publika nyckel, bekräfta att meddelandet är äkta. 

    $ gpg2 --verify meddelande_från_stefan.txt
    gpg: Signatur gjordes tor 15 aug 2013 03:56:47 CEST med RSA nyckel-id AE9A6599
    gpg: Korrekt signatur från "Stefan...
    gpg: VARNING: Denna nyckel är inte certifierad med en pålitlig signatur!
    gpg:          Det finns inget som indikerar att signaturen tillhör ägaren.

Lägg märke till varningen på de sista två raderna. Den visas för att Vlad ännu inte signerat Stefans nyckel. 

När Vlad är säker på att han tagit emot en autentisk nyckel från Stefan kan han signera den med sin egen nyckel för att slippa varningen. 

Först måste han visa fingeravtrycket för Stefans nyckel, med följande kommando. 

    $ gpg2 --fingerprint 'Stefan'
    ...
    Nyckelns fingeravtryck = AAAA XXXX ....

Då kan Vlad ringa Stefan och bekräfta fingeravtrycket över telefon genom att läsa upp det. Stefan kommer se samma fingeravtryck på sin dator genom att köra samma kommando. 

När fingeravtrycket har bekräftats kan Vlad signera nyckeln med gpg2. 

    $ gpg2 --lsign-key 'Stefan'

Vlad blir frågad om han verkligen vill signera Stefans nyckel med sin egen, sedan måste han skriva in sin nyckels lösenord. 

>Skillnaden mallen lsign och sign är att en lsignerad nyckel inte kan exporteras. 

När Vlad nu igen kör samma verify kommando som tidigare så får han ingen varning. 

## Kryptering

Nu har följande saker inträffat. 


*  Stefan och Vlad har installerat GPG2 och skapat egna personliga nycklar. 
*  Stefan har exporterat sin nyckel och skickat den till Vlad
*  Vlad har importerat Stefans nyckel. 
*  Vlad har bekräftat äktheten av Stefans nyckel genom att signera den med sin egen. 
*  Vlad har läst och bekräftat äktheten av ett meddelande skickat och signerat av Stefan. 

Härnäst ska vi pröva att kryptera data och skicka den för att avkrypteras av mottagaren. 

### Vlad ska skicka ett krypterat meddelande till Stefan

Vlad vill både kryptera och signera en fil med sin privata nyckel, så att ingen kan läsa filen och så att Stefan kan bekräfta att filen är från Vlad när han tar emot den. 

Innan det kan hända måste så klart Vlad exportera sin nyckel och Stefan importera den, så de upprepar stegen som har beskrivits ovan. 

Vlad exporterar sin nyckel och skickar den till Stefan. 

    $ gpg2 --export --armor 'Vladimir Petrov'

Stefan tar emot nyckeln och importerar den. 

    $ gpg2 --import vladimirs_nyckel.txt

Stefan kontrollerar fingeravtrycket på nyckeln. 

    $ gpg2 --fingerprint 'Vladimir Petrov'

Stefan ringer Vladimir och bekräftar att han har rätt fingeravtryck, sedan signerar han Vlads nyckel med sin egen. 

    $ gpg2 --sign-key 'Vladimir Petrov'

Notera att Stefan inte använder ''--lsign-key'' eftersom Vlad har gett honom tillåtelse att exportera hans nyckel och skicka den vidare bland sina vänner. 

Nu kan Vlad äntligen kryptera sitt viktiga meddelande. 

    $ gpg2 --sign --output meddelande.gpg --encrypt --recipient 'Stefan'
    Du behöver en lösenfras för att låsa upp den hemliga
    nyckeln för användaren: "Vladimir Petrov `<vlad@email.ru>`"
    ...
    
    Hej Stefan
    
    Detta bör du inte avslöja för någon annan!
    
    Vlad är inte mitt riktiga namn.
    
    / Vladimir (med absolut säkerhet)

Notera att kommandot kräver Vladimirs personliga lösenord för signeringen. Vill han bara kryptera så kan han hoppa över argumentet ''--sign'' och inget lösenord kommer krävas. 

Den krypterade och signerade filen som skapas ''meddelande.gpg'' skickas till Stefan. 

#### Skicka en krypterad fil

Kan också kryptera en fil direkt så här.

    $ gpg2 --output krypterad_fil.txt.gpg --encrypt -r 'Stefan Midjich' icke_krypterad_fil.txt

* Du anger en mottagare med ``-r`` även om den bara krypteras åt dig själv.
* Du kan även signera filen samtidigt med ``--sign``.

Avkryptera den så här.

    $ gpg2 --decrypt krypterad_fil.txt.gpg

### Stefan tar emot en krypterad fil från Vlad

Stefan väcktes mitt i natten av att hans fönsterruta krossades och en tegelsten flög igenom. Yrvaken och nedkyld av den kalla luften utifrån stapplar han över krossat glas till brukstenen som ligger på sovrumsgolvet och noterar att ett SD-minneskort är fasttejpat till den. 

På tejpen står det med tuschpenna "Från Vlad". 

På minneskortet verkar ligga en mystisk fil som bara heter ''meddelande.gpg''. 

FIXME mer om avkryptering.

## Se också


*  [Lösenordshantering med pass(1)](./lösenordshantering i unix med pass)

*  [The GNU Privacy Handbook](http://www.gnupg.org/gph/en/manual/book1.html)

*  [OpenPGP Best Practices av Riseup.net](https://help.riseup.net/en/security/message-security/openpgp/best-practices)

*  [How to transition to a larger key](https://www.apache.org/dev/key-transition.html)
