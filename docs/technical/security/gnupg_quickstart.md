# GnuPG Quickstart

In my opinion this is an evolved version of my old Swedish post [GnuPG för nybörjare](../gnupg_foer_nyboerjare).

## Generate your personal key

    $ gpg2 --gen-key

### Maybe generate a revocation cert

These days GnuPG2 does this automatically on Fedora and places the revocation cert in ``~/.gnupg/openpgp-revocs.d/<key id>.rev``, but in case yours didn't you can generate one like this.

    $ gpg2 --armor --output .gnupg/file.rev --gen-revoke <key id>

### Set your default private key if you have more than one

Edit ``~/.gnupg/gpg.conf`` and set ``default-key`` to your key ID. You can list the key IDs like this.

    $ gpg2 --list-secret-keys --keyid-format long
    -------------------------------
    sec   rsa2048/XXXXKEYIDXXXX 2013-08-15 [SC]

The first line of each key will be the ID.

If you don't do this the first key in your key ring is automatically used to sign and encrypt things. You can also specify a key ID to use at each command with ``--default-key <key id>``.

!!! note "Key ID can also be parts of the human readable name"
    Only the first part is enough, or the e-mail address.

## Public key exchange

### Export your public key in a readable format and publish it for people to use.

    $ gpg2 --export --armor <key id>

### Display your key fingerprint which should be verified with your contacts before they sign your key.

    $ gpg2 --list-public-keys --with-fingerprint 'Stefan Midjich'
    pub   rsa2048 2013-08-15 [SC]
          95D2 6D99 54BA 33C4 CA33  0219 253F 1616 AE9A 6599

The second line of each key is the fingerprint.

### Someone can import your public key to their key ring like this.

    $ gpg2 --show-keys --with-fingerprint StefanMidjich.pubkey
    pub   rsa2048 2013-08-15 [SC]
          95D2 6D99 54BA 33C4 CA33  0219 253F 1616 AE9A 6599
    $ gpg2 --import StefanMidjich.pubkey
    $ gpg2 --list-keys

### Trust someones public key by signing it with your own

    $ gpg2 --sign-key 'Stefan Midjich'

## How someone encrypts a message for you

    $ gpg2 --output encrypted.gpg --encrypt -r 'Stefan Midjich' cleartext.file

### Encrypt without first trusting the recipients key

This is not recommended but you can still encrypt a message for someone in your keyring without trusting their key.

    $ gpg2 --output encrypted.gpg --encrypt -r 'Stefan Midjich' --trust-model always cleartext.file

## How you decrypt their message

    $ gpg2 --decrypt encrypted.gpg

## Signing a message from stdin

!!! note "Signing requires a private key"

    $ gpg2 --sign --output signed_message.txt --recipient 'Stefan Midjich'
    my message
    Ctrl+d to end
