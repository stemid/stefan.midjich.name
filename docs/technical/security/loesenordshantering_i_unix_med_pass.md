# Lösenordshantering i Unix med pass(1)

[Pass](http://zx2c4.com/projects/password-store/) är en lösenordshanterare för Unix-lika system, fungerar utmärkt på Linux och jag använder den personligen på Mac OS. 

Den använder sig av [GPG](http://www.gnupg.org/) för kryptering. Den finns i Debian och Ubuntu under paketnamnet ''pass'' och i Mac OS installerade jag den genom [Homebrew](http://brew.sh/). 

## Konfiguration av GPG2

Generera först en personlig nyckel i GPG2, jag svarade standardsvar på alla frågor och fyllde i mitt förnamn, efternamn och min e-postadress när jag skulle identifiera nyckeln. 

Se till att ange ett bra och långsiktigt lösenord. 

    $ gpg2 --gen-key
    ...
    $ gpg2 --list-secret-keys

FIXME Skapa en subnyckel för pass istället för att använda huvudnyckeln. 
## Konfiguration av pass

När nyckeln är skapad kan pass initieras genom att ange namnet av nyckeln som precis skapades. 

    $ pass init 'Stefan Midjich'

I mitt fall angav jag mitt namn, det räcker för att den ska hitta rätt nyckel. 

Att skapa ett nytt lösenord är snabbt och enkelt, lägg märke till att tredje argumentet är namnet på lösenordet men även en sorts hierarki var lösenordet ska lagras under ''~/.password-store/''. 

    $ pass insert arbetsformedlingen.se/stemid
    /Users/stemid/.password-store/arbetsformedlingen.se
    Enter password for arbetsformedlingen.se/stemid:
    Retype password for arbetsformedlingen.se/stemid:

Det finns nu en katalog med en användare skapad i den. 

    $ find .password-store
    .password-store
    .password-store/.gpg-id
    .password-store/arbetsformedlingen.se
    .password-store/arbetsformedlingen.se/stemid.gpg

Det går att göra mycket mer, som att generera slumpmässiga lösenord med eller utan specialtecken. 

## Visa lösenord

Till sist kan man visa lösenord genom att bara använda ''pass sökväg/användare''. 

    $ pass arbetsformedlingen.se/stemid

Då kommer gpg2 be dig om ditt lösenord för din nyckel innan lösenordet skrivs ut på skärmen i klartext. 
