# Förnya certifikat med certutil

NSS använder sig av certutil och om man vill dumpa OpenSSL för NSS så behöver man lära sig det. 

Först lista alla befintliga certifikat. 

```
sudo certutil -L -d sql:/etc/pki/nssdb

Certificate Nickname                                         Trust Attributes
                                                             SSL,S/MIME,JAR/XPI
testca                                                       CTu,u,u
www                                                          u,u,u

```

Bekräfta namnet på rootcertet och certet du vill förnya. 

I vänstra kolumnen är ett vanligt namn för certet. Se ''-t'' argumentet i manualen för en förklaring av bokstäverna i högra kolumnen. 

I detta fallet heter vår CA *testca* och vårt servercert heter *www*. 

## Skapa en CSR med den gamla privata nyckeln

    $ certutil -d . -R -k "NSS Certificate DB:testca" -s "CN=rootca,o=Example.com,c=US" -a -o rootca.req
