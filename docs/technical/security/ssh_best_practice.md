# SSH Best practice

Writing down some of my personal tricks for using OpenSSH. These are already well known but it helps to write them down for myself.

## Enter ssh key password for ssh-agent at login

Or rather, when your shell first runs. That's when it will request your key password and add the keys into ssh-agent so you can use them without entering your password over and over.

1. Create a password protected key.

    ``$ ssh-keygen -t ed25519 -f $HOME/.ssh/id_ed25519``

2. Edit ``$HOME/.bashrc``.

        if [ -z "$SSH_AUTH_SOCK" ]; then
          eval `ssh-agent -s`
          ssh-add
        fi

3. Next time your shell starts you'll be asked for the password to these keys.

4. Add more keys to this method.

    Modify the shell code in step 2 slightly, replace the ``ssh-add`` line with one where you specify which keys to add.

        $ ssh-add ~/.ssh/id_2_ed25519

    This could also be a for loop if you're creative enough.

### ssh-add tutorial

1. List keys added to ssh-agent.

        $ ssh-add -l

2. Remove a key by filename.

        $ ssh-add -d ~/.ssh/id_2_ed25519
