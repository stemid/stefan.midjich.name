# Självsignerat certifikat med certutil

NSS använder sig av certutil och om man vill dumpa OpenSSL för NSS så behöver man lära sig det. 

>**Varning** Jag hade problem med denna metoden och httpd 2.2.15 på RHEL6 senast jag försökte, hade inte tid att felsöka vidare utan fick gå tillbaka till mod_ssl. 
> / stemid
# Skapa en CA med certutil

Skapa en NSS databas-katalog om du inte redan har en, alternativt se till att den är tom. 

    $ sudo certutil -S -s "CN=Serverdrift" -n ca.localhost -x -t "CT,C,C" -v 120 -d sql:/etc/pki/nssdb

Slå upp alla argumenten i manualen, som t.ex. ''-v'' anger att certet ska vara giltigt i 120 månader. ''-n'' är smeknamnet på certet. 

Nu skapar vi en CSR och en nyckel. 

    $ sudo certutil -R -s "CN=ca.localhost, O=Serverdrift , L=Malmoe, ST=Scania, C=SE" -o servernamn.csr -d sql:/etc/pki/nssdb

Signera nyckeln. 

    $ sudo certutil -C -i servernamn.csr -o servernamn.crt -c ca.localhost -d sql:/etc/pki/nssdb -6 serverAuth

Tryck ctrl+d för att avsluta prompten som kommer från ''-6'' argumentet. 

# Skapa självsignerat certifikat

Slutligen kan vi skapa självsignerade certifikat också. 

    $ sudo certutil -S -n "servernamn" -s "cn=servernamn" -c "ca.localhost" -t "C,C,C" -v 120 -d sql:/etc/pki/nssdb
