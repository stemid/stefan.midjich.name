# Best practices for setting up Linux

These mostly apply to CentOS/RHEL and Debian/Ubuntu which are my two most used types of Linux.

## Network

Always use vmxnet3 in vcenter because these distros all have a built in driver for it.

## Disk

### Separate system disk from the rest 

For the 1st disk, system disk, it's generally best to follow the installer defaults.

Following the default in the installer /boot would end up on a primary partition of its own, bootable, and LVM would take the rest of the disk and use it for /.

### Use LVM on the whole disk, no partition 

This is relevant for 2nd disk, and other disks that might be added after the 1st disk.

So to be clear, don't create a partition on the 2nd disk, just use pvcreate /dev/sdb and LVM will use the whole disk.

This might complicate things when you want to put /var on there for example. I sometimes have to do this task after install and rsync over all the files manually to the new volume. A co-worker claimed he figured out how to do it in the CentOS installer though.

But that's just once during install, after that it makes expanding the disk much simpler and safer.

### Never put an LVM volume group on two different disks 

This is basically a cheap and unreliable software raid. And if you're doing it in a hypervisor environment you never know where that 2nd disk will go after a storage migration so you might end up with an unusable volume group.

### Use a separate disk for your app data

Think about the main function of your server and where you think all the data will be stored. This volume should be on its own disk for easier expansion in the future, following the LVM rules outlined above.

Here are some examples;


*  Mailserver - Put /var on its own disk because you'll need both logs and mails.

*  Webserver - Also /var.

*  Proprietary appplications from Symantec, IBM for example - Put /opt on a separate disk because most proprietary applications use /opt. But research this first so you're sure. (also I tend to link /var/opt to /opt to avoid having two opt dirs)

*  Database - /var/database where you can then create sub-dirs for mysql or postgresql. Never set the volume as your datadir, always use a sub-dir.

### Optimize your database disk

If mounting a disk that should be as fast as possible you can add the noatime option to fstab.

    /dev/mapper/vg01-lv_var_database /var/database ext4 noatime 0 2

But only do this if you have a specific mount like /var/database, not on the entire /var disk because it might have unwanted consequences.

## Users

### Disable root user

A small but cheap act of security is to disable the root user by not setting a password for it or by using the ''passwd -l'' command.

In most cases I just avoid setting a root password in the CentOS setup, and instead create an administrative user account with sudo.

But in other cases where the setup does not allow this you could try using ''passwd -l'' to lock the root account.

The root account should never be used to login. There is simply no reason for it and it only leaves you open for root brute force attacks. Always use sudo, and you can of course start a root shell with sudo.

### Personal user accounts

With the above tip implemented you should also strive to use personal user accounts for all your users. In practice this is best done with some sort of central auth like LDAP or AD. Alternatively ansible can be used to manage user accounts on many systems.

### System users

When creating a user for a custom service, don't create a normal user. Instead use the -r flag when creating to make a sort of system user.

    $ useradd -r -s /usr/sbin/nologin systemuser-name

Makes it easier to separate actual users from service/system users. Most pre-packaged services already do this.

## Locale

Always pick your locale, in my case Swedish. 

There's almost no downside to this in Linux. Only advantages like being able to use Swedish characters and keyboards, and getting Swedish mirror servers for stuff like package installation and NTP.

The only downside is when asking for help on english speaking community forums. In those cases you can prefix a command with ''LANG=C'' or ''LANGUAGE=C'' to reset all the error messages to their default before you paste it online.

Log files have always been in english in my experience, it's only terminal stdout output that needs to be reset.

## Security

### Bash histcontrol

When you write commands that contain passwords these commands are saved to bash history. To globally avoid saving commands in bash history you can use the following in ''/etc/profile.d/histcontrol.sh''.

    export HISTCONTROL="ignoreboth"

And then when you write a command with a password, go to the beginning of the line and insert a space. This will cause bash to ignore the command in history.

Which also means you can't use the up-arrow to repeat the command so you'll have to copy/paste it to repeat it.
