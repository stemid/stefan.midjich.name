# Mount shares with autofs

!!! Note
    This focuses on later versions of autofs in Fedora and CentOS 8 where they have a /etc/auto.master.d directory.

## Install autofs and dependencies

    $ sudo dnf install autofs cifs-utils nfs-utils

## Configure each share server

I choose to create one file for each server, this example is for the "nas" server.

**Filename**: /etc/auto.master.d/nas.autofs

```
/media/nas      /etc/auto.nas --timeout=1200
```

The first column (/media/nas) is the parent directory where all shares will be mounted, autofs will create this path for you.

The second column (/etc/auto.nas) is additional configuration for your share server, see below.

### /etc/auto.nas

This file defines all the shares to mount from this server.

```
*       -rw,hard,intr   nas:/volume1/&
backups -fstype=cifs,credentials=/etc/nas.backups.creds,uid=backup,gid=backup,file_mode=0660,dir_mode=0770      ://nas/backups
```

The first line is NFS and uses a special syntax that covers multiple shares on one NFS server. Wildcard * is mapped to ampersand & so anything under the path nas:/volume1/ will be mounted, if it exists.

The second line is a CIFS example that only mounts one specific share, with username and password stored in a separate file with 0600 file permissions. It also mounts using the backup user so if your users wants access to this share, add yourself to the backup group. This user and group must be created on most systems.

    $ sudo useradd -r -d /nonexisting -s /sbin/nologin backup
    $ sudo usermod -a -G backup your-own-username

!!! Remember
    You should logout and kill all your bash instances before your new group will be usable.

### CIFS credentials in /etc/nas.backups.creds

```
username=user
password=secret.
```

## Restart autofs

    $ sudo systemctl restart autofs

## Shares are mounted under /media

Now shares will be mounted on-demand when you attempt to access the share path.

    $ ls -lath /media/nas/Downloads


