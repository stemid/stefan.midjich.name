# Edit Clonezilla ISO

This is how I edit a Clonezilla ISO file to insert my own [backup and restore script](https://gitlab.com/-/snippets/2084918) for a quicker process.

## Mount the ISO file as a disk

    sudo mount -t iso9660 -o loop clonezilla-live-2.7.1-22-amd64.iso /mnt

### Now copy all its contents to a temporary directory.

    mkdir /tmp/clonezilla_iso
    cd /mnt
    tar -cf - . | tar -xvf - -C /tmp/clonezilla_iso

## Unpack the squashfs filesystem

    cd /tmp
    sudo unsquashfs clonezilla_iso/live/filesystem.squashfs

## Chroot into squashfs and make your updates

    sudo chroot squashfs-root /bin/bash
    # exit

## Re-pack squashfs filesystem

    sudo mksquashfs squashfs-root/ clonezilla_iso/live/filesystem.squashfs -noappend -always-use-fragments

## Create new ISO

``-isohybrid-mbr /usr/share/syslinux/isohdpfx.bin`` might be distro specific but check your package manager for the filename.

```
cd /tmp/clonezilla_iso
sudo xorriso -as mkisofs -R -r -J -joliet-long -l -cache-inodes \
    -iso-level 3 -isohybrid-mbr /usr/share/syslinux/isohdpfx.bin \
    -partition_offset 16 -A 'Clonezilla Custom' -b syslinux/isolinux.bin \
    -c syslinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table \
    -eltorito-alt-boot --efi-boot boot/grub/efi.img -isohybrid-gpt-basdat \
    -isohybrid-apm-hfsplus ./ > /tmp/clonezilla-live-custom-amd64.iso
```
