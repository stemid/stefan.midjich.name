# Tmux read-only guest session over SSH

Problem: I want to let co-workers watch me work for educational purposes, but I work best on my own laptop.

Solution: Permit them SSH access to a shared read-only tmux session.

## Step 1: Create guest user login shell script

This executable will be set as the login shell for the guest user.

```
#!/usr/bin/env bash

exec /usr/bin/tmux -S /var/lib/tmux-sessions/guest attach -r
```

The ''-r'' argument attaches a read-only session, but at this time of writing read-only users are still able to resize the window. A patch exists to prevent this but I'm not interested.

## Step 2: Create tmux sessions directory

This is where the shared tmux session will live.

    $ sudo mkdir -p /var/lib/tmux-sessions
    $ sudo chown -R stemid:wheel /var/lib/tmux-sessions

## Step 3: Create guest user account

    $ sudo useradd -d /home/guest -m -s /usr/bin/guest-login guest

## Step 4: Add your co-workers public SSH key to the guest account

    $ sudo mkdir -p /home/guest/.ssh
    $ sudo vim /home/guest/.ssh/authorized_keys
    $ sudo chmod 0700 /home/guest/.ssh
    $ sudo chmod 0600 /home/guest/.ssh/authorized_keys
    $ sudo chown -R guest:guest /home/guest/.ssh


## Step 5: Create a new tmux session

The session file created must have its permissions changed after creation.

    $ tmux -S /var/lib/tmux-sessions/guest new -s guest
    $ chgrp guest /var/lib/tmux-sessions/guest

## Step 6: Create a reverse ssh tunnel on some server you can both access

Let's say linux-web02 is a server we both have access to. So I'll create a listening port on it that leads back to my own laptop like this.

    $ ssh -Rlocalhost:1502:localhost:22 linux-web02

My co-worker will connect to it like this.

    $ ssh -J linux-web02 -p 1502 -l guest localhost

## Step 7: Disable the guest user

Toggle the guest user like this.

    $ sudo usermod -s /usr/bin/nologin guest
    $ sudo usermod -s /usr/bin/guest-login guest

## Step 8: Bonus pointers

 1.  Disable password login in your OpenSSH server
 2.  Use AllowGroup ssh in your sshd config and add your own user and any other ssh guest user to the ssh group
 3.  Then remove the guest user from the group for added security

# See also


*  [Technical guides](../)
