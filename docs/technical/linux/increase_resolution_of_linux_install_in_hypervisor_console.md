# Increase resolution of Linux install in hypervisor console

This works mainly in VMware and virt-manager (qemu) consoles for when you start a CentOS install and the GUI has a terrible resolution.

Append the following to boot parameters. For CentOS/RHEL that would mean pressing tab on the boot options that say ``Install CentOS 7``.

    vga=791

Press enter to boot.

## See also

*  [Linux video mode numbers](http://en.wikipedia.org/wiki/VESA_BIOS_Extensions#Linux_video_mode_numbers)
