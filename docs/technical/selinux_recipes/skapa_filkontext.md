# Skapa filkontext

Du kan ändra kontext på en fil med chcon, men det kommer inte bestå eftersom kontext läses tillbaka vid varje omstart från en central SElinux databas. 

Därför finns verktyget ``semanage``. 

1. Installera semanage.

        $ sudo yum install policycoreutils-python -y

2. Skapa manuell fil-context.

        $ sudo semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/uploads(/.*)?'

    Regexen på slutet betyder att allt under katalogen ``/var/www/uploads`` ska märkas med denna filkontext, som i detta fallet är ``httpd_sys_rw_content_t`` typen. 

3. Lista alla sådana regler. 

        $ sudo semanage fcontext -l

4. Radera din regel. 

        $ sudo semanage fcontext -d -t httpd_sys_rw_content_t '/var/www/uploads(/.*)?'
