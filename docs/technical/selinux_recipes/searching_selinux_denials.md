# Searching SElinux denials

Find out the name of the binary running, might be different from the systemd service name. Here's an example for alertmanager showing todays denials.

    sudo ausearch -m avc -c alertmanager -ts today

# See also


*  [SElinux recept](../)

*  [Official RedHat 6 docs](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security-enhanced_linux/sect-security-enhanced_linux-fixing_problems-searching_for_and_viewing_denials)
