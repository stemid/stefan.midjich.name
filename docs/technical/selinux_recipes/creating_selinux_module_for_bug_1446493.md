# Creating SElinux module for Bug 1446493

I thought this would be a good example of creating a simple semodule that permits file access using macros.

In this case it's a workaround for [RH bug #1446493](https://bugzilla.redhat.com/show_bug.cgi?id=1446493) in Fedora where smartd is not allowed access to NVMe devices. 

Things to consider:


*  What type of access smartd is requesting (according to audit logs it's getattr, read and ioctl).

*  What source and target context (source is fsdaemon_t and target is nvme_device_t).

*  We'll be using pre-defined macros to write the module, which is like using a function that hides the classic selinux logic.

*  The file smartd wants access to is a character/block device file, not a regular file (/dev/nvme0).

You need the ''selinux-policy-devel'' package installed to write and build modules.

# Writing the module

Create a new directory with a unique name to describe your module. In my case I created smartd_bug1446493.

Create the file ''smartd_bug1446493.te'' and read the comments to find out more.

```
# Just defining a name and version
policy_module(smartd_bug1446493,1.0.0)

# Any pre-existing type you use must be "imported" with gen_require

gen_require(` type fsdaemon_t; type device_t; type nvme_device_t; ')

# These macros are special for block/character device files. They

# take three arguments that are 1) domain type (source process context), 2) 
# container (directory) type and 3) file type (target)

getattr_blk_files_pattern(fsdaemon_t, device_t, nvme_device_t)
read_blk_files_pattern(fsdaemon_t, device_t, nvme_device_t)

getattr_chr_files_pattern(fsdaemon_t, device_t, nvme_device_t)
read_chr_files_pattern(fsdaemon_t, device_t, nvme_device_t)

```

See all the file patterns defined [here](https://github.com/qkdreyer/selinux-refpolicy/blob/master/policy/support/file_patterns.spt) or ''/usr/share/selinux/devel/include/support/file_patterns.spt'' in the ''selinux-policy-devel'' package on your system.

Of course, were it a normal file and you couldn't just relabel the file with [semanage fcontext](./create_selinux_file_context), then you could use the getattr_files_pattern macros that are for regular files and don't have the chr/blk prefix.
