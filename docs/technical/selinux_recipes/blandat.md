# Blandade gamla SElinux recept

Gamla recept jag ännu inte sorterat in under egna sidor.


# Tillåt FTP i användares hemkataloger

    sudo setsebool -P ftp_home_dir=1

>**Observera** ''-P'' argumentet tvingar alla ändringar man gjort att skrivas till disk, utan ''-P'' kommer ändringar försvinna vid omstart. 
# Tillåt anslutningar till MySQL socket från FTP-server

Som standard tillåts inte anslutningar till mysql.sock filen av SELinux. 

    sudo setsebool -P ftpd_connect_db=1

Det finns liknande inställningar för Apache och många andra program, sedan finns det även inställningar som ''allow_user_mysql_connect'' som rent allmänt tillåter anslutningar till mysql.sock filen. Ta en titt på ''getsebool -a'' för mer värden. 

# Tillåt webbsidor i /home

Apache kan inte tillåta webbsidor under /home utan att först tillåta det i SElinux. 

    sudo setsebool httpd_enable_homedirs=on

En typisk ledtråd på det visas här från ''/var/log/audit/audit.log''. 

    type=AVC msg=audit(1383747482.625:8019): avc:  denied  { getattr } for  pid=1356 comm="httpd" path="/home/kund" dev=dm-0 ino=24023 scontext=unconfined_u:system_r:httpd_t:s0 tcontext=unconfined_u:object_r:user_home_dir_t:s0 tclass=dir
    type=SYSCALL msg=audit(1383747482.625:8019): arch=c000003e syscall=6 success=no exit=-13 a0=7f3fbbda7470 a1=7fff2b12b7d0 a2=7fff2b12b7d0 a3=1 items=0 ppid=1353 pid=1356 auid=500 uid=48 gid=48 euid=48 suid=48 fsuid=48 egid=48 sgid=48 fsgid=48 tty=(none) ses=1016 comm="httpd" exe="/usr/sbin/httpd" subj=unconfined_u:system_r:httpd_t:s0 key=(null)

Vad jag gjorde för att hitta rätt SElinux attribut var följande. 

    getsebool -a | grep http
    getsebool -a | grep http | grep home

# Tillåt HTTPD att skriva till kataloger

Bästa praxis är att tillåta skrivningar i kataloger utanför DocumentRoot, så det är situationen jag beskriver här. 

En hypotetisk applikation som har DocumentRoot i ''/home/kund/public'' och en cache-katalog i ''/home/kund/cache''. 

För att httpd ska kunna skriva till ''/home/kund/cache'' måste man göra två saker. 

Först se till att användaren är med i apache-gruppen, för då kan användaren själv ändra gruppägare på ''/home/kund/cache'' till apache. 

    chgrp -R apache ~/cache
    find ~/cache -type d -exec chmod 0770 {} \;
    find ~/cache -type f -exec chmod 0660 {} \;

Sedan måste root ställa in säkerhetskontext på katalogen för att httpd ska kunna skriva. 

    sudo chcon -Rt httpd_sys_rw_content_t /home/kund/cache

# Tillåta HTTPD att ansluta till MySQL

    sudo setsebool httpd_can_network_connect_db=on

# Ansluta till MySQL socket med en användare

    sudo setsebool allow_user_mysql_connect=on
    
# Tillåt mod_proxy

    sudo setsebool httpd_can_network_connect = 1

# Audit2Allow

Det är ett underbart verktyg för alla undantagsfall som inte täcks av värden i sebool. 

Installera först paketet ''policycoreutils-python''. 

När man ser att något spärras i audit.log, som i exemplet nedan där modulen ''pam_mkhomedir.so'' (''mkbhomedir_helper'') hindras från att skapa hemkataloger när någon loggar in över SSH, då kan man förvandla den loggen till en tillåtande policy. 

    type=AVC msg=audit(1385049496.654:14118): avc:  denied  { write } for  pid=19463 comm="mkhomedir_helpe" name="SYDIT" dev=dm-0 ino=2228248 scontext=unconfined_u:system_r:sshd_t:s0-s0:c0.c1023 tcontext=unconfined_u:object_r:home_root_t:s0 tclass=dir

Spara den raden från audit.log, eller rader om det är fallet, i en egen fil som vi ska köra audit2allow på. Här har jag sparat raden i ''audit_mkhomedir_helper.log''. 

    audit2allow -m min_modul -l -i audit_mkhomedir_helper.log > mkhomedir_helper.te

Nu har det skapats en sorts policy i textformat som i mitt fall ser ut så här, men den ska inte redigeras manuellt. 

```
module min_modul 1.0;

require {
        type home_root_t;
        type sshd_t;
        class dir write;
}

#============= sshd_t ==============

#!!!! This avc can be allowed using the boolean 'allow_polyinstantiation'

allow sshd_t home_root_t:dir write;
```

Varningen kan man bli av med genom att köra ''sudo setsebool allow_polyinstantiation=on'', vilket också behövs för att kunna tillåta skapandet av hemkataloger. 

Notera första raden där jag anger namnet på *min_modul*, glöm inte att döpa moduler så att de syns i ''semodule -l'' och inte skriver över varandra. 

Sedan skapar vi en binär modulfil av det. 

    checkmodule -M -m -o mkhomedir_helper.mod mkhomedir_helper.te

Och omvandlar den till ett SElinux paket. 

    semodule_package -o mkhomedir_helper.pp -m mkhomedir_helper.mod

Till sist lägger vi till SElinux-paketet i kärnan, det kan ta lite tid. 

    sudo semodule -i mkhomedir_helper.pp

**Observera** att i verkliga situationer är det smidigast att bygga upp SElinux policy vid installation av en maskin och därför använda ''setenforce off'' för att låta audit.log presentera samtliga tillåtanden som krävs för att tjänsten ska fungera. Annars kanske tjänsten stannar innan den hinner köra färdigt och din policymodul kommer inte fungera. 

# sesearch

På RHEL6 är verktyget i paketet setools-console. 

Verktyget söker i policymoduler efter regler som matchar så det kan vara mycket information som kräver förkunskap i selinux för att tolkas. 

Argumenten -s och -t anger källa och mål, som är vanligt i policy moduler att man anger en kontext typ som källa och en annan som mål. Det betyder att källan tillåts åtkomst till målet. 

# Användarroll (staff_r)

Som standard är användaren du loggar in med unconfined, den tillåts ganska mycket friheter. 

    id -Z
    unconfined_u:...

Det går att ändra med usermod men då blir kontot mer begränsat och kan kräva handpåläggning. 

    sudo usermod -Z staff_r minanvändare

# SSH-tunnel

När du är i staff_r rollen får du inte skapa ssh-tunnlar som lyssnar på andra portar. Du kan få följande avc. 

    type=AVC msg=audit(1441367230.457:827): avc:  denied  { name_bind } for  pid=8705 comm="ssh" src=1488 scontext=staff_u:staff_r:ssh_t:s0-s0:c0.c1023 tcontext=system_u:obje
ct_r:ssh_port_t:s0 tclass=tcp_socket permissive=0

Då räcker det inte att tillåta den icke-standard porten för ssh_port_t så här. 

    sudo semanage port -a -t ssh_port_t -p tcp 1488

Det finns även en boolean som är relevant och den kan ses om man kör sesearch. 

    sudo sesearch -AC -s ssh_t -c tcp_socket -t ssh_port_t|grep name_bind
    ET allow ssh_t ssh_port_t : tcp_socket name_bind ; [ selinuxuser_tcp_server ]

Det som kommer tillåta ssh-klienten att lyssna på portar är med andra ord. 

    sudo setsebool -P selinuxuser_tcp_server=1
# Klassificera om en port

De flesta portar har olika selinux typer men försöker du använda en tidigare okänd och oklassificerad port så kanske det stoppas av selinux. Då kan du klassificera porten med en selinux typ som din process har åtkomst till. 

T.ex. om man vill att rsyslog ska kunna ansluta till port 5544. 

    sudo semanage port -a -t syslogd_port_t -p tcp 5544

Då ger man porten TCP/5544 typen syslogd_port_t, som rsyslog tillåts att använda. 

Hur tar man reda på vilka porttyper en process får använda? Rsyslog kör under kontext syslogd_t, så här kan man visa alla TCP-portar som tillåts. 

    sudo sesearch -AC -s syslogd_t -c tcp_socket

# Använda en redan klassificerad port

Vill man att en annan tjänst ska använda port 8443 t.ex. måste man ge den porten två typer. Den har redan http_port_t vilket man kan se här. 

    sudo semanage port -l | grep 8443

Så vi ger den en till typ så här. 

    sudo semange port -m -t syslogd_t -p tcp 8443

Nu kan även syslog använda den porten. 


