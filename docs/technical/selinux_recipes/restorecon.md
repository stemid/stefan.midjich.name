# Restorecon

Varje fil har ett säkerhetskontext (security context) i SElinux, dessa kontext överförs inte av rsync t.ex. så därför kan det vara bra att känna till ''restorecon -Rv /mnt'' som återställer säkerhetskontext på alla filer under en viss katalog. 
