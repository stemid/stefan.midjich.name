# Get permission denied with SElinux enabled, but no AVC denials

Have a program that works with setenforce 0 but gives some type of permission denied error with setenforce 1 and you see no avc denials?

In that case try doing ''sudo semodule -D -B''.

This will rebuild the selinux policy modules and ignore "don't audit". Which in practicality means Redhat ignores certain read/write avc denials assuming they're caused by leaking file descriptors.

Now do ausearch again and check for fresh denials.

    $ sudo ausearch -m avc -ts today

To revert re-build policy or you might get a lot more AVC denials than you want.

    $ sudo semodule -B
