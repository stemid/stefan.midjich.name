# Create SElinux file context

Situation where a process needs access to something but the default context of the files won't allow it and you are able to change the file context without negative effects. 

One example is wanting Postfix to deliver your local mail to Evolution. Evolution stores your local mail under ''~/.local/share/evolution/mail/local'' in a Maildir format. But that whole directory (~/.local/share) has the context ''data_home_t'' which postfix is not allowed to access by default. 

You have two options, create a policy module or relabel the files. 

Creating a policy module would give postfix access to all data_home_t files, which is too broad. 

# Pre-requisite

Install ''policycoreutils-python-utils'' (might be called policycoreutils-python on older distros).

    $ sudo dnf install policycoreutils-python-utils

# Re-label the files

So you re-label the files that postfix needs to access like this. 

    $ sudo semanage fcontext -a -t mail_spool_t '/home/your-users-homedir/.local/share/evolution/mail/local(/.*)?'

And then ensure the files are re-labeled with restorecon.

    $ restorecon -Rv ~/.local/share/evolution

This is more permanent than simply using chcon to change a files label, because semanage manages the central "database" of labels for SElinux.

# Which label do I use?

Different in every situation but ''semanage fcontext -l'' and grep are your friends here. Also of course look at other files that the process already accesses with ''ls -lZ''. 

    $ ls -lZ /var/spool/mail
    ...
 1. rw-rw----. 1 stemid mail unconfined_u:object_r:mail_spool_t:s0 0  2 aug 14.31 stemid
