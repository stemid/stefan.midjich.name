# Stänga av SELinux

Kort svar: Gör det inte, lär dig hellre hur det fungerar och använd SElinux för att skydda ditt system.

Långt svar: Ibland måste man felsöka eller köra mjukvaror som kräver hela policy moduler och behöver därför "stänga av" SElinux.

Det är **absolut inte rekommenderat att sätta SElinux i *disabled***. Istället ska man använda *Permissive* för felsökning.

!!! note Förklaring
    Smått förenklat är skillnaden att i Permissive loggas allt som normalt sett hade stoppats till ``/var/log/audit/audit.log``. I Disabled stänger du av hela SElinux (loggar, filkontext) så att en återhämtning från Disabled till Permissive kan orsaka problem.

!!! warning
    Har du otur kan ett hopp från Disabled till Enforcing rent av få ditt RHEL system att sluta boota. Om du inte tar [vissa åtgärder](#ga-fran-disabled-till-enforcing) innan omstart.

## Exempel

För använda Permissive temporärt i felsökningssyfte behövs bara ett kommando som root. 

    sudo setenforce 0

Och för att starta det igen. 

    sudo setenforce 1

Då är selinux i *permissive* och kommer släppa igenom allt samtidigt som det loggas. 

Permanent behöver man redigera filen ``/etc/selinux/config`` och ändra följande inställning från enforcing till permissive. 

    SELINUX=permissive

Starta sedan om systemet. **Det är absolut inte rekommenderat att sätta SELINUX i *disabled*.**

## Gå från Disabled till Enforcing

Ändra först ``/etc/selinux/config`` filen så ``SELINUX=enforcing``.

Skapa sedan filen ``/.autorelabel`` innan omstart, den kommer utlösa en märkning av SElinux context på alla filer vid uppstart.

    $ sudo touch /.autorelabel
