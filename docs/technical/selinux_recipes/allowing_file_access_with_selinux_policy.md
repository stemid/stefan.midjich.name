# Allowing file access with SElinux policy module

This is a relatively rare case where the following conditions apply.

 1.  Cannot resolve using semanage fcontext and restorecon.
 2.  One process context is trying to access a file context it does not normally have access to.

I've chosen to illustrate this with an example where rsyslog wants to read files owned by a program called bcreporter. 

The bcreporter service uses selinux file context ''public_content_rw_t'' on its files, which rsyslog is normally not allowed to read.

The bcreporter files are under ''/var/archive/bluecoat'', anything below including the parent dir is labeled ''public_content_rw_t''. 

## Example of AVC denials

This is just an example of what it might look like when rsyslog is trying to read a directory it's not allowed to read. 

In this case searching is denied for source context **rsyslogd_t** to target context **public_content_rw_t**.

```

type=AVC msg=audit(1507647724.855:750599): avc:  denied  { search } for  pid=2987 comm="in:imfile" name="bluecoat" dev=dm-3 ino=52690960 scontext=unconfined_u:system_r:syslogd_t:s0 tcontext=unconfined_u:object_r:public_content_rw_t:s0 tclass=dir
...
type=AVC msg=audit(1507647724.855:750600): avc:  denied  { search } for  pid=2987 comm="in:imfile" name="bluecoat" dev=dm-3 ino=52690960 scontext=unconfined_u:system_r:syslogd_t:s0 tcontext=unconfined_u:object_r:public_content_rw_t:s0 tclass=dir

```
# Policy module

## Install requirements

    $ sudo yum install selinux-policy-devel

## Work environment

Create a new directory for your module, name it something unique like ''selinux-rsyslog-bluecoat''.

    $ mkdir selinux-rsyslog-bluecoat
    $ cd selinux-rsyslog-bluecoat

## The module

Create the file ''rsyslog_bluecoat.te'' with the following text.

```

# Just defining a name and version

policy_module(rsyslog_bluecoat,1.0.0)

# Any pre-existing type you use must be "imported" with gen_require

gen_require(`
    type syslogd_t;
    type public_content_rw_t;
')

search_dirs_pattern(syslogd_t, public_content_rw_t, public_content_rw_t)
read_files_pattern(syslogd_t, public_content_rw_t, public_content_rw_t)

# For a case where the parent dir has var_t context you need to change the 2nd argument

#search_dirs_pattern(syslogd_t, var_t, public_content_rw_t)
# and don't forget to "import" var_t type with gen_require


```

## Compile module

    $ make -f /usr/share/selinux/devel/Makefile rsyslog_bluecoat.pp

Hopefully you'll have a module now, otherwise read the errors and try to figure them out.

## Import module

    $ sudo semodule -i rsyslog_bluecoat.pp
