# Börjar med mod_nss

!!! note Ej skriven av Stefan Midjich
    Den krassliga svenskan är faktiskt skriven av en vän till mig som invandrade från USA. Detta skrev han ihop åt mig kanske 3 år efter han anlänt i Sverige. Så ändå ganska bra jobbat.

Målet: Byta ut mod_ssl mot mod_nss
Varför? Efter heartbleed har det visat sig att openssl kanske är inte det mest pålitligt TLS implementation, och dessutom har RHEL börjat gå över till NSS.

Transitionen till mod_nss är dessutom ganska simpelt.

## Installation

Guiden utgår ifrån att du köra redhat/centos.


*  ''yum install nss-util nss-tools mod_nss'' på RHEL/CentOS
*  ''apt-get install libnss3-tools'' på Debian/Ubuntu

Några nya verktyg finns:


*  ''certutil''
*  ''pk12util''

Det är de två som kommer användas.

I httpd katalogen finns det


*  ''/etc/httpd/conf.d/nss.conf''
    * Här i ingår konfigurationen för NSS
*  ''/etc/httpd/alias/''
    * En katalog som innehåller alla NSS cert/nyckel databaser

## Konfiguration

Eftersom målet är bara att byta ut mod_ssl mot mod_nss så beskrivs det endast det som behövs för detta, och inte ytterligare nss koncept.

 1.  Skapa en includes katalog i httpd
    - ''mkdir /etc/httpd/includes''
 2.  Skapa en fil i katalogen som kommer innehåller ett mal till alla virtualhosts som kommer köra mod_nss
    - ''vim /etc/httpd/includes/nssbase.conf''
    - I den filen:

    NSSProtocol SSLv3,TLSv1.0,TLSv1.1
    NSSCipherSuite +rsa_rc4_128_md5,+rsa_rc4_128_sha,+rsa_3des_sha,-rsa_des_sha,-rsa_rc4_40_md5,-rsa_rc2_40_md5,-rsa_null_md5,-rsa_null_sha,+fips_3des_sha,-fips_des_sha,-fortezza,-fortezza_rc4_128_sha,-fortezza_null,-rsa_des_56_sha,-rsa_rc4_56_sha,+rsa_aes_128_sha,+rsa_aes_256_sha
    NSSEngine on
    NSSCertificateDatabase /etc/httpd/alias


*  I nss.conf ändrar
    * ''Listen 8443'' till ''Listen 443''
    

*  Om du vill, du kan ta bort den default virtualhost som finns i nss.conf (OBS: lämna allt annat kvar)
## Förbrukning

### Skapa pkcs12 cert/key
Om du inte redan har en pkcs12 fil som innehåller både nyckel och cert, skapa en så här:

''openssl pkcs12 -export -in yourcert.pem -key yourkey.key -name "A clever name" -out newfile.pk12''

Det kommer fråga om ett lösenord, man kan lämna det tömt om man vill (i fall att man inte göra det, kommer apache be om det lösenordet vid varje omstart).

Namnet som du anger kommer sen användas i både cert databasen och i virtualhost konfiguration, så använder nåt lätt och tydligt.

### Importera till databasen

Nu när du har en fil med både cert och nyckel, för att importera kommer du använda pk12util:

''pk12util -i newfile.pk12 -d /etc/httpd/alias/''

Ange lösenord som användas när du skapade pkcs12 filen.

För att kolla om det har gått bra, kör:

''certutil -L -d /etc/httpd/alias/''

Du bör se namnet som du gav när du skapade pkcs12 filen i listan.

För att se att privata nyckeln här också importerat:

''certutil -K -d /etc/httpd/alias''

### Virtualhost

För att avnända ditt cert/nyckel par, skapa en virtualhost så här:

    `<VirtualHost *:443>`
    ServerName secure.example.com
    include /etc/httpd/includes/nssbase.conf
    NSSNickname Your Clever Name
    ...
    `</virtualhost>`
    
Starta om apache, och kolla i error loggen för att säkerställa att allt här kommit upp.

