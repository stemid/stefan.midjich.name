# AD autentisering för CIFS med Samba 3

Detta tog faktiskt ett par dagar att få rätt och Samba dokumentationen var näst intill värdelös. 

**Målet:** Dela ut en katalog över CIFS (SMB) från en Linux server med Samba 3.6 och autentisera användarna med Active Directory. 

För artikelns skull gjorde jag detta på en Debian Wheezy server. 

##### Några vanliga termer som kan förekomma i texten

AD-server | dc02.domain.local
----------|-------------------
AD-Admin  | Administrator
AD-klient med Samba | webb01.domain.local
AD domän | domain.local
AD arbetsgrupp | domain

## Installera paket

    sudo apt-get install samba winbind cifs-utils krb5-user openntpd

## Konfigurera DNS

Se till att alla namn inblandade, speciellt dina domänkontrollanter (DC-servrar) kan lösas upp utan problem. Det kan se ut så här i ''/etc/resolv.conf''. 

    search domain.local
    domain domain.local
    nameserver dc02.domain.local

Se till att din maskin kan lösa upp sitt eget namn, men även att namnet finns i namnservern (dc02.domain.local). Så ''/etc/hosts'' kan se ut så här. 

    127.0.0.1       localhost
    10.221.111.51   webb01.domain.local webb01

### Testa DNS

Se till att alla relevanta maskiner kan lösas upp, t.ex. pinga dc02.domain.local. 
## Konfigurera NTP

Jag hade inga problem med tid mot Debians egna serverpool, men det kan vara bra att använda domänkontrollanten som tidserver. Redigera ''/etc/openntpd/ntpd.conf'' och ändra raderna längst ner i filen till att se ut så här. 

    # sync to a single server
    server dc02.domain.local
    
    # use a random selection of 8 public stratum 2 servers
    # see http://twiki.ntp.org/bin/view/Servers/NTPPoolServers
    #servers pool.ntp.org
    
    # Choose servers announced from Debian NTP Pool
    #servers 0.debian.pool.ntp.org
    #servers 1.debian.pool.ntp.org
    #servers 2.debian.pool.ntp.org
    #servers 3.debian.pool.ntp.org

Starta om NTP-tjänsten. 

    sudo service openntpd restart
## Konfigurera Kerberos

''/etc/krb5.conf'' kan se ut så här. 

```ini
[libdefaults]
default_realm = DOMAIN.LOCAL
ticket_lifetime = 24000
clock-skew = 300

[realms]
DOMAIN.LOCAL = {
 kdc = DC02.DOMAIN.LOCAL:88
 admin_server = DC02.DOMAIN.LOCAL:464
 default_domain = DOMAIN.LOCAL
}

[domain_realm]
.DOMAIN.LOCAL = DC02.DOMAIN.LOCAL
DOMAIN.LOCAL = DC02.DOMAIN.LOCAL
```

### Testa Kerberos

Omedelbart ska man kunna begära ett autentiseringsbevis (//token//) från vår domänkontrollant med ''kinit''-kommandot. 

    sudo kinit Administrator@DOMAIN.LOCAL

Lägg märke till versalerna både i kommandot och i ''/etc/krb5.conf'' från tidigare. Otroligt nog är de faktiskt viktiga. 

Kontrollera att servern godkände lösenordet med ''klist''. 

    sudo klist

Det går även att börja om genom att radera autentiseringsbeviset med ''kdestroy''. 

## Konfigurera Samba

Jag ska vara ärlig, jag vet inte mycket av vad som pågår här. Jag vet bara att det fungerar och ska försöka förklara de viktiga raderna så gott som möjligt. 

```ini
[global]
workgroup = DOMAIN
realm = DOMAIN.LOCAL
server string = %h server
security = ads
allow trusted domains = no
local master = no
domain master = no
interfaces = eth0 10.221.111.51/24
bind interfaces only = yes
log level = 0 auth:10 smb:10
log file = /var/log/samba/log.%m
max log size = 1000
#syslog only = no
syslog = 0
load printers = no
printing = bsd
printcap name = /etc/printcap
# Bug #8676 workaround
idmap config * : backend = tdb
idmap config * : range = 2000-4999
idmap config DOMAIN : backend = rid
idmap config DOMAIN : range = 10000-49999
winbind use default domain = yes
winbind enum users = yes
winbind enum groups = yes
encrypt passwords = yes
valid users = @"Domain Users"

[webbrot]
comment = Kundens Share
path = /var/www/webbsida.domain.local
valid users = @"Domain Users"
writable = yes
force user = kunden
force group = kunden
force directory mode = 0775
force create mode = 0664
```

På Windows 2008 och upp så är början av domänen arbetsgruppen (//WORKGROUP//). 

Starta alltid om Samba efter ändringar. Jag har även plockat upp en vana att starta winbind efter samba, dock vet jag inte riktigt varför än. 

    sudo service winbind stop
    sudo service samba restart && sudo service winbind start

### Testa Samba och gå med i AD domän

Vi kan testa att Samba fungerar genom att be den gå med i AD domänen som en medlem. Detta kommandot ska, om det fungerar, skapa en ny dator i OU-trädet på DC-servern. Den ska ligga under ''CN=webb01,OU=Computers,DC=DOMAIN,DC=LOCAL'', namnet är domännamnet av servern så DNS måste verkligen fungera. 

    sudo net ads join -U Administrator
    Enter Administrator's password:

Status på medlemskapet kan testas med samma kommando och andra argument. 

    sudo net ads testjoin
    Join is OK

### Testa Winbind

Winbind ska kunna lista alla användare i domänen, annars är något fel. 

    sudo wbinfo -i stemid

Det kommandot kommer till exempel visa information för användaren stemid. Ett annat argument som ''wbinfo -u'' kommer lista alla användare och ''wbinfo -g'' listar alla grupper. 

Det går även att se informationen via NSS med kommandot ''getent passwd -s winbind''. 

## Konfigurera NSS

NSS är en sorts namntjänst i Linux som kan slå upp mycket mer än bara namn, bland annat lösenord. 

Redigera filen ''/etc/nsswitch.conf'' och ändra följande rader, jag visar bara de tre rader som behöver ändras. 

    passwd:         compat winbind
    group:          compat winbind
    shadow:         compat
    
    hosts:          files dns wins

Jag har lagt till winbind som en tjänst för att slå upp lösenord, och wins som en tjänst för att slå upp värdnamn (//hostnames//). 

### Testa NSS

När detta är gjort så kommer ''getent passwd'' kommandot från tidigare även lista AD-användare som standard. Likaså kommer ''getent group'' lista alla grupper i AD, efter det har listat lokala grupper på Linux-servern. 
