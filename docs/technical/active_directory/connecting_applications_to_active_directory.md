# Connecting applications to AD

Here I'm collecting all I learn about connecting applications to AD, most often this is done with LDAP.
# Recursive groups

Most applications can handle recursive groups in LDAP/AD with the search filter. 

Here's one such filter example.

    (&(objectClass=user)(
    memberOf:1.2.840.113556.1.4.1941:=CN=My nested group,DC=domain,DC=local
    )(name=Stefan Midjich))

The OID placed in memberOf is a sort of AD recursive search function that tells LDAP to recursively search the group ''CN=My nested group'' for the name=Stefan Midjich.

So don't ask me how it works but there are apparently pre-defined OIDs like that, it's not random or specific to the OIDs in your directory.

# See also


*  [MSDN info on Search filter syntax](https://msdn.microsoft.com/en-us/library/aa746475(VS.85).aspx)
