# AD Autentisering med Kerberos i Debian

>**Observera** att det finns en [uppdaterad artikel här](../ad_autentisering_i_linux) som täcker en bättre metod. 

Här går jag igenom hur man ansluter Linux-servrar till en Active Directory domän med Kerberos, Samba och Winbind. 

Den här artikeln är till stor del en kopia av [AD autentisering i RHEL 6](../ad_autentisering_i_rhel_6), men inriktad på Debian Squeeze och Wheezy istället. 

>**Observera** att jag hoppar över alla sektioner från [originalartikeln](../ad_autentisering_i_rhel_6) som inte skiljer mellan RHEL och Debian. 

## Installera paket

    sudo yum install samba winbind krb5-user ntp

## Konfigurera NSS

Detta skiljer sig lite från RHEL. 

Redigera filen ``/etc/nsswitch.conf`` och ändra följande rader, jag visar bara de tre rader som behöver ändras. 

```
passwd:         winbind compat
group:          winbind compat
shadow:         winbind compat
hosts:          files dns wins
```

Jag har lagt till winbind som en tjänst för att slå upp lösenord, och wins som en tjänst för att slå upp värdnamn (//hostnames//). 

Ordningen av winbind före compat är viktig. 

## Konfigurera PAM

Namnet på filerna skiljer sig avsevärt från RHEL men innehållet har jag nästan kopierat från RHEL eftersom PAM är densamma i Linux. 

Så här ser filen ``/etc/pam.d/common-auth`` ut. 

```
auth        required      pam_env.so
auth        sufficient    pam_unix.so nullok try_first_pass
auth sufficient pam_winbind.so use_first_pass
auth        requisite     pam_succeed_if.so uid >= 500 quiet
auth        required      pam_deny.so
```

Filen ``/etc/pam.d/common-account``. 

```
account     required      pam_unix.so
account     sufficient    pam_localuser.so
account sufficient pam_winbind.so use_first_pass
account requisite                       pam_deny.so
account required                        pam_permit.so
```

Filen ``/etc/pam.d/common-password``. 

```
password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok
password sufficient pam_winbind.so use_first_pass
password        requisite                       pam_deny.so
password        required                        pam_permit.so
```

Till sist filen ``/etc/pam.d/common-session``. 

```
session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
session required pam_winbind.so use_first_pass
```

### Skapa hemkataloger automatiskt

Eftersom detta riktar sig till servrar och jag inte vill behöva skapa hemkataloger manuellt på varje server så finns modulen ``pam_mkhomedir.so`` som kan skapa hemkataloger automatiskt. 

För att slå på den, redigera filen ``/etc/pam.d/sshd`` och lägg till följande rad ovanför alla rader som börjar med ``session``. 

    session required pam_mkhomedir.so skel=/etc/skel umask=0022

# Se också


*  [AD autentisering i RHEL 6](../ad_autentisering_i_rhel_6)
*  [Samba Wiki: Samba & Active Directory](http://wiki.samba.org/index.php/Samba_%26_Active_Directory)
