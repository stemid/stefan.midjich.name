---
description: AD Autentisering i RHEL 7 & Ubuntu 14.04
title: AD Autentisering i RHEL 7 & Ubuntu 14.04
---

# AD Autentisering i RHEL 7 & Ubuntu 14.04

Startar en ny guide som dokumenterar hur man använder AD-providern i SSSD eftersom jag nyligen fick prova detta i RHEL7 och det fungerade otroligt smidigt. 

Har på senare tid även testat denna metod i Ubuntu 14.04, så länge SSSD ad-providern är relativt ny funkar det bra. 

## Paketinstallation

    sudo yum install oddjob oddjob-mkhomedir sssd samba-common realmd \
    openldap-clients krb5-workstation -y

## DNS och nät

Se till att DNS och NTP är korrekt konfad till en AD-server, och se till att **AD-servern är Windows 2008 R2 eller högre**. I sådana fall behövs inga ändringar på AD-servern som t.ex. Identity Management for UNIX. 

I andra fall får man se en av [mina äldre artiklar](ad_autentisering_i_linux.md). 

**Obs** Se till att din AD server kan hantera NTP frågor och inte bara kör Windows Time. Det finns ett knep för detta som jag inte tänker gå in på här.

Se även till att alla nödvändiga portar är öppnade, [lista här](ad_autentisering_i_rhel_6.md#natverk-for-ad.md).

## Konfiguration av Kerberos

I ``/etc/krb5.conf`` kan man få lägga till ``default_realm`` om det inte fungerar utan. 

```
[libdefaults]
 default_realm = DOMAIN.LOCAL

[domain_realm]
 .domain.local = DOMAIN.LOCAL
 domain.local = DOMAIN.LOCAL
```

## Konfiguration av realmd

Så här ser ``/etc/realmd.conf`` ut. 

```
[users]
default-home = /home/%d/%u

[domain.local]
fully-qualified-names = no
```


*  default-home - Skapar ``/home/domain.tld/användare`` för alla användare som loggar in
*  fully-qualified-names - Använder bara kortnamn när det är avstängt, annars måste man logga in med användare@domain.tld

**OBS**: ``override_homedir`` i ``realmd.conf`` funkar inte. Man ska använda antingen ``override_homedir`` i nss sektionen av ``sssd.conf``, eller override_homedir i AD sektionen. Se ``man sssd.conf`` och ``man sssd-ad``.

Detta ligger till grund för inställningarna som sedan automatiskt skapas i ``/etc/sssd/sssd.conf``. 

## Frivillig konfiguration av SSSD

Helt frivilligt att konfa ``/etc/sssd/sssd.conf`` manuellt men här är ett exempel. **OBS**: Verktyget ``realm`` tycker om att skriva över ``/etc/sssd/sssd.conf`` vid join. 

```
[sssd]
domains = domain.local
config_file_version = 2
services = nss, pam, sudo
debug_level = 4

[nss]
filter_users = localadm, root
filter_groups = localadm, root

[pam]
pam_pwd_expiration_warning = 14

[domain/domain.local]
id_provider = ad
auth_provider = ad
access_provider = ad
chpass_provider = ad
cache_credentials = True

# This uses kerberos for auth

ldap_sasl_mech = GSSAPI

ldap_schema = ad

use_fully_qualified_names = False
fallback_homedir = /home/%d/%u

default_shell = /bin/bash
krb5_realm = DOMAIN.LOCAL
realmd_tags = manages-system joined-with-samba
krb5_store_password_if_offline = True
```

Det är relativt få ändringar från standarden, ett par saker jag lagt till är;


*  ``services = sudo`` - så man kan byta lösenord också.

*  Lagt till ``filter_users`` och ``filter_groups`` för att filtrera bort lokala administratörer

Annars är detta nästan rakt av från RedHats dokumentation som länkas längre ner på sidan. 

## Join

Idealt ska detta vara allt. 

    sudo kinit admin
    sudo realm join domain.local

Detta skapar en standardkonfiguration i ``/etc/sssd/sssd.conf`` baserat på ``/etc/realmd.conf``. 

Jag har dock funnit att följande process kan vara bra när man t.ex. joinar klonade VMs eller re-joinar en VM med en annan metod eller till annan domän. 

1. ``sudo bash`` - bli root för enklare kommandon
2. ``systemctl stop sssd`` - stäng av SSSD tjänsten
3. ``rm -f /var/lib/sss/db/*`` - radera SSSD tjänstens cache
4. ``kinit admin`` - testa auth mot kerberos med en domänadmins konto
5. ``klist`` - se om du har en användartoken från kerberos
6. ``realm list`` - kolla så ingen annan domän finns med i listan
7 ``realm leave domain.local`` - finns det domäner i listan så gör en leave för varje domän till listan är tom
7. ``realm discover DOMAIN.LOCAL`` - kör en discover först och se till att du inte har några fel
8. ``realm join DOMAIN.LOCAL`` - join i domänen, äntligen ;)
9. ``realm list`` - bekräfta att du är joinad
10. ``systemctl start sssd`` - starta SSSD tjänsten
11. ``getent passwd admin`` - testa SSSD tjänsten genom att begära ett namn från AD och se till att värdena stämmer

## Felsökning

>Fungerar inget här [se gammal guide](#se-ocksa).

### Problem med join

Först och främst kan man prova att bara köra ``realm join`` igen ett par gånger. 

Kan prova att ha maskinens lokala hostnamn i ``/etc/hosts`` innan man joinat AD. 

Fungerar DNS och tidsynk felfritt så kan man prova att bara lämna domänen och gå tillbaka. 

    sudo realm leave domain.local
    sudo realm join -U admin domain.local

Eller att använda ett login av en AD administratör vid join som här ovan. 

Kolla alltid loggen av SSSD också, finns i ``/var/log/sssd`` på CentOS, eller i ``journalctl -flau sssd``.

Aktivera debug logging i ``/etc/sssd/sssd.conf``.

    [domain/domain.local]
    debug = 9

Då sparas extra information i ``/var/log/sssd``. 

### Problem med uppslag

Vid problem med uppslag mot NSS, t.ex. fel grupptillhörigheter eller fel hemkatalog, prova att rensa SSS cache. 

    sss_cache -U användare
    sss_cache -G grupp
    sss_cache -E # rensar allt

Och som sista åtgärd, radera alla filer. 

    systemctl stop sssd
    rm -f /var/lib/sss/db/*
    systemctl start sssd
    getent passwd användare

Se också till att nscd tjänsten **inte** är igång, den kan störa uppslag med sitt cache.

### krb5_child: Server not found in Kerberos database

Händer vid login, namnuppslag fungerar bra. 

**Lösning**: Prova att gå ur domänen och manuellt radera alla instanser av den klientens Computer objekt från katalogen innan du gör en re-join. 

## Tips

### Ubuntu och hemkataloger

I RHEL hanterar oddjob-mkhomedir skapandet av hemkataloger men Ubuntu 14.04 har inte riktigt kommit dit än så man använder den gamla hederliga ``pam_mkhomedir`` som finns i paketet ``libpam-modules``.

    sudo apt-get install libpam-modules -y

Sedan måste det skapas en PAM-tjänst för mkhomedir, se den här filen för exempel som ska läggas under ``/usr/share/pam-configs/mkhomedir`` i Ubuntu. 

```
Name: Create home directory during login
Default: yes
Priority: 900
Session-Type: Additional
Session:
    required  pam_mkhomedir.so umask=0022 skel=/etc/skel
```

Sedan kan man antingen använda ``pam-auth-update`` för att uppdatera PAM eller detta skriptet jag gjorde för att göra det via ansible. 

```
#!/bin/sh

cat > $1 <<EOF
libpam-runtime/profiles="Create home directory during login, SSS authentication, Unix authentication"
EOF
```

### Varför är lastlog enorm?

Lastlog är en så kallad "sparse" fil i Linux som betyder att den tilldelas utrymme den inte använt ännu. Vad som gör problemet mer komplext när du använder SSSD är att lastlog tilldelas utrymme baserat på mängden UID som hittas i systemet. 

Så om du ansluter en maskin till ett stort AD, utan specifika filter i sssd.conf, kommer lastlog "växa" proportionellt för att hushålla alla nya använder IDn. Men sparse filer tar inte upp allt det där utrymmet på disk.

Använder man filbackup så gäller det för filbackup-agenten att vara medveten om Linux sparse-filer, annars kanske man tar upp massa onödigt utrymme på sin backupserver.

## Se också


*  [Felsökning av SSSD från en äldre teknikguide](ad_autentisering_i_linux.md#felsokning)
*  [RedHat dokumentation om SSSD](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Windows_Integration_Guide/SSSD-AD.html#sssd-config)
*  [SSSD AD Access control](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/5/html/Deployment_Guide/config-sssd-domain-access.html)
*  [Äldre sida om AD Autentisering i Linux](ad_autentisering_i_linux.md)
*  [Sudoers med AD grupper](ad_autentisering_i_linux.md#sudoers)
*  [Why is the lastlog file so large?](https://access.redhat.com/articles/3314)
