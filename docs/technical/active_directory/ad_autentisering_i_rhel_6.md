# AD Autentisering med Kerberos i RHEL 6

>**Observera** att det finns en [uppdaterad artikel här](../ad_autentisering_i_linux) som täcker en bättre metod. 

Här går jag igenom hur man ansluter Linux-servrar till en Active Directory domän med Kerberos, Samba och Winbind. 

Den här artikeln är till stor del en kopia av [AD autentisering för CIFS med Samba](../ad_autentisering_foer_cifs_med_samba), men inriktat på RedHat Enterprise Linux 6 istället för Debian. 

## Nätverk för AD

Följande portar behöver vara öppna mot varje domänkontrollant för att kunna ansluta en Linuxserver till en AD-domän. 


*  tcp/domain
*  tcp/88
*  tcp/ldap
*  tcp/445
*  tcp/464
*  tcp/ldaps
*  udp/domain
*  udp/ntp
*  udp/445
*  udp/464
*  udp/netbios-ns
*  udp/389
*  udp/88

## Installera paket

    sudo yum install samba krb5-workstation krb5-libs ntp

## Konfigurera DNS

Redigera ``/etc/resolv.conf`` och lägg till nätets domänkontrollanter. 

```
search sydit.inet
domain sydit.inet
nameserver 10.220.100.10
nameserver 10.220.100.11
```

Se till att deras namn går att lösa upp, i detta fallet är namnen ``vmwin-dc01``, och ``vmwin-dc02``. 

Se även till att serverns egna namn går att lösa upp, antingen genom att lägga till det i DNS på domänkontrollanterna eller genom att lägga till det i filen ``/etc/hosts``. 

## Konfigurera NTP

Redigera filen ``/etc/ntp.conf`` och kommentera ut standardservrarna från RedHat, ersätt istället med nätets domänkontrollanter. 

```
server vmwin-dc01.sydit.inet
server vmwin-dc02.sydit.inet
#server 0.rhel.pool.ntp.org iburst

#server 1.rhel.pool.ntp.org iburst
#server 2.rhel.pool.ntp.org iburst

#server 3.rhel.pool.ntp.org iburst
```

Starta sedan och aktivera tjänsten *ntpd*. 

    sudo service ntpd start
    sudo chkconfig ntpd on

## Konfigurera Kerberos

Versaler och gemener är väldigt viktiga i Kerberos-sammanhang. Så här ser min ``/etc/krb5.conf`` ut. 

```ini
[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[libdefaults]
 default_realm = SYDIT.INET
 ticket_lifetime = 24h
 clock-skew = 300

[realms]
 SYDIT.INET = {
    kdc = VMWIN-DC01.SYDIT.INET:88
    kdc = VMWIN-DC02.SYDIT.INET:88
    admin_server = VMWIN-DC01.SYDIT.INET:464
    admin_server = VMWIN-DC02.SYDIT.INET:464
    default_domain = SYDIT.INET
 }

[domain_realm]
 sydit.inet = SYDIT.INET
```

### Begär Kerberos-biljett

Använd ett AD-konto, för att autentisera med Kerberos och få en biljett.

    sudo kinit svc_unix_auth@SYDIT.INET

Kontrollera med ``klist``. 

## Konfigurera Samba

```ini
[global]
 workgroup = SYDIT
 realm = SYDIT.INET
 server string = %h server
 security = ads
 allow trusted domains = no
 local master = no
 domain master = no
 interfaces = eth0 10.220.100.61/24
 bind interfaces only = yes
 log level = 0 auth:10 smb:10
 log file = /var/log/samba/log.%m
 max log size = 1000
#syslog only = no

 syslog = 0
 load printers = no
 printing = bsd
 printcap name = /etc/printcap
# Bug #8676 workaround

 idmap config * : backend = tdb # Note that this affects all domains, replace * with DOMAIN
 idmap config * : range = 2000-4999
 winbind use default domain = yes
 winbind enum users = yes
 winbind enum groups = yes
 template shell = /bin/bash
 encrypt passwords = yes
```

Här händer mycket som jag inte går igenom, t.ex. anger man domänen längst upp och arbetsgruppen. Du anger också vilken typ av AD kontakt med ``security = ads``, och om Samba ska agera som en domänkontrollant eller inte. 

Längre ner med ``idmap`` anger man även hur AD användares ID ska mappas upp i Unix. 

``winbind use default domain = yes`` är viktigt senare när vi skapar hemkataloger för vi kan inte stödja mer än en domän. 

``template shell`` anger så klart vilket skal användare från AD ska ha, annars har de alla ``/bin/false``. 

Det går även att lägga till ``disable netbios = yes`` om man inte bryr sig om netbios broadcasts. Är egentligen onödigt för AD. 

### Starta Samba och Winbind

Aktivera smb och winbind tjänsterna. 

    sudo chkconfig smb on
    sudo chkconfig winbind on

Nu måste winbind alltid startas efter Samba. 

    sudo service winbind stop
    sudo service smb restart && sudo service winbind start

### Gå med i domänen med Samba

För att faktiskt gå med i domänen används nu ``net ads`` kommandot. 

    sudo net ads join -U svc_unix_auth
    Enter svc_unix_auth's password:
    Using short domain name -- SYDIT
    Joined 'VMLNX-DEPLOY02' to dns domain 'sydit.inet'

Återigen använder vi ett konto som kan lägga till arbetsstationer för att gå med i domänen, till exempel domänadmin. 

När det kommandot lyckas så skapas ett DNS-inlägg på domänkontrollanterna för klientens värdnamn, och ett inlägg i AD under till exempel ``CN=vmlnx-deploy02,OU=Computers,DC=SYDIT,DC=INET``. 

Det kan testas med ``dig``. 

    dig @vmwin-dc01 vmlnx-deploy02.sydit.inet
    ...
    vmlnx-deploy02.sydit.inet. 3600 IN      A       10.220.100.61

Det går även att testa status av domän-medlemskapet. 

    sudo net ads testjoin

### Testa winbind

Med ``wbinfo`` kan vi få massa uppgifter från domänen. Till exempel så här. 

    sudo wbinfo -i stemid
    stemid:*:2000:2000:stemid:/home/SYDIT/stemid:/bin/false

## Konfigurera NSS

NSS är en sorts namntjänst i Linux som kan slå upp mycket mer än bara namn, bland annat lösenord. 

Redigera filen ``/etc/nsswitch.conf`` och ändra följande rader, jag visar bara de tre rader som behöver ändras. 

    passwd:         winbind files
    group:          winbind files
    shadow:         winbind files
    
    hosts:          files dns wins

Jag har lagt till winbind som en tjänst för att slå upp lösenord, och wins som en tjänst för att slå upp värdnamn (//hostnames//). 

Ordningen av winbind före files är viktig. Byt plats på winbind och files om ni vill att lokala konton ska fungera före AD-konton. 

### Testa NSS

När detta är gjort så kommer ``getent passwd`` kommandot lista AD-användare. Likaså kommer ``getent group`` lista alla grupper i AD, efter det har listat lokala grupper på Linux-servern. 

## Konfigurera PAM

Så här ser filen ``/etc/pam.d/password-auth`` ut. 

```
auth        required      pam_env.so
auth        sufficient    pam_unix.so nullok try_first_pass
auth sufficient pam_winbind.so use_first_pass
auth        requisite     pam_succeed_if.so uid >= 500 quiet
auth        required      pam_deny.so

account     required      pam_unix.so
account     sufficient    pam_localuser.so
account     sufficient    pam_succeed_if.so uid < 500 quiet
account sufficient pam_winbind.so use_first_pass
account     required      pam_permit.so

password    requisite     pam_cracklib.so try_first_pass retry=3 type=
password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok
password sufficient pam_winbind.so use_first_pass
password    required      pam_deny.so

session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
session required pam_winbind.so use_first_pass
```

De fyra raderna som jag lagt till är dessa. 

```
auth sufficient pam_winbind.so use_first_pass
account sufficient pam_winbind.so use_first_pass
password sufficient pam_winbind.so use_first_pass
session required pam_winbind.so use_first_pass
```

Sedan redigerar man filen ``/etc/pam.d/system-auth`` så den kan se ut så här. 

```
auth        required      pam_env.so
auth        sufficient    pam_unix.so nullok try_first_pass
auth sufficient pam_winbind.so use_first_pass
auth        requisite     pam_succeed_if.so uid >= 500 quiet
auth        required      pam_deny.so

account     required      pam_unix.so
account     sufficient    pam_localuser.so
account     sufficient    pam_succeed_if.so uid < 500 quiet
account sufficient pam_winbind.so use_first_pass
account     required      pam_permit.so

password    requisite     pam_cracklib.so try_first_pass retry=3 type=
password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok
password sufficient pam_winbind.so use_first_pass
password    required      pam_deny.so

session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
session required pam_winbind.so use_first_pass
```

De fyra rader jag lagt till är som följer. 

```
auth sufficient pam_winbind.so use_first_pass
account sufficient pam_winbind.so use_first_pass
password sufficient pam_winbind.so use_first_pass
session required pam_winbind.so use_first_pass
```

### Skapa hemkataloger automatiskt

Eftersom detta riktar sig till servrar och jag inte vill behöva skapa hemkataloger manuellt på varje server så finns modulen ``pam_mkhomedir.so`` som kan skapa hemkataloger automatiskt. 

För att slå på den, redigera filen ``/etc/pam.d/sshd`` och lägg till följande rad ovanför alla rader som börjar med ``session``. 

    session required pam_mkhomedir.so skel=/etc/skel umask=0022

#### SElinux

På RHEL 6 fanns en bug i selinux-policy paketet som gjorde att filen ``/sbin/mkhomedir_helper`` hade fel context. 

    $ ls -lZ /sbin/mkhomedir_helper
    rwxr-xr-x. root root system_u:object_r:bin_t:s0       /sbin/mkhomedir_helper

Det löste sig enkelt genom att köra detta. 

    sudo chcon -t oddjob_mkhomedir_exec_t /sbin/mkhomedir_helper

Då får den rätt context. 

    $ ls -lZ /sbin/mkhomedir_helper
    rwxr-xr-x. root root system_u:object_r:oddjob_mkhomedir_exec_t:s0 /sbin/mkhomedir_helper

Så när en användare loggar in genom SSH för första gången och dess hemkatalog skapas, då ska hemkatalogen ha context typen ``user_home_dir_t``, **inte** ``home_root_t`` vilket är fel context för en användares hemkatalog. Se ett exempel här. 

```
$ ls -laZ /home/SYDIT/
drwxr-xr-x. root   root          unconfined_u:object_r:home_root_t:s0 .
drwxr-xr-x. root   root          system_u:object_r:home_root_t:s0 ..
drwxr-xr-x. stemid domain admins unconfined_u:object_r:user_home_dir_t:s0 stemid
```

### Tillåt endast vissa grupper att logga in

Det kan vara bra att endast tillåta vissa AD-grupper att logga in och det gör jag med ``pam_listfile``, en modul som kollar mot en vanlig textfil av tillåtna grupper vid inloggning. 

Redigera ``/etc/pam.d/system-auth`` och lägg bara till en extra rad ovanför raden med winbind som vi la till tidigare. 

    auth required pam_listfile.so onerr=fail item=group sense=allow file=/etc/login.group.allow
    auth sufficient pam_winbind.so use_first_pass

Argumentet ``file`` anger att filen ``/etc/login.group.allow`` ska innehålla en enkel lista av grupper som får logga in, en grupp per rad. 

## Konfigurera sudo

För att tillåta sudo utan lösenord för alla domänadmins redigera filen ``/etc/sudoers.d/domain_admins``. 

```
User_Alias DOMAINADMINS=%domain\x20admins
DOMAINADMINS ALL=NOPASSWD: ALL
```

Grupper med mellanrum i namnet måste alltså använda ``\x20`` istället. 

# Se också

*  [AD autentisering i Debian](../ad_autentisering_i_debian)
*  [Samba Wiki: Samba & Active Directory](http://wiki.samba.org/index.php/Samba_%26_Active_Directory)
*  [Portar som används av AD](http://technet.microsoft.com/en-us/library/dd772723%28v=ws.10%29.aspx)
