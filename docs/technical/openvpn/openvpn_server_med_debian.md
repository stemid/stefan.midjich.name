# OpenVPN server med Debian Wheezy

Så här skapar du en OpenVPN server med Debian 7 (wheezy). 

>Kan bli problem på Jessie på grund av att OpenVPN där kompileras med iproute. Jag har fått denna konfiguration att fungera på Jessie med iproute så som det rekommenderas i [dokumentationen](https://openvpn.net/index.php/open-source/documentation/howto.html#security) men på Wheezy är det omöjligt och även onödigt. 

## Installera paket

Installera följande paket. 

    sudo apt-get install openvpn dnsmasq

Skapa en användare för openvpn som ska äga loggar och processen. 

    sudo useradd -r -s /usr/sbin/nologin openvpn
    sudo passwd -l openvpn
    sudo mkdir /var/log/openvpn
    sudo chown openvpn /var/log/openvpn
    sudo mkdir /etc/openvpn/scripts


## Konfigurera OpenVPN

Vi behöver ett gränssnitt för tunneln, kallar det för tun666 och skapar det med openvpn. Ägaren kommer vara användaren openvpn. 

    sudo openvpn --mktun --dev tun666 --user openvpn --group openvpn

Vi skapar konfigurationsfilen ''/etc/openvpn/vpn.sydit.se.conf'' och den ser ut så här. 

```
server
port 1194
proto udp
dev tun666
comp-lzo
keepalive 10 120
persist-key
persist-tun
status /var/log/openvpn/openvpn-status.log
tls-auth vpn.sydit.se_static.key 0

user openvpn
group openvpn

push "redirect-gateway def1"
push "dhcp-option DNS 10.0.5.1"

script-security 2
up scripts/vpn.sydit.se.up.sh
down scripts/vpn.sydit.se.down.sh

ca easy-rsa/keys/ca.crt
cert easy-rsa/keys/vpn.sydit.se.crt
key easy-rsa/keys/vpn.sydit.se.key
dh easy-rsa/keys/dh2048.pem

server 10.0.5.0 255.255.255.0
```

Sedan anger vi namnet på den filen, utan .conf-suffixet, i ''/etc/default/openvpn'' för att den ska startas som en tjänst på servern. 

    AUTOSTART="vpn.sydit.se"

Filerna som anges längst ner kommer vi skapa i steget [#Skapa Nycklar](#Skapa Nycklar)
### Förklaring av konfiguration

Förklarar filen uppifrån och ner, först så använder vi port 1194 så om den inte passar kan ni byta. 

''tls-auth'' är en statisk nyckel som varje klient måste ha en kopia av, och den skapas så här. 

    sudo openvpn --genkey --secret /etc/openvpn/vpn.sydit.se.static.key

Scriptet måste ha exekverbara rättigheter 0755, användaren openvpn måste även kunna köra ''/sbin/ip'' med sudo utan lösenord så skriv följande konfiguration med kommandot ''sudo visudo -f /etc/sudoers.d/openvpn''. 

    openvpn ALL=(ALL) NOPASSWD: /usr/sbin/service dnsmasq stop, /sbin/iptables -F

Detta täcker även allt annat som behövs för resten av guiden. 

''push'' skickar konfiguration till klienterna och ''push redirect-gateway def1'' betyder att default gateway för klienterna blir VPN-tunneln. Det går även att lägga till egna rutter här som t.ex. ''push "route 192.168.22.0 255.255.255.0"'' om man bara vill tunnla ett nät, eller ''push "dhcp-option DOMAIN swehack.local"'' för att pusha ut en sökdomän till DNS. 

''up'' och ''down'' är två skript som körs vid start och stop av tunneln. Så här ser up-skriptet ut. 

```
#!/bin/bash
iptables-restore < /etc/iptables_forward.ipv4_tun666
service dnsmasq restart
```

Notera filen ''/etc/iptables_forward.ipv4_tun666'' som vi skapar senare i guiden. 

Och så här ser down-skriptet ut. 

```
#!/bin/bash
sudo iptables -F
sudo service dnsmasq stop
```

## Konfigurera nätverk

Vi behöver en iptables-brandvägg som kan skicka vidare paket, den konfigurationen skriver vi i ''/etc/iptables_forward.ipv4_tun666'' och den ser ut så här. 

```
nat
:PREROUTING ACCEPT [286:32048]
:INPUT ACCEPT [235:28555]
:OUTPUT ACCEPT [30:3483]
:POSTROUTING ACCEPT [8:504]
-A POSTROUTING -o eth0 -j MASQUERADE
COMMIT

*filter
:INPUT ACCEPT [6293:4854607]
:FORWARD ACCEPT [20:2098]
:OUTPUT ACCEPT [4344:804399]
-A FORWARD -i eth0 -o tun666 -m state --state RELATED,ESTABLISHED -j ACCEPT
-A FORWARD -i tun666 -o eth0 -j ACCEPT
COMMIT
```

Läs in den konfigurationen i iptables så här. 

    sudo iptables-restore < /etc/iptables_forward.ipv4_tun666

Se även till att gruppen openvpn kan läsa filen. 

    sudo chgrp openvpn /etc/iptables_forward.ipv4_tun666
    sudo chmod 0640 /etc/iptables_forward.ipv4_tun666

Det kommer vidarebefordra paket från tunnelgränssnittet tun666 till gränssnittet eth0, så ändra eth0 till något mer aktuellt gränssnitt. 

Till sist behöver vi ange att kärnan ska tillåta vidarebefordring av paket med sysctl så här. 

    sudo sysctl net.ipv4.ip_forward=1

Och det kan göras permanent i filen ''/etc/sysctl.d/99-ip_forward.conf'' med följande text. 

    net.ipv4.ip_forward=1

## Konfiguration av Dnsmasq

Dnsmasq används för att erbjuda DNS över VPN, det är en tjänst som bara skickar vidare alla frågor till systemets egna DNS som oftast är i ''/etc/resolv.conf''. 

För att Dnsmasq ska lyssna på tunnelgränssnittet tun666 skriver vi följande i filen ''/etc/dnsmasq.d/openvpn_tun666.conf''. 

    interface=tun666
    except-interface=eth0

Den andra raden gör att dnsmasq inte lyssnar på vårt vanliga gränssnitt. 

## Skapa nycklar

Nu ska vi använda easy-rsa, så vi börjar med att kopiera katalogen. 

    sudo cp -r /usr/share/doc/openvpn/examples/easy-rsa/2.0 /etc/openvpn/easy-rsa

Redigera filen ''/etc/openvpn/easy-rsa/vars'', bara följande rader behöver ändras. 

    export EASY_RSA="/etc/openvpn/easy-rsa"
    export KEY_SIZE=2048
    
    export KEY_*

Först gör jag så att man kan köra skripten från var som helst, ökar nyckelstorleken och sen fyller jag i något relevant för serverns CA informationen i de sista raderna som börjar med KEY_. 

Inkludera vars-filen i ditt skal, kör sedan ''clean-all'' första gången för att rensa upp och skapa din CA med ''build-ca''. 

    source /etc/openvpn/easy-rsa/vars
    sudo -E /etc/openvpn/easy-rsa/clean-all
    sudo -E /etc/openvpn/easy-rsa/build-ca --pass

Svara på frågorna som skriptet ''build-ca'' ställer, standardvärdena fylldes i vars-filen tidigare. 

CA-certet som nu skapas i ''/etc/openvpn/easy-rsa/keys/ca.crt'' är väldigt viktigt och måste hållas säkert. Därför anger jag argumentet ''--pass'' på slutet men om man inte vill vara tvungen att ange ett lösenord för att signera nya certifikat så kan man utesluta det argumentet. 

Till sist skapa serverns nyckel, generera [Diffie Hellman](wp>Diffie–Hellman_key_exchange) parametrar och skapa den statiska nyckeln för tls-auth. 

    sudo -E /etc/openvpn/easy-rsa/build-key-server vpn.sydit.se
    sudo -E /etc/openvpn/easy-rsa/build-dh
    sudo openvpn --genkey --secret /etc/openvpn/vpn.sydit.se_static.key

Skriv värdnamnet för din server som argument till ''build-key-server''. 

Nu finns det flera nya filer i ''/etc/openvpn/easy-rsa/keys'', här är en lista på de som vi redan använt tidigare i guiden när vi [konfigurerade openvpn](#Konfigurera OpenVPN). 


*  ''easy-rsa/keys/ca.crt'' - Certifikatutgivaren, detta ska delas ut till klienter senare i [Skapa nytt klientcertifikat](#Rutin/ Skapa nytt klientcertifikat).

*  ''easy-rsa/keys/vpn.sydit.se.crt'' - Serverns certifikat, detta skickas av servern till klienten vid anslutningen. 

*  ''easy-rsa/keys/vpn.sydit.se.key'' - Serverns hemliga nyckel som måste hållas skyddad på servern. 

*  ''easy-rsa/keys/dh2048.pem'' - [wp>Diffie Hellman](wp>Diffie Hellman) parametrar, dessa ska anses publika. 

Nu kan openvpn-tjänsten startas, kolla i ''/var/log/daemon.log'' efter fel. 
## Rutin: Skapa nytt klientcertifikat

Detta görs varje gång en ny klient ska använda VPN-tunneln. 

>Kom ihåg ''source /etc/openvpn/easy-rsa/vars'' om du inte redan gjort det från de tidigare stegen.

    sudo -E /etc/openvpn/easy-rsa/build-key klient_stemid

Namnet på slutet är mest för att själv hålla reda på filerna och vem de tillhör. 

Nu har vi två nya filer som ska delas ut till klienten, inklusive två andra filer som alla klienter måste ha. Här är en lista på vilka filer som klienter måste få för att kunna ansluta. Sökvägarna visas relativt från ''/etc/openvpn'' om man följt den här guiden. 


*  ''easy-rsa/keys/klient_stemid.crt'' - Klientens certifikat, namnet beror på kommandot ''build-key'' från tidigare i guiden. 

*  ''easy-rsa/keys/klient_stemid.key'' - Klientens nyckel, samma gäller dens namn som klientens certifikat.

*  ''vpn.sydit.se_static.key'' - Krävs för tls-auth alternativet som höjer säkerheten lite. 

*  ''easy-rsa/keys/ca.crt'' - Är CA certifikatet som alltid krävs för OpenVPN.

### Bygga en klientkonfiguration

Det är smidigast att packa ihop varje klients konfiguration i en mapp, här är ett exempel som följer guiden. 

    mkdir klient_stemid
    cp /etc/openvpn/easy-rsa/keys/ca.crt klient_stemid/
    cp /etc/openvpn/vpn.sydit.se_static.key klient_stemid/
    cp /etc/openvpn/easy-rsa/keys/klient_stemid.crt klient_stemid/
    cp /etc/openvpn/easy-rsa/keys/klient_stemid.key klient_stemid/

Skapa sedan filen ''klient_stemid/openvpn.conf'' och se till att den ser ut så här. 

```
client
dev tun
proto udp
remote vpn.sydit.se 1194
resolv-retry infinite
nobind
persist-key
persist-tun
mute-replay-warnings
resolv-retry infinite
comp-lzo
tls-auth vpn.sydit.se_static.key 1

# Used on Linux

#script-security 2
#up /etc/openvpn/update-resolv-conf

#down /etc/openvpn/update-resolv-conf

ca ca.crt
cert client_stemid.crt
key client_stemid.key
```
# Se också


*  [..:..:openvpn](../../openvpn)

*  [Artikeln anpassad till CentOS 7](./openvpn_server_med_centos_7)

*  [.:OpenVPN klientkonfiguration](./OpenVPN klientkonfiguration)
