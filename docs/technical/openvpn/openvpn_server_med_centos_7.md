# OpenVPN server med CentOS 7

Här dokumenterar jag underligheter i att konfigurera OpenVPN på CentOS 7. 

## Serverkonfiguration

Först skapa en fil för din server, jag kallar min för ''/etc/openvpn/minserver.conf''. Namnet på filen (//minserver//) används senare för att starta tjänsten. 

```
server 10.8.0.0 255.255.255.0
port 1194
proto udp
dev tun666
comp-lzo
keepalive 10 120
tls-auth minserver_static.key 1

ifconfig-pool-persist ipp.txt
persist-key
persist-tun

user openvpn
group openvpn

push "redirect-gateway def1"
push "dhcp-option DNS 10.8.0.1"
push "dhcp-option DOMAIN domain.local"

ca ca.crt
cert minserver.crt
key minserver.key
dh dh2048.pem

#status /var/log/openvpn/openvpn-status.log

#log-append /var/log/openvpn.log
#verb 3
```

### Anteckningar om serverkonfiguration

Ovan konfiguration *kapar* hela standardrutten, med andra ord *default route*, hos klientsystemet. Det betyder att all trafik går som standard genom tunneln. Så att starta en sådan VPN kan avsluta befintliga anslutningar. 

Det går även att rutta bara ett nät, t.ex. ett hemmanätverk man vill komma åt utifrån på säkert vis. Ersätt då raden med ''push "redirect-gateway def1"'' i konfen ovan med följande rad. 

    push "route 192.168.0.0 255.255.255.0"

Då har man nätverket 192.168.0.0/24 hemma och därför skickas en rutt ut för det nätet som går via tunneln. 

Tänk på att byta namnserver och domän till något som passar det nät man tunnlar in i. 

    push "dhcp-option DNS 192.168.44.1"
    push "dhcp-option DOMAIN domain.tld"
## DNS

**Observera** att jag använder ingen dnsmasq för OpenVPN personligen eftersom jag tunnlar in i ett nät där det redan finns en VPN så det räcker att ''push'':a den via OpenVPN. 

Simpel Dnsmasq konfiguration som binder till tun-enheten och använder sig av systemets namnservrar i ''/etc/resolv.conf'' för att vidarebefordra anrop från VPN-tunneln. 

Redigera ''/etc/dnsmasq.d/openvpn_tun0.conf''.

```
interface=tun666
except-interface=enp2s0
```

**Observera** att namnet på nätverksenheten kan variera. 
## Konfigurera brandvägg

Se till att brandväggen är på, jag föredrar att använda den nya firewall-cmd metoden över direkta iptables-kommandon. 

    sudo systemctl enable firewalld
    sudo systemctl start firewalld

Detta är den knepiga biten för alla nybörjare i Systemd. 

    sudo firewall-cmd --add-service=openvpn
    sudo firewall-cmd --permanent --zone=trusted --add-interface=tun0
    sudo firewall-cmd --permanent --zone=trusted --add-masquerade
    sudo firewall-cmd --permanent --direct --passthrough ipv4 -t nat -A POSTROUTING -s  10.8.0.0/24 -o enp2s0 -j MASQUERADE
    sudo firewall-cmd --reload

I näst sista kommandot ska enp2s0 bytas ut mot ditt externa nätverkskort på maskinen. 

Aktivera routing i kärnan. 

    sudo sysctl net.ipv4.ip_forward=1

## Systemd tjänster

Denna biten var kul och lärorik för nya systemd-användare. 

Låtsas att vår VPN heter *minserver* och skapa två kataloger. 

    sudo mkdir /etc/systemd/system/openvpn@minserver.service.d
    sudo mkdir /etc/systemd/system/dnsmasq.service.d

Sedan skapar man filer som kan lägga till ytterligare konfiguration till de systemd.units som pakethanteraren installerat för OpenVPN och Dnsmasq. 

Först ''/etc/systemd/system/openvpn@minserver.service.d/dnsmasq.conf''.

```
[Unit]
Requires=dnsmasq.service
Before=dnsmasq.service
```

Sedan ''/etc/systemd/system/dnsmasq.service.d/openvpn.conf''.

```
[Unit]
BindsTo=openvpn@minserver.service
After=openvpn@minserver.service
```

Ladda om systemd. 

    sudo systemctl daemon-reload
    sudo systemctl stop dnsmasq
    sudo systemctl restart openvpn@minserver

Nu kommer Dnsmasq starta direkt efter OpenVPN, så att tun-enheten existerar när Dnsmasq startar. Detta är underbart för det eliminerar behovet för de gamla up/down-skripten man brukade använda i OpenVPN konfigurationen. 


# Felsökning

Återigen använder vi konfigurationens namn. 

    sudo journalctl -flau openvpn@minserver

# Se också


*  [Rutin för att skapa nycklar från en äldre teknikguide](./openvpn_server_med_debian#skapa_nycklar)

*  [teknik:openvpn](teknik/openvpn)

*  [Subnätskalkylator](http://www.subnet-calculator.com/cidr.php)
