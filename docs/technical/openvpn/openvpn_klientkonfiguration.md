# OpenVPN klientkonfiguration

Lista olika operativsystem och hur man konfar OpenVPN, följer guiden [.:OpenVPN server med Debian](./OpenVPN server med Debian).

## OpenVPN klientkonfiguration på Linux

Så här ser min klientkonfiguration ut på Debian Linux. 

```
client
dev tun
proto udp
remote min.vpn.server 443
resolv-retry infinite
nobind
persist-key
persist-tun
mute-replay-warnings
resolv-retry infinite
comp-lzo
verb 5
tls-auth min.vpn.server.static.key 1

ca ca.crt
cert laptop.crt
key laptop.key
```

Observera att ''verb 5'' är onödigt mycket debug-data och kan tas bort eller sänkas för de flesta. 

Observera även att up/down skriptet ''/etc/openvpn/update-resolv-conf'' följer med openvpn på Debian. 

## Använd OpenVPN klient på Windows

##### Steg 1: Ladda ner OpenVPN

Välj den senaste kliten för din Windows-version [här](https://openvpn.net/index.php/open-source/downloads.html). 

##### Steg 2: Installera OpenVPN

Tryck Nästa och välj standardinställningar genom hela installationen. Windows-säkerhet kommer be dig godkänna installationen minst två gånger. 

##### Steg 3: Packa upp klientkonfigurationen

Ska komma i en .zip fil, packa upp den. Filen innehåller en fil som heter klient.ovpn där klient är namnet på din klient. T.ex. helena.ovpn. 

Packa upp innehållet i en egen mapp och flytta mappen till ''C:\Program\OpenVPN\config\''.

##### Steg 4: Starta OpenVPN GUI

På skrivbordet eller i startmenyn ska nu finnas programmet OpenVPN GUI, starta det. 

##### Steg 5: Anslut OpenVPN

OpenVPN GUI har en liten grå ikon nere i verktygsfältet, högerklicka på den och välj Anslut. 

Ett fönster visas med info, när fönstret försvunnit ska det lysa grönt nere i verktygsfältet och du ska vara ansluten. 

##### Steg 6: Testa anslutning

Prova att pinga någon IP på andra sidan VPN tunneln. T.ex. 192.168.22.1. 

Prova även att se vad din IP-adress visas som på webbsidan [ipa.sh](https://ipa.sh/).
## OpenVPN klientkonfiguration på Macintosh

På Macintosh är det varmt rekommenderat att använda [Tunnelblick](https://code.google.com/p/tunnelblick/), men även då måste man importera en vanlig OpenVPN konfiguration och min brukar se ut så här innan importeringen. 

```
client
dev tun
proto udp
remote min.vpn.server 443
resolv-retry infinite
nobind
persist-key
persist-tun
mute-replay-warnings
resolv-retry infinite
comp-lzo
tls-auth min.vpn.server.static.key 1

ca ca.crt
cert jobbdator.crt
key jobbdator.key
```

# Se också


*  [teknik:openvpn](teknik/openvpn)

*  [.:OpenVPN server med Debian](./OpenVPN server med Debian)
