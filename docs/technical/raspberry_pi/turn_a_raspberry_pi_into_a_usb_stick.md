# Turn a Raspberry Pi into a USB stick

In this guide I turn a Raspberry Pi Zero W into a USB mass storage device, or a "smart USB-memory stick" in other words.

That means you'll be able to plug a USB cable from one of the USB ports on the Raspberry Pi to any other device that can use USB storage like a PC for example, and use it like a USB-storage device to transfer files.

As far as I've read online only the Raspberry Pi Zero W can be used like this, the regular Raspberry Pi devices do not have the hardware capable to act as a USB mass storage device.

Why would you want to do this? There are some proprietary devices out there that share data by writing them to USB storage devices and having a Linux OS there to receive the data live can help with exporting that data to a more modern system like Prometheus and Grafana.

## Install Raspberry Pi OS Lite

[Download it here](https://www.raspberrypi.org/software/operating-systems/) and use the [Raspberry Pi Imager](https://www.raspberrypi.org/software/) or follow their [other instructions](https://www.raspberrypi.org/documentation/installation/installing-images/README.md).

The rest are just basic steps for anyone who has setup a raspberry pi before, skip to [Create a USB mass storage device](#create-a-usb-mass-storage-device) if you're already familiar.

### Pre-boot OS config

Before booting you should enable ssh. This and WiFi setup is done with the Raspberry Pi Imager, or you can mount the first volume of the SD card and do the following;

1. Create an empty file called ``ssh`` in the root (/boot volume of the SD card) to enable SSH.
2. Create a file called ``wpa_supplicant.conf`` in the root, this will be copied into /etc at boot.

#### Example of wpa_supplicant.conf

This is an example for Sweden, you should use a different ISO3166 country code.
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=SE

network={
    ssid="<your ssid here>"
    psk="<your psk here>"
    key_mgmt=WPA-PSK
}
```

### Post-boot OS config

You can login to Raspberry Pi OS with the username ``pi`` and the password ``raspberry`` over SSH by default.

First run ``sudo raspi-config`` and select Advanced to expand your storage so it fills the entire SD card.

Reboot for changes to take effect.

Then you should make sure you're fully upgraded by running ``sudo apt update && sudo apt upgrade`` and reboot again.

## Create a USB mass storage device

Edit the file /boot/config.txt and add the following line to the end of the file;

```
dtoverlay=dwc2
```

Edit the file ``/etc/modules`` and add the following line to the end;

```
dwc2
```

Reboot for changes to take effect. This will enable the kernel module USB driver which provides "gadget mode".

### Create and format a container file

This file will be the filesystem of your USB storage device. Change 1024 for a larger USB storage device.

    sudo dd if=/dev/zero of=/piusb.bin bs=1M count=1024 status=progress conv=fsync
    sudo mkdosfs /piusb.bin -F 32 -I

### Enable the USB storage device

Every time you want to enable the USB storage device from inside the Raspberry Pi OS you run this command;

```
sudo modprobe g_mass_storage file=/piusb.bin stall=0 removable=1
```

And to disable it;

```
sudo modprobe -r g_mass_storage
```

Now you can plug it into a PC or any other device like a dehumidifier for example and it should act just like a normal USB storage device, all data will be written to the ``/piusb.bin`` file we created earlier.

### Mount the USB storage from inside Raspberry Pi OS

To see the data written to the USB storage we must mount it inside the Raspberry Pi OS, **while it is mounted we cannot see any changes written to the data from the device where it is plugged in**. So if you want to script this you should mount it, read the data, and unmount it immediately.

```
sudo mount -t vfat -o users,umask=000 /piusb.bin /mnt
```

Now you can read and write data, but you must unmount it for it to be visible to any other devices where you might plug the USB device in. So be careful about trying to write the same data from two devices.

```
sudo umount /mnt
```

### Enable USB storage device on boot

To make it a USB storage device every time it boots you must create ``/etc/modprobe.d/g_mass_storage.conf`` with the following content;

```
options g_mass_storage file=/piusb.bin stall=0 removable=1
```

And add the line ``g_mass_storage`` to the file ``/etc/modules``.

## g_mass_storage is deprecated

According to some sources the kernel module used here (``g_mass_storage``) is actually deprecated and replaced by [rpi-tools](https://github.com/librerpi/rpi-tools).
