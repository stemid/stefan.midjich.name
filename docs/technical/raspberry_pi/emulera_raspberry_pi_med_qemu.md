# Emulera Raspberry Pi med Qemu

När man arbetar mycket med Raspberry Pi datorer så kan det vara bra att göra ändringar i systemet utan att behöva starta upp det på en raspberry. 

Installera först paketet qemu-system-arm. 

Sedan använder jag qemu så här. 

# Skaffa en Qemu kärna för Raspberry Pi

[Här](https://github.com/dhruvvyas90/qemu-rpi-kernel) har någon laddat upp en qemu-kernel fil för Raspberry Pi. 

**Notis**: Jag tror den är begränsad till 256M RAM men jag vet inte tillräckligt om qemu i tillfället för att vara säker. Allt jag vet just nu är att den vägrar boota med 512M RAM i qemu, trots att det är så mycket minne en nyare modell av raspi har. 

# Skaffa en bildfil av raspbian wheezy

Eller något annat favoritsystem, jag använder Raspbian som exempel så ladda ner den [härifrån](https://www.raspberrypi.org/downloads/). 

# Utöka storleken på raspbian bildfilen

    qemu-img resize -f raw 2015-05-05-raspbian-wheezy.img +2G

# Kör igång qemu

Först måste vi fixa lite saker i Raspbian innan vi kan emulera det i qemu. Så första uppstarten sker i ett sorts återställningsläge där vi startar /bin/sh direkt för att kunna redigera filer utan att systemst startar. 

    qemu-system-arm -kernel kernel-qemu -cpu arm1176 -m 256 -M versatilepb -append "root=/dev/sda2 panic=1 init=/bin/sh rw" -hda 2015-05-05-raspbian-wheezy.img

Notera att jag använder ''-m 256'' vilket ger mig 256M RAM, som jag nämnde tidigare verkar kernel-qemu vara låst till den mängden. 

När vi väl startat Raspbian på det här viset måste vi redigera två filer, först ''/etc/ld.so.preload'' och kommentera ut den enda raden där så den ser ut så här. 

    #/usr/lib/arm-linux-gnueabihf/libcofi_rpi.so

Till sist skapa filen ''/etc/udev/rules.d/90-qemu.rules'' med följande innehåll. 

```
KERNEL=="sda", SYMLINK+="mmcblk0"
KERNEL=="sda?", SYMLINK+="mmcblk0p%n",
KERNEL=="root", SYMLINK+="mmcblk0p2",
```

Sista raden gör så att ''/dev/root'' länkas till ''/dev/mmcblk0p2'' vilket är systemvolymen på SD-kortet. Det är nödvändigt för att raspi-config ska kunna utöka systemvolymen. 

Sedan kan vi starta qemu igen, på riktigt denna gången. 

    qemu-system-arm -kernel kernel-qemu -cpu arm1176 -m 256 -M versatilepb -append "root=/dev/sda2 panic=1" -drive file=2015-05-05-raspbian-wheezy.img,index=0,media=disk,format=raw

Och så kan vi emulera en raspberry pi, göra ändringar till img-filen för att sedan skriva dem till ett SD-kort på det traditionella viset. 

    sudo dd if=2015-05-05-raspbian-wheezy.img of=/dev/mmcblk0 bs=4096

# Emulera USB-enhet från värden

Kan vara så att vi vill testa Wifi-nätverket på Raspbian innan vi skriver SD-kortet och startar minidatorn. Då måste vi skicka igenom en USB-enhet (USB-Wifi dongel) från värddatorn till det emulerade systemet. 

FIXME: stemid

# Se också


*  [USB Host Device Assigned to Guest](http://www.linux-kvm.org/page/USB_Host_Device_Assigned_to_Guest)

*  [Dokument om att emulera Raspberry Pi](http://www.linux-mitterteich.de/fileadmin/datafile/papers/2013/qemu_raspiemu_lug_18_sep_2013.pdf)
