# WiFi Accesspunkt med Raspberry Pi

>**Varning**, förkunskaper om Debian är ett krav för att följa den här guiden just nu.

Här skapar jag en trådlös router med en [Raspberry Pi](http://www.raspberrypi.org/) modell B, och en [TP-Link Nano USB-adapter](http://www.tp-link.com/en/products/details/?model=TL-WN725N) från Webhallen. 

**Observera** att dessa trådlösa nätverkskort ofta släpps i flera olika versioner med olika chipsets, därför är det en jungel att hitta ett som både fungerar väl i Linux och har *master-läge*. Utan *master* eller *ap* läge kan en trådlös adapter eller ett nätverkskort aldrig agera som en accesspunkt. 

Jag hade inte hittat det här kortet om det inte vore för *threadlock* från [..:..:Forskningsavdelningen](../../Forskningsavdelningen) i Malmö. 
# Installera WiFi USB-adaptern

Linux är ju ett DIY-system, så det krävs en hel sektion bara för att installera en USB-adapter. 

## Se om en trådlös adapter fungerar

Dyker inget wlan0 gränssnitt upp omedelbart vid anslutning så behövs något extra för att den ska fungera. 

```
$ ip l show wlan0
3: wlan0: `<NO-CARRIER,BROADCAST,MULTICAST,UP>` mtu 1500 qdisc mq state DOWN mode DEFAULT qlen 1000
    link/ether 64:66:b3:24:0b:ac brd ff:ff:ff:ff:ff:ff

```

## Ta reda på chipset

Fungerar adaptern inte så måste vi ta reda på dess chipset först, med ''lsusb'' kommandot kan vi få samma PCI-information som i Windows enhetshanterare. 

```
$ lsusb
Bus 001 Device 004: ID 0bda:8179 Realtek Semiconductor Corp.

```

Vårt kort är gjort av Realtek och har [Device ID 8179](http://pcidatabase.com/search.php?device_search_str=8179&device_search=Search), som faktiskt delas av tre olika enheter men bara en WiFi-adapter. 

## Hitta drivrutin

Tyvärr hjälper detta inte så mycket men tur är att webhallen på sin produktsida visar det hela namnet av enheten som **TL-WN725N**. En Google-sökning ger oss dock tre olika chipset för tre olika versioner av den här enheten. 

Ytterligare sökningar på Google och trådar på forum tyder på att [detta är](https://github.com/liwei/rpi-rtl8188eu) den rätta drivrutinen för Raspberry Pi. 

## Kompilera drivrutin

Innan drivrutinen kan kompileras måste man uppdatera sitt Raspbian OS, jag kör [Raspbian Server Edition (wheezy)](http://sirlagz.net/tag/raspbian-server-edition/) men kommandot är förmodligen från originaldistron. 

    $ sudo rpi-update

För detta krävs en internetanslutning och lite tålamod. 

Sedan är det bara att följa instruktionerna i filen ''README.md'' i drivrutinens git-förråd. 

# Konfigurera accesspunkt

Vi är inte färdiga med kompileringen, för att mjukvaran [hostapd](http://w1.fi/hostapd/) ska fungera med vår nya drivrutin så måste vi kompilera en speciell version av hostapd från [Realtek](http://www.realtek.com.tw/downloads/downloadsView.aspx?Langid=1&PNid=21&PFid=48&Level=5&Conn=4&DownTypeID=3&GetDown=false)((Välj RTL8188CUS och ladda ner drivrutinen för Linux)). 

## Kompilera hostapd från Realtek

Installera först hostapd från Debians paketförråd, bara för att få katalogen ''/etc/hostapd'', filen ''/etc/init.d/hostapd'' och filen ''/etc/default/hostapd'' skapade. 

Ignorera nu hostapd-paketet som kommer med Debian Wheezy för ett tag och packa upp filen ''RTL8192xC_USB_linux_v3.4.4_4749.20121105.zip'' från Realteks hemsida. 

Gå in i katalogen ''RTL8188C_8192C_USB_linux_v3.4.4_4749.20121105/wpa_supplicant_hostapd'' och packa upp filen ''wpa_supplicant_hostapd-0.8_rtw_20120803.zip''. 

Gå in i den nya katalogen ''wpa_supplicant_hostapd-0.8/hostapd/'' och starta kompileringen. 

```
$ make
$ sudo make install
```

Detta installerar en ny hostapd-binär i ''/usr/local/bin'' istället för ''/usr/sbin'' som Debian hellre använder. 

## Konfigurera hostapd

I sökvägen ''RTL8188C_8192C_USB_linux_v3.4.4_4749.20121105/wpa_supplicant_hostapd/'' av Realteks drivrutin finnes filen ''rtl_hostapd_2G.conf'' som är grunden till filen jag använder. 

Några av de här raderna är specifika för Realteks version av hostapd. 

```
interface=wlan0
ctrl_interface=/var/run/hostapd
ssid=Kontrapunkt
channel=6
auth_algs=1
wmm_enabled=0
driver=rtl871xdrv
beacon_int=100
hw_mode=g
ieee80211n=1
ht_capab=[SHORT-GI-20][SHORT-GI-40][HT40+]
max_num_sta=8

# Logging

logger_syslog=-1
logger_syslog=1
```

Den här skapar ett öppet WLAN vem som helst kan ansluta till utan lösenord, det finns flera exempel av ''hostapd.conf'' med lösenord på Google men det är inte mitt mål med den här accesspunkten. 

För att kunna använda startskriptet som Debians hostapd-paket installerat måste man redigera en rad i filen ''/etc/init.d/hostapd''. 

    DAEMON_SBIN=/usr/local/bin/hostapd

Så den pekar mot Realteks version av hostapd-binären. 

Startar vi tjänsten nu så bör vi se ett nytt WLAN i närheten som heter Kontrapunkt. Annars kan man felsöka genom att starta tjänsten manuellt i terminalen. 

    sudo /usr/local/bin/hostapd -dd -P /var/run/hostapd.pid /etc/hostapd/hostapd.conf
# Konfigurera Router

Ett öppet WLAN gör inte mycket nytta om där inte finns en router också. I vårt fall behöver vi följande komponenter. 


*  Router med [iptables](http://packages.debian.org/wheezy/iptables) för att vidarebefordra paket och översätta adresser

*  DHCP med [dnsmasq](http://packages.debian.org/wheezy/dnsmasq) för att dela ut IP-information

*  DNS med [dnsmasq](http://packages.debian.org/wheezy/dnsmasq) för att lösa upp namn

## Dnsmasq

Vi börjar bakifrån bara för att få igång DHCP på vår AP. 

[Dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) är ett litet program som kan göra två saker, DNS vidarebefordring och DHCP. 

I vårt fall tänker vi låta Dnsmasq vidarebefordra DNS-frågor till namnservern i ''/etc/resolv.conf'' av routern, detta kräver ingen konfiguration på Debian. 

Men för DHCP ska vi skapa filen ''/etc/dnsmasq.d/Kontrapunkt.conf'' och fylla den med följande rader. 

```
interface=wlan0
dhcp-range=172.16.44.10,172.16.44.150,12h
dhcp-option=option:router,172.16.44.1
dhcp-option=option:dns-server,172.16.44.1
#dhcp-option=option:ntp-server,172.16.44.1
```

Startar vi nu tjänsten dnsmasq så ska vi inte bara kunna ansluta till vårt nya WLAN utan även få en ip-adress i spannet 172.16.44.10 till 172.16.44.150. 

På en dator med bara en anslutning fungerar detta utmärkt men ibland om du ska testa det trådlösa nätet på en dator som redan har trådat nät så måste man lägga till en extra statisk route för nätet 172.16.44.0 att gå igenom 172.16.44.1 med nätmask 255.255.255.0. 

## IPtables

Vi kan lösa upp namn från klienter i nätverket nu men vi kan varken pinga eller ansluta till någon. För det behöver vår accesspunkt vara en router. 

Först aktiverar vi vidarebefordring av paket mellan nätverkskort i Linux genom att sätta ett sysctl värde. 

    $ sudo sysctl net.ipv4.ip_forward=1

Gör det permanent genom att skapa filen ''/etc/sysctl.d/ip_forwarding.conf'' och skriva samma argument där på en enskild rad. 

    net.ipv4.ip_forward=1

Nu för att säga till iptables-brandväggen att översätta adresser och vidarebefordra trafik mellan wlan0 och eth0 gränssnitten. 

```
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
```

Har allt fungerat bra nu så ska vi omedelbart kunna pinga google.se från en WLAN-ansluten klientdator. 

Vi vill dock göra ändringen i iptables permanent genom att spara reglerna i en fil som körs vid varje uppstart. 

Spara först reglerna i filen ''/etc/iptables.ipv4.nat''. 

    $ sudo iptables-save | sudo tee /etc/iptables.ipv4.nat

Uppdatera sedan ''/etc/network/interfaces'' och lägg till en rad som återställer iptables-reglerna när wlan0-gränssnittet aktiveras. Hos mig ser det ut så här. 

```
allow-hotplug wlan0
auto wlan0
iface wlan0 inet static
        address 172.16.44.1
        netmask 255.255.255.0
        up iptables-restore `< /etc/iptables.ipv4.nat</file>`
# Se också


*  [Raspberry Pi WiFi USB-adaptrar](http://elinux.org/RPi_USB_Wi-Fi_Adapters)

*  [TP-Link TL-WN725N V2 works out of the box on Raspbian](http://www.raspberrypi.org/phpBB3/viewtopic.php?f=26&t=29752)

*  [Access Point (AP) for TP-Link TL-WN725N V2](http://www.raspberrypi.org/phpBB3/viewtopic.php?f=91&t=54946)

*  [wireless AP with hostapd and realtek chipset (works)](http://www.raspberrypi.org/phpBB3/viewtopic.php?f=46&t=25921)
