>**OBS** arbete pågår här för att skriva ner allt jag gjorde för att bygga min övervakningskamera. 

# Övervakningskamera med Raspberry Pi

>**2015-10-15**: Har precis provat [motioneyeos](https://github.com/ccrisan/motioneyeos) och blev supernöjd så det finns inget behov att göra en egen från grunden. Ska bara kompilera OpenVPN på den så är den perfekt. 

>**2016-02-05**: SKa överge [motioneyeos](https://github.com/ccrisan/motioneyeos) eftersom det verkar hänga sig efter en månads drift ungefär. 

Jag har tidigare byggt en övervakningskamera med en Raspberry Pi modell B och deras kameramodul. Då fanns dock inga bra skal för kameramodulen så den såg [lite löjligt ut](http://i.imgur.com/APwlZED.jpg) och hållaren för kameran var inte alls stabil. 

Nu finns det dock [flera](https://www.modmypi.com/modmypi-model-b-plus-raspberry-pi-case-black?filter_name=camera) [bra](https://www.modmypi.com/one-nine-design-pi-case-white-model-b-plus?filter_name=camera) [alternativ](https://www.modmypi.com/nwazet-pi-camera-box-bundle-plus?filter_name=camera&page=2) för att bygga kameror så jag bestämde mig för att göra en ny med en Raspberry Pi modell B+. 

# Utrustning

Här är vad som behövs för en trådlös övervakningskamera. 


*  Raspberry Pi modell B+

*  Raspberry Pi kameramodul 1080p v1.3

*  Valfritt kamerahus för B+ och kameramodul (se produktbeskrivning för att se om ett skal passar)

*  3 meter USB till micro-USB kabel

*  USB strömadapter

*  32G micro SDHC

*  [WiFi USB Nano 802.11N](https://www.modmypi.com/wireless-usb-1n-nano-adaptor-802.11N-wifi-dongle?filter_name=wifi)

USB-Wifi-dongeln från modmypi.com fungerar alltid, men man kan vara frestad att köpa TP-Link 150Mbps Wireless Nano USB adapter för att vissa har fått den att fungera men jag kan absolut inte få [drivrutinen](https://github.com/lwfinger/rtl8188eu) att fungera. Jag får alltid felet ''Association request to the driver failed'' i wpa_cli. 
# Skalet

De skal, eller kamerahus, jag har sett har alla varit otroligt enkla att sätta ihop så jag behöver egentligen inte säga något mer. 

Det enda jag upptäckte var att kabeln till kameramodulen ibland måste vridas lite för att nå kontakten, detta är inga problem om du bara vrider den 180 grader och inte viker den någonstans. Sedan är det så klart bäst för kabeln att ligga stilla inne i skalet utan att man pillar på den för ofta. 

# Operativsystem

Jag provade [Pidora](http://www.pidora.ca/) denna gången eftersom jag är frälst på Fedora för privat bruk, det var inte moget. Stötte på ett mycket märkligt fel i Pidora när jag skulle kompilera drivrutinen för USB-Wifi-kortet, den kunde inte alls känna igen makron från linux/version.h trots att filen låg på rätt plats och fanns med i inkluderingssökvägarna för kompilatorn. 

Så jag fick ta Raspbian som jag använde sist, men nu för tiden finns det [flera alternativa OS tillgängliga](http://www.raspberrypi.org/downloads/). 

## Installation

Bäst är att följa [instruktionerna](http://www.raspberrypi.org/documentation/installation/installing-images/README.md). 

    sudo dd if=2014-12-24-wheezy-raspbian.img of=/dev/sdb bs=4M

SSH-tjänsten är startad så det är bara att logga in med pi-kontot och lösenordet ''raspberrypi''. 

Efter inloggning kan man köra ''sudo raspi-config'' för att bland annat utöka volymen till att använda allt utrymme på SDHC-kortet och förbereda för att använda kameramodulen. 

## Användare

Jag brukar lägga till en admin-användare och låsa användarna pi och root. Men oavsett hur du väljer att göra så ska din användare vara med i netdev gruppen. 

    sudo useradd -d /home/admin -m -G sudo,netdev admin
    sudo passwd admin
    sudo passwd -l root
    sudo passwd -l pi

## WiFi

Har man valt rätt WiFi-adapter så är det bara att konfa wpasupplicant i filen ''/etc/wpa_supplicant/wpa_supplicant.conf'' så här. 

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
ap_scan=1

network={
 ssid="phlebas_nomap"
 psk="hemlis"
 id_str="home"
 priority=99
}
```

Skriv sedan följande konfiguration i ''/etc/network/interfaces''. 

```
auto lo

iface lo inet loopback
iface eth0 inet manual

allow-hotplug wlan0
auto wlan0
iface wlan0 inet dhcp
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```

Kan starta wpa_supplicant manuellt för att felsöka. 

    sudo /sbin/wpa_supplicant -P /var/run/wpa_supplicant.wlan0.pid -i wlan0 \
    -D nl80211,wext -c /etc/wpa_supplicant/wpa_supplicant.conf
## Kamera LED

Stäng av kamrans LED med följande rad i ''/boot/config.txt''. 

    disable_camera_led = 1
## Motion

>**2016-06-06**: I raspbian jessie behövs ingen egenkompilerad version av motion, standardpaketet fungerar utmärkt. 

Motion är arbetshästen i min övervakningskamera, programmet kan reagera på rörelse och börja spela in video eller ta bilder. 

Som standard har motion inte stöd för raspberry pi kameran så man måste använda en fork. Klona [detta förrådet](https://github.com/dozencrows/motion). 

    git clone -b mmal-test https://github.com/dozencrows/motion

Sedan behövs raspberry pi userland filer också, så klona [det förrådet](https://github.com/raspberrypi/userland). 

    git clone https://github.com/raspberrypi/userland

Installera sedan alla nödvändiga paket. 

    sudo apt-get build-dep motion
    sudo apt-get install cmake

När motion ska kompileras måste man ange sökvägen till userland filerna. 

    cd motion
    USERLANDPATH=/home/admin/userland cmake .
    make

Nu kan man installera binären också för att förenkla med startskript osv. 

    sudo install -m 0755 motion /usr/local/bin

