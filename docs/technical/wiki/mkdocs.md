# Welcome to MkDocs

* This page has been modified to document features specific to my own setup of mkdocs.
* For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## Inserting newlines

To force newlines you can insert the "invisible" unicode character U+FEFF as a paragraph.

```
U+FEFF

U+FEFF

U+FEFF

```

## Fancy ordered lists

This only works because of my ``extra_css`` setting in mkdocs. To see what it looks like in Markdown just click the [Edit](https://gitlab.com/stemid/stefan.midjich.name/edit/master/docs/technical/wiki/mkdocs.md) icon in the corner.

1. Just add another paragraph under the first bullet point and indent it with 4 spaces.

    [:material-account-circle:You](#fancy-ordered-lists) can even use fancy icons :fontawesome-regular-laugh-wink:.

2. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

    Aenean semper mattis nisi, a tempor urna interdum ac. Mauris vestibulum placerat commodo. Nunc enim odio, tincidunt sit amet nisi at, eleifend posuere ipsum.

    Integer nec lorem purus. Vivamus blandit blandit consectetur. Nulla faucibus eget ipsum ut maximus. Cras porta elementum mauris, in volutpat lacus pellentesque in.

3. Ett steg kan även inkludera kod om du skjuter in det dubbelt så många gånger.

        Alltså 8 mellanrum, eller kanske 4 tabbar om du fortfarande
        använder tabbar. Jag gör inte det.

## Internal links

I keep forgetting how to do these internal relative links but basically you ``[go back one step for articles in same category](../page_in_same_category#header-anchor)`` and ``[go back two steps for other categories](../../category/other_page)``.
