# Setting up OpenLDAP on CentOS

I'm doing this using Ansible but here are some notes on what I've learned.

# Concepts

## slapd


*  This service is the actual ldap service and it uses a seemingly complicated database called hdb or mdb.

*  CentOS 7 uses the deprecated hdb format.

*  The package on CentOS 7 automatically generates a slapd database in /etc/openldap/slapd.d based on the file /usr/share/openldap-servers/slapd.ldif. This would be step #9 in the OpenLDAP guide linked below.

*  So my recommendation is to purge that database after package install and make your own using slapadd.

# See also


*  [OpenLDAP Administrator's Guide: Quickstart](http://www.openldap.org/doc/admin24/quickstart.html)
