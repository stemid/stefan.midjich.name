# Hur du tystar ner en användare i Mastodon

Detta gör du från Mastodons webbgränssnitt.

1. Gå till vilket inlägg som helst gjort av den användaren du vill tysta ner.
2. Klicka på menyn med tre punkter längst ner till höger av inlägget.
3. Klicka på "Tysta @användaren".

![Silence user](/files/mastodon silence user.png)

Detta hänger även med om ni exporterar ert konto och flyttar till en annan instans, så blockeringen är "global" i Fediverse.
