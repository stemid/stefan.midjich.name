# Ansible Inledning

Ansible är ett deploymentverktyg som används för att automatisera installation av paket, konfigurationsfiler och andra åtgärder på flera servrar samtidigt. 

## Kommandon

I Ansible 1.8 finns flera kommandon men de tre som kommer användas mest beskrivs lite kort här. 

### ansible

Detta är ett kommando som bara kan göra en uppgift/modul åt gången. Läs mer [om moduler här](#moduler).

#### Exempel

Detta kör modulen setup för alla servrar i gruppen mkbnet. 

    ansible -m setup mkbnet

### ansible-playbook

Kör en så kallad spelbok, en fil full av uppgifter. Läs mer om [spelböcker här](#spelböcker).

#### Exempel

Kör spelboken mkbnet_test.yml-

    ansible-playbook mkbnet_test.yml

### ansible-vault

Hanterar krypterade filer. Detta är ibland nödvändigt för att lagra känslig information i variabler i ansible. 

#### Exempel

Skapa en krypterad fil, redigera den krypterade filen och till sist kör en spelbok som använder den krypterade filen. 

    ansible-vault create group_vars/mkbnet.yml
    ansible-vault edit group_vars/mkbnet.yml
    ansible-playbook --ask-vault-pass mkbnet_hemlisar.yml
## YAML

Ansible använder YAML-format för sina spelböcker. YAML är ett simpelt *nyckel & värde* språk där du anger en unik nyckel med kolon och sedan ett värde för den nyckeln. Det finns även stöd för listor. 

Här är ett exempel på en lista i YAML. 

	:::YAML
	- min_lista:
	  - 1
	  - 2
	  - 3


Du kan även göra namngivna medlemmar i listor. 

	:::YAML
	- min_lista:
	  - namn: ett
	    värde: 1
	  - namn: två
	    värde: 2


Användning av YAML i Ansible är väldigt nära besläktat med Python så för att hänvisa till en medlem i den listan kan man skriva t.ex. ``min_lista.namn``. 

## Spelböcker

Spelböcker är en rad instruktioner som ska utföras. Ansible använder SSH för att ansluta till servrar så i princip loggar den in med SSH, överför skripts som ska motsvara din spelbok och sedan kör dem på servern. 

En spelbok är en fil som kan innehålla flera *spelningar*, eller plays som Ansible kallar det i sin dokumentation. Så här kan en spelbok med två spelningar se ut. 

	:::YAML
	---
	- hosts: mkbnet
	  sudo: True
	  roles:
	    - authorized_keys
	    - bootstrap_common
	    - scollector
	    - nagios_agent
	
	- hosts: ['mkbnetnaredir']
	  user: mkbnetadm
	  sudo: True
	  roles:
	    - dnsmasq
	  tasks:
	    - name: setup rsyslog
	      copy: src=etc/rsyslog.conf dest=/etc/rsyslog.conf owner=root group=root mode=0644


En snabb förklaring av vad som syns här. 

``hosts:`` låter dig ange en grupp av servrar som du vill köra kommandon på. Värdet kan antingen vara namnet av en grupp, som ovan är *mkbnet*, eller en lista av enskilda värdnamn, som ovan är ``['mkbnetnaredir']``. Läs mer om servrar och servergrupper under [#Inventering](#Inventering). 

``sudo:`` anger om alla kommandon ska använda sudo eller inte. 

``roles:`` är en lista av roller som ska appliceras på alla ``hosts:`` i den körningen. Läs mer om roller under [#Roller](#Roller). 

``tasks:`` är en lista av uppgifter som kan köras på maskinerna. 

## Roller

En roll är ett sätt att gruppera flera uppgifter som är gemensamma för många olika maskiner. Så om du t.ex. vill göra en generisk installation av en mjukvara så gör du det i en roll. Roller måste vara så generiska som möjligt och inte ha någon kund eller server-specifik information. 

Alla roller lagras i mappen ``roles`` i gitförrådet. 

En roll kan ses som ett paket så det finns en struktur av mappar och filer. Här är ett exempel på en roll. 

```
- epel/
  - tasks/main.yml
  - defaults/main.yml
 ```

Rollen epel installerar EPEL paketförrådet på RedHat-system. Den består av två mappar som har var sin fil. main.yml är som en standardfil för alla mappar i en roll. Du kan ha fler filer där som inkluderas av main.yml men det är alltid main.yml som körs av ansible. 

``tasks/main.yml`` är en vanlig spelbok med en uppgift. 

	:::YAML
	---
	- name: install EPEL repository on RedHat
	  yum: name={{ epel_url }} state=present
	  when: ansible_os_family == 'RedHat'


``defaults/main.yml`` är bara en samling variabler, standardvariabler som kan åsidosättas av andra variabler. 

	:::YAML
	epel_url: http://ftp.lysator.liu.se/pub/epel/6/i386/epel-release-6-8.noarch.rpm


Just variabeln epel_url kan man se i spelboken ovan på rad 3 inom två parenteser. 

## Variabler

Precis som varje roll kan ha sina egna standardvariabler så kan detta även gälla för servrar och servergrupper. 

Enskilda servrar kan ha sina egna variabler i ``host_vars/servernamn.yml``, medan grupper av servrar kan ha gemensamma variabler i ``group_vars/gruppnamn.yml``. 

Här är ett exempel från filen ``host_vars/mkbnetnaredir.yml``. 

```yaml
dnsmasq_config:
  - mkbnet.conf
```

Och här är ett exempel från filen ``group_vars/mkbnet.yml``. 

```yaml
host_customer: MKB Net
host_local: False
ansible_ssh_user: mkbnetadm
```

Variabler har en prioriteringsordning, här visar jag ett exempel med högst prioritet först. 

*  playbook vars
*  host_vars
*  group_vars
*  role vars

Läs mer om variablers prioriteringsordning och se en komplett lista av den [här i ansibles dokumentation](http://docs.ansible.com/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable). 
## Inventering

Ansible använder en fil för att hålla reda på servrar och servergrupper. Den anges antingen av ``-i`` argumentet till ansible-kommandon, eller av miljövariabeln ANSIBLE_HOSTS. 

Filen följer INI-format som är mycket simpelt. Här är ett exempel. 

	:::INI
	[mkbnet:children]
	mkbnet-db
	mkbnet-netadmin
	
	[mkbnet-db]
	mkbnetdb02
	mkbnetdb03
	
	[mkbnet-netadmin]
	mkbnetnadb
	mkbnetnamon1
	mkbnetnamon2
	mkbnetsrv01
	mkbnetsrv02
	mkbnetsrv03
	mkbnetsrv04
	mkbnetnaredir


Här visas tre grupper, två av dem har servrar i sig men den första gruppen ``mkbnet:children`` är en sorts huvudgrupp för de två andra. 

## Moduler

Moduler är allt som ansible kan göra på servrar. I de flesta installationer kan du hitta dem i ``/usr/share/ansible``. De är kategoriserade under olika mappar baserat på deras funktionalitet. 

När man skriver uppgifter så använder man alltid moduler. Här är ett exempel på en uppgift som använder copy-modulen för att kopiera en fil till servern. 

	:::YAML
	- name: copy file to server
	  copy: src=files/test.conf dest=/home/stemid/files/test.conf owner=stemid group=stemid mode=0640


Allt som är efter kolonet är argument till modulen, så det är vanligt att man öppnar en modulfil för att läsa dess argument. Alla moduler i ansible är skrivna i Python, eftersom det är vanliga textfiler så står det klara exempel på argument och hur man använder dem i början av filerna. Modulen copy ligger t.ex. i ``/usr/share/ansible/files/copy``. 

Moduler kan även användas direkt utan en spelbok, så här. 

    ansible -m copy -a 'src=files/test.conf dest=/home/stemid/files/test.conf owner=stemid group=stemid mode=0640' mkbnet

Detta kör copy-modulen på samma sätt som ovan, mot servergruppen mkbnet. 

## Se också

*  [Ansibles hemsida med dokumentation på engelska](http://ansible.com)
*  [Ansible: prioriteringsordning av variabler](http://docs.ansible.com/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable)
