# Ansible mot Windows

Här dokumenterar jag hur jag får igång Ansible mot Windows servrar, Ansible har redan [bra dokumentation](http://docs.ansible.com/ansible/intro_windows.html) så jag går bara igenom det jag hade problem med. 


# Kerberos

Se till att ''/etc/krb5.conf'' kan läsas av användaren du kör ansible som, alltså 0644 fungerar bra oftast.

Så här kan min krb5.conf se ut. 

```
[realms]
DOMAIN.INET = {
 kdc = vmwin-dc01.domain.inet
 kdc = vmwin-dc02.domain.inet
}

[domain_realm]
.domain.inet = DOMAIN.INET
domain.inet = DOMAIN.INET
```

Glöm inte heller att ha pythons kerberos-paket installerat, antingen i virtualenv eller globalt, så att ansible kan nå det. Annars försöker Ansible inte använda kerberos och fungerar bara med lokala konton. 

Du **måste** använda **exakt** samma namn som serverns objektnamn i AD. 

Har du ingen DNS till det nät du loggar in på måste du lägga in alla hostar i din ''/etc/hosts'' manuellt. 
# Konfiguration av Ansible

För exempel skapar vi en hostgrupp som heter windows och lägger in lite servrar i den. Jag har ingen DNS konfad från min arbetsstation till det AD dit jag ansluter så jag redigerar även ''/etc/hosts'' och lägger till följande hostar. 

    [windows]
    vmwin-dc01.domain.inet
    vmwin-vc01.domain.inet

Testa med följande exempel. 

    $ ansible windows -m win_ping --limit vmwin-vc01.domain.inet --ask-vault-pass
# Se också


*  [Ansibles dokumentation](http://docs.ansible.com/ansible/intro_windows.html)
