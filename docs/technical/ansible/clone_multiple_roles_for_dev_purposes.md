# Clone multiple roles for dev purposes

Ansible makes it easy for users of playbooks to clone the required roles using requirements.yml. But it's much harder for developers because each role that ansible-galaxy clones will be missing its .git metadata.

So the whole requirements.yml and ansible-galaxy method is useless for developers sharing code or using different workstations.

I found a neat solution using .gitconfig and bash.

## Step 1: .gitconfig

This is a really neat trick in general, not only when used with Ansible. It tells git to always replace one repo url with another. So you can recognize your own urls where you have SSH access and replace https with ssh.

For example, my own [Ansible repos on Gitlab](https://gitlab.com/stemid-ansible) look like this in ''$HOME/.gitconfig''.

    [url "git@gitlab.com:stemid-ansible"]
    insteadOf = https://gitlab.com/stemid-ansible

## Step 2: ansible-galaxy

    $ ansible-galaxy install -g -r requirements.yml

The ''-g'' option will keep metadata, including the .git dir.

>**2019-11-11** this old solution is no longer required and kept around for nostalgia.

Go to your roles dir and run this simple oneliner.

    $ pushd ../../roles
    $ while read -r url; do git clone $url; done < <(grep src ../playbooks/ansible-prometheus/requirements.yml | cut -d' ' -f3)
    $ popd

Obviously replace the paths with your own but in this context ''../playbooks/ansible-prometheus/requirements.yml'' is the path back to the requirements.yml file with the roles you want to install.
