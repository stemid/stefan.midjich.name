# Ansible YAML multiline strings

Some styles to remember.

*  ''>'' - Most common multiline string, does not preserve new lines
*  ''|'' - Preserves newlines
*  ''>-'', ''|-'' - As above but strips trailing new line

## See also

*  [https://stackoverflow.com/a/21699210/1662806](SO answer describing types of multiline strings)
