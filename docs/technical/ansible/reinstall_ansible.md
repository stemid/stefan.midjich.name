# Reinstall Ansible

This is just one method of reinstalling Ansible that I follow on my systems. You can also use distro package managers or Homebrew. 


*  Ansible is installed in virtualenv under ''~/.venv/ansible''.

## Deactivate and remove virtauelenv

    (ansible) $ deactivate
    $ rm -rf ~/.venv/ansible

## Re-create and activate virtualenv

    $ virtualenv ~/.venv/ansible
    $ source ~/.venv/ansible/bin/activate


## Install basic packages

This is recommended practice for virtualenv under python 2.7.

    $ pip install -U setuptools pip readline
    $ pip install -U ipython

>I just like to have iPython available and under 2.7 it requires the latest pip version before installing.

## Install Ansible from git

    $ git clone https://github.com/ansible/ansible
    $ cd ansible
    $ git checkout stable-2.4
    $ pip install -r requirements.txt
    $ python setup.py install
