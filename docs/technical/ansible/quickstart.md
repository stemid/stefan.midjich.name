# Quickstart for Ansible

This does not cover how to write Ansible playbooks, see my older guides for that. Or the Ansible docs.

This is a quick setup guide for Ansible.

## Use ~/.ansible.cfg

Here's an example based on my own file.

```
[defaults]
roles_path = ~/Ansible/roles:./roles
ansible_managed = Ansible managed, DO NOT EDIT MANUALLY: {file} modified on %Y-%m-%d %H:%M:%S by {uid} on {host}
remote_tmp = ~/.ansible/tmp
gathering = smart

#fact_caching = redis
#fact_caching_timeout = 86400
#fact_caching_connection = localhost:6379:0

[ssh_connection]
pipelining = True
scp_if_ssh = True
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no
host_key_checking = False

[inventory]
enable_plugins = advanced_host_list, host_list, ini, yaml
#cache = True
```

You might want to change the following;

* ``roles_path`` to your own path where you want your roles to reside.
* ``fact_caching`` uncomment these three lines only if you have a redis server and the python redis module installed.

### Redis cache on Fedora

    $ sudo dnf install redis
    $ pip install --user redis

## See also

* [Ansible best practices](ansible_best_practices.md)
