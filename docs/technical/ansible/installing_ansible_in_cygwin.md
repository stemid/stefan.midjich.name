# Installing Ansible in Cygwin on Windows 7

This document describes installing and running Ansible in Cygwin on Windows 7.
## Step 1: Download cygwin

Download the standard cygwin setup.exe and run it.

## Step 2: Select cygwin packages

Select to install the following packages.


*  python2-setuptools

*  python

*  python-devel

*  python-crypto

*  gmp

*  libgmp-devel

*  libffi-devel

*  openssl

*  openssl-devel

*  git

*  openssh

*  gcc-core

*  gcc-g++

*  make

And any other packages you might need personally.

## Step 3: Create virtualenv

If file names differ try tabbing to auto-complete them in your cygwin.

    $ easy_install-2.7 pip
    $ pip install virtualenv
    $ mkdir -p .venv/ansible2
    $ virtualenv .venv/ansible2
    $ source .venv/ansible2/bin/activate
    
Ensure you activate the virtualenv for the next step so you don't install Ansible globally.

## Step 4: Install Ansible

As of writing this, 2017-02-24, pycrypto suffers a bug on cygwin that prevents it from compiling. 

    $ CFLAGS="-g -O2 -D_BSD_SOURCE" pip install -U pycrypto
    $ pip install ansible

## Step 5: Disable ControlMaster

Ansible in Cygwin does not work with ControlMaster so you must disable it in ansible.cfg.

    [ssh_connection]
    ssh_args = -o ControlMaster=no
