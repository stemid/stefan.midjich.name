# Render a template to a file

If you want to see what a template will look like before deploy you can render it to a file like this.

    $ ansible -i inventory/hosts.default -m template -a "src=alerts/default/AvailableMemory.yml dest=test.yml" all

