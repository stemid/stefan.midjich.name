# Decrypt an inline variable

This was a challenge because ansible-vault normally only works on vault files that contain no yaml. But I've been making a habit of using ansible-vault blocks inline in yaml variables.

Example of decrypting a password in a ``pg_hba`` list stored inside ``vars/stage.yml``.

    $ ansible -i inventory/hosts.stage -e @vars/stage.yml \
      -m debug -a 'msg={{ pg_hba[3].password }}' \
      --vault-id=cia@.vault-password-file all
