# Ansible in Gitlab CI Pipeline

These are some notes on using the latest Ansible with the latest Gitlab CI pipelines, as of **2019-06-29** at least.

## File variables

Go into Settings, CI/CD, Variables and create a variable of type File. This means the **value of the variable will actually be the path to a temporary file containing the contents of the variable**.

So a pipeline using Ansible might have a script command like this.

```
- ansible-playbook -i "$ANSIBLE_INVENTORY_STAGING" deploy.yml -t bootstrap --vault-id="project@$ANSIBLE_VAULT_PASSWORD_FILE"
```

Can also be used for private ssh keys by for example creating a variable called SSH_PRIVATE_KEY in Gitlab Variables and then using it as a File path.

```

- eval $(ssh-agent -s)
- chmod 0600 "$SSH_PRIVATE_KEY"
- ssh-add "$SSH_PRIVATE_KEY"
```

## Example .gitlab-ci.yml

This file is from a [Siptrackd](https://github.com/sii/siptrackd) repo so aims to test and deploy siptrackd from its repo.

```yaml
image: python:2.7-stretch

variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"


stages:
 1.  test
 2.  deploy


cache:
    paths:
    - .cache/pip
    - _trial_temp


before_script:
 1.  python -V


test_package:
    stage: test
    script:
    - apt-get update -y
    - apt-get install libldap2-dev libmariadb-dev libsasl2-dev -y
    - pip install -r requirements.txt
    - python setup.py install
    - python -m twisted.trial tests


update_staging:
    stage: deploy
    script:
    # prep environment for installing and running ansible
    - eval $(ssh-agent -s)
    - chmod 0600 "$SSH_PRIVATE_KEY"
    - ssh-add "$SSH_PRIVATE_KEY"
    - mkdir -p ~/.ssh
    - cp "$SSH_CONFIG" ~/.ssh/config
    - chmod 0600 ~/.ssh/config

    # install and run ansible
    - pip install ansible
    - echo git clone "$ANSIBLE_GIT_REPO" ansible
    - git clone "$ANSIBLE_GIT_REPO" ansible
    - pushd ansible
    - ansible-galaxy install -r requirements.yml
    - ansible-playbook -i "$ANSIBLE_INVENTORY_STAGING" update.yml -t backend
    - ansible-playbook -i "$ANSIBLE_INVENTORY_STAGING" update.yml -t frontend
    - popd

    only:
    - staging

```

### Variables


*  SSH_PRIVATE_KEY - A private key created for deploying this siptrackd software, it's created as a File variable in Gitlab.
*  SSH_CONFIG - A ready made config for all the hosts that we need to talk to, and being able to set various options to be used when git clones over ssh.
*  ANSIBLE_GIT_REPO - The git repo of the Playbooks that can install the siptrackd service.
*  ANSIBLE_INVENTORY_STAGING - Path to an inventory file specific for the staging environment. This inventory file can set some top level variables to control how the service is setup and separated from other instances on the same server.
