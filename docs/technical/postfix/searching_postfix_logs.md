---
title: Searching Postfix logs
summary: The tools I use to make searching Postfix logs easier
date: 2021-06-07
---

# Searching Postfix logs

Postfix logs on a busy relay can be confusing because the sessions are spread out in the logs, so my natural instinct is to want to gather them for a better overview. This can be done with ELK and in my opinion an ELK is the best long term solution, but here I will go through some relatively quick solutions that don't require new infrastructure.

## Enable long session IDs

First of all this will make many things work better, [according to docs this was added after Postfix 2.8](http://www.postfix.org/postconf.5.html#enable_long_queue_ids).

    postconf enable_long_queue_ids=yes

## postfix-log-parser

This is a wonderful tool that [Naoto Ishizawa on Github](https://github.com/youyo/postfix-log-parser) made in Golang. But at time of writing the project seems abandoned so I've added support for long session IDs and fixed a bug with some postfix logs in [the main branch of my own fork on Github](https://github.com/stemid/postfix-log-parser/tree/main).

### How to build on CentOS

Quick instructions for those who know what they're doing, requires ``$HOME/.local/bin`` in your PATH.

    sudo yum install golang
    curl -sLo - https://github.com/Songmu/goxz/releases/download/v0.7.0/goxz_v0.7.0_linux_amd64.tar.gz | tar -xvzf -
    cp goxz_v0.7.0_linux_amd64/goxz .local/bin/
    git clone -b main https://github.com/stemid/postfix-log-parser
    cd postfix-log-parser
    make build
    tar -xvzf pkg/postfix-log-parser_linux_amd64.tar.gz -C /tmp
    cp /tmp/postfix-log-parser_linux_amd64/postfix-log-parser ../.local/bin/

### How to use postfix-log-parser with jq

This is where the fun starts, using jq is about as strange as git was when I first started. I'll try to add more examples as they pop up in daily work.

This command will simply spit out nicely formatted json you can inspect, each session is one object.

    sudo cat /var/log/maillog | postfix-log-parser | jq

#### Filter by multiple values

    sudo cat /var/log/maillog | postfix-log-parser | \
        jq '. | select(.from == "noreply.appen@domain.tld" and .client_ip == "10.11.12.13")'

#### String jq filters together in Bash

Use ``-r`` to output raw json you can parse with another call to jq.

    sudo cat /var/log/maillog | postfix-log-parser | \
        jq -r '. | select(.from == "noreply.appen@domain.tld" and .client_ip == "10.11.12.13")' | \
        jq '.messages[].to'

## Better Postfix queue management

I also wrote [this tool on Gitlab snippets](https://gitlab.com/-/snippets/1981318) to easier search the Postfix queue, now that the postqueue command returns JSON.
