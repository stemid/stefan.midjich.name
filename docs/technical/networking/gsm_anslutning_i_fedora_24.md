# GSM Anslutning i Fedora 24

Gnomes grafiska skal för att konfigurera en GSM anslutning i Fedora 24 var inte tillräckligt för mig, antingen ofärdigt eller buggigt, man kom inte framåt. 

Här visas hur du konfar en GSM anslutning i NetworkManager via nmcli istället. 

Jag har gjort detta på en Thinkpad X260 med inbyggd GSM-modul från Huawei som listades i lsusb. 

    $ lsusb
    ...
    Bus 001 Device 007: ID 12d1:15c1 Huawei Technologies Co., Ltd. 

# Konfiguration i nmcli

Se till att du inte har några gamla anslutningar kvar med typen gsm som kanske stör dig. När jag använde Gnomes gränssnitt t.ex. så las det till flera anslutningar som inte visades direkt. 

    $ nmcli con show
    NAMN              UUID                                  TYP              ENHET   
    Telia 3G          40c7aac0-a039-......................  gsm              ttyUSB0 

Manipulera en anslutning antingen med UUID eller namn, UUID är mer unikt så klart. 

    $ nmcli con delete 'Telia 3G'

## Skapa anslutning i nmcli

Nmcli är interaktivt så använd ''help'', ''print'' och läs noga i terminalen för att komma framåt. Jag kan bara ge en snabbguide av hur jag gjorde. 

    $ nmcli con edit con-name 'Telia 3G'
    Ange anslutningstyp: gsm
    ...
    nmcli> goto ipv4
    nmcli ipv4> set method auto
    nmcli ipv4> back
    nmcli> goto gsm
    nmcli gsm> set number *99#
    nmcli gsm> set apn online.telia.se
    nmcli gsm> back
    nmcli> goto connection
    nmcli connection> set autoconnect false
    nmcli connection> back
    nmcli> print
    ...
    nmcli> save
    nmcli> quit

## Aktivera anslutning i nmcli

    $ nmcli con up 'Telia 3G'
    Det krävs PIN-kod för den mobila bredbandsenheten
    ...
    Anslutning aktiverades (aktiv sökväg för D-Bus: /org/freedesktop/NetworkManager/ActiveConnection/2)

    
# Felsökning

Den relevanta tjänsten i Fedora heter ModemManager och det är även där loggarna finns. 

    $ sudo journalctl -flau ModemManager

Se även till att den tjänsten är igång och om du stöter på problem kan den alltid startas om. 

```
`<warn>`  couldn't load IMSI: 'SIM PIN required'
`<warn>`  couldn't load Operator identifier: 'Cannot load Operator ID without IMSI'
`<warn>`  couldn't load list of Own Numbers: 'SIM PIN required'

```

Raderna ovan är ett bra tecken, då ska du bara behöva konfa anslutningen och starta den för att få upp en ruta som frågar dig om PIN-kod för SIM-kort. 

Så här kan det se ut när anslutningen lyckas och är uppe, den ska även visas i Gnomes taskbar vid din Wifi-anslutning och du ska så klart få en ny route och ett nytt gränssnitt med en IP.

```
`<info>`  Modem /org/freedesktop/ModemManager1/Modem/0: 3GPP Registration state changed (unknown -> registering)
`<info>`  Modem /org/freedesktop/ModemManager1/Modem/0: 3GPP Registration state changed (registering -> home)
`<info>`  Modem /org/freedesktop/ModemManager1/Modem/0: state changed (enabling -> registered)
`<info>`  Simple connect state (5/8): Register
`<info>`  Simple connect state (6/8): Bearer
`<info>`  Simple connect state (7/8): Connect
`<info>`  Modem /org/freedesktop/ModemManager1/Modem/0: state changed (registered -> connecting)
`<info>`  Modem /org/freedesktop/ModemManager1/Modem/0: state changed (connecting -> connected)
`<info>`  Simple connect state (8/8): All done
```

Detta är ett lite jobbigare fel som dök upp efter att jag tagit ut och stoppat in SIM-kortet. 

    `<warn>`  Modem couldn't be initialized: Couldn't check unlock status: SIM not inserted

Allt jag gjorde för att lösa det var att stänga min laptop, gå och lägga mig, sova i 8 timmar ;) och när jag vaknade startade jag om ModemManager (igen) och problemet var löst. 

Ett sista tips är att du kan se vilken PIN-kod du tidigare lagrat på en anslutning med -s argumentet. 

    $ nmcli con show -s 'Telia 3G'
    ...
    `<hemlisar>`

# Se också


*  [Fedora Wiki om nmcli](https://fedoraproject.org/wiki/Networking/CLI)

*  [AskUbuntu fråga om GSM anslutning](https://askubuntu.com/questions/537406/configure-gsm-connection-using-shell-nmcli)

*  [Telia instälnningar för MMS och Surf](https://www.telia.se/privat/support/info/installningar-for-mms-och-surf-ovriga-mobiler)
