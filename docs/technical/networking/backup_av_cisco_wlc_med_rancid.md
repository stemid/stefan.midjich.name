# Backup av Cisco WLC med RANCID

Jag använder mig av;


*  Ubuntu 16.04

*  Rancid 3.3

*  Git som förråd för backupen

# Installation

Installera som vanligt. 

    $ sudo apt-get install rancid

# Global Konfiguration

Denna konfig görs globalt, redigera först ''/etc/rancid/rancid.conf'' och ändra följande rader.

```

CVSROOT=git://git@gitserver/rancid; export CVSROOT
RCSSYS=git; export RCSSYS
LIST_OF_GROUPS="gruppnamn"; export LIST_OF_GROUPS
MAX_ROUNDS=1; export MAX_ROUNDS

```


*  Ändra gitserver till din gitserver.

*  Ändra gruppnamn till en passande grupp som är namnet av ditt git-förråd.

*  ''MAX_ROUNDS=1'' är en workaround för tillfället eftersom ciscowlc.pm inte verkar rapportera slutet korrekt till ''/usr/lib/rancid/bin/rancid'' och därför fortsätter den försöka 4 gånger och höjer chansen att inte få en komplett konfig överförd på grund av nätverks-latens troligtvis.

Redigera ''/etc/rancid/rancid.types.conf'' så den ser ut så här.

```
cisco-wlc8;script;rancid -t cisco-wlc8
cisco-wlc8;login;wlogin -noenable -t 120
cisco-wlc8;module;ciscowlc
cisco-wlc8;inloop;ciscowlc::inloop
cisco-wlc8;command;ciscowlc::ShowUdi;config paging disable
cisco-wlc8;command;ciscowlc::ShowUdi;show udi
cisco-wlc8;command;ciscowlc::ShowSysinfo;show sysinfo
cisco-wlc8;command;ciscowlc::ShowConfig;show run-config

```

''ciscowlc::ShowUdi;config paging disable'' är ett fulhack för att kunna köra kommandot ''config paging disable'' först. Annars vill Cisco mata ut all data sida för sida genom att trycka på Enter flera gånger.

Redigera ''/etc/hosts'' och lägg till din WLC enhets IP-adress där. T.ex.:

```

172.16.55.45 grupp-wlc01

```

# Lokal konfiguration

Bli rancid-användaren och gör resten av konfigurationen i det kontot. 

    $ sudo su rancid
    $ cd; pwd;
    /var/lib/rancid

## SSH

Skapa en SSH-nyckel för rancid som ska användas i git-förrådet. 

    $ mkdir .ssh
    $ ssh-keygen -N "" -f .ssh/id_rsa

> Installation av ssh-nyckeln i git-förrådet visas ej här.

## .cloginrc

Redigera ''.cloginrc'' med följande info.

```

# Gruppnamn

add password grupp-wlc01 hemligt. hemligt.
add noenable grupp-wlc01

```

## Git

Skapa ett tomt git-förråd för din grupp.

    $ git init gruppnamn
    $ cd gruppnamn/
    $ git remote add origin git@gitserver:rancid/gruppnamn

Notera sökvägen till git-förrådet, ska stämma överens med sökvägen från ''/etc/rancid/rancid.conf''.

## PATH

Skapa en ''.bashrc'' åt rancid och se till att PATH inkluderar alla rancids verktyg som ligger under ''$HOME/bin''.

    $ echo 'PATH=$PATH:$HOME/bin' >> .bashrc

>''$HOME/bin'' är faktiskt en symlänk till ''/usr/lib/rancid/bin''.

# Testa

Kör detta som rancid-användaren.

    $ echo $PATH
    /bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin:/sbin:/usr/sbin:/var/lib/rancid/bin
    $ rancid -d -t cisco-wlc8 grupp-wlc01

# Installera

Installera i ''/etc/cron.d/rancid'' med en lägre frekvens än normalt eftersom run-config på WLC ofta ändras. Den visar mer än bara konfiguration. 

```

1 1 * * * rancid /usr/bin/rancid-run gruppnamn

```

Ange gruppnamnet för att inte begränsa hela Rancid till en låg frekvens. Andra enheter kan ha egna cron-rader. 

# Fallgropar

## Inget slut på configen

''/usr/share/perl5/ciscowlc.pm'' ska rapportera ''$found_end=1'' till ''/usr/lib/rancid/bin/rancid'' så att den vet att configen är överförd men detta sker inte och därför fortsätter rancid köra enligt MAX_ROUNDS värdet i ''/etc/rancid/rancid.conf''. 

Jag märkte att efter flera försök så hände det att konfigurationen inte överfördes helt. Men för det mesta har den lyckats överföra allt på första försöket. 
