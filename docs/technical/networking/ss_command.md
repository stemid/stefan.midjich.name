# ss command

The ``ss`` command is going to replace ``lsof`` in my toolbox so here are some conversion recipes.

Many of these should be run as root but I'm not going to write out sudo for all of them because it's superfluous.

Some basic arguments;

* ``-p`` Show process info
* ``-l`` Listening sockets
* ``-n`` Do not resolve addresses or service names

## Listening ports and their PIDs

These all display the same info.

    $ lsof -Pni | grep LISTEN
    $ ss -plnf inet
    $ ss -pln4

## Listening TCP ports and their PIDs

Because only TCP has a state, this uses a **state filter**.

    $ ss -pn4 state listening
    $ ss -plnt

## Listening UDP ports and their PIDs

    $ ss -plnu

## Unix sockets and their PIDs

    $ ss -plnf unix
    $ ss -plnx

## TCP connections and their related PIDs

    $ lsof -Pni
    $ ss -pnf inet
    $ ss -pn4

## Filters

ss also has some powerful filtering, reminding me of BPF but not the same syntax. The two last arguments, in order, are a state filter and a custom ss filter. The state filter starts with the word ``state`` so you can skip it by simply omitting that word.

### State filters

UDP will never show up if you specify a state filter, because UDP is stateless.

    $ ss -an4 state listening

### Web server connections

    $ ss -pnf inet state all 'dport = :http or dport = :https'

### Match a source port for all protocols

    $ ss -pn state all 'sport = :8000'

### Filter out TCP connections

``-A QUERY`` is not the traditional filter but some sort of custom query.

    $ ss -aA 'all,!tcp'
