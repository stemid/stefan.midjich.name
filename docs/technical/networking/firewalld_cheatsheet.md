# Firewalld cheatsheet

**Warning for personal opinion:** I thought Firewalld seemed unnecessary at first. Zones are great for personal computers where you can separate your Home, Work and VPN connections with zones.

But for servers I failed to see the use. Now I've changed my mind and think it's fine as long as you use it for simple stuff like regulating service traffic on a server for example.

Anything more advanced and I disable firewalld and use IPtables directly.

# Open port from specific source address

Create a "rich rule".

    firewall-cmd --add-rich-rule=' \
    rule family="ipv4" source address="10.160.80.249/32" \
    port protocol="tcp" port="9242" accept'

# Allow certain source address access to service

Can be done without rich rules by creating a new zone for your service.

    $ sudo firewall-cmd --new-zone=mysqlzone --permanent
    $ sudo firewall-cmd --reload
    $ sudo firewall-cmd --zone=mysqlzone --add-servcice=mysql
    $ sudo firewall-cmd --zone=mysqlzone --add-source=192.168.22.80

Now you only allow connections from 192.168.22.80 in the mysqlzone to your mysql service.

This makes the first rich rule on the page unnecessary.

    $ sudo firewall-cmd --new-zone=customzone --permanent
    $ sudo firewall-cmd --zone=customzone --add-port=9242/tcp
    $ sudo firewall-cmd --zone=customzone --add-source=10.160.80.249
