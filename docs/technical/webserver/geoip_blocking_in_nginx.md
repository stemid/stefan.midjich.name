# GeoIP blocking in Nginx

This is how I setup GeoIP blocking in Nginx.

Unfortunately as of recently (2020-01) epel have removed the geoip module from their repo because of the uncertainty of the Maxmind GeoIP database. I still use it, and build my own, because it solves a very important problem for me. But it's not future proof and a new solution is needed.

## Installing build dependencies for Nginx

    $ sudo yum install '@Development Tools' GeoIP-devel gperftools-devel gd-devel perl-ExtUtils-Embed

## Building nginx

You must build nginx exactly like the CentOS nginx package in epel. Find out the exact configure arguments by running ``nginx -V``. This here is an example from CentOS 7 around late 2019, nginx 1.16.1.

```
$ ./configure --prefix=/usr/share/nginx --sbin-path=/usr/sbin/nginx \
  --modules-path=/usr/lib64/nginx/modules --conf-path=/etc/nginx/nginx.conf \
  --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log \
  --http-client-body-temp-path=/var/lib/nginx/tmp/client_body \
  --http-proxy-temp-path=/var/lib/nginx/tmp/proxy \
  --http-fastcgi-temp-path=/var/lib/nginx/tmp/fastcgi \
  --http-uwsgi-temp-path=/var/lib/nginx/tmp/uwsgi \
  --http-scgi-temp-path=/var/lib/nginx/tmp/scgi --pid-path=/run/nginx.pid \
  --lock-path=/run/lock/subsys/nginx --user=nginx \
  --group=nginx --with-file-aio --with-ipv6 --with-http_ssl_module \
  --with-http_v2_module --with-http_realip_module --with-stream_ssl_preread_module \
  --with-http_addition_module --with-http_xslt_module=dynamic \
  --with-http_image_filter_module=dynamic --with-http_sub_module \
  --with-http_dav_module --with-http_flv_module --with-http_mp4_module \
  --with-http_gunzip_module --with-http_gzip_static_module \
  --with-http_random_index_module --with-http_secure_link_module \
  --with-http_degradation_module --with-http_slice_module \
  --with-http_stub_status_module --with-http_perl_module=dynamic \
  --with-http_auth_request_module --with-mail=dynamic --with-mail_ssl_module \
  --with-pcre --with-pcre-jit --with-stream=dynamic --with-stream_ssl_module \
  --with-google_perftools_module --with-debug \
  --with-cc-opt='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -m64 -mtune=generic' \
  --with-ld-opt='-Wl,-z,relro -specs=/usr/lib/rpm/redhat/redhat-hardened-ld -Wl,-E' \
  --with-http_geoip_module=dynamic
$ make
$ sudo cp ./objs/ngx_http_geoip_module.so /usr/lib64/nginx/modules/
```

# Configuring nginx to use GeoIP

Once you have the module working, and the GeoIP.dat file from Maxmind, you can create this config in ``/etc/nginx/conf.d/geoip.conf``.

```
geoip_country /usr/share/GeoIP/GeoIP.dat;
map $geoip_country_code $allowed_country {
  default no;
  EU yes;
  SE yes;
  DK yes;
  NO yes;
  FI yes;
  DE yes;
  NL yes;
  GB yes;
  PL yes;
  HR yes;
  CH yes;
  FR yes;
  ES yes;
  SK yes;
  CZ yes;
  AT yes;
  RS yes;
  ME yes;
  IT yes;
  GR yes;
  AL yes;
  EE yes;
  FO yes;
  IS yes;
  MK yes;
  PT yes;
  IE yes;
  #RO yes;
  #TH yes;
}
```

And then you can setup a location block that looks like this for example.

```
location /sign_up {
  if ($allowed_country = no) {
    return 401;
  }
  try_files $uri @proxy;
}
```
