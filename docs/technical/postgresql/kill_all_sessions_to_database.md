# Kill all user sessions to a single database in PostgreSQL

This is something I found myself doing a few times and it's not obvious.

Start a super user shell and run this, replace <my-database> with your database name.

```
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = '<my-database>'
  AND pid <> pg_backend_pid();
```
