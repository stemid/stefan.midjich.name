---
title: How to build Alacritty on Fedora 34
date: 2021-07-02
summary: Building and setting up Alacritty from source with Cargo on Fedora 34
---

# How to build Alacritty on Fedora 34

## Install dependencies

    sudo dnf install '@Development tools' cmake gcc-c++ libxcb-devel \
        libxkbcommon-devel fontconfig-devel cargo

## Build and install Alacritty

    cargo install alacritty

## Install icon and Gnome .desktop file

Here is an example using files from a specific release, you can figure out yourself how to use these paths with other files.

    mkdir -p ~/.icons ~/.config/alacritty ~/.local/share/applications
    curl -sLo ~/.icons/Alacritty.svg \
        https://github.com/alacritty/alacritty/releases/download/v0.8.0/Alacritty.svg
    curl -sLo ~/.local/share/applications/Alacritty.desktop \
        https://github.com/alacritty/alacritty/releases/download/v0.8.0/Alacritty.desktop
    curl -sLo ~/.config/alacritty/alacritty.yml \
        https://github.com/alacritty/alacritty/releases/download/v0.8.0/alacritty.yml

## Setup Nord color scheme

Easiest is to just append the nord.yml from [this Github repo](https://github.com/arcticicestudio/nord-alacritty) to your alacritty.yml and comment out the old colors property.

    curl -sL https://raw.githubusercontent.com/arcticicestudio/nord-alacritty/develop/src/nord.yml \
        >> ~/.config/alacritty/alacritty.yml
    vim ~/.config/alacritty/alacritty.yml
    # and comment out the old colors: statement and everything that belongs to it.
