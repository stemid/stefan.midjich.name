# Installing Fractal on Fedora 31

[Fractal](https://wiki.gnome.org/Apps/Fractal) is a Gnome GUI client for [Matrix.org](https://matrix.org/faq/) written in Rust.

As of writing it has no RPM package from official sources so here is how I built it on Fedora 31.

# Clone git repo

    $ git clone https://gitlab.gnome.org/GNOME/fractal

# Switch to a release tag

The master branch required libraries not available in Fedora 31 so as of writing I selected the latest release, 4.2.2.

    $ git checkout 4.2.2

# Dependencies

    $ sudo dnf install cairo cairo-devel meson cargo gdk-pixbuf2-devel gtk3-devel libhandy-devel \
    gstreamer1-devel gspell-devel gtksourceview4-devel gstreamer1-plugins-base-devel \
    gstreamer1-plugins-bad-free-devel gst-editing-services-devel

# Build

I'm doing a build with a local $HOME target here to avoid using sudo. So you need $HOME/.local/bin in your PATH.

    $ meson . _build --prefix=~/.local
    $ ninja -C _build

# Install

    $ ninja -C _build install

# Run

Now just run ''fractal'', the install step will have setup a .desktop file for you so in Gnome you should be able to press your Meta key and just type in fractal to make its icon show up.
