# Run Synology Assistant on Fedora with containers

[Synology Assistant only have an Ubuntu version on their website](https://www.synology.com/en-uk/support/download/DS620slim?version=7.0#utilities) so this is a perfect opportunity for a guide on running one distros packages on another distro.

No more unpacking a deb and converting, just run it in containers. 💖

## Warning

This is just one way to run one gui application based on Qt, others might have _special needs_.

## Download Synology Assistant 64-bit Ubuntu .deb package

* [Link](https://www.synology.com/sv-se/support/download)

## Create a container image locally

* Important to note here that you can repeat the buildah commit step as often as you want if you want to add something to your image, or save it as another name.
* The qtlibs.txt is just a list of necessary Qt5 packages found on [this forum](https://forum.qt.io/topic/110119/installing-qt-5-on-debian).
* Save this into a script and you can re-create the container whenever you need, because you might delete the image if you're clearing space on your disk in the future.

```
ctr=$(buildah from docker.io/ubuntu:20.04)
buildah config --workingdir=/mnt --cmd='' --entrypoint=/bin/bash $ctr
buildah copy $ctr synology-assistant_7.0.2-50046_amd64.deb /root/
export DEBIAN_FRONTEND=noninteractive
buildah run -e DEBIAN_FRONTEND $ctr apt update -y
buildah run -e DEBIAN_FRONTEND $ctr \
    apt install vim less libxcb-xinerama0 libglib2.0-0 xauth -y
buildah run -e DEBIAN_FRONTEND $ctr \
    dpkg -i /root/synology-assistant_7.0.2-50046_amd64.deb
while read -ru9 lib; do
    buildah run -e DEBIAN_FRONTEND $ctr apt install "$lib" -y
done 9<qtlibs.txt
buildah commit $ctr ubuntu:20.04-local
```

## Create the container to verify that Synology Assistant works

```
podman run -ti --name=ubuntu_local \
    -v "/tmp/.X11-unix:/tmp/.X11-unix:Z" \
    -v "$HOME/.Xauthority:/root/.Xauthority:Z" \
    -e "DISPLAY=${DISPLAY}" \
    --net=host --privileged ubuntu:20.04-local
root@container:/mnt# /opt/Synology/SynologyAssistant/SynologyAssistant
```

>I've only ever managed to run it with privileged but of course I'd avoid that if I could.

## Enter the container shell again after exiting

```
podman start -a ubuntu_local
```

## Remember to delete it before podman run

```
podman rm ubuntu_local
```

## Make a permanent image for Synology Assistant

This just starts Synology Assistant directly instead of bash.

```
buildah config --entrypoint=/opt/Synology/SynologyAssistant/SynologyAssistant $ctr
buildah commit $ctr synology-assistant:20.04
```

Now you can run the program like this.

```
podman run -ti --rm --name=synology_assistant \
    -v "/tmp/.X11-unix:/tmp/.X11-unix:Z" \
    -v "$HOME/.Xauthority:/root/.Xauthority:Z" \
    -e "DISPLAY=${DISPLAY}" \
    --net=host --privileged synology-assistant:20.04
```

Or even create a shell alias for it. This is like the "poor mans" [Toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/).

    alias synoassistant='podman run -ti --rm --name=synology_assistant \
    -v "/tmp/.X11-unix:/tmp/.X11-unix:Z" \
    -v "$HOME/.Xauthority:/root/.Xauthority:Z" -e "DISPLAY=${DISPLAY}" \
    --net=host --privileged synology-assistant:20.04'

But you still have to delete it when you're done before you run the alias again; ``podman rm synology_assistant``.

# See also

* [Ubuntu command on Fedora](../ubuntu_command_on_fedora)
* [Blog: Running GUI Applications inside Docker Containers](https://medium.com/@SaravSun/running-gui-applications-inside-docker-containers-83d65c0db110)
