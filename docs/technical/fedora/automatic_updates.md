# Automatic updates in Fedora with dnf-automatic

I prefer this method;

* Download updates regularly to cache.
* Manually run ``dnf -C update`` to update only with downloaded packages from cache.
* Set ``keepcache=1`` in ``/etc/dnf/dnf.conf``.
* Manually perform ``dnf clean cache`` when ``/var/cache/dnf`` gets too big.

## Install

    $ sudo dnf install dnf-automatic

## Configure

Edit ``/etc/dnf/automatic.conf`` and make sure the following settings are set.

```ini
[commands]
download_updates = yes
apply_updates = no
```

!!! note "I wouldn't recommend automatic installation of updates"
    Because Fedora is a bit cutting edge so I prefer knowing when and which updates are installed, to trace potential issues.

## Start

    $ sudo systemctl enable --now dnf-automatic.timer

### Optional

There are also some specialized timers for specific configurations, so you don't have to edit automatic.conf.

    $ sudo systemctl enable --now dnf-automatic-download.timer

This would have the same effect.
