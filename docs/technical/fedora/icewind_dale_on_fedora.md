# Icewind Dale on Fedora 33

You can buy Icewind Dale Enhanced Edition from GOG.com but it won't run on Fedora directly because it's missing some old Ubuntu files.

* Download them from [packages.ubuntu.com](http://security.ubuntu.com/ubuntu/pool/main/o/openssl1.0/libssl1.0.0_1.0.2n-1ubuntu5.6_amd64.deb)
* Unpack the .deb file in Gnome's Archive Manager, it's integrated into the File manager so it's very easy.
* Unpack the data file that is inside the .deb archive.
* Find the two files ``libcrypto.so.1.0.0`` and ``libssl.so.1.0.0``.
* Drag and drop them into ``GOG Games/Icewind Dale Enhanced Edition/game/`` folder that is under your user's Home.
* Now you can start the game.
