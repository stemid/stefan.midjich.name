# Växla till HDMI-ljud i Fedora

I min Thinkpad X230 har jag en DisplayPort som jag kan ansluta till HDMI på en TV eller skärm. Den hanterar även ljud men tyvärr måste jag växla ljudet manuellt via pavucontrol eller pacmd. 

Här visar jag hur jag automatiskt kan växla ljud när en DisplayPort kabel ansluts. 

# Udev

Det hela utlöses med en udev-regel som ser ut så här. 

    ACTION=="change", SUBSYSTEM=="drm", RUN+="/home/stemid/bin/dp_hotplug.sh"

Den kan sparas i en fil som heter ''/etc/udev/rules.d/70-dp_hotplug.rules''. 

Ladda om regler. 

    sudo udevadm control --reload-rules
# Pulse Audio

Med PulseAudio kan man byta ljudprofil för sitt ljudkort. 

Visa alla profiler för ljudkortet och kontrollera om rätt HDMI-profil används med pacmd. 

    pacmd list-cards

På min dator ser det ut så här. 

```
$ pacmd list-cards|grep active
        active profile: `<output:analog-stereo>`
```

Och HDMI-profilen heter ''output:hdmi-stereo+input:analog-stereo''. 


# Skript

På min laptop är DisplayPort HDMI1, jag har dock tre andra HDMI-portar enligt ''xrandr'' så det kan vara bra att kontrollera. 

Kan också vara bra att kontrollera statusen i ''/sys/class/drm/card0/*HDMI-A-1/status''. 

Här är skriptet jag använder, hänvisar till detta i udev-regeln ovan. 

```
#!/bin/bash

Username=stemid
UserID=$(id -u $Username)
HDMIStatus=$(cat /sys/class/drm/card0/*HDMI-A-1/status)

export PULSE_SERVER="unix:/run/user/$UserID/pulse/native"

if [ $HDMIStatus = 'connected' ]; then
    sudo -u $Username pactl --server $PULSE_SERVER set-card-profile 0 output:hdmi-stereo+input:analog-stereo
    sudo -u $Username notify-send HDMI "Connected"
else
    sudo -u $Username pactl --server $PULSE_SERVER set-card-profile 0 output:analog-stereo
    sudo -u $Username notify-send HDMI "Disconnected"
fi

```

Se till att det är exekverbart och anslut sedan en kabel för att testa. 
# Felsökning

Kontrollera reaktionen från udev när kabel ansluts. 

    sudo udevadm monitor

Utlös falsk händelse i subsystemet *drm*. 

    sudo udevadm trigger -s drm

