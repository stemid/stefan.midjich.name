# Dela ut mapp anonymt via CIFS från Fedora 23

Dela ut katalogen ''/data0/share''. 

## Skapa katalog

Först skapa en katalog och se till att hela sökvägen dit kan läsas av nobody eller världen. 

    sudo mkdir /data0/share
    sudo chmod 0777 /data0/share
    sudo chmod 0755 /data0

## Installera Samba

    sudo dnf install samba samba-client -y

## Konfa Samba

Sedan redigera ''/etc/samba/smb.conf'', detta är allt som behövs i den filen. 

```
[global]
        workgroup = WORKGROUP
        server string = Samba Server Version %v
[share]
        path = /data0/share
        valid users = nobody

```

## Sätt lösenord för gästanvändare

    sudo smbpasswd -a nobody

## Montera i Windows

Montera som en nätverksplats via utforskaren, ange adress \\din.ip\share och logga in med *nobody* och lösenordet du satte. 
