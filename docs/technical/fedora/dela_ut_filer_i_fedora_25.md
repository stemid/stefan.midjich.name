# Dela ut filer i Fedora 25

    $ sudo dnf install samba policycoreutils-python-utils

Redigera ''/etc/samba/smb.conf'' och lägg till följande i slutet. 

```
[shared]
 comment = Shared files
 path = /var/opt/shared
 public = yes
 writable = no
 printable = no
```

Utdelningen heter ''shared'' och filerna ligger under ''/var/opt/shared'' skrivskyddade. 

Skapa utdelningspunkten och tillåt samba att använda den.

    $ sudo mkdir /var/opt/shared
    $ sudo semanage fcontext -a -t samba_share_t '/var/opt/shared(/.*)?'

Tillåt samba genom brandväggen och starta tjänsten. 

    $ sudo firewall-cmd --add-service samba
    $ sudo systemctl start samba

De sista två kommandona är inte permanenta ändringar för att kunna använda detta tillfälligt hemma hos vänner t.ex. med min laptop. 

Flytta in filer i ''/var/opt/shared'' och montera ''smb://ip/shared''.
