# Building GnuCash on Fedora

This is how I last built GnuCash 3.2 from source on Fedora 27.

## Dependencies

    $ sudo dnf install cmake ninja-build webkitgtk4-devel guile-devel guile \
    aqbanking-devel gwenhywfar-gui-gtk2-devel gwenhywfar-devel ktoblzcheck ktoblzcheck-devel \
    libofx-devel libofx boost boost-devel libdbi libdbi-devel libsecret libsecret-devel

>**Note**: You can't build GnuCash 3.2 on Fedora 27 because it depends on gwenhywfar-gui-gtk3 which appears in Fedora 28. To be continued...

## cmake

I want a local install and I always download source into ''~/src'' so here are my cmake arguments.

    $ cmake -D CMAKE_INSTALL_PREFIX=$HOME/.local -G Ninja ~/src/gnucash-3.2
