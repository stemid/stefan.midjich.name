# Run an Ubuntu command on Fedora

What if something is packaged for another distro but not for Fedora? Use podman to run it quickly and simply without polluting your system with strange emulations or converted Deb packages.

Here is how I run the munpack command to unpack all the MIME components from an e-mail source file.

## Create the container image using buildah

This is mostly to re-use the container multiple times.

    $ container=$(buildah from ubuntu:latest)
    $ buildah run $container apt update
    $ buildah run $container apt install mpack bash -y
    $ buildah config --entrypoint /bin/bash $container
    $ buildah commit $container ubuntu:myowntag
    $ podman images

!!! note "Set your own tag to something that makes sense"
    You should be able to identify what the container image does by its tag. For misc commands maybe name it "misc-commands" or just "misc".

## Run the container image with podman

Pretend you have a mail.txt source file in your current working directory.

    $ podman run -ti -v "$(pwd):/mnt:Z" localhost/ubuntu:myowntag
    root@f3b644b36a6c:/# cd /mnt
    root@f3b644b36a6c:/mnt# munpack mail.txt
    tempdesc.txt: File exists
    image001.jpg (image/jpeg)
    =Xiso-8859-1XQXG=C5S_Nyhetsbrev_2020.pdfX= (application/pdf)

## Add another command to the container image

To add more stuff into your image simply use your image as from tag when creating the container variable.

    $ container=$(buildah from localhost/ubuntu:myowntag)
    $ buildah run $container apt update
    $ buildah run $container apt install nmap socat -y
    $ buildah commit $container ubuntu:myowntag

## Run an Ubuntu command from your own shell with Podman

What if we don't want to start the shell first and just run the command as if in our host OS? This might be useful for scheduled tasks (cron, systemd.timer) or scripts.

Start by generating a new container, either from an ubuntu image or from your own image.

    $ container=$(buildah from ubuntu:latest)
    $ buildah run $container bash -c 'apt update && apt install -y unburden-home-dir'

!!! note "You can run multiple commands by first calling bash -c"
    Each command you run that changes files will create another file system layer in your container image, so prefer multiple file changing commands in one run instead of several layers being generated.

* Set a workdir which means the container will start in that directory.
* Set the entrypoint to unburden-home-dir, meaning that this command will always be executed when you run the container.

    $ buildah config --workingdir /mnt/home $container
    $ buildah config --entrypoint /usr/bin/unburden-home-dir $container

* Commit these changes to your stored container image, or some new tag if you want to separate them.

    $ buildah commit $container ubuntu:myowntag

# See also

* [What does the podman --privileged flag do?](https://www.redhat.com/sysadmin/privileged-flag-container-engines)

