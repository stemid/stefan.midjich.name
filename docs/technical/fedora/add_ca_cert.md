# Add CA cert in Fedora

## Convert crt to pem

If you get a .crt file it might be a binary format so you need to first convert it to pem base64 encoding like this.

    $ openssl x509 -in root_ca.crt -out root_ca.pem -outform PEM

## Install PEM file into trust anchors

    $ sudo cp ca_cert.pem /etc/pki/ca-trust/source/anchors/sydit_ca.pem
    $ sudo update-ca-trust

Now your OS trusts this CA and won't give warnings when you try to use any of their signed certs.
