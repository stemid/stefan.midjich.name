# Building Signal Desktop on Fedora

In **2020-03-29** this guide was re-written from an old method and spec file to a new one fetched [from here](https://github.com/negativo17/Signal-Desktop).

Here's how I build my own RPM for [Signal Desktop](https://github.com/signalapp/Signal-Desktop). This guide has been evolving since Fedora 28, and last tested on Fedora 35.

## Why?

The Signal org provides no official RPM and I don't trust 3rd parties building their own so I go through this process every time I need to update Signal Desktop.

## Setup rpmbuild

Pick somwhere to build your RPMs, I've picked ``$HOME/src/rpmbuild`` and hard coded that into my .rpmmacros file. So it looks like this.

```
%_topdir %(echo $HOME)/src/rpmbuild
%_tmppath %(echo $HOME)/src/rpmbuild/tmp

%__arch_install_post \
    [ "%{buildarch}" = "noarch" ] || QA_CHECK_RPATHS=1 ; \
    case "${QA_CHECK_RPATHS:-}" in [1yY]*) /usr/lib/rpm/check-rpaths ;; esac \
    /usr/lib/rpm/check-buildroot
```

    mkdir -p $HOME/src/rpmbuild
    mkdir $HOME/src/rpmbuild/{SPECS,SOURCES}

## Download SPEC and SOURCE for Signal

### Clone the git repo with the SPEC and patch files

    mkdir $HOME/src
    cd $HOME/src
    git clone https://github.com/negativo17/Signal-Desktop
    cp Signal-Desktop/*.spec ~/src/rpmbuild/SPECS/
    cp Signal-Desktop/*.{patch,desktop} ~/src/rpmbuild/SOURCES/
    cd ~/src/rpmbuild

>**NOTE** That the last working commit I've successfully built Signal-Desktop with [is this one](https://github.com/negativo17/Signal-Desktop/tree/fedora-35).

### Download the source release tarball into the SOURCES directory

Update the command to match the version in Signal-Desktop.spec file.

    curl -Lo SOURCES/Signal-Desktop-5.4.0.tar.gz 'https://github.com/signalapp/Signal-Desktop/archive/v5.4.0.tar.gz#/Signal-Desktop-5.4.0.tar.gz'

## Install yarn

Some might install it using npm as a global package but I prefer using [official repos](https://yarnpkg.com/lang/en/docs/install/#centos-stable). So follow those instructions on installing the yarn repo.

## Install dependency packages

Signal-Desktop requires node 16 and you can [install a specific release in Fedora using streams](https://nodejs.org/en/download/package-manager/#centos-fedora-and-red-hat-enterprise-linux).

    sudo dnf module install nodejs:16/common
    sudo dnf install rpm-build python2 openssl-devel gcc-c++ \
        '@Development Tools' git-lfs yarnpkg npm
    npm install -g node-gyp

### If you have an old nodejs module installed from before

    sudo dnf remove nodejs
    sudo dnf module reset nodejs:14

## Build

As of 2021-01-18 I need the ``QA_RPATHS`` environment variable set for rpmbuild to succeed, due to my rpmmacros file running check-rpaths so this has nothing to do with Signal or the build patches.

    export QA_RPATHS=$(( 0x0001|0x0002|0x0008 ))
    rpmbuild -bb SPECS/Signal-Desktop.spec

## Install

    sudo dnf install RPMS/x86_64/Signal-Desktop-1.39.4-1.fc33.x86_64.rpm

# Known issues

## Yarn cache

Rpmbuild results in something like this error:

```
error Command failed.
Exit code: 128
Command: git
Arguments: pull
Directory: /home/stemid/.cache/yarn/v6/.tmp/6b9ea74d442c0d03a1b4c2a1b8696978
```

Can also be ``Exit code: 1``, just purge the cache and run rpmbuild again.

    rm -rf BUILD/Signal-Desktop-* tmp/rpm-tmp* ~/.cache/yarn/v6/*

## Signal does not start from the Gnome applications menu

The default Signal-Desktop.desktop file included has not been working for me, it was pointing at an empty file for some reason. Here is one that works for me.

```ini
[Desktop Entry]
Type=Application
Name=Signal
GenericName=Messenger
Comment=Signal Private Messenger
Icon=Signal-Desktop
Exec=/usr/lib64/Signal-Desktop/signal-desktop --use-tray-icon --no-sandbox
Categories=Network
StartupNotify=true
MimeType=x-scheme-handler/sgnl;
```
