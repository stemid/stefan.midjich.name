# Using Yubikey in Fedora

Written on Fedora 34 and 35.

## keytocard won't work

You might get this error;

    gpg: selecting card failed: No such device

Restart pcscd.

    sudo systemctl restart pcscd

## See also

* [Yubikey docs on using it with OpenPGP](https://support.yubico.com/hc/en-us/articles/360013790259-Using-Your-YubiKey-with-OpenPGP)
* [Blog on re-signing password store with other keys](https://medium.com/@davidpiegza/using-pass-in-a-team-1aa7adf36592)
* [Extensive guide to yubikey and gpg](https://github.com/drduh/YubiKey-Guide)
