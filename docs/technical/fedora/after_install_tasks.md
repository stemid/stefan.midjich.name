# Fedora: After install tasks

>This page is obsolete; I've since made [an ansible repo](https://gitlab.com/stemid-ansible/playbooks/fedora-workstation.git) which is my setup process for a new Fedora workstation.

Things I do after I do a fresh install of Fedora Workstation. Haven't done this in a while so better write it down, I mostly upgrade and use my backups.

*  Setup [Solarized color theme in gnome-terminal](https://github.com/Anthony25/gnome-terminal-colors-solarized)

*  Install ''adobe-source-code-pro-fonts'' package

*  Install ''mscore-fonts''

*  Install [Fedy](https://www.folkswithhats.org/) and use it to install things like Visual Studio Code, Google Play Desktop, Arc theme and relevant tweaks.

*  Install and enable postfix.

*  Setup ''mailbox_home=.local/share/evolution/mail/local/'' in postfix.

*  ''semanage fcontext -a -t mail_spool_t '/my/home/.local/share/evolution/mail/local(/.*)?'''

*  ''restorecon -Rv ~/.local/share/evolution''
