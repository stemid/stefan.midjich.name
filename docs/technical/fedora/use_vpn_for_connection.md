# Always use a VPN for a specific connection

Easier than you think if you just open nm-config-editor manually instead of via Gnome UI.

Either press Alt+F2 and type nm-connection-editor into the run-box, or run it from a terminal shell.

This advanced connection editor allows you to select your coffee house connection for example, click the cog wheel to edit it and under the "General" tab you can select to connect a specific VPN every time you're using your coffee shop wifi.

