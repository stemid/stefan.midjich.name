# Deploying a Python app from a git repo to AWS Beanstalk

The only issue necessary to document so far has been that the awsebcli doesn't work very well.

First I had to downgrade urllib3 (using python3 and pip3), this was shown in a very clear exception error when trying to run eb cli.

Then I had to downgrade awsebcli itself because eb deploy didn't work.

And finally eb deploy's archive function claims to include git submodules but does not. So I wrote a short script to create an artifact file and set my config to use artifact zip file instead.


