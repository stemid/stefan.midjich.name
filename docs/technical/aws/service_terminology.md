# Service terminology

For deploying a Python application with backend, frontend and database these are the services I've been using. Their names and their function, **listed in order of importance when setting up a new app**.

# S3

S3 buckets are storage places for static websites, HTML, Javascript, CSS and other static files served to clients.

Primarily used for storage they can also be used to host static sites from long automatically generated domains like bucket-name.s3.eu-central-1.amazonaws.com for example.


# Route 53

This is amazons DNS service. Here you can point your own custom domain to an S3 bucket for example, or a CloudFront distribution. 

# ACM

Amazon Certificate Manager is used to generate public and private SSL certs.

For a public S3/CloudFront website a public certificate is good enough, and it's free to generate.

It can also be used to generate certificates for backend services such as APIs.

As of writing you can only create certificates in the us-east-1 (N. Virginia) region if you want to use them in CloudFront. Any other regional certs will not show up in CloudFront.
# CloudFront

To have an S3 site protected by SSL you must place CloudFront "infront" of it. CloudFront is among other things an SSL terminator and a web cache. So Cloudfront fetches data from an S3 bucket, caches or, and serves it from its cache.

# Elastic Beanstalk

This is a whole suite of Amazon services used to deploy applications, in my case Python WSGI applications but also many other languages.

A sort of umbrella service that uses other Amazon services such as EC2 and RDS to deploy your app. 

# EC2

I have not had to use EC2 directly yet, only through Beanstalk, but EC2 are essentially VPS instances of Linux servers where you can install packages, run services and for example deploy WSGI Python services.

# RDS

Database service that is also deployed through Beanstalk but can be managed directly if needed. Also here Beanstalk deploys it and provides all the information required to connect to it.

# ELB

Elastic Load Balancer is usually put infront of an Elastic Beanstalk application to terminate TLS or to load balance between multiple instances of app servers.

# Lambda

Bind API paths as routes to lambda functions. Or setup a scheduled lambda to act as a "cron" job.

# Amplify

CLI tool to help manage and deploy applications in AWS.

# AWS AppSync

What is used when amplify adds an API.
