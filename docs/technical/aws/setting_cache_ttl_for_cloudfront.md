# Setting cache TTL for CloudFront

When using CloudFront infront of an S3 bucket to terminate SSL for example it also caches the data it fetches from S3. Default is to cache at least 24 hours, maybe more.

To change this go into your CloudFront distribution, Behaviours tab and select the Behaviour. You most likely have one already defined for HTTP/HTTPS redirection.

This is where Maximum and Minimum TTL values are defined.

For staging environments I set Minimum to 0, Maximum to 60 seconds and Default to 60.

For prod you should of course use a higher TTL but even then I'd likely not use higher than 24 hours.

>Even when changing the default TTL values CloudFront might still keep your old content until you specifically invalidate all your objects under the Invalidation tab. Create new and type in ''/*'' as object path to invalidate everything.
