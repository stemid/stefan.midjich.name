# Elastic Beanstalk and TLS

By default Elastic Beanstalk is deployed in a manner suitable for staging or dev environments. There is no load balancer and the RDS database is reset with the application.

There is no way to use ACM TLS certificates without a load balancer and I tried many times to deploy a let's encrypt cert in the EC2 instance but failed in strange ways.

So I simply enabled the load balancer for the Beanstalk application under the Configuration/Capacity section.

Then in the Loadbalancer configuration, which was now enabled, I made sure to terminate HTTPS as a listener to HTTP on the instance. And here I could also add ACM certs.

And after the ELB was setup I could change my domain in Route53 so it pointed to the ELB.
