# CloudFront and S3

To use TLS with a static S3 site you need CloudFront as a frontend layer before the S3 bucket.

In short these are the steps that worked for me.

 1.  Create the S3 bucket without static website config and without any bucket policy.
 2.  Create the CloudFront distribution.
 3.  Set the Origin Domain Name of the distribution to your full S3 bucket domain name, do not use the shorter address suggested by the AWS interface.
 4.  In the same Origin setting you can let CloudFront create your bucket policy for you which is best.
 5.  Most other settings for the CloudFront distribution are common sense or good as defaults. For example set the regions to save money, set TLS SNI to save money and so forth.
 6.  Now the site should work for you via the cloudfront domain. Here I ran into a funny issue where it worked for me from my home IP but not from anywhere else until I added the following bucket config.
 7.  Enable static website hosting on the bucket.
 8.  Add a standard bucket policy for allowing public access to it. See below for my full policy.

After this you can setup a domain in Route53 as an alias and point it to the CloudFront distribution to use your own custom domain instead of the provided cloudfront domain.

# Bucket policy

The first block in the Statement list can be automatically added by CloudFront.

```
{
    "Version": "2012-10-17",
    "Id": "RandomPolicyId",
    "Statement": [
        {
            "Sid": "3",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity XXXXXXXXREDACTXXXXXXXX"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::bucket-name/*"
        },
        {
            "Sid": "RandomSid",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::bucket-name/*"
        }
    ]
}
```
