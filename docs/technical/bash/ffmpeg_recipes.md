# ffmpeg recipes

Here I save things I do with ffmpeg, in case I need it in the future.

## Video speed

* [Read more in the docs](https://trac.ffmpeg.org/wiki/How%20to%20speed%20up%20/%20slow%20down%20a%20video).

### Speed up 4x using setpts filter

    ffmpeg -i GH010117.MP4 -r 16 -filter:v "setpts=0.25*PTS" -an GH010117.4x.mp4

* ``-an`` removes audio too.

## Cut video

    ffmpeg -ss 00:00:22.0 -i GH010112.MP4 -c copy -t 00:04:00.0 GH010112.cut.mp4

* ``-ss`` starting time, 22 seconds.
* ``-t`` run time, can be omitted to run until end of video.

## Convert mp4 to webm

    ffmpeg -i PXL_20220421_140451609.mp4 -c:v libvpx-vp9 -crf 30 -b:v 0 -b:a 128k -c:a libopus smulan\ ylar.webm

* ``-crf 30`` decides video quality, a lower value produces larger files and better quality.
* ``-b:a 128k`` can be changed to get better audio quality, it's the bitrate.

## Drawtext filter

This is not regarding subtitles, only for example to show how many times you've sped up a video by imposing a ''x4'' on it.

### Find out if ffmpeg is compiled with support for drawtext

You should see three things colored by grep in this output.

    ffmpeg -h 2>&1|grep --color -E '(libfreetype|fontconfig|libfribidi)'

### Add simple text string

Here I just add the text ``x4`` starting at pixel 300x300 in x/y axis.

    ffmpeg -i GH010117.x4.mp4 -vf "drawtext=text='x4':x=300:y=300:fontsize=42:fontcolor=white" GH010117.x4.text.mp4

I tried combining this filter with another filter like setpts to slow down a video but the result was unexpected so I found it best to do it in two runs.

### Position text relative to x axis

This uses a pre-defined variable called ``dar`` to determine the text position in the y axis, relative to the x axis.

    ffmpeg -i GH010122.cut1.mp4 -vf "drawtext=text='*cleaning noises*':x=240:y=x/dar:fontsize=42:fontcolor=white" GH010122.cut1.text.mp4

### Fade in and out text at specific interval

    ffmpeg -i GH010122.cut4.x4.mp4 -filter_complex "drawtext=text='x4 speed':x=240:y=x/dar:fontsize=42:fontcolor=white:enable='between(t,0,57)',fade=t=in:start_time=0:d=0.5:alpha=1,fade=t=out:start_time=56.5:d=0.5:alpha=1[fg];[0][fg]overlay=format=auto,format=yuv420p" GH010122.cut4.x4.text.mp4

* This uses the standard parameter ``enable`` that is not part of drawtext but many filters support using it.
* I stole this from [Stackoverflow](https://stackoverflow.com/questions/50735335/ffmpeg-adding-text-to-a-video-between-two-times) so it's over my head but take note of the seconds 0 and 57 that are start and stop times for the text.
* Additionally 0.5 and 56.5 are start and stop interval for the fade in, and fade out, respectively.

### Show text at specific time interval

This also uses the ``-filter_complex``, in order to use the [enable option](https://ffmpeg.org/ffmpeg-filters.html#Timeline-editing).

    ffmpeg -i GH010170.cut1.mp4 -filter_complex "drawtext=enable='between(t,0,8)':text='Andra ->':x=600:y=640:fontsize=42:fontcolor=white" GH010170.cut1.text.mp4

# See also

* [Bash recipes](./recipes.md)
* [ffmpeg filters documentation](https://ffmpeg.org/ffmpeg-filters.html)
* [Ottverse.com blog about drawtext filter](https://ottverse.com/ffmpeg-drawtext-filter-dynamic-overlays-timecode-scrolling-text-credits/).
* [Ultimate Guide to Ffmpeg blog post](https://img.ly/blog/ultimate-guide-to-ffmpeg/)

