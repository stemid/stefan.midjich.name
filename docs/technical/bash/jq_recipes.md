---
title: jq recipes
date: 2021-07-09
summary: Recipes for using the CLI tool jq
---

# JQ recipes

JQ is an amazing tool when it solves your problems but I use it so rarely that it's hard to remember the syntax.

I'm expanding this page as I do new things in jq, doing them twice just to document them here so hopefully I can learn them in time and not have to look them up.

## Select item from a list of objects by a key value in each object

A good example here is the Firefox logins.json file.

    cat .mozilla/firefox/XXX.default-release/logins.json | \
      jq '.logins[] | select(.hostname | contains("facebook"))'

