# Patchning av RHEL

Denna guiden utgår ifrån följande principer;

* Kritiska säkerhetsuppdateringar ska hämtas automatiskt när de blir tillgängliga.
* Uppdateringar ska inte installeras automatiskt.
* Uppdateringar ska installeras manuellt från yum cache vid ett servicefönster.

## Notiser om säkerhetsuppdateringar från RedHat

### Från e-postlistan RHSA

Gå till [RedHats sida Notifications and Advisories](https://access.redhat.com/security/updates/advisory) och följ instruktionerna för att antingen få notiser via RedHats kundportal eller via deras mailinglista, rhsa-announce.

### Via kundportalen rhn.redhat.com

Registrera samtliga system på RHN så kan man slå upp deras Errata för att se alla buggar som påverkar systemen och deras svårighetsgrad.

## Hämta säkerhetsuppdateringar automatiskt med yum-cron

### Installera yum-cron

    $ sudo yum install yum-cron

### Konfigurera yum-cron

Redigera filen ``/etc/yum/yum-cron.conf`` och läs följande kommentarer.

```ini
#  What kind of update to use:
# default                            = yum upgrade
# security                           = yum --security upgrade
# security-severity:Critical         = yum --sec-severity=Critical upgrade
# minimal                            = yum --bugfix update-minimal
# minimal-security                   = yum --security update-minimal
# minimal-security-severity:Critical =  --sec-severity=Critical update-minimal
update_cmd = security-severity:Critical

# Whether updates should be downloaded when they are available.
download_updates = yes

# Whether updates should be applied when they are available.  Note
# that download_updates must also be yes for the update to be applied.
apply_updates = no

# Maximum amout of time to randomly sleep, in minutes.  The program
# will sleep for a random amount of time between 0 and random_sleep
# minutes before running.  This is useful for e.g. staggering the
# times that multiple systems will access update servers.  If
# random_sleep is 0 or negative, the program will run immediately.
# 6*60 = 360
random_sleep = 3
```

* ``update_cmd = security-severity:Critical`` betyder att bara kritiska uppdateringar kommer hämtas.
* ``apply_updates = no`` tillsammans med ``download_updates = yes`` betyder att uppdateringar bara kommer laddas ner och lagras i yum cache.
* ``random_sleep = 3`` är viktigt att minska från standardvärdet 360 annars kan varje uppdatering ta upp till 6 timmar.

#### Konfigurera meddelanden när uppdateringar finns tillgängliga

För att få ett e-post måste servern ha en *null mailer* som lyssnar på localhost port 25 och skickar brev vidare till en server som tillåts skicka ut på internet.

```ini
[emitters]
# Name to use for this system in messages that are emitted.  If
# system_name is None, the hostname will be used.
system_name = min-server.bolag.tld

# How to send messages.  Valid options are stdio and email.  If
# emit_via includes stdio, messages will be sent to stdout; this is useful
# to have cron send the messages.  If emit_via includes email, this
# program will send email itself according to the configured options.
# If emit_via is None or left blank, no messages will be sent.
emit_via = email

[email]
# The address to send email messages from.
# NOTE: 'localhost' will be replaced with the value of system_name.
email_from = root@localhost

# List of addresses to send messages to.
email_to = driftgrupp@bolag.tld,monitoring@bolag.tld

# Name of the host to connect to to send email messages.
email_host = localhost
```

* ``system_name`` Borde vara servernamnet så mottagaren av e-post kan identifiera vilken det gäller.
* ``emit_via`` Ställer in att skicka meddelanden om uppdateringar via e-post.
* ``email_from`` Anger avsändaren, tips är att ange patchnivå i avsändaren. T.ex. ``security-critical@localhost``.
* ``email_to`` Är en komma-separerad lista av mottagare.
* ``email_host`` Kan antingen vara en null mailer på localhost eller ett relä i nätverket.

### Aktivera yum-cron

    $ sudo systemctl enable --now yum-cron

Detta aktiverar ett cron-jobb som kör ett yum-cron skript, via ``/etc/cron.daily/0yum-daily.cron`` och ``/etc/cron.hourly/0yum-hourly.cron``.

#### Inaktivera 0yum-hourly.cron jobbet (valfritt)

Om du inte konfar ``/etc/yum/yum-cron-hourly.conf`` så är det onödigt att jobbet ``/etc/cron.hourly/0yum-hourly.cron`` körs varje timme och förorenar loggen. Stäng av det genom att ta bort exekverbara rättigheter.

    $ sudo chmod 0644 /etc/cron.hourly/0yum-hourly.cron

## Alternativ 1: Installera uppdateringar manuellt

Finns ingen övervakning kan det vara önskvärt att installera uppdateringar manuellt. Se ovan för hur man konfigurerar ett e-postmeddelande när uppdateringar finns tillgängliga i cachet.

``-C`` flaggan betyder att enbart de uppdateringar som ligger hämtade i yum cache installeras.

    $ sudo yum -C update

!!! note
    Fördelen med att bara installera uppdateringar från cache är att brevet man får från yum-cron med tillgängliga uppdateringar är en exakt lista av vad som installerats. Kör man utan ``-C`` så hämtas nya uppdateringar som kan ha kommit ut sedan brevet skickades.

## Alternativ 2: Installera uppdateringar automatiskt, med omstart

Finns övervakning av servern så kan man ställa in servern att starta om automatiskt när uppdateringar har installerats.

1. Konfa yum-cron att installera uppdateringar.

    Först anger man ``apply_updates = yes`` i ``/etc/yum/yum-cron.conf`` för att installera uppdateringar automatiskt.

2. Ställ in anacron att köra yum-cron mellan 03-07.

    Sedan måste ``START_HOURS_RANGE`` i filen ``/etc/anacrontab`` redigeras för att köra jobb mellan vissa timmar på dygnet. 

    T.ex. ``START_HOURS_RANGE=3-7`` för att köra mellan 03 och 07 på morgonen.

3. Skapa ett cronjobb för omstart.

    Till sist skapar man ett cron-jobb i ``/etc/cron.d/restart`` med följande innehåll.

    ``30	8	*	*	Mon		root	needs-restarting -r 2>&1 >/dev/null || reboot``

    Detta betyder att varje måndag klockan 08:30 kommer servern starta om, bara om den har installerat uppdateringar som kräver omstart.

## Grunder i yum kommandot

### Kontrollera vilka uppdateringar som finns tillgängliga att installeras

    $ sudo yum -C check-update

### Se historik av vilka uppdateringar som installerats

    $ sudo yum history
    ID     | Inloggad användare       | Datum och tid    | Åtgärd(er)     | Ändrade
    -------------------------------------------------------------------------------
    183 | root <root>              | 2020-04-24 09:05 | Update         |    1   
    ...

### Se detaljerad information om en uppdatering

```
$ sudo yum history info 183
Inlästa insticksmoduler: fastestmirror
Transaktions-ID: 183
Starttid       : Fri Apr 24 09:05:52 2020
Start-rpmdb    : 541:33b8d0f26ce141a7b0f67b9c0fb9772f4569f3fb
Sluttid        :             09:05:54 2020 (2 sekunder)
Slut-rpmdb     : 541:9fda8d6708a22cf6951920705a2531f9a75cf534
Användare      : root <root>
Returkod       : Lyckades
Transaktionen utförd med:
    Installerade     rpm-4.11.3-40.el7.x86_64                      @base
    Installerade     yum-3.4.3-163.el7.centos.noarch               @base
    Installerade     yum-plugin-fastestmirror-1.1.31-52.el7.noarch @base
Ändrade paket:
    Uppdaterade python2-jmespath-0.9.0-3.el7.noarch @extras
    Uppdatering                  0.9.4-1.el7.noarch @epel
history info
```

### Rulla tillbaka en uppdatering

    $ sudo yum history rollback 183
