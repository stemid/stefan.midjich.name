# Nätboota CentOS 6.3

Utökar de två dokumenten om att //[Nätboota Debian Squeeze](../mixed/naetboota_debian_squeeze.md)// och //[Nätboota Ubuntu 12.04](../mixed/naetboota_ubuntu_12_04.md)// genom att kort beskriva ändringarna jag gjorde för att nätboota CentOS 6.3. 

## Skapa systemfiler

Hämta först de relevanta systemfilerna från en spegelserver. Dessa går även att hitta på en CentOS DVD i en katalog med samma namn. 

    cd /var/www/pxe.swehack.se
    mkdir centos_amd64
    cd centos_amd64
    wget -rl 10 -nH -np --cut-dirs=8 ftp://mirrors.se.kernel.org/centos/6.3/os/x86_64/isolinux/



# Se också


*  [Nätboota Ubuntu 12.04](../mixed/naetboota_ubuntu_12_04.md)
*  [Nätboota Debian Squeeze](../mixed/naetboota_debian_squeeze.md)
