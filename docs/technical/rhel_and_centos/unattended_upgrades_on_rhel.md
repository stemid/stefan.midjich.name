# Unattended upgrades on RHEL or CentOS

## What you need


*  Null mailer on each system to mail notifications

*  Access to repository or subscription channel

# Install

    sudo yum install yum-cron

# Configure

This is an excerpt of the important lines from the file ''/etc/yum/yum-cron.conf''. 

```

[commands]
update_cmd = default
update_messages = yes
download_updates = yes
apply_updates = yes
random_sleep = 360

[emitters]
system_name = None
emit_via = email
output_width = 80

[email]
email_from = root@localhost
email_to = yourself@example.com
email_host = localhost

```

Look at the comments describing these lines in the default configuration file that is installed by the package and they should be self-explanatory.

# Enable

This essentially enables a cron job.

    sudo systemctl enable yum-cron; sudo systemctl start yum-cron

# Keeping track of updates installed

This is a tip if you have a need to keep track of absolutely each upgrade installed. 

Set your ''apply_updates'' setting to no and your ''download_updates'' setting to yes. 

You will get a notification over e-mail when updates have been downloaded on the system.

When you then update your system manually you run ''yum update -C'' because the ''-C'' argument restricts your updates to those that have been downloaded and nothing else. So in essence you have a list in your inbox of all the updates you're installing when you use this argument.

# Regular reboots

If you're not doing manual updates with yum -C to use the cache, then I'd suggest you also set your server to automatically reboot regularly.

As long as it's being monitored this should not be an issue and it's not far from what they've been doing for Windows servers already. We Linux server admins need to bury our uptime-pride and just get with the update program.
