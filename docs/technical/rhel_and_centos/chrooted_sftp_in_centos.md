# Chrooted SFTP in CentOS

I previously wrote about setting up a [Chrooted SFTP with PAM MySQL in RHEL](../../mixed/chrootad_sftp_med_pam_mysql_pa_rhel6/) in Swedish. I've now had to do another such configuration but without MySQL so I decided to write it [in Ansible of course](https://gitlab.com/stemid-ansible/playbooks/chrooted-sftp.git).

No need to go into depth about this because it's all in the Ansible repo.

## Advantages

I prefer this method over [other methods](https://sanitarium.net/unix_stuff/sftp_chroot.html) because it uses ``pam_exec`` to bootstrap directories with a script. Avoiding the need for a large and complex /etc/fstab file with many bind mounts.

## Bind mount

As far as I could tell bind mounting a users home directory under a new root was necessary. So ChrootDirectory is set to /var/sftp/%u and users home directory remains under /home/%u. 

That means users that login have /home/sftpuser as $HOME set and will attempt to cd into there automatically. So I mount their actual home under /var/sftp/sftpuser/home/sftpuser to simulate this path in the chroot.

This was a vast improvement to my previous setup, which of course had a previous VsFTPD server I had to work around.
