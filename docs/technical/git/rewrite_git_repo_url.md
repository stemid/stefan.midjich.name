# Rewrite a git repository URL

Situation could be that you have Ansible related requirements.yml files that you wish to distribute to public users. So in these files you'll use public URLs like this.

```yaml
- src: https://gitlab.com/stemid-ansible/roles/nginx.git
  scm: git
```

But at the same time you're the developer and owner of those repos so if you run ansible-galaxy you don't want to go via HTTPS but rather via SSH, which would look like ``git@gitlab.com:stemid-ansible/roles/nginx.git``.

So to rewrite that url you can use the following config in ``~/.gitconfig``.

```
[url "git@gitlab.com:stemid-ansible"]
  insteadOf = https://gitlab.com/stemid-ansible
```
