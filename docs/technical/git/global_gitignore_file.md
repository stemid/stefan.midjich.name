# Global .gitignore file

Polluting every repo with your .gitignores is a bad idea in the long run so please prefer a global .gitignore.

## Config

Add core.excludesFile to your ``~/.gitconfig``.

```
[core]
  excludesFile = ~/.gitignore
```

And then put your ignores in ``~/.gitignore``.
