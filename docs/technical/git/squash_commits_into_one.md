# Squash commits into one to clean up the log

If you're often doing small fixup commits, or "Work In Progress" commits, that clutter your git log then this is the trick for you.

I always prefix such small commits with WIP: (Work In Progress).

    $ git rebase -i HEAD~N

N here being the number of commits you want to squash from the last one.

This will open an editor allowing you to pick one commit you want to save in the log, and others you want to squash into that one commit by prefixing each line with pick or squash keywords.

So normally you'd prefix the first one (oldest one) with pick, and the others with squash.

# See also


*  [The best blog post I've found on this topic](https://www.internalpointers.com/post/squash-commits-into-one-git)
