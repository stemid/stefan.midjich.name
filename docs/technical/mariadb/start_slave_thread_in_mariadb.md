# Start slave thread in MariaDB

This is how you use ``START SLAVE THREAD`` in MariaDB to start replicating data from one MariaDB server to another.

I use this to create read-only slaves for Galera clusters for example. In that case it doesn't matter which cluster node you replicate from but I tend to choose a write-enabled master server.

!!! note
    All commands will be run inside a MySQL root shell, or another user with the SUPER privilege.

## Step 1: Check status of slave thread

    > show all slaves status;
    Emtpy set


