# Migrating from older database server to MariaDB 10 with Galera replication

Here I've documented a technique I've used a couple of times to migrate a client from an old MySQL 5.5 or 5.6 to a new MariaDB 10 galera cluster.

Simply put I start a slave thread in the new cluster which replicates any changes to it live. Until it's time to migrate, at which point you shutdown services in the old system so no writes are being made, shutdown the slave thread and migrate your services over to the new MariaDB system.

# Enable binary logs

The old system must have binary logs enabled. So just set ''log_bin=On'' in the config and restart.

# Make a master dump of all databases

Your new DB server must be *seeded* with all the data that is in the old server, and this is best done with a mysqldump.

Ensure you get master info data with these arguments.

    $ mysqldump -A --master-data > master_data.sql

Transfer the file to your new server and import it.

    $ mysql -u root < master_data.sql

These steps have omitted potential user authentication required in both steps to keep the command examples simple. In real life I use a ''--defaults-file'' and pipe the dump straight into ssh to transfer it to the receiving server in the same command.

# Start slave replication thread

Logon to the new DB server and run ''start slave'', check the log for status. This will establish a connection to the old DB server on port 3306.

    mysql> start slave;

# See also


*  [More database guides](teknik/guider/database/)
