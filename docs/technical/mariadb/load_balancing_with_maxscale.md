# Loadbalancing with MaxScale

When your application does not have support for loadbalancing to multiple master DB nodes it's best to have a loadbalancer infront. 

HAProxy is a free alternative but so far I've only tried MaxScale. 

>**Observe** that MaxScale uses a special commercial open source license that requires you pay for using it with more than 3 DB nodes.

# MaxScale install

You must download it [through the MariaDB portal](https://mariadb.com/kb/en/mariadb-enterprise/mariadb-maxscale-14/mariadb-maxscale-installation-guide/), there is no package repository yet. 

# MaxScale Configuration

The configuration can be broken down into these steps.

 1.  Define backend servers
 2.  Define a service to use your backend servers
 3.  Define a listener to accept TCP traffic on a port (3306 for example)
 4.  Define a monitor to monitor the health of the cluster and decide which backend servers to use
 5.  (Optional) Define a MaxAdmin service to use the maxadmin cli-tool

## Defining backend servers

```
[node1]
type=server
address=node1
port=3306
protocol=MySQLBackend
serv_weight=3

[node1]
type=server
address=node1
port=3306
protocol=MySQLBackend
serv_weight=2

[node1]
type=server
address=node1
port=3306
protocol=MySQLBackend
serv_weight=2
```

Some notes on this:


*  The section name can be anything and this is what you use later to refer to the server

*  The ''address'' must be an IP or DNS name that can be resolved

*  ''serv_weight'' decides how many connections are given to the node

## Define the service

The service decides in which way you will loadbalance connections, for example you can split reads from writes or you can try to balance all reads and writes across all nodes. 

Here's an example that balances all reads and writes across all nodes based on their ''serv_weight''. 

```
[Galera Service]
type=service
router=readconnroute
router_options=synced
servers=node1,node2,node3
user=maxscale
passwd=secret.
max_slave_connections=100%
weightby=serv_weight
```

Some notes:


*  ''router'' can be set to many different things, see the docs

*  ''router_options'' decides if you want to balance connections only to synced nodes

*  ''user'' & ''passwd'' give maxscale administrative access to the server and requires special grants that you can read about later

FIXME Work in progress.
# See also


*  [ MaxScale Configuration & Usage Scenarios](https://mariadb.com/kb/en/mariadb-enterprise/mariadb-maxscale-14/maxscale-configuration-usage-scenarios/)

*  [ MySQL Cluster setup and MaxScale configuration](https://mariadb.com/kb/en/mariadb-enterprise/mariadb-maxscale-14/mysql-cluster-setup-and-maxscale-configuration/)

*  [MaxScale documentation on Github](https://github.com/mariadb-corporation/MaxScale/tree/2.0/Documentation)
