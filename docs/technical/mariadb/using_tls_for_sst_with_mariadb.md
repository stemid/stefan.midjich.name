# Using TLS for SST with MariaDB

This might be good when you're spreading your cluster across DCs. The instructions below were written by a nickname called ''sibiria'' in the #maria channel at freenode. I have yet to verify them.

FIXME TODO: Verify and clarify this article.
## Create a simple CA and a server key/cert

Create the CA that will be used to sign the server cert:

	openssl req -new -newkey rsa:4096 -x509 -nodes -days 1826 -sha256 -keyout mariadb_ca.key > mariadb_ca.crt

Create the server key and certificate signing request:

	openssl req -newkey rsa:2048 -nodes -days 1826 -sha256 -subj '/C=SE/ST=-/L=-/O=Organization/OU=-/CN=mariadb/emailAddress=some@thing.com' -out mariadb.csr -keyout mariadb.key

Sign the server's certificate using the CA:

	openssl x509 -req -in mariadb.csr -days 1826 -sha256 -CA mariadb_ca.crt -CAkey mariadb_ca.key -set_serial 01 -out mariadb.crt

You can use a single client key/cert for all nodes, or you can create one unique set for each node if you so prefer. These certs and keys can also be used by clients to establish encrypted general connections to the MariaDB instances, not just for protecting replication data between Galera. Don't forget to protect the CA and server keys.


## Add a user for remote (non-localhost) connections that will require TLS

	grant RELOAD, LOCK TABLES, PROCESS, REPLICATION CLIENT [, AND ANYTHING ELSE YOU MAY NEED] on *.* to 'yoursstuser'@'%' identified by 'yoursstpassword' require ssl;


## Settings to add to wsrep_provider_options

	socket.ssl_key=/etc/pki/tls/private/mariadb.key;socket.ssl_cert=/etc/pki/tls/certs/mariadb.crt;socket.ssl_ca=/etc/pki/tls/certs/mariadb_ca.crt


## Configuration clause to add to your my.cnf

	[sst]
	encrypt=4
	ssl-ca=/etc/pki/tls/certs/mariadb_ca.crt
	ssl-cert=/etc/pki/tls/certs/mariadb.crt
	ssl-key=/etc/pki/tls/private/mariadb.key


Start your nodes up again and confirm on each node that TLS is in use for replication by running 'journalctl -xe' and looking for 'WSREP: SSL handshake successful'
