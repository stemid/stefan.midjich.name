# Nätboota Debian Wheezy

Detta är en uppföljare på artikeln [Nätboota Debian Squeeze](../naetboota_debian_squeeze), så jag tänker beskriva hur jag uppgraderade min nätbootmiljö från Squeeze till Wheezy. 

Det betyder att jag har följt alla stegen i den ovannämnda artikeln, och detta är vad jag gjorde härnäst. 

## DHCP

Inget har ändrats här, i mitt fall har jag lagt till en ny laptop som är anledningen för min uppgradering till Wheezy. 

Så mina fyra extra rader i ``dhcpd.conf`` kan se ut så här. 

    host zakalwe {
      hardware ethernet f0:de:f1:b8:80:0a;
      fixed-address zakalwe.swehack.local;
    }

Annars är det i exakt samma group-block som jag lägger till datorn, med samma sökväg i ``filename`` och samma servernamn i ``next-server``. 

Så vida du inte vill kunna utplacera både Squeeze och Wheezy, i vilket fall du måste skapa två olika sökvägar. Det täcker jag inte här. 

## Tftp-server

Den konfigurerades i [del 1 av artikelserien](./naetboota_ubuntu_12_04#tftp-server). 

## Apache

Inga konfigurationsändringar i Apache.

## Skapa systemfiler

Här händer magin, vi måste uppgradera våra befintliga systemfiler från Squeeze till Wheezy. 

Sökvägen till [Debians FTP](ftp://ftp.se.debian.org/debian/dists/wheezy/main/installer-amd64/current/images/netboot/) är nästan identisk, vi bara byter ut squeeze mot wheezy. 

    $ cd /var/www/pxe.swehack.se/debian_amd64
    $ tar -xvjf ../old_debian_amd64.tar.bz2 .
    $ wget -rl 10 -nH -np --cut-dirs=8 ftp://ftp.se.debian.org/debian/dists/wheezy/main/installer-amd64/current/images/netboot/
    $ chown -R tftp:www-data .

Nästan samma kommandon som i gamla artikeln, jag bara kopierar de gamla squeeze-filerna åt sidan så de är sparade först. 

Redigera nu filen ``debian_amd64/pxelinux.cfg/default`` precis som i den gamla artikeln. 

Här måste vi lägga till ett extra argument i raden ``append``, så den ska nu se ut så här. 

    append auto=true hostname= keymap=sv netcfg/choose_interface=eth0 netcfg/get_domain=swehack.internal url=http://pxe.swehack.se/debian_amd64/preseed.cfg vga=normal initrd=debian-installer/amd64/initrd.gz ramdisk_size=16432 root=/dev/rd/0 rw  --

Det nya argumentet är ``keymap=sv``, annars kommer man få frågan om tangentbordslayout i början av installationen. 

>**Observera** att jag köpte en ny Lenovo laptop och var tvungen att inte bara stänga av *Secure Boot* i BIOS utan även slå över helt till *Legacy startup* läge för att kunna använda den här konfigurationen. Alla nya datorer nu för tiden har det problemet och jag har ännu inte utforskat hur man ska nätboota med UEFI. 

## Preseed

I ``preseed.cfg`` måste ett antal rader ändras för att anpassa oss efter Wheezy. Här följer endast raderna som ändrats från den gamla filen i [Nätboota Debian Squeeze](../naetboota_debian_squeeze#preseed).

```
#Locale

d-i debian-installer/language string sv
d-i debian-installer/country SE

#Keymap

d-i keymap select sv

#Mirror

d-i mirror/suite string wheezy

```

# Extra firmware

I mitt fall när jag skulle installera Wheezy så rekommenderades extra firmware för mitt nätverkskort. Detta måste hämtas från ``non-free`` paketförrådet och därför inkluderas det inte som standard med Debian. 

Jag behöver egentligen inte denna firmware under installation eftersom jag nätbootar trådat, men jag tänker gå igenom hur jag laddar firmware ändå. 

Det innebär att vi måste modifiera filen ``debian_amd64/debian-installer/amd64/initrd.gz`` som vi hämtade från [Debians FTP](ftp://ftp.se.debian.org/debian/dists/wheezy/main/installer-amd64/current/images/netboot/) tidigare. 

## Hämta firmware

Min webbserver är redan en Wheezy-server, och den har ``non-free`` tillagt i ``/etc/apt/sources.list``, så jag behöver bara köra följande kommando för att hämta den firmware jag behöver. 

    $ apt-get source firmware-realtek

Så sparas den i katalogen ``firmware-nonfree-0.36+wheezy.1/realtek/rtl_nic``. Det är katalogen ``rtl_nic`` som vi ska kopiera. 

## Packa upp initrd.gz

Se till att stå i katalogen ``debian_amd64`` som vi har skapat i [den gamla artikeln](../naetboota_debian_squeeze). 

    $ mkdir debian-installer/amd64/initrd
    $ cd debian-installer/amd64/initrd
    $ zcat ../initrd.gz | cpio -iv

Nu har vi packat upp initrd.gz filen i samma katalog som vi står i, där har vi en hel kopia av ett Debian-system. 

Så nu ska vi placera firmware-katalogen i ``./lib/firmware`` katalogen. 

    $ mkdir ./lib/firmware
    $ cp -r ../../../../firmware-nonfree-0.36+wheezy.1/realtek/rtl_nic ./lib/firmware/
    $ find . -print0 | cpio -0 -H newc -ov | gzip -c > ../initrd.gz
    $ cd ../../../

Nu ska ni vara tillbaka i ``debian_amd64`` katalogen om ni följt instruktionerna rätt. 

Nätboota maskinen som vanligt nu så ska den inte längre fråga om extra firmware. 
# Se också

*  [Nätboota Debian Squeeze](../naetboota_debian_squeeze)
*  [Nätboota Ubuntu 12.04](./naetboota_ubuntu_12_04)
*  [Ansible inledning](../../ansible/inledning)
*  [Preseed dokumentation](http://d-i.alioth.debian.org/manual/en.i386/apb.html)
