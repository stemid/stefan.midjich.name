# Drifthandbok: Hemmabyggt NAS

## Lista ZFS volymer

    $ sudo zfs list

## Utöka utrymme på en ZFS volym

    $ sudo zfs set volsize=60G pool1/vm-db01-disk1

## Upptäcka volymer på klient

Se vilka volymer som tillhör vilket NAS.

    $ sudo iscsiadm -m session -P 3

## Migrera en volym

Kan använda dd på klientsystemet t.ex..

    $ sudo dd if=/dev/sdf of=/dev/sdb bs=64K conv=noerror,sync status=progress

## Fallgropar

ZFS verkar tappa kontakt med volymerna, tills jag listar ut varför måste jag starta om OS för att allt ska fungera igen. 

Troligtvis är det någon tjänst som behöver startas om.


# Se också


*  [projektsida: hemmabyggt nas](../projekt/hemmabyggt_nas)

*  [installationshandbok: ZFS på Fedora 25](./diskhantering_i_linux/zfs_pa_fedora_25)
