# Radera historik i Dokuwiki

Jag är lite klumpig och har mer än en gång laddat upp känslig information till github eller en wiki. 

Här visar jag hur man kan radera historik från en sida i dokuwiki. 

Första knepet är att bara söka igenom alla filer efter den känsliga strängen, då är det viktigt att använda zgrep. 

    sudo find /var/www/wiki/data -name '*.gz' -exec zgrep 'min hemlis' {} \;

Under ''/var/www/wiki/data/attic'' lagras alla historiska ändringar som gjorts på sidor. Så säg att sidan jag vill ändra historiken för heter *teknik:guider:Teknikguide*, då hittar jag filer under ''/var/www/wiki/data/attic/teknik/guider/teknikguide.####.txt.gz'' som innehåller ändringarna. 

#### representerar en Unix tidsstämpel. Dessa tidsstämplar finns även i filen ''/var/www/wiki/data/meta/teknik/guider/teknikguide.changes'' i stigande ordning, så den senaste ändringen visas längst ner i filen. 

Filen changes kan se ut så här, jag visar bara de tre sista raderna. 

```
1420838748      00.00.00.00  E       teknik:guider:teknikguide      stemid          
1420839004      00.00.00.00  E       teknik:guider:teknikguide      stemid  [Användare]     
1420839018      00.00.00.00  E       teknik:guider:teknikguide      stemid  [WiFi]
```

Så om ändringen jag vill radera är rad 2 med tidsstämpel 1420839004, så måste jag radera filen ''.../data/attic/teknik/guider/teknikguide.1420839004.txt.gz''. 

Självklart är det bra att ta backup på filer innan du raderar dem. 
