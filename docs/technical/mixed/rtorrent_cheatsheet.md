# rTorrent cheatsheet


*  Enter - Add torrent or magnet link without starting

*  Backspace - Add and start immediately

*  ^d - Stop torrent, and remove it if it's stopped

*  ^o - Modify inactive torrent's download dir

*  ^s - Start torrent

# See also


*  [Official docs](https://github.com/rakshasa/rtorrent/wiki/User-Guide)
