# Chrootad SFTP med pam_mysql för autentisering i RHEL6

För att konfa pam_mysql läs [Autentisering med PAM MySQL i RHEL 6](../autentisering_med_pam_mysql_i_rhel6). 

Mitt mål var att lägga till SFTP till en befintlig FTP-server, därav började jag med guiden ovan. Så jag ville även använda samma användardatabas i MySQL som vsftpd gör i den guiden. 

Jag valde att göra all konfiguration i en separat OpenSSH process för att undvika att slutanvändare loggade in på den administrativa OpenSSH-servern som jag använder. 

Samtidigt vill du kunna använda normal patchhantering via yum och därför var det bäst att skapa en symlänk till sshd binären istället för att duplicera den tjänsten på något annat sätt.

## OpenSSH konfiguration

Först länka ``/usr/sbin/sshd`` till ``/usr/sbin/chroot_sshd``, detta leder till att PAM-tjänstenamnet byts till chroot_sshd så man slipper skapa en röra i ``/etc/pam.d/sshd``. 

    sudo ln /usr/sbin/sshd /usr/sbin/chroot_sshd

Skapa sedan en egen konfigurationskatalog för Chrootad OpenSSH. 

    sudo mkdir /etc/chroot_sshd

Kopiera moduli-filen och skapa host-nycklar. 

    sudo cp /etc/sshd/moduli /etc/chroot_ssh/moduli
    sudo ssh-keygen -N "" -t dsa -f /etc/chroot_ssh/ssh_host_dsa_key
    sudo ssh-keygen -N "" -t ecdsa -f /etc/chroot_ssh/ssh_host_ecdsa_key

Skapa ``/etc/chroot_ssh/sshd_config``. 

```
AllowGroups sftpusers

Protocol 2
HostKey /etc/chroot_ssh/ssh_host_dsa_key
HostKey /etc/chroot_ssh/ssh_host_ecdsa_key
Port 1022
PermitRootLogin no
PasswordAuthentication yes
ChallengeResponseAuthentication no
GSSAPIAuthentication no
GSSAPICleanupCredentials no
LoginGraceTime 60
MACs hmac-sha2-512,hmac-sha2-256
Ciphers aes256-cbc,aes256-ctr
PrintMotd no
AuthorizedKeysFile .ssh/authorized_keys
UsePAM yes
#Banner /etc/chroot_ssh/ssh_banner

ChrootDirectory /var/sftp/%u
ForceCommand internal-sftp
X11Forwarding no
AllowTcpForwarding no
PermitTunnel no

Subsystem       sftp    internal-sftp
```

## PAM konfiguration

Redigera filen ``/etc/pam.d/chroot_sshd`` med följande information. Detta är i princip exakt samma som vsftpd använder i den förra guiden. 

```
session    optional     pam_keyinit.so    force revoke
auth    required        pam_mysql.so    config_file=/etc/pam_mysql.conf
account required        pam_mysql.so    config_file=/etc/pam_mysql.conf
```

## Hemkataloger och rättigheter

Vsftpd har alla sina hemkataloger i ``/home``. Jag kan tyvärr inte använda dem direkt om jag vill chroota mina SFTP-användare. 

Så vi skapar ``/var/sftp`` som ny hemkatalog. Se ChrootDirectory i sshd-konfigurationen ovan. 

    sudo mkdir /var/sftp
    sudo chown -R root:root /var/sftp

Sedan måste varje användare ha en egen katalog under ``/var/sftp`` och den katalogen måste ägas av root:root. 

Exempel:

    sudo mkdir /var/sftp/luser
    sudo chown -R root:root /var/sftp/luser

Till sist, för att användarna ska ha tillgång till sina hemkataloger måste de monteras in under deras nya hemkatalog i ``/var/sftp``. 

    sudo mount --bind /home/luser /var/sftp/luser/ftp_home

Lite omständig process att göra för många användare men nödvändigt för säkerheten. 

### Automatisering

Processen ovan kan automatiseras något med pam_exec.so. Lägger till följande rad i min ``/etc/pam.d/chroot_sshd`` fil som nämns ovan. 

    auth required  pam_exec.so     log=/var/log/pam_exec.log /usr/local/sbin/bootstrap_sftp.bash

Skriptet kan se ut [så här](https://gist.github.com/stemid/abbc96e5b3cac83b0985557bb91d15a0). 

>**Observera** att tyvärr kunde jag inte använda autofs här på grund av en konflikt med SElinux. Det kanske går att lösa med en policyändring men i princip så ändrar autofs-tjänsten kontext på alla kataloger den ska arbeta mot, som t.ex. /var/sftp i detta fallet. Kontext autofs_t tillåter inte ens root att skapa nya filer där. 

## Starta din OpenSSH

Kör man RHEL6 som jag gjorde kan det vara frestande att skapa ett upstart-jobb för sin chroot_sshd men gör inte det eftersom upstart körs på RHEL6 under system_u:system_r vilket betyder att den startar din chroot_sshd med samma selinux kontext och då kommer den sshd-processen inte lyssna på vissa policys som krävs för att kunna ansluta till MySQL. 

Så jag kopierade istället ``/etc/init.d/sshd`` till ``/etc/init.d/chroot_sshd`` och tog bort lite onödiga saker som att skapa host-nycklar. Se den [här på github](https://gist.github.com/stemid/92bfc569b1926596ead5). 

Sen starta tjänsten. 

    sudo chkconfig chroot_sshd on
    sudo service chroot_sshd start

### SElinux och port 1022

Vill du verkligen använda port 1022 som jag skriver i exempelkonfigurationen så behöver SElinux konfas för det. 

    sudo semanage port -a -t ssh_port_t -p tcp 1022

## Säkerhetsaspekt med SFTP och MITM

Det finns inget bra skydd mot MITM-attacker med SFTP så en mitigering kan vara att använda SSHFP-inlägg i DNS, tillsammans med DNSSEC helst. 

Här är ett skript på [gist](https://gist.github.com/stemid/892fc4d51f13f0241970) som kan användas för att generera SSHFP-inlägg som du sedan lägger in i din DNS. 

## Se också

*  [Autentisering med PAM MySQL i RHEL 6](../autentisering_med_pam_mysql_i_rhel6)
*  [Chrootad SFTP artikel #1](http://ramblingfoo.blogspot.se/2015/05/howto-no-ssh-logins-sftp-only-chrooted.html)
*  [Chrootad SFTP artikel #2](https://wiki.archlinux.org/index.php/SFTP_chroot)
*  [Chrootad SFTP artikel #3](http://www.thegeekstuff.com/2012/03/chroot-sftp-setup/)
