# Autentisering med PAM MySQL i RHEL 6

Här ska vi utforska pam_mysql som är en modul för [..:unix:PAM](../unix/PAM) som låter oss autentisera användare mot en MySQL databas. 

Detta kräver att du har följande saker i ordning. 


*  [RHEL 6](../installation av rhel6) eller CentOS 6.

*  vsftpd

*  Någon MySQL, antingen från Oracle eller [MariaDB](../mysql_ha_med_pacemaker).

*  [..:unix:SELinux](../unix/SELinux) konfigurerat att tillåta anslutningar från FTP-server till MySQL-socket.

## Installation av paket

Pam_mysql finns inte i standard paketförråd för RHEL, så jag installerar EPEL-förrådet [härifrån](http://mirror.nsc.liu.se/fedora-epel/6/x86_64/repoview/epel-release.html). 

    wget http://mirror.nsc.liu.se/fedora-epel/6/x86_64/epel-release-6-8.noarch.rpm
    sudo rpm -i epel-release-6-8.noarch.rpm

Sedan kan man installera pam_mysql direkt med yum. 

    sudo yum install pam_mysql

## Konfiguration av pam_mysql

Jag tänker inte aktivera pam_mysql autentisering för operativsystemet, utan för [FTP-servern vsftpd](https://security.appspot.com/vsftpd.html), som är en tjänst. 

I vsftpd kan man ange vad tjänstenamnet ska vara för pam, med inställningen ''pam_service_name'', så jag antar att namnet är vsftpd. Då ska man redigera filen ''/etc/pam.d/vsftpd'' så att den ser ut så här. 

    session    optional     pam_keyinit.so    force revoke
    auth    required /lib64/security/pam_mysql.so config_file=/etc/pam_mysql.conf
    account required /lib64/security/pam_mysql.so config_file=/etc/pam_mysql.conf

Här anger vi en konfigurationsfil i ''/etc/pam_mysql.conf'', den ska se ut så här. 

    users.host=/var/lib/mysql/mysql.sock
    users.database=vsftp
    users.db_user=vsftp
    users.db_passwd=hemlis
    users.table=accounts
    users.user_column=username
    users.password_column=password
    users.password_crypt=3
    users.disconnect_every_operation=true
    
    log.enabled=yes
    log.table=logs
    log.message_column=message
    log.pid_column=pid
    log.user_column=username
    log.host_column=host
    log.rhost_column=rhost
    log.time_column=time
    
    #verbose=1

I den filen anger vi bland annat vad databasen ska heta, vad tabellerna ska heta och vad kolumnerna i tabellerna ska ha för namn. Därför ska vi nu skapa en databas som följer det namnschemat. 

Anslut till ett mysql-skal med root-kontot, kör följande kommandon för att skapa databasen, dess tabeller och en användare som vsftp kan logga in med. 

    mysql> create database vsftp;
    mysql> use vsftp;
    mysql> create table accounts ( id int auto_increment not null primary key,
    username varchar(30) not null,
    password varchar(50) not null
    );
    mysql> create table logs ( id int auto_increment not null primary key,
    message varchar(255),
    pid int,
    username char(128),
    host char(128),
    rhost char(128),
    time timestamp
    );
    mysql> grant select on vsftp.accounts to vsftp@localhost identified by 'hemlis';
    mysql> grant insert on vsftp.logs to vsftp@localhost identified by 'hemlis';
    mysql> flush privileges;

## Skapa användare i MySQL

Nu kan vi skapa vår första användare i PAM MySQL, logga in i MySQL skalet igen, som root eller en användare med förmågan att skriva till accounts tabellen. 

    mysql> insert into accounts (username, password) values ('testkonto', md5('Testkonto2013'));

Så det är allt. :)

Testa att logga in samtidigt som secure loggen visar resultatet från pam_mysql modulen. 

    sudo tail -0f /var/log/secure|grep pam_mysql

## Noteringar

Både Debian och Ubuntu har i skrivande stund version 0.7 av pam_mysql, vilket betyder att det bör gå att följa den här artikeln. Jag tänker dock använda mig av RHEL 6.3, och eftersom det är ett betalsystem så skulle jag egentligen säga att CentOS 6.3 är motsvarigheten. 

Mellan RHEL och CentOS borde den här artikeln vara identisk. 

Version 0.7 eller högre av pam_mysql-paketet är ett krav eftersom den versionen har stöd för en konfigurationsfil som jag tycker gör konfigurationen lite snyggare och läsligare. 

# Se också


*  [Installation av RHEL6](../installation_av_rhel6)

*  [.:Chrootad SFTP med pam_mysql på RHEL6](./Chrootad SFTP med pam_mysql på RHEL6)

*  [..:unix:SELinux](../unix/SELinux)

*  [..:unix:iptables](../unix/iptables)

*  [Online readme för pam_mysql](http://pam-mysql.sourceforge.net/Documentation/package-readme.php)
