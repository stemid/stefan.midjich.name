FIXME: Arbete på denna guide pågår fortfarande i skrivande stund. 

# Backup i RHEL6 med Kickstart, Ansible och Attic

Denna guide beskriver en lösning jag gjorde där flera fysiska klienter skulle backas i olika nätverk som ibland hade brandväggar mellan sig. Alla klienter var HP ProLiant G8 maskiner med iLO advanced och RHEL 6 installerat. 

Lösningen fokuserar på att kunna backa stora mängder data, men samtidigt på att kunna återställa små mängder data snabbt för att återställa tjänsten. 

Backup sker i två steg, där man först och främst gör alla konfigurationsändringar i Ansible och pushar ut dem via SSH till klienterna. För det andra så körs attic regelbundet och tar backup på hela systemen, med eventuellt vissa undantag på specifika kataloger. 

Så här är rollerna av de olika mjukvarorna. 

## Kickstart


*  Återställning av grundsystem via iLO. 

*  Används utan ''network''-alternativet så den ställer frågan om IP-adress, annars sker resten automagiskt. 
## Ansible


*  Hantering av systemkonfiguration. 

*  Återställning av systemkonfiguration via SSH. 

## Attic


*  Återställning av databasdumpar som behövs för att återställa tjänst. 

*  Återställning av bulkdata som är bland annat arkiverade logfiler och backuper från andra system. Denna data behövs inte för att återställa tjänsten. 

# Installationshandbok

## Skapa ISO med kickstart

Montera först RHEL ISO. 

    mkdir {rhel_iso,rhel_mnt}
    sudo mount -o loop rhel-server-6.5-x86_64-dvd.iso rhel_mnt/

Kopiera allt innehåll till en ny katalog. 

    shopt -s dotglob
    cp -ri iso_mnt/* rhel_iso/

### Skriv kickstart

Skapa en ks.cfg direkt i roten av iso-trädet. 

    vim rhel_iso/ks.cfg

Den kan se ut så här. 

```
keyboard sv-latin1
lang sv_SE
timezone Europe/Stockholm

#Root account disabled

rootpw --iscrypted $6$...

#Initial user

user --name admin --iscrypted --groups wheel --password $6$...

#Reboot after installation

reboot

#Use text mode install

text

#Install OS instead of upgrade

install
cdrom
#url --url http://10.221.168.23/rhel6-x86_64

#System bootloader configuration

bootloader --location=mbr

#Clear the Master Boot Record

zerombr yes

#Partition clearing information

clearpart --all --initlabel

#Create partitions

partition /boot --size=500 --fstype ext2 --asprimary #Boot
#partition swap --size=1g --asprimary #Swap

partition pv.01 --size=1 --fstype ext4 --grow #Rest

#Create lvm

volgroup vg_system pv.01
logvol swap --vgname=vg_system --size=1024 --name=lv_swap
# I haven't seen precise results with percent but it accomplishes the goal of

# leaving a little on the PV for new volumes and expansion.
logvol / --vgname=vg_system --size=20000 --grow --percent=80 --name=lv_root

#System authorization infomation

auth  --useshadow

#Firewall configuration

firewall --disabled

#Do not configure the X Window System

skipx

services --enabled ssh

#Packages

%packages
openssh-server
openssh-clients
vim
curl
nmap
%end

%post --log=/root/ks-post.log
echo '%wheel ALL=NOPASSWD: ALL' > /etc/sudoers.d/wheel
visudo -cf /etc/sudoers.d/wheel || rm /etc/sudoers.d/wheel
```

Denna filen frågar efter nätverksinformation eftersom den ska användas för att återställa flera olika maskiner utan PXE så måste det göras så här. 

## Skapa ISO

Jag skapar den på Debian så jag använder genisoimage. På RHEL kan man bara byta ut kommandot mot mkisofs. 

    cd rhel_iso
    genisoimage -o /var/rhel6-server-x86_64-dvd.iso -b isolinux/isolinux.bin -c isolinux/boot.cat --no-emul-boot --boot-load-size 4 --boot-info-table -J -R -V disks .
# Se också


*  [Hur du lägger in en kickstart fil i en RHEL ISO](https://access.redhat.com/solutions/60959)

*  [RHEL Kickstart alternativ](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/s1-kickstart2-options.html)
