# Dnsmasq på OpenBSD 5.5

Jag installerar dnsmasq från binära paket eftersom jag gör det på min relativt svaga router. 

    sudo pkg_add dnsmasq

Redigera sedan ''/etc/rc.conf.local'' och lägg till dnsmasq i pkg_scripts. 

    pkg_scripts="dnsmasq"
