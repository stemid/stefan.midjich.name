# DNSSEC med Bind

Guiden utfördes på en RHEL6 server med Bind 9.8. Du måste ha minst Bind 9.7 för att göra omsignering av zoner med dynamisk DNS i sista halvan av guiden.

Zonen som signeras heter **example.com** och lagras i filen **db.example.com**. 

# Förberedande

Installera tjänsten haveged. Den hjälper till att skapa nycklar och signera zoner snabbare genom att tillföra entropy till PRNG. Ska autostartas.

# DNSSEC

## Konfiguration

I huvudkonfen för Bind ska följande rader in under options.

    dnssec-enable yes;
    dnssec-validation yes;
    dnssec-lookaside auto;

>**OBS**: Detta är enda konfen som behövs på slavar för att ta emot signerade zoner.
## Skapa nycklar

Detta ska göras för varje zon du vill signera. 

Först skapas en Zone Signing Key (ZSK).

    $ dnssec-keygen -a NSEC3RSASHA1 -b 2048 -n ZONE example.com

Sedan en KSK eller Key Signing Key.

    $ dnssec-keygen -f KSK -a NSEC3RSASHA1 -b 4096 -n ZONE example.com

Lägg sedan till filerna i din zonfil, här är ett skalkommando som kan göra det.

Så här kan det se ut längst ner i zonfilen, varierar beroende på filnamnen av nycklarna som skapats. 

```

$INCLUDE Kexample.com.+007+36123.key
$INCLUDE Kexample.com.+007+05054.key
```

## Signera zonen

    $ dnssec-signzone -3 \
    $(head -c 1000 /dev/random | sha1sum | cut -b 1-16) \
    -A -N INCREMENT -o example.com -t db.example.com

Head-kommandot skapar en slumpmässig salt-sträng. 

Nu ska det skapats en fil som heter db.example.com.signed, se till att den finns innan du fortsätter.

## Redigera zon-definitionen

Zonens definition ska se lite annorlunda ut, här är ett exempel.

```
zone "example.com" in {
    type master;
    file "db.example.com.signed";
    //auto-dnssec allow;
    //update-policy local;
};
```

Raderna som är kommenterade behövs i nästa del av guiden.

# Omsignering av zoner

Signeringen av en zon går ut efter 30 dagar så zonen måste signeras om regelbundet. Vissa säger varje dag. 

Innan Bind 9.7 var detta svårt och manuellt eftersom du var tvungen att anropa dnssec-signzone och du var tvungen att uppdatera serienummer i zonen först. 

Men sedan 9.7 använder vi DDNS för att dynamiskt signera om zonen. 

## Konfiguration av zon-definitionen

DDNS kräver två extra rader i definitionen av zonen.

```
zone "example.com" in {
    type master;
    file "db.example.com.signed";
    auto-dnssec allow;
    update-policy local;
};
```

> **Viktigt**: När DDNS används ska den gamla zonfilen inte längre redigeras. Uppdateringar görs dynamiskt via nsupdate kommandot och filen db.example.com.signed uppdateras på disk av named men ska aldrig redigeras manuellt. Serienummer uppdateras automatiskt och kommer inte längre följa någon egen standard med datum+löpnummer.

Starta om Bind och se till att du inte får fel i loggarna.

### update-policy

Här är en snabb genomgång av update-policy som jag inte utvecklar mer i denna guiden.

Direktivet ''update-policy'' kan vara kraftfullt genom att ange regler för olika nycklar. Så du kan skapa egna nycklar, definiera dem med ett ''key'' direktiv och sedan skriva ''update-policy-rule'' som definierar vilka sorters uppdateringar en nyckel får göra.

    $ dnssec-keygen -a HMAC-SHA512 -b 512 -n HOST ddnskey
    $ grep ^Key Kddnskey*.private
    Key: sW...==
    $ grep -A3 ddnskey /etc/named.conf
    key "ddnskey" {
       algorithm hmac-sha512;
       secret "sW...==";
    };
 1. ---
    update-policy { grant ddnskey name example.com A; };

Genom att ange ''update-policy local'' tar vi den enklaste vägen där Bind själv skapar en nyckel under ''/var/run/named/session.key'' som måste användas i nästa steg.

## Uppdatering av dynamisk DNS

Med DDNS görs uppdateringar genom nsupdate-verktyget. Antingen interaktivt eller med en fil av kommandon. 

    $ nsupdate -k /var/run/named/session.key
    > zone example.com.
    > update add test.example.com 7200 TXT "testar"
    > send

Kommandon kan läggas i en fil och köras med ''nsupdate filnamn''.

Du kan fortfarande se zonfilen ''db.example.com.signed'' men du ska **absolut inte redigera den**.

>God praxis är att alltid använda zone i början av filen för att ange exakt vilken zon du vill hantera. Annars försöker nsupdate själv lista ut det.

### Exempel på nsupdate kommandon

Radera ett inlägg.

    zone example.com.
    update delete spam.example.com TXT
    send

Flera uppdateringar på en gång, sparar bandbredd. 

    update delete spam.example.com TXT
    update add spam.example.com 86400 TXT "test"
    update add testdns.example.com 86400 A 172.16.0.1
    send

Uppdatera bara om det inte finns några befintliga inlägg med samma namn redan.

    prereq nxdomain nyttnamn.example.com
    update add nyttnamn.example.com 86400 A 172.16.2.3
    send

Gör en radering först och ett nytt inlägg bara om det inte redan existerar.

    update delete nyttnamn.example.com A 172.16.0.1
    send
    prereq nxdomain nyttnamn.example.com
    update add nyttnamn.example.com 86400 A 172.16.2.4
    send

Lägg till en MX pekare.

    update add mail.example.com 86400 MX 10 mail.example.com
    send

Radera delegering till annan namnserver, detta kräver att du anger zone i början av filen.

    zone example.com.
    update delete delegering.example.com. NS dns1.externdns.net.
    update delete delegering.example.com. NS dns2.externdns.net.
    send

# Rotering av ZSK

Det sker automatiskt av Bind om du använder nsupdate och dynamiska DNS uppdateringar.
# Felsökning

Se serienummer med ''dig''. 

    $ dig +multi @ns-1660.awsdns-15.co.uk dn.se SOA
    ...
    ;; ANSWER SECTION:
    dn.se.                  900 IN SOA ns-1660.awsdns-15.co.uk. awsdns-hostmaster.amazon.com. (
                                1          ; serial
                                7200       ; refresh (2 hours)
                                900        ; retry (15 minutes)
                                1209600    ; expire (2 weeks)
                                86400      ; minimum (1 day)
                                )


# Se också


*  [DigitalOcean guide för DNSSEC](https://www.digitalocean.com/community/tutorials/how-to-setup-dnssec-on-an-authoritative-bind-dns-server--2)

*  [Bind manual om update-policy](http://www.zytrax.com/books/dns/ch7/xfer.html#update-policy)

*  [Manual för nsupdate](http://www.rtfm-sarl.ch/articles/using-nsupdate.html)
