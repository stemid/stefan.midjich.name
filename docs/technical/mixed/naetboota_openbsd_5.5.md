# Nätboota OpenBSD 5.5

Skrev den här guiden främst för att jag skulle installera OpenBSD 5.5 på ett [PCengines APU-bräde](http://pcengines.ch/apu.htm), så det finns lite extra tips i den just för APU-installation. 

Jag använder fortfarande en Debian-server för DHCPd, TFTPd och HTTPd så se [tidigare](./naetboota_debian_squeeze.md) [artiklar](./naetboota_ubuntu_12_04) för mer info. 

Vad man behöver här är följande. 

*  DHCP-server
*  Webbserver
*  pxeboot filen från en [OpenBSD FTP-spegel](ftp://ftp.eu.openbsd.org/pub/OpenBSD/5.5/amd64/)
*  bsd.rd filen från en [OpenBSD FTP-spegel](ftp://ftp.eu.openbsd.org/pub/OpenBSD/5.5/amd64/)

## Konfigurera DHCP

Detta ska redan vara bekant för de som nätbootat Debian eller Ubuntu men så här ser min konfiguration ut. 

```
  group {
    filename "auto_install";
    next-server pxe.swehack.local;

    host gw2 {
      hardware ethernet 00:0d:b9:XX:XX:XX;
      fixed-address gw2.swehack.local;
    }
    }
```

Filnamnet ``auto_install`` en symlänk på tftp-servern till pxeboot-filen som nämndes tidigare. Att döpa filen i filename till ``auto_install`` eller ``auto_upgrade`` är ett nytt knep i OpenBSD 5.5 för att automatisera installationer. 

Här kommer en sorglig del av OpenBSD in i leken, pxeboot filen kommer automatiskt leta efter saker under / av next-server. Vilket betyder att det inte tjänar något till att försöka organisera sina tftp-filer i olika kataloger eftersom pxeboot kommer leta efter pxe.swehack.local/etc/boot.conf eller pxe.swehack.local/bsd.rd t.ex.. 

### Kompilera pxeboot (bonus)

Detta gjorde jag bara för att kunna peka till en annan boot.conf än den i ``/etc/boot.conf``. 

Redigera filen ``sys/stand/boot/boot.c`` i källkodsträdet, hitta raden som säger ``/etc/boot.conf`` och se till att den ser ut som nedan. 

    cmd.conf = "/openbsd55/etc/boot.conf";

Stå sedan i ``sys/arch/amd64/stand/pxeboot`` och kör make för att kompilera ihop pxeboot-filen. 

Likaväl kan man få installationen att använda en egen partitionering om man redigerar skripten i ``distrib/amd64/miniroot/``. 

## Konfigurera TFTP

Jag har de flesta relevanta filer i tftprot/openbsd55 men tyvärr måste jag ha ``auto_install`` symlänken från ``/tftprot/auto_install -> /tftprot/openbsd55/pxeboot``. 

Annars är det bara att dumpa in pxeboot och bsd.rd filerna i openbsd55-katalogen och se till att de kan läsas av tftp-processen. 

Under boot kommer bsd.rd söka efter filerna ``/tftprot/macaddress-install.conf`` och ``/tftprot/install.conf`` så jag länkar även den från ``/tftprot/openbsd55/install.conf``. 

!!! note
    Är filename ``auto_upgrade`` så söker den efter motsvarande filer med upgrade istället för ordet install i filnamnen. 

Det finns ett exempel på den filen i [autoinstall(8)](http://www.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man8/autoinstall.8?query=autoinstall&sec=8)-manualen men så här ser min ut ungefär. 

För strukturens skull har jag valt att länka ``/tftprot/00:0d:b9:XX:XX:XX-install.conf -> /tftprot/openbsd55/install.conf``. 

```
System hostname = gw2.swehack.local
Start sshd(8) by default = yes
Start ntpd(8) by default = yes
NTP server = default
Do you expect to run the X Window System = no
Password for root = $2a...D36
Change the default console to com0 = yes
Which speed should com0 use = 115200
Setup a user = stemid
Full name for user stemid = Stefan Midjich
Password for user = $2a...v1W
Public ssh key for user = ssh-rsa AAAAB3NzaC1yc2EAAAADAQA...uAVsDHxh stemid@zakalwe
What timezone are you in = Europe/Stockholm 
Location of sets = http 
HTTP Server = ftp.eu.openbsd.org
Use (A)uto layout, (E)dit auto layout, or create (C)ustom layout = C
```

Personligen var jag inte nöjd med autoinstall i OpenBSD eftersom jag inte kunde lista ut hur man skulle ange sin egen partitionstabell utan att redigera startskripten i bsd.rd. 

## Konfigurera HTTPd

**Observera** mest att alla filer utom pxeboot och bsd.rd kommer hämtas från HTTP-servern. Och då behandlas next-server som denna HTTP-server, och filerna hämtas direkt från roten. 

DHCPd kommer troligtvis lösa upp namn i next-server innan pxeboot och OpenBSD tar över så filen hämtas från ``http://ip-adress/install.conf``. 

Jag brukar låta tftpd och httpd dela på samma katalog, och filerna ägs av tftp:www-data på Debian. 

## Starta installationen

Nu är det bara att starta upp installationen. 

Är installationen på ett APU-bräde så behöver man ansluta till det via korsad seriell kabel. **Observera att en USB adapter räcker oftast inte utan man behöver ansluta USB-adaptern till en korsad kabel, och sedan ansluta kabeln till APU-brädet. 

Jag brukar använda screen för detta eftersom det alltid fungerar bra. 

    sudo screen /dev/ttyUSB0 115200,cs8

## Serieport

För att kunna ansluta till APU-brädet med seriekabel måste man först redigera ``/etc/boot.conf``.

    set tty com0
    stty com0 115200

Sedan aktivera konsollen och ändra hastigheten i ``/etc/ttys``. 

    console "/usr/libexec/getty std.115200"  vt220   on secure

# Se också


*  [Nätboota Ubuntu 12.04](./naetboota_ubuntu_12_04)
*  [Dnsmasq på OpenBSD 5.5](./dnsmasq_pa_openbsd_5.5.md)
*  [OpenBSD FAQ6](http://www.openbsd.org/faq/faq6.html#PXE)
*  [OpenBSD manual: autoinstall](http://www.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man8/autoinstall.8?query=autoinstall&sec=8)
*  [OpenBSD manual: boot.conf](http://www.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/i386/boot.conf.5?query=boot.conf&sec=5&arch=i386)
