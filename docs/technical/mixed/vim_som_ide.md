# Vim som IDE

Vim som utvecklingsmiljö har diskuterats många gånger på nätet men här samlar jag punkter som utgör mitt perspektiv allteftersom jag kommer på dem. 

Jag har använt Vim som IDE ganska länge men det är först senaste året som jag arbetat med stora projekt som kräver mer än en enskild vim.

Jag försöker använda standard vim8 så mycket som möjligt. Alltså inga tunga plugin-hanterare som pathogen. Min filosofi är att börja med standard-vim och sedan bygga på allteftersom behovet uppstår. Jag kodar mest Python, Javascript, HTML och CSS så jag har fokuserat syntax filer och konfig på de språken. 

# Plugins

Jag använder vim8 och där är det mycket enklare och snyggare med plugins. 

Skapa katalogstrukturen för dina plugins. 

    $ cd .vim
    $ mkdir -p pack/my-plugins/{start,opt}

I ''start'' ligger plugins som autostartar, i ''opt'' ligger de som inte autostartar. 

Sedan kan man bara git-klona ett förråd rakt ner i katalogerna så funkar det oftast. Annars är pluginet dåligt gjort och förtjänar inte min tid. ;)

Läs mer i '':he package''.


# Öppna filer med Tab-komplettering

    :e ../projek`<Tab>`
    :e ../projekt1/`<Tab>`
    :e ../projekt2/
    `<Ctrl+E>`
    `<Tab>`
    :e ../projekt2/fil.py

Använd **Ctrl+E** för att gå till slutet av raden och få Tab att börja bläddra i nästa katalog. 

Använd **Ctrl+A** för att lista filer som kommer tabbas i nästa katalog, detta avslutar också bläddrandet i första katalognivån. 

    :e ../proj`<Tab>`
    :e ../projekt1/
    `<Ctrl+A>`
    filen.py projektet.yml

# Flikar

Det viktigaste hjälpmedlet; istället för att starta nya instanser av vim, starta ny flik. 

Redigera en fil i ny flik.

    :tabe ../projekt/fil.py

Ange startkatalog för den nya flikens fönster.

    :lcd ../projekt

Gå fram och tillbaka mellan flikarna. 

    :tabn
    :tabp

Hoppa mellan flikar med siffror i "normalt läge", alltså inte kommandoläge och inte skrivläge. 

    3gt
    2gt

Flikarna numreras från 1, trycker du 0gt så hamnar du på första fliken precis som 1gt.

## Döpa flikar

Vim har som standard ganska jobbiga namn på flikar.

Jag använder pluginet [Taboo](https://github.com/gcmt/taboo.vim) med vim8, med denna konfigen i min vimrc.

    let g:taboo_tab_format=" %N [%P]%m"
# Buffer

Lista alla buffer, kan också ses som öppna filer.

    :ls
    1 %a   "repl.py"                      line 36
    2  a   "~/Development/projekt1/view.py" line 0

Ta bort buffer som matchar ett filnamnsmönster genom att skriva in ditt mönster och trycka Ctrl+a för att låta Vim expandera mönstret till alla buffer som matchar.

    :bd ~/Development/projekt1*
    `<Ctrl+a>`

# Verktyg

## The Silver Searcher

Kommandot ag som installeras med paketet ''the_silver_searcher'' på Fedora är ett smidigt sätt att söka i din källkod.

## Tmux

Något verktyg som tmux, eller screen, är nödvändigt för att enkelt köra kommandon och REPL verktyg samtidigt som Vim är igång. 

Effekten är densamma som en modern IDE men allt styrs via tangentbordet så klart, du växlar fönster mellan REPL och Vim med tangentbordet. I vissa fall kan man visa både Vim och en terminal i samma tmux-panel. 


*  Meta+ Ctrl+n - Att snabbt växla mellan två fönster i tmux.


## Mina plugins


*  [Taboo](https://github.com/gcmt/taboo.vim)



# Se också


*  [Vim tab madness](http://joshldavis.com/2014/04/05/vim-tab-madness-buffers-vs-tabs/)

*  [Vim as a Python IDE](http://www.liuchengxu.org/posts/use-vim-as-a-python-ide/)
