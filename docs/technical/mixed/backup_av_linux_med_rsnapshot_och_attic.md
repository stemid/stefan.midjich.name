# Backup av Linux med rsnapshot och attic

>**OBS**: Har sedan detta skrevs gått vidare till borgbackup istället eftersom attic hade problem med stora dataset och många andra små buggar som gjorde det instabilt.

Här beskriver jag ett backupsystem jag gjorde baserat på [rsnapshot](http://rsnapshot.org/) och [Attic](https://attic-backup.org/). 

Jag har skapat detta system i Ansible, men istället för att visa alla ansible filer försöker jag här enkelt förklara konfigurationen. 

Fördelarna med detta backupsystemet över andra hemmasnickrade lösningar för Linux är främst Attic och dess deduplicering av data. Personlig erfarenhet av Attic som backupsystem ger mig en bättre ratio av deduplicering än t.ex. den proprietära Symantec Netbackup. I mitt fall var det nästan 10 till 1 ratio för data som är blandade textfiler, arkiv och binära systemfiler från Linux. 

Ett alternativ till Attic är [bup](https://bup.github.io/) men jag valde Attic för dess förmåga att radera specifika versioner av backupen. I skrivande stund kunde bup inte radera individuella versioner ur en dedup-yta. 

Bup kunde dock backa genom att logga in över ssh på en maskin och *pulla* datan över nätverket. Medans med attic behöver jag en överföring av rsnapshot som mellanlagrar datan innan den dedupliceras. 
## Terminologi & övergripande design

*Deduplicering* är någon sorts magi som görs med block av data för att undvika att lagra kopior av samma data. Så t.ex. ett Linuxsystem utgörs av en systemvolym som ofta har mycket liknande information, därför dedupliceras den datan så att den bara lagras en gång för alla backuper som görs. 

Detta sparar ju massa lagringsyta. 

*Dedup-yta* är i detta fallet ett förråd, faktiskt ett typ av git-förråd som även kallas repository i Attic-terminologi. 

*Rsnapshot rotkatalog* är dit datan först överförs över nätverket. Det är en sorts mellanlagring och därför använder jag inte rsnapshots funktion att lagra flera versioner av den datan utan jag behöver bara en inkrementell version som uppdateras vid varje körning. 

En backup har följande steg. 

 1.  Rsnapshot gör en dataöverföring av all data, detta lagras i endast en inkrementell version i rsnapshot-rotkatalog
 2.  Attic tar den datan och packar ihop den i sitt dedup-förråd


#  Installation 

Rsnapshot finns i de flesta distros, men attic kräver Python3 och på RHEL6 var jag tvungen att installera det manuellt. Sedan kunde jag använda pip3 för att installera Attic. 

# Konfiguration av rsnapshot

I ''/etc/rsnapshot.conf'' är det följande saker som utmärker sig. 

I *rsync_long_args* brukar jag lägga till argumentet ''--rsync-path='sudo /usr/bin/rsync''' för att köra rsync via sudo på de olika servrarna som ska backas. Då slipper jag tillåta root via ssh och jag kan begränsa den användaren till bara rsync kommandot i sudoers. 

*cmd_preexec* använder jag mest för att skapa lite timing-information om backupen och kanske en låsfil. Här är ett exempel på skriptet jag kör i *cmd_preexec*. 

```
#!/usr/bin/env bash
# {{ ansible_managed }}

[ -r /etc/default/cygdup ] && . /etc/default/cygdup

[ -r /usr/local/bin/cygdup_lib.bash ] && . /usr/local/bin/cygdup_lib.bash

log_msg notice "Starting rsnapshot backup"

:
```

Jag läser in lite konfigurationsvärden och har ett bibliotek med funktioner för loggning men annars kan man hoppa över denna inställningen helt i de flesta fall. 

Mycket mer intressant är *cmd_postexec* för det är så jag startar Attic direkt efter varje körning av rsnapshot. Här är ett exempel. 

```
#!/usr/bin/env bash
# {{ ansible_managed }}

# Post processing for rsnapshot.

# Defaults

export PATH=$PATH:/usr/local/bin
export TIMEFORMAT='%lR'
Bup_Cmd=bup
Snapshots_Dir=/var/backups/snapshots
Dedup_Dir=/var/backups/dedup
Report_RCPT='min.mail@domain.tld'

[ -r /etc/default/cygdup ] && . /etc/default/cygdup

[ -r /usr/local/bin/cygdup_lib.bash ] && . /usr/local/bin/cygdup_lib.bash

# First measure how long the preceeding rsnapshot operation took.

current_time=$(date +%s)
monthly=$(date +%Y-%m)
weekly=$(date +%Y-%m.%V)

# Get creation time of rsnapshot pidfile

lockfile_created=$(stat -c %Z "$Rsnapshot_Lockfile")

# Compare with current time.

diff_time=$((current_time - lockfile_created))

log_msg notice "Rsnapshot operation finished in $(print_minutes $diff_time)"

# BUP_DIR is legacy leftover from bup, which requires this environment variable to exist.

export BUP_DIR=$Dedup_Dir
[ -d $BUP_DIR ] || mkdir -p $BUP_DIR

# Init BUP_DIR, this only needs to be done once.

$Bup_Cmd list $BUP_DIR &>/dev/null || $Bup_Cmd init $BUP_DIR

for backup_dir in "$Snapshots_Dir"/daily.0/*; do
    # Take last component of backup_dir
    backup_name=${backup_dir##*/}

    log_msg notice "Starting deduplication for $backup_name"

    $Bup_Cmd create "$BUP_DIR::$backup_name-$(date +%Y-%m-%d)" "$backup_dir" 2> >(log_msg info)
    rc=$?

    if [ $rc -ne 0 ]; then
   log_msg warn "Deduplication failed for $backup_name"
   continue
    fi

    log_msg notice "Deduplication finished for $backup_name"
done

# Update current time and compare

current_time=$(date +%s)
diff_time=$((current_time - lockfile_created))

log_msg notice "Backup finished in $(print_minutes $diff_time)"

# Now send e-mail report

backup_report=/tmp/backupreport
grep -i "^$(LANG=C date +'%b %d')" /var/log/messages | grep deduplication > $backup_report
$Bup_Cmd info "$BUP_DIR::$backup_name-$(date +%Y-%m-%d)" >> $backup_report

cat $backup_report | mail -s "Backup report $(date +'%Y-%m-%d')" "$Report_RCPT"

exit 0
```

Här är också ett exempel på ''/etc/default/cygdup'' filen med lite konfiguration. 

```
Bup_Cmd=/usr/local/sbin/attic
Snapshots_Dir=/var/backupdata/snapshots
Dedup_Dir=/var/backupdata/repo.attic
Rsnapshot_Lockfile=/var/run/rsnapshot.pid
Report_RCPT="{{cygdup_report_rcpts|join(', ')}}"
```

Den sista raden tar mottagare från ansible och skapar en lista, kan också skrivas manuellt som e-postadresser dit man vill skicka backuprapporter. 

Detta skriptet sköter det mesta som Attic behöver automagiskt. I stort sett är det bara en skriptad metod av exemplet som visas på Attics hemsida, och en loop genom alla servrar som ska backas. 

Till sist är libfilen här också ifall någon skulle vilja ta min lösning rakt av istället för att skriva eget. 

```
# {{ ansible_managed }}

log_facility=deduplication

log_msg() {
    priority=$1 && shift
    msg=$1 && shift

    [ -n "$priority" ] || return 1

    if [ -n "$msg" ]; then
    logger -p "local0.$priority" -t $log_facility "$msg"
    else
    logger -p "local0.$priority" -t $log_facility
    fi
}

print_minutes() {
    seconds=$1
    echo $seconds | awk '{print strftime("%H:%M:%S", $0, 1)}'
}
```
# Återställning av data

Lista alla tillgängliga backuper i ett förråd med *list*-funktionen. 

    sudo attic list /var/backupdata/repo.attic

Se mer info och beräkna dedup-ratio med *info*. 

Återställning av datan kräver llfuse-modulen i kärnan, för att datan ska monteras på en monteringspunkt och kunna visas som ett vanligt filsystem. 

    sudo attic mount /var/backupdata/repo.attic::2015-06-17.server02 /mnt/server02

Namnet bakom ''::'' tas direkt från listan, jag brukar döpa dem till datumet då de togs och servernamnet. Detta kan så klart variera. 

Sedan kan man återställa filerna därifrån med rsync eller något liknande verktyg. 

Avmontera som vanligt med ''sudo umount /mnt/server02''. 
