# Rikta om stdin för en process i Bash

Ett exempel på hur du kan rikta om stdin av en process i bash, målet är att kunna skicka data till input av en process som kör i bakgrunden, från en process i förgrunden. 

Stegen i breda drag är;

 1.  Skapa FIFO fil.
 2.  Skapa bakgrundsprocess som läser input från FIFO filen.
 3.  Skriv till FIFO fil.

Exemplet använder Netcat för att enklast kunna visa resultat så börja med att öppna två terminaler och starta en Netcat server på den ena. 

    nc -k -l 3400

Servern lyssnar på port TCP/3400.

Gå sedan in på klienten och skapa en FIFO fil.

    mkfifo nc.fifo

Starta sedan netcat klienten och peka den mot servernamnet som du använde tidigare. 

    nc localhost 3400 <nc.fifo

Nu väntar netcat på data från FIFO filen istället för ditt skal som den normalt hade gjort. 

    echo 'hejsan' > nc.fifo

Har allt fungerat bra kommer du se budskapet *hejsan* på serveränden. 

# FIFO

FIFO står för *First In, First Out* och är en speciell sorts fil i Unix-lika system som inte lagrar data som vanliga filer utan istället buffrar data för läsning. En FIFO kö är inte oändlig. 

FIFO filer skapas med kommandot ''mkfifo''. 
