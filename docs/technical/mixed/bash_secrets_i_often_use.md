# Bash secrets I often use

Here are some relatively unusual bash use cases that I've ended up relying on. I'm trying to list them in the order of how necessary they are in my daily bash use.

Constant work in progress as you never stop learning. Sources hopefully at the bottom.

# Ctrl+r history search

Simply hit Ctrl+r to search through your history for a previously entered command. 

# Ctrl+w, Ctrl+u, Ctrl+y, Ctrl+a, Ctrl+e

I believe these are called emacs mode, either way I've used them since I was a teenager and they're still used daily.


*  w - Erase last word (up to a word boundary)

*  u - Erase whole line

*  y - Bring back whatever you erased with w and u

*  a - Go to start of command line

*  e - Go to end of command line

# printf can replace date

```
$ printf '%(%Y%m%d.%H%M%S.%s)T\n'        
20180206.113447.1517913287
```

```
$ printf -v mydate '%(%Y%m%d.%H%M%S.%s)T\n'
$ echo $mydate
20180206.113529.1517913329
```

# Parameter Expansion

Just search for ''Parameter Expansion'' in the manual, it's all good.

# Pipe from subshell command

For example loop the output of a command line for line.

```

while read -r line; do echo "$line"; done < <(grep -v 'nothing' /etc/file.cfg)

```
# Bypass shell alias

Run a command with ''\ls'' to bypass any aliases called ls.



# Sources


*  [Bash is weird by Dylan Araps](https://dylanaraps.com/2018/02/05/bash-tricks/)
