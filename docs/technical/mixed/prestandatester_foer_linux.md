# Prestandatester i Linux

Här samlar jag lite olika typer av tester man kan göra, allteftersom jag upptäcker dem eller testar dem själv. 

## Ta tid på kommando

Alla kommandon kan köras med ''time'' framför. T.ex..

    time dd if=/dev/zero of=fil.txt bs=4096 count=1000000

För att mäta tiden som kommandot tar att exekvera. 

## Netcat filöverföring

Netcat är bra för att göra en filöverföring över nätverk helt utan overhead av kryptering eller annat som applikationsprotokoll kan tillföra. 

På server starta en lyssnande netcat-demon på en port. Rikta om all data från den processen till filen ''test.fil''. 

    nc -kl 25000 > test.fil

På klient skjut in data till netcat samtidigt som den ansluter till din netcat-server. 

    dd if=/dev/zero bs=4096 count=1000000 oflag=sync | nc servernamn 25000

## dd skrivtest

Använd verktyget dd (disk destroyer) som skrivtest genom att skapa filen ''min_fil.noll'' full av nollor. 

    dd if=/dev/zero of=min_fil.noll bs=4096 count=1000000 oflag=sync

''count=1000000'' anger att man skriver 1 miljon block som är 4096 bytes stora. 

''oflag=sync'' anger att datan ska skrivas ut till fil innan dd avslutas, annars kommer den bara buffras för skrivning senare när OS tar sig tid. 

dd kan övervakas med en USR1 signal, här är ett exempel. 

    dd if=/dev/zero of=fil.test bs=1G count=2 oflag=sync & pid=$!
    while kill -USR1 $pid; do sleep 1; done

Nu skriver den ut statistik från dd varje sekund till din terminal, fram tills dd avslutas. 
