# Automatisera med Automator i Mac OS

Jag har endast använt Automator i Mac OS Lion så skärmdumparna nedan är därifrån. 

# Kör Bash script

Jag har ett litet script som startar [Dwarf Fortress](http://www.bay12games.com/dwarves/) åt mig. Vanligtvis skriver jag bara dwarf i skalet eftersom det ligger i $HOME/bin men nu har jag en .app jag kan köra. Med Spotlight t.ex. 

Scriptet är enkelt men anpassat för min säregna installation av DF, så försök inte kopiera utan att veta vad ni gör. 

	#!/bin/sh
	PWD="$HOME/Applications/Dwarf Fortress"
	export DYLD_LIBRARY_PATH=${PWD}/libs
	export DYLD_FRAMEWORK_PATH=${PWD}/libs
	cd "${PWD}"; "${PWD}/dwarfort.exe"

Starta Automator och börja med att skapa ett Program. 

{{..:automator01.png|Välj Program först}}

Välj sedan Verktyg i menyn längst till vänster. 

{{..:automator06.png|Välj verktyg, dra kommandotolkskript till höger}}

När det är valt kan ni dra alternativet 'Kör kommandotolkskript' från nästa meny till högra arbetsytan. 

Nu ska det se ut så här, men peka sökvägen till ert script, och lägg till egna argument vid behov. Enkelt och kraftfullt. 

{{..:automator07.png|Kör kommandotolkskript}}

Spara programmet, njut. 

# Montera delade kataloger

Först av allt starta Automator och när den frågar vilket typ av projekt du vill göra så väljer du Program. 

{{..:automator01.png|Välj Program först}}

I listan längst till vänster väljer du *Filer och mappar*. 

{{..:automator02.png|Välj Filer och mappar}}

Sedan ska du dra *Hämta angivna servrar* från nästa lista och in i arbetsytan till vänster. Då ska det se ut så här. 

{{..:automator03.png|Dra Hämta angivna servrar till vänster}}

Här kan du lägga till alla servrar som du vill ska monteras genom att trycka på Lägg till knappen för varje server. Det ser då ut så här. 

{{..:automator04.png|Lägg till}}

När alla dina servrar lagts till ska du dra objektet *Anslut till servrar* från menyn direkt till höger, till arbetsytan. Så det till slut kan se ut så här. 

{{..:automator05.png|Färdigt}}

Välj nu att spara Programmet där du vill ha det, jag brukar dra in det i Dockan sen också för att komma åt det enkelt. Att dubbelklicka eller köra programmet kommer be om användarnamn och lösenord, då kan du välja att spara det i nyckelringen så sker det lite mer automagiskt i fortsättningen. 

## Montera delade kataloger med applescript

Åtgärden ovan kan även göras med apple script, kommer fungera på ungefär samma sätt. 

	tell application "Finder"
		mount volume "smb://server.domain/share$/" as user name stefan
		mount volume "afp://macserver.domain/katalog/" as user name stefan
	end tell

Så det går att fylla på med hur många rader som helst här, spara detta i AppleScript-redigeraren så kan även det fungera med ett enkelt dubbelklick och inskrivning av lösenord. 

