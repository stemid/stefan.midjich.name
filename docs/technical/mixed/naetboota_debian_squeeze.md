# Nätboota Debian Squeeze

Här fortsätter vi på dokumentet [nätboota ubuntu 12.04](../naetboota_ubuntu_12_04) genom att gå igenom samma procedur för Debian Squeeze. Därför gör jag de första stegen lite kortare för att endast gå igenom skillnaderna. 

## DHCP

Här är det bara sökvägen som skiljer, för att organisera våra olika operativsystem. 

```
group {
 filename "debian_amd64/pxelinux.0";
 next-server pxe.swehack.se;
  
 host debianlab01 {
  hardware ethernet 00:0C:29:DF:D2:CD;
  fixed-address debianlab01.swehack.se;
 }
 host debianlab02 {
  hardware ethernet 00:0C:29:EA:CF:06;
  fixed-address debianlab02.swehack.se;
 }
}
```

## Tftp-server

Inget behöver ändras i konfigurationen, bara en ny katalog ska läggas till. 

    mkdir /var/www/pxe.swehack.se/debian_amd64

## Apache

Inget behöver ändras i Apache. 

## Skapa systemfiler

Vi hämtar systemfilerna från [Debians FTP](ftp://ftp.se.debian.org/debian/dists/squeeze/main/installer-amd64/current/images/netboot/) istället för en ISO. 

```
cd /var/www/pxe.swehack.se/debian_amd64
wget -rl 10 -nH -np --cut-dirs=8 ftp://ftp.se.debian.org/debian/dists/squeeze/main/installer-amd64/current/images/netboot/
chown -R tftp:www-data .
```

Redigera filen ``debian_amd64/pxelinux.cfg/default`` så den ser ut så här. 

```
include debian-installer/amd64/boot-screens/menu.cfg
default debian-installer/amd64/boot-screens/vesamenu.c32

label Preseed Debian
    menu default
    kernel debian-installer/amd64/linux
    append auto=true hostname= netcfg/choose_interface=eth0 netcfg/get_domain=swehack.internal url=http://pxe.swehack.se/debian_amd64/preseed.cfg vga=normal initrd=debian-installer/amd64/initrd.gz ramdisk_size=16432 root=/dev/rd/0 rw  --

prompt 0
timeout 5
```

Kickstart stöds inte riktigt av Debian Squeeze, jag hoppas Wheezy ska ha bättre stöd. 

Debian preseed kan ta emot konfiguration direkt från bootargumenten, i detta fallet måste vi skicka in ett antal argument för att undvika att den ber om bekräftelse för värdnamnet den får från DHCP. 

## Preseed

[Preseed](http://d-i.alioth.debian.org/manual/en.i386/apb.html) är metoden vi ska använda istället för kickstart.

Redigera filen som ``url=`` argumentet pekar mot i ovannämnda fil, ``debian_amd64/preseed.cfg``, så den ser ut så här.  

```
#DHCP 
d-i netcfg/choose_interface select eth0
d-i netcfg/get_hostname string unassigned-hostname
d-i netcfg/get_domain string swehack.internal

#Locale
d-i debian-installer/locale string sv_SE

#Keymap
d-i console-keymaps-at/keymap select se-latin1
d-i console-keymaps-at/keymap select se-latin1

#Mirror
d-i mirror/country string enter information manually
d-i mirror/protocol string http
d-i mirror/http/hostname string ftp.se.debian.org 
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string 
d-i mirror/suite string squeeze

#Timezone
d-i clock-setup/utc boolean true
d-i time/zone string CET
d-i clock-setup/ntp boolean true
d-i clock-setup/ntp-server string se.pool.ntp.org

#Partitioning, lvm
d-i partman-auto/method string lvm

#Remove old LVM volumes and software RAID
d-i partman-lvm/device_remove_lvm boolean true
d-i partman-md/device_remove_md boolean true

#Confirm writing LVM volumes
d-i partman-lvm/confirm boolean true
d-i partman-lvm/confirm_nooverwrite boolean true

#All files in one volume
d-i partman-auto/choose_recipe select atomic

#Finish LVM
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

#APT
d-i apt-setup/non-free boolean true
d-i apt-setup/contrib boolean true

#Standard base packages
tasksel tasksel/first multiselect standard

#Additional packages
d-i pkgsel/include string openssh-server build-essential vim curl rsync nmap

#Skip root, use sudo
d-i passwd/root-login boolean false

#User account
d-i passwd/user-fullname string Stefan Midjich
d-i passwd/username string stemid
#d-i passwd/user-password password changeme
#d-i passwd/user-password-again password changeme
d-i passwd/user-password-crypted password $1$...

#Install grub on MBR if no other OS exists
d-i grub-installer/only_debian boolean true

#Avoid that last message about the install being complete.
d-i finish-install/reboot_in_progress note
d-i cdrom-detect/eject boolean false

#Popularity contest
d-i popularity-contest/participate  boolean false
```

Lägg märke till raderna med lösenord, här måste man antingen välja klartext med ett enkelt lösenord eller skriva in ett krypterat lösenord. 

### Generera ett lösenord

På Debian kan det göras med openssl så här. 

    openssl passwd -1 -salt "$(openssl rand 6 -base64)"

## Postinstall

Lägg till följande rad i preseed-filen. 

    #Postinstall commands
    d-i preseed/late_command string wget http://pxe.swehack.se/debian_amd64/postinstall -O /target/postinst && chmod 755 /target/postinst && debconf-disconnect bin/in-target /postinst && rm /target/postinst

Skapa sedan ett skript på webbservern i ``debian_amd64/postinstall`` som kan se ut så här. 

```bash
#!/bin/bash

mkdir /home/stemid/.ssh
echo 'ssh-rsa AAAA...' > /home/stemid/.ssh/authorized_keys

chown -R stemid:stemid /home/stemid/.ssh
chmod 0600 /home/stemid/.ssh/authorized_keys

usermod -a -G staff stemid

echo '%staff ALL=NOPASSWD: ALL' > /etc/sudoers.d/staff
chmod 0440 /etc/sudoers.d/staff
```

Det skriptet kan då användas för att köra kommandon i det installerade systemet, i chroot. 

# Se också


*  [Nätboota Debian Wheezy](../naetboota_debian_wheezy)
*  [Nätboota Ubuntu 12.04](../naetboota_ubuntu_12_04)
*  [Ansible](../../ansible/inledning)
*  [Preseed dokumentation](http://d-i.alioth.debian.org/manual/en.i386/apb.html)
