# Nätboota Ubuntu 12.04

Här visar jag hur jag skapade en PXE nätboot-server i mitt nätverk. För just den här artikeln väljer jag att installera ett par Ubuntu 12.04 servrar i VMware Fusion. 

## Konfiguration av DNS

Min namnserver kommer med OpenBSD så konfigurationen är lite annorlunda men här är ett exempel av hur jag gör på en Debian Wheezy. Bör inte skilja sig mycket från en Ubuntu Precise. 

Installera först namnservern Bind9. 

    apt-get install bind9

Den lyssnar på alla gränssnitt som standard, det kan ändras i ''/etc/bind/named.conf.options''. Redigera filen ''/etc/bind/named.conf.local'' för att lägga till en zon. 

    zone "staging.int" {
   type master;
   file "/var/lib/bind/swehack.se";
    };

Väldigt enkel zon eftersom vi bara har en server på ett intranät. 

Skapa zonfilen i ''/var/lib/bind/swehack.se'' som vi angivit ovan. 

    $TTL 3600
    $ORIGIN .
    swehack.se.  SOA     ns1.swehack.se.        noc.swehack.se. (
                2013011001
                10800
                3600
                604800
                3600
        )
    
        NS      ns1.staging.int.
        A       192.168.22.1
    
    $ORIGIN staging.int.
    
    ubuntulab01    IN      A       192.168.22.101
    ubuntulab02    IN      A       192.168.22.102
    ns1            IN      A       192.168.22.1

Igen, en väldigt simpel zon för demonstrationssyfte. 

Nu kan vi använda ''fixed-address'' i dhcpd.conf här nedan, då slår DHCP-servern upp namnet och ger oss den ip-adressen som vår DNS svarar med. 

## Konfiguration av DHCP

Jag har en [.:alixrouter med openbsd](./alixrouter med openbsd) hemma som är min DHCP-server. I den har jag följande konfiguration för mina Ubuntu servermaskiner. 

    option  domain-name "swehack.se";
    option  domain-name-servers 192.168.22.1;
    option routers 192.168.22.1;
    use-host-decl-names on;
    
    subnet 192.168.22.0 netmask 255.255.255.0 {
   group {
    filename "ubuntu_amd64/pxelinux.0";
    next-server pxe.swehack.se;
    
    host ubuntulab01 {
     hardware ethernet 00:0C:29:AC:EE:FA;
     fixed-address ubuntulab01.swehack.se;
    }
    host ubuntulab02 {
     hardware ethernet 00:50:56:36:B9:F1;
     fixed-address ubuntulab02.swehack.se;
    }
   }
    }

De viktiga raderna är så klart filename och next-server. Den förstnämnda anger filsökvägen som ska anropas på tftp-servern, och next-server är själva tftp-servern som ska användas. 

Jag går inte in djupare i hur man konfigurerar en DHCP-server på OpenBSD här, men bemärk att den även agerar som en intern DNS med en egen view för mitt intranät. Så namnen ubuntulab01 och ubuntulab02.swehack.se är giltiga värdnamn i mitt intranät hemma. 

Eftersom både OpenBSD och Debian använder ISC DHCP-server så är konfigurationen väldigt lik på Debian.

## Tftp-server

Installera paketet tftpd-hpa på Debian. 

    apt-get install tftpd-hpa

Konfigurera filen ''/etc/default/tftpd-hpa'' så den ser ut så här. 

    TFTP_USERNAME="tftp"
    RUN_DAEMON="yes"
    TFTP_DIRECTORY="/var/www/pxe.swehack.se"
    TFTP_ADDRESS="0.0.0.0:69"
    TFTP_OPTIONS="-l --secure"

Jag har valt att använda katalogen ''/var/www/pxe.swehack.se'' för att lagra alla installationer jag tänker nätboota. I detta fallet, med Ubuntu för amd64, kommer det finnas en underkatalog som ni såg i DHCP konfigurationen ovan, ubuntu_amd64. 

    mkdir -p /var/www/pxe.swehack.se/ubuntu_amd64

## Apache

Vi behöver Apache för att dela ut kickstart konfigurationen. Redigera ''/etc/apache2/sites-available/pxe.swehack.se'' så den ser ut så här. 

    `<VirtualHost pxe.swehack.se:80>`
    DocumentRoot /var/www/pxe.swehack.se
    ServerName pxe.swehack.se
    
    `<Directory /var/www/pxe.swehack.se>`
    Order allow,deny
    Allow from all
    `</Directory>`
    `</VirtualHost>`

Aktivera sidan och ladda om Apache-tjänsten. 

    a2ensite pxe.swehack.se
    service apache2 reload
## Skapa systemfiler

Filerna som behövs för att nätboota Ubuntu kan hittas på [alternate CDn](http://releases.ubuntu.com/12.04/). Så vi monterar den ISO-filen och kopierar dem till webbroten. 

    mount -o loop ubuntu-12.04.1-alternate-amd64.iso /mnt/cdrom
    cp -R /mnt/cdrom/install/netboot/* /var/www/pxe.swehack.se/ubuntu_amd64/
    chown -R tftp:www-data /var/www/pxe.swehack.se

Sedan redigerar vi filen ''ubuntu_amd64/pxelinux.cfg/default'' så den ser ut så här. 

    include ubuntu-installer/amd64/boot-screens/menu.cfg
    default ubuntu-installer/amd64/boot-screens/vesamenu.c32
    
    label Kickstart Ubuntu
          menu default
          kernel ubuntu-installer/amd64/linux
          append ks=http://pxe.swehack.se/ubuntu_amd64/ks.cfg vga=normal initrd=ubuntu-installer/amd64/initrd.gz ramdisk_size=16432 root=/dev/rd/0 rw  --
    
    prompt 0
    timeout 5

De 4 extra raderna jag har lagt till här gör att när den vanliga boot-menyn visas så är det sista alternativet (//Kickstart Ubuntu//) förvalt, så tryck bara Enter för att fortsätta. 

Jag redigerar även timeout värdet som vanligtvis är 0 så den väntar för evigt. Med ett värde av 5 sekunder så kommer den nästan omedelbart välja alternativet som är förvalt. 

I raden som börjar med append ser ni kickstart-filen som delas ut av Apache. 

## Kickstart

Kickstart började som ett program från RedHat för att automatiskt välja alternativ under installation. Förhoppningsvis kan resten av öppen källkodsvärlden adoptera detta som en standard för det finns nu stöd för det i Ubuntu och Debian. 

Paketet system-config-kickstart i Ubuntu har ett grafiskt gränssnitt för att skapa kickstart-filer som är bra att utgå ifrån. 

Här är ett exempel för detta dokumentet som jag placerar i katalogen ''/var/www/pxe.swehack.se/ubuntu_amd64/'', självklart är lösenordet inte äkta. 

    #System language
    lang sv_SE
    
    #Language modules to install
    langsupport sv_SE
    
    #System keyboard
    keyboard se
    
    #System mouse
    mouse
    
    #System timezone
    timezone Europe/Stockholm
    
    #Root password
    rootpw --disabled
    
    #Initial user
    user --name stemid --fullname "Stefan Midjich" --iscrypted --password $1$qwerty...
    
    #Reboot after installation
    reboot
    
    #Use text mode install
    text
    
    #Install OS instead of upgrade
    install
    
    #Use Web installation
    url --url http://ftp.df.lth.se/ubuntu/
    
    #System bootloader configuration
    bootloader --location=mbr 
    
    #Clear the Master Boot Record
    zerombr yes
    
    #Partition clearing information
    clearpart --all --initlabel 
    
    #Create partitions
    partition /boot --size=200 --fstype ext2 --asprimary #Boot
    partition swap --size=512 --asprimary #Swap
    partition pv.01 --size=1 --fstype ext4 --grow #Rest
    
    #Create lvm
    volgroup vg0 pv.01
    logvol / --vgname=vg0 --size=1 --grow --name=root
    
    #System authorization infomation
    auth  --useshadow  --enablemd5 
    
    #Firewall configuration
    firewall --disabled 
    
    #Do not configure the X Window System
    skipx
    
    #Packages
    %packages
    ssh
    vim
    curl
    nmap
    
I botten får man välja vilka paket som ska installeras, hela kategorier kan väljas med @prefix. 

Notera även raderna där partitioner och logiska volymer skapas, här kan andra diskar användas med argumentet ''--ondrive'' och självklart kan storlekar ändras. Läs mer om det i [kickstart-manualen](https://access.redhat.com/knowledge/docs/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/s1-kickstart2-options.html). 

Bemärk också att Ubuntu väldigt nyligen saknade stöd för att skapa LVM med kickstart. Jag vet inte hur nytt det är men det tog många försök att få det rätt. Gör man fel så kan man t.ex. få fram standardpartitioneringen och en ruta som ber om godkännande. 

### Kända fel

Jag kan bara inte ange vilka andra grupper som min användare ska vara medlem i. 

    user --name stemid --groups staff

Då stannar installationen och ignorerar hela den raden. Så efter uppstart måste jag lägga till min användare i den gruppen. 
## Postinstall

Vi kan även lägga till lite extra saker i slutet av kickstart-filen som ska köras efter, eller före, installationen. Var dock försiktiga med ''%pre'' eftersom det kan förstöra för kickstart. 

    #Postinstall
    %post
    mkdir -p /home/stemid/.ssh
    echo 'ssh-rsa AAAAB...' > /home/stemid/.ssh/authorized_keys
    chmod 0600 /home/stemid/.ssh/authorized_keys
    chown -R 1000:1000 /home/stemid/.ssh
    echo '%staff ALL=NOPASSWD: ALL' > /etc/sudoers.d/staff
    chmod 0440 /etc/sudoers.d/staff


Tyvärr körs postinstall-skriptet **innan** användare skapas, vilket gör det ganska värdelöst om man inte skapar sina användare i själva post-skriptet. Jag ville bara nämna att man kan göra mycket där, speciellt om man använder ''--interpreter /usr/bin/python''. 

Här ovan t.ex. fular jag mig lite och skapar en SSH-nyckel men jag sätter ägaren till 1000 och gruppen till 1000 för jag vet att den första användaren och gruppen som skapas i Ubuntu har dessa ID. 

# Se också


*  [.:Nätboota Debian Squeeze](./Nätboota Debian Squeeze)

*  [.:Nätboota Debian Wheezy](./Nätboota Debian Wheezy)

*  [Konfigurera DHCP](./alixrouter_med_openbsd#konfigurera_dhcp)

*  [..:ansible](../ansible)

*  [RedHat dokumentation för Kickstart](https://access.redhat.com/knowledge/docs/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/s1-kickstart2-options.html)

*  [Mer RedHat dokumentation för Kickstart](https://access.redhat.com/knowledge/docs/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/ch-redhat-config-kickstart.html)

*  [Ubuntu Wiki om nätbootning](https://help.ubuntu.com/community/PXEInstallServer)

*  [Ubuntu Wiki om Kickstart kompatibilitet](https://help.ubuntu.com/community/KickstartCompatibility)

*  [Ubuntu bug #48311](https://bugs.launchpad.net/ubuntu/+source/kickseed/+bug/48311)

*  [Preseed exempel](http://pastebin.com/k02VMT1v)
