# Crash course in Tmux

This is focused on getting a beginner quickly started with a shared tmux session.

# Starting/Attaching tmux

Start a new session by running tmux alone.

    $ tmux

Or a shared session.

    $ tmux -S /tmp/shared.tmux

Attach to your default session.

    $ tmux attach

Or attach to your shared session.

    $ tmux -S /tmp/shared.tmux attach

Name your session.

    $ tmux -S tmp/shared.tmux new-session -s mysession

Attach to named session.

    $ tmux -S /tmp/shared.tmux attach -t mysession

List sessions.

    $ tmux list-session

# Share your session with others

Make the session file you specified with -S readable by a group. (Create the group first and add your users into it)

    $ chgrp tmux /tmp/shared.tmux

# Navigating tmux

Every command is prefixed by the *prefix key* which is **ctrl-b** by default. Shown here are default key bindings but you can also change them.

## Exit tmux without killing it (detach)

This is probably the first thing you'll want to do.


*  **Prefix+d** - d for detach.
## Switch window

The windows are shown at the bottom numbered 0 to 9. 

*  **Prefix+n** - Next window, cycle to the right.

*  **Prefix+p** - Previous window, cycle to the left.

## Switch pane

The window might be split into panes, horizontally or vertically.


*  **Prefix+Arrow key** - Move to the pane in the direction of the arrow key, up, down etc..

## Zoom in on pane


*  **Prefix+z** - Zooms in on the active pane. Again will zoom out. Or use another pane navigation command.
# Tmux commands

To execute tmux commands you hit **Prefix+:** (colon) which starts the command prompt at the bottom of tmux, in the status bar.

## Split window into pane


*  **:splitw -v -p 50** - Split window vertically by 50%.

*  **:splitw -h -p 50** - Split window horizontally by 50%.

## Open new window


*  **:neww** - Open a new window.

*  **:neww -n vim vim** - You can also name a window with **-n** and execute a command in it as final argument.

## Close a window

To close a window simply exit whatever is running in it, shell or program.

But you can also force a window closed.


*  **:killw** - Kill the window.

# Tips and tricks

That's it for the crash course but here are some tips from my own config.

## Configure tmux in .tmux.conf

The default prefix key is awkwardly placed imo so I use Ctrl-f.

Put this into your ''$HOME/.tmux.conf'' file.

```
# Change prefix key to Ctrl-f
set-option -g prefix C-f

# Set window numbering starting at 1 instead of 0

set-option -g base-index 1

# Make active window more visible

set-window-option -g window-status-current-bg red

# Terminal history

set -g history-limit 9001


```
