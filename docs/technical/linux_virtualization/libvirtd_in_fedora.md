# Virtualization with libvirtd on Fedora 30

I found [the official Fedora docs](https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-virtualization/) lacking so here's how I setup libvirtd on Fedora 30.

## Install and run

    $ sudo dnf install @virtualization-headless opus libdrm libpciaccess
    $ sudo systemctl enable --now libvirtd

## Virt-manager access with ssh key

Create a user account on the server which is a member of the group ``libvirt``. Or add your own user to this group.

    $ sudo usermod -a -G libvirt virt-manager

Add your ssh key to this user account.

Now you can run virt-manager from your laptop and connect to the libvirt server, click Add new connection.

![Create new connection in virt-manager](/files/virt-manager_create_new_connection01.png)

Select Custom URI in the dropdown.

![Select Custom URI](/files/virt-manager_select.png)

Enter the URI, the whole URI should look something like ``qemu+ssh://virt-manager@nas02/system`` so don't forget the system part at the end which isn't visible in the screenshot.

![Custom URI](/files/virt-manager_connection_uri01.png)

Now you can connect to one or more KVM/libvirtd hypervisors from the comfort of your own laptop and the virt-manager GUI.

## Setting up an ISO data store

For simplicity I put my ISO files on a NFS share, but that's of course much slower than having them local.

I already have an NFS share setup with autofs and mounted under /media/nas/Downloads, so my ISO path would be /media/nas/Downloads/Misc/ISO.

This is real simple to do in the virt-manager GUI if you double click a hypervisor connection and look at the Storage tab.

## Setting up a custom vm image store over NFS

One of my hypervisors has a lot of BTRFS storage space so I'm sharing a BTRFS volume over NFS to my other hypervisor.

This requires ``sudo setsebool -P virt_use_nfs=on`` for SElinux to allow it.

Then you can just mount it under ``/var/lib/libvirt/images/nas02`` for example, because my BTRFS server is called nas02.

Here's my fstab entry.

    nas02:/var/btrfs/vmimages /var/lib/libvirt/images/nas02   nfs     defaults        0       0

After that you add it in the virt-manager storage GUI.
