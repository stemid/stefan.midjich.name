# Virtualisering med Kimchi

Här är några saker jag uppmärksammat i installationen av Wok, Ginger och Kimchi. 

Detta är aktuellt i skrivande stund, 2016-09-13, med grenen master från deras github-förråd. 

## Bakgrund

Kimchi är ett plugin till Wok som är ett webbramverk för utveckling av HTML5 applikationer. I wok finns ett plugin som heter Ginger som ger dig möjlighet att utveckla plugins för hantering av Linux system. Kimchi är alltså ett plugin utvecklat med hjälp av Ginger. 

# Installation

    git clone --recursive https://github.com/kimchi-project/wok
    cd wok
    ./build-all.sh
    for plugin in src/wok/plugins/*; do
    pushd $plugin
    sudo make install
    popd
    done
    sudo make install

# Konfiguration

Det mesta görs i ''/etc/wok''.

Wokd använder portar som kanske inte tillåts av SElinux, därför behöver de ändras och klassificeras med ''semanage port'', se länken längst ner på sidan för mer info.

Installationsskriptet installerar även en systemd-unit för wokd som är huvhudtjänsten. 

Det installeras även en fil i nginx konfigurationen för att proxya webbanslutningar från webben till wokd.




# Se också


*  [.:Virtualisering i CentOS 7 med libvirtd](./Virtualisering i CentOS 7 med libvirtd)

*  [Klassificera om portar med SElinux](..//selinux_recept/blandat#klassificera_om_en_port)
