# virt-manager i Fedora 25

Hur du ansluter till en remote libvirt-server från Fedora. 

# Konfa servern

Se [Remote management av libvirt med virt-manager](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Virtualization_Administration_Guide/sect-Virtualization-Remote_management_of_virtualized_guests-Remote_management_over_TLS_and_SSL.html).

Tillåt libvirt-tls i brandväggen. 

    $ sudo firewall-cmd --add-service libvirt-tls --permanent

## Konfa gäst VM för konsoll

Stäng ner din VM och redigera den under `<graphics>`. Låtsas att du vill nå din gäst på server IP 192.168.22.80.

    $ sudo virsh shutdown vm-web-1
    $ sudo virsh edit vm-web-1
    ...
    `<graphics type='spice' autoport='yes' listen='192.168.22.80' keymap='sv'>`
    `<listen type='address' address='192.168.22.80'/>`
    `</graphics>`


Tillåt även inkommande anslutningar för gästerna. 

    $ sudo firewall-cmd --add-port=5901/tcp --permanent
