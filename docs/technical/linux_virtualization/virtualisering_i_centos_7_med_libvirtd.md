# Virtualisering i CentOS 7 med libvirtd

Här går jag snabbt igenom saker jag uppmärksammer med installation- och konfigurationsprocessen, samt länkar till relevant dokumentation. 

>**Observera** att jag i skrivande stund använder Kimchi som gränssnitt till libvirt, vilket betyder att guiden här mest täcker saker som Kimchi inte sköter åt mig redan. 

Eftersom all libvirt konfiguration är i XML så är det himla smidigt att ha något form av gränssnitt. T.ex. [oVirt](https://www.ovirt.org) eller [Kimchi](https://github.com/kimchi-project/kimchi).

## Paketinstallation

    sudo yum group install 'Virtualization Host'
    sudo yum install kvm virt-manager libvirt virt-install qemu-kvm

## Starta tjänst

    sudo systemctl start libvirtd

## Virsh

>Härifrån och neråt kan det vara intressant att prova ett gränssnitt som sköter virtualisering åt dig, t.ex. Kimchi som du kan läsa mer om [här](./Virtualisering med Kimchi).

Virsh är verktyget som används för att göra i princip alla ändringar av din KVM hostingmiljö. 

*  Nätverk
*  Lagring
*  Konsoll
*  Bootenhet
*  osv.

## Nätverk

### Bryggning

Bryggade nätverk är den typ jag föredrar hemma, så att alla gäster får IP från mitt LAN och uppför sig som nya maskiner i mitt LAN. Det kräver att du bryggar ett fysiskt nätverkskort till en virtuell brygga. 

Här är ett exempel där mitt fysiska nätverkskort heter enp2s0. 

Filen ``/etc/sysconfig/network-scripts/ifcfg-enp2s0``.

```
TYPE="Ethernet"
#BOOTPROTO="dhcp"

NAME="enp2s0"
UUID="9f1309da-XXXX-skapas-av-systemet"
DEVICE="enp2s0"
ONBOOT="yes"
BRIDGE="virbr0"
NM_CONTROLLED=no
```

Och filen för bryggan ser ut så här.

```
NAME="virbr0"
DEVICE="virbr0"
TYPE=Bridge
ONBOOT=yes
BOOTPROTO="dhcp"
NM_CONTROLLED=no
DELAY=0
DEFROUTE="yes"
PEERDNS="yes"
PEERROUTES="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_PEERDNS="yes"
IPV6_PEERROUTES="yes"
IPV6_FAILURE_FATAL="no"
```

Men **observera** att använder du ett system som Kimchi så skapas bryggan åt dig och det kan faktiskt bli fel om du redan har en brygga fördefinierad. I mitt fall valde jag att låta Kimchi använda virbr1 så den fick lägga upp allt, då skapas inga filer under ``/etc/sysconfig`` heller.

### Statisk DHCP-utdelning för VM

>**OBS** detta är bara aktuellt om du använder NAT-typen av nätverk för gäster.

Finns ett nätverk redan skapat så kan detta göras enkelt med ``virsh net-update``. Här är ett par exempel. 

    $ sudo virsh net-update default add dns-host \
    "`<host ip='192.168.122.80'>``<hostname>`web01`</hostname>``<hostname>`web01.swehack.local`</hostname>``</host>`" --current
    $ sudo virsh net-update default add ip-dhcp-host \
    "`<host mac='52:54:00:85:51:19' name='web01.swehack.local' ip='192.168.122.80'/>`" --current

Det ser lite rörigt ut men argumentet med XML data kan även peka till en fil som innehåller datan och ser lite snyggare ut. 

```
    <dhcp>
      <range start='192.168.122.2' end='192.168.122.254'/>
      <host mac='52:54:00:85:51:19' name='web01.swehack.local' ip='192.168.122.80'/>
    </dhcp>
```

    $ sudo virsh net-update default add ip-dhcp-host update_data.xml --current

>Detta resulterar i ändrade filer under ``/var/lib/libvirt/dnsmasq``, den dnsmasq-instansen styrs ej av din vanliga dnsmasq.service unit.

Läs mer i virsh manualen.

## Lagring

### iSCSI LUN

Hemma från mitt Synology NAS när jag konfigurerar en iSCSI LUN till libvirt så visas alla som fulla, alltså 100% nyttjad kapacitet. Jag vet inte om det är specifikt för Synology, eller libvirt, men det är bara att fortsätta. När du väl börjar installera ett OS på din VM så kommer du inte stöta på några problem.

Även Kimchi kommer fortsätta visa 100% full på iSCSI luns, trots att VMs körs på dem.

```
$ sudo virsh vol-info --pool nas unit:0:0:0
Namn:           unit:0:0:0
Type:           block
Capacity:       50,00 GiB
Allocation:     50,00 GiB
```

### Ändra startenhet

För att starta från en ISO, eller sluta starta från en ISO.

    $ sudo virsh edit web01-vm-1
    ...
    `<os>`
    `<type arch='x86_64' machine='pc-i440fx-rhel7.0.0'>`hvm`</type>`
    `<boot dev='cdrom'/>`
    `<boot dev='hd'/>`
    `<boot dev='network'/>`
    `</os>`

Flytta på raden med ``dev='cdrom'`` för att ändra ordningen av ISO t.ex..

## Se också

*  [Libvirt dokumentation om nätverksformat](https://libvirt.org/formatnetwork.html)
*  [Ändra keymap för VNC i en VM](https://github.com/kimchi-project/kimchi/issues/411#issuecomment-120455563)
*  [Remote management av libvirt med virt-manager](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Virtualization_Administration_Guide/sect-Virtualization-Remote_management_of_virtualized_guests-Remote_management_over_TLS_and_SSL.html)
*  [.:Virtualisering med Kimchi](./Virtualisering med Kimchi)
