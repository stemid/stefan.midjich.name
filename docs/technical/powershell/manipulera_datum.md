# Manipulera datum i Powershell

    bpdbjobs -report -t `"$((get-date $(get-date -uFormat "%m/%d/%Y %H:%M:%S")).AddHours(-18))`" | out-file -enc ascii "C:\shared files\bpdbjobs.txt"
