# Gitlab CI tips

# Run script templates inside job scripts

## Define the script

```yaml
.template_script: &template_script
  - echo "some more script output"
```

## Run the script in a job

```yaml
My job:
  script:
    - *template_script
```

# Predefined variables

* [List of all variables and example values](https://docs.gitlab.com/ee/ci/variables/#list-all-environment-variables)
* [Predefined environment variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
