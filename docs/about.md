# About me

* Nerdy guy from Malmö Sweden, who believes strongly in open source software.
* <a rel="me" href="https://mastodon.se/@stemid">stemid@mastodon.se</a>
* [stemid@gitlab](https://gitlab.com/stemid)
* [stemid@github](https://github.com/stemid)

## Public keys

### swehack [at] gmail [punkt] com

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGNxQAYBEADILEL9d+qdtWOLeGyorhJ2OECLzArUcHUbfzIAzILkIkDGNMMV
jJPuWB4x9JiT/jNlYtU9etgsrIVUDG/3fCa2KfX1sbzmQi9vsxihd3qzCPxifrDJ
zEvkK4WsNzQ/6UVI7Ccep9TGei70ShU/8j9vwlzTlJbNSuzsfvMuSmyLZMWOJMvC
3krSSm16VV2xIXTivXD3juQtcILQGELzJW2KIvOkHViMN4miaS+3vti/oZ10qZVV
+o8KMjv/pAHiGYQG2pVh0CUFLi77ebuh//9sHnBKUscF0TOP3Ox5PpJOcaoUH/c+
OiJFZ3DyDCIJzrTgAzv3mKTQIqjHcXQoa7X42BouzBSVojkKcjaGdFdqyLLPgx1f
RLgFXrWpk0FYuJt1M4gAh0SvkzDxM6Yl1Utcb2Wgfk9FE8bf64gmjW9pA40nSXEi
Tv7y4DJ+OR7EokTdK5MIpnfecGtOTP21xeoQJxVaSW9euraloF3zxojMCXvgCMLg
pjMwwJwaf+yy171Bt/Xs5XlkyzPCG+QsSEOrumVM9y8uatciLHUdid/PPDM/0DR2
YnXgW1ne/LqZIrYZs12pq4uTek8pkEOnv+LNP3OIB3rTLKeaE0TtmqjtoAQkZD2/
xvnNFoAI7zEjcGxqGK/y0KMg5LimWT7r9HqyleXGx9S3FZUBGPExnm/7aQARAQAB
tCpTdGVmYW4gTWlkamljaCAoR21haWwpIDxzd2VoYWNrQGdtYWlsLmNvbT6JAlEE
EwEIADsWIQQJ2GtplRW00mmPSCjQGPZjcltshwUCY3FABgIbAwULCQgHAgIiAgYV
CgkICwIEFgIDAQIeBwIXgAAKCRDQGPZjcltsh6M7EAC75c99nTIp7eG2UQ0vf1uo
3deRL30R7tLeq8RvFrvsmc+5f8TgMgFXUQsPNbpm1kqAwWrPSdYjofwGwCnb4FKz
lmEsQTVjylragnCx9dPWS5Y4SAL0mPYU5ST4Zbvr3tkFnjGacd3JAzjtj4KsG9uM
3CfSHEj9hTrtnuBFFhoOFSjYdPtMlw3m7fC91Nb/Fv3zu7pTqC5luPhU71XnZVP+
crnpn0x2UgH9/X6JHiRf/lTsdnAv3kGzhAAm7YJA4nymUTVO3m0t/ZnUz1zv/zOj
z1dBtvh+1eFM0tbbd+RDNd1OPiTOXdYdy9eXWpJOJqqv3WhBgFgLBg/Hh3cC+2Ft
yerW22//4kTY7526Jj0sZuM4ur7pSfR/zoI19aKbZzbq5h8SfQs1DR09FjkLcPc2
uJsxWiIRI5/wnwLBI4imH6SALnQEfLgk9MpNTAK8YCKhEMx2GhalmdVBwT0CC4yZ
LkrCHD8cjrWRjc1rjBhzgZAEWeuSuItO24q2BY9VWzMFoKUyNSj5zmIY4LYHtRGn
fxOKkrnDfNearSHjPVHCW+bkvuPgz3bk6SG7UYTXzrur+Atgam9c3dfOrrR1vveI
rw9Kb4djRVyawgdQsp96UPHs3hwwmWp44eBYgYjJyC9dJzSUPKUPdQwxkze13dlj
T16pCvEKRmMkkr0YWHILprkCDQRjcUAGARAAzBmFEidowow+xXsBmADZwfmABXGq
e2h77tvnY6Ow497tKo7pp57jCLKsW59gPu9DgLOtE0En8KnVbTmFw8fPsDLZoihU
TWgW3uaknMNA4Z8fXe5sQFH8lj4JZ2sdTt07h3fIadGeSejTrn12RLIDpkG756nh
UkK7tpEgWkd6pye/wSLdGMso70rFkWUbCW8s3YlwTL8IeiNrFFIK3T/Tjhv/J1ii
KKdRmC2mOz/SSYtg5QsNMzK/WgtuhnLwi0eM0Un/3aI/FIyqzY5+Oh6XpAIGcgly
fpFAyds+tq8/GUaAQwLHUxQ27//7JzFASpPkBe3fPaWdnoLguRm0ESSNks1mby3Z
5BrO1rSOgBEkzRBT5vOmuhbaAdVAofKVN58sIae4x/VgCCuywr5gH1H6LImxKy6J
S+5U/bfP8oS8r4R7bWMdQMWs8fkm9kG5+b010G+IetCf0cqobH2Ur9zDvjhZDfjq
vfd1w7IutnvlfK8oiovCubvNG9dJhBD5hTxzlhopMnroiAL+3OiInGfacUkmWvEu
9ls4WQ/4RkPZjp3eJc9Dl1oDrRCrR/X9ohmGAV+t6+OQTCDVe4SdoqbUawhYthXd
H+s9EigTVIHA7d/XcrK7xE4Mr62VW+v6aAaVnWHcosqSFxfEJeU4WgE1MQh+etLN
aQGBAPsQBpVJBTcAEQEAAYkCNgQYAQgAIBYhBAnYa2mVFbTSaY9IKNAY9mNyW2yH
BQJjcUAGAhsMAAoJENAY9mNyW2yHO54QALmtAUWFdTsW+ouOzZHFJiRAvau1CAq6
Gx/aTvXEGmyjusYwrfI4gIHZNhEwsMQuhlcewZsNtI/drPd8dQG6dk0S62IPdo6y
UtTlapmBXXnooPVEn3CM6NJhlyJLDchpkAu8SlcV6KeK/CTaxLkcVvDydGWhi3iQ
To1KB0dkfttXkPK07qlK3BsIab2VW5jd645YUNNx+XDMAjqNB/x7f6MQ+NW5t18j
LnDojFTsMC+UzSwcBSfcSSVEKpKzkWhB69wl8GaFLdpePUc9PvNTjzoKFVLfporl
dII2Jh8YrueYdduf/rofKKQAszsInucOFUEy37C1VqdhBGD9koHNUSLkMUfWJawg
B2v67jfsBB/k6kEPNGt9Lg6maL0slxfo3t5FFarShHVcmCtrf2TMgRF7Nkz9whyI
GdSf7qwSwTUbbJSVp5FMRNo3c9T9+KyiQoDvZUzJpCZHzbLoOv8kIvN9txwFXr6E
MIi44DYeIk0+RHS1GhfwQdNFBZUkRA+Nx9Km4JNwTRlcSSXN5gsmDmt00wI0ZSB5
V73l/+p/W1sCmwrhvw7GPLxCn+Qm5dLxCSDJQS0eEOfQxLSg8QjYb8Pqz12Tvgsg
V+gRjTv9smT7FBFEnHLO4XdUuRbcs6wCHyHr97EcpaRm2/xcHYsO+NDHWlH/PEiz
qr2H3FFCI7jC
=UpKc
-----END PGP PUBLIC KEY BLOCK-----
```

### stefan [at] midjich [punkt] name

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    
    mQENBFIMJ+oBCAC3HvAm5qy4cZHcx+foq7tCEYsqyVoc3pYeaBelMSgLowrkQFgP
    W4mC/9W9VfZKtpmdcbjfZ2mVR5rEm0fAvf5yv0taeoiyoFWV1pqfXsx/brWqCzT/
    qhGR9u8FOK8VOGlMjD7hzLlbOJe3Qigr6jwZED+aJc2fztKJpQmXwNLdHZI/Na9d
    H1oUd17MFEk/L0EUsiSQ/inTHXMBbNJJfveXIstbFBj7Rrdx78RNLm/bCudu1zV8
    rPrlGnxu89445mQ8smKaXyZX3gs2uRfKne89NMUhVcAxfeHtZRhLeD3+4CxQx2ej
    zqlWv+V71oDKTrGDJc83xSj/hvJnABkDBAB5ABEBAAG0JFN0ZWZhbiBNaWRqaWNo
    IDxzdGVmYW5AbWlkamljaC5uYW1lPokBNwQTAQIAIQIbAwIeAQIXgAUCVzd3mwUL
    CQgHAwUVCgkICwUWAgMBAAAKCRAlPxYWrpplmf+QCACiHGQBrDbK/ZtS9PNS+dpg
    /gH88C60C9hp0e23b0EJMslKOkx7W1WrRJoe8dvme9NRlrNYP8dY45C5Yun1rfZB
    QaCIyGTrIjcoZ/MM85ZhUmiedCV84erhZt3/FLmm3Yb7E2LfWiUnn4gdWHrqj14J
    1x8kwtfDP7Vplc0hnPjjS1B5dZtC1D9dAChqf+7+MtEzodEYBbTMDUaWv6LmAbkO
    E6AnWDEsJndjX9lPUPzOn9SyhesucocIHMl+BrdDg7UwshQUK7iFIKJNGpl7EIJ6
    FzrLq1meimMC6mtC8M7yT4snrZ+6nuoOBj4TT+4EIXO9uKscf39KYusa7VPuNQNJ
    uQENBFIMJ+oBCADwIK9XMYgSBMZb5PODLNjnCw4AuLpoqvoIkl8P3Zy+MWsjx/bu
    XvLmiTMWaC/I55UskEFO/i9HjeEaRYJQe25UbvpzsaNUo2JYhW7e1oCaxNM1CYzq
    DTgjWIH9drOX42hEpjc5Y495ML6D2wAiiboD3kfl1m6VCrdxQJRyBMgDMNkPeA8z
    uRDKtJ83BjnlYJ7SQhr2zuR8b2m5dQ6wAYRqzM+DCGEN4uzCH7V2lpHkbe9tlgJn
    NMz9VuyEbGgUZaaM+8W+5prJaL4xSI7VdhZDjjrcw0/JslNQCKU0yLYy6AIa6Zli
    tVHlP2IZeiCDSH1sTDESWgH/REqPe1wd+zuZABEBAAGJAR8EGAECAAkFAlIMJ+oC
    GwwACgkQJT8WFq6aZZmX1wf6AtJC6LzWaE4zcgIRTJaSkmV+8i6osgVmlTaGEQ8g
    kHfZibKs80muD7a2kH/kDIe0sYDTB80ZfXu7RCvj11JRO4UVBaVz6hJjWTUFCbDG
    QN2m3AeDHOecYVqyJ79a06PW6op6+JJCR1ZSByLjZ/k1wjesRLODje6Qq6VVYOx3
    bhAsIj48OeSuav3zEcngAIHfJQiA5nW1ip/0j4TTQPr4HasgHC4W3B2iRtgTMdX2
    fDPesUuR5hliD7vzDI4KhkYbyVC61MKjriu6IQWszZZnRYFAlBeWu3ZpxuO9BpIn
    Ft78uz4Lr23bbDbpoT6RtGySnEc2wIxKfXH+3ovKvNeWuLkBjQRWAp8AAQwAinPf
    K3ToDSeMZd7ASLZPSLk3VSx4JZxAobqnDyuBQMRUU+fIEWfFqDjg+WaovaPdjKjq
    px8XtUx0Mr0y6p0lbCcXKLNRC4T5DaRawiFnicUaXYpHI7oZJ5EL7ffahEL7HDAO
    mDfSh6XaiOgfGH+apM60iD+xvXkVuxPBIumxzXXweoNFHGvESSzcf8hjQUlwXANv
    OkOtleDYwcRRsrqx+1A26uezhzkraI8RynB2aJZg2Qr30PASgzpY16ZGgRh7ts0d
    wV9z7mSq7LcIajn5BExYk8E6s/oa8RFe+Le879Hg7aenMdb9cRs2ZVRxyH1R4/nb
    uDM2VPXuZ7mraa9dEQNYJm9F/nyFvEa3MWNzJhEIAkPeRpzc0hS6SO2qVeqKpuN2
    4a9CW2pLD4gxhLab8xhCNX/2rkq4XSNo21l1qQAixR8r3Jhzkiydtncm6Lba4CZQ
    ekGlTxvXS1tzN1E4GRW+6Ze90ORaoYhfMvwa7UDHEjffx5SOLdwGZgjLwHcPABEB
    AAGJASUEGAECAA8FAlYCnwACGyAFCRLMAwAACgkQJT8WFq6aZZmdHQgAla1BH/pW
    3KA8MBy3I3SSvoDWzEWLcSwePbvcXyj4WyssrLNM7qaqJmwAexFMhos20cxMy5Dc
    C/NA9DsnD/TIXaX5fIAViBh/dtKcvLk+uxQOEL62/iTEsg/W76vHCwuC6Rx83j6i
    FD/zFqLsnfSO0vzjZAK0xwNzwMtagDI7PLfL6Sja8OTGQOulQfCSxCGsQPtRSMYy
    VeJguZpw0UKkQURhwOIRnU2t8yEDvqzNt5joVOmX6dI+OBaaHNOp+hiMFhZot8RE
    O3pA7wAxNH3NNhgIYhXhfRhblsZOX5cfL9xTlu/TF8HxCL0qLZ2Pm5BN+Vx6awf2
    Dt7SJHNlphkMAQ==
    =bRMq
    -----END PGP PUBLIC KEY BLOCK-----

## Fingerprints

### Omemo laptop1

    f089a63a38262b582f49aa947a751f4f18389048ce74b3e7212423e72b9bfb6b

# About this site

* Combined personal blog and wiki.
* Some pages are in English 🇬🇧
* Some pages are in Swedish 🇸🇪

## History

* 2012 - [The Wiki](sydit/wiki.sydit.se.md) was started because it helps me to write down documentation.
* 2017 - I started writing new pages in English and slowly but surely translating old pages to English. So expect a mixed bag of Swedish and English from now on.
* 2020-03-22 - During the big quarantine I decided I also wanted to use the wiki as a blog and personal website so I moved it to stefan.midjich.name.

## Deployment

The whole site is available at, and deployed from, [Gitlab.com/stemid/stefan.midjich.name](https://gitlab.com/stemid/stefan.midjich.name.git).

## Why use a Wiki for a blog?

Because just like people ideas aren't static. If I write something down I want the option to update it. So I've long felt that a Wiki is better suited for blogging.

