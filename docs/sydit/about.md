# About SydIT

[SydIT](https://sydit.se) is only a concept for now.

It's a name I chose to represent all the services I host for other people. It was meant to be an organization or a non-profit with the focus of helping people adopt open source but for now it's only a concept and a place to gather all the services, contact info and perhaps status info.

