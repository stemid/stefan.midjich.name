# Wiki.sydit.se

The old deprecated predecessor of this site was [wiki.sydit.se](https://wiki.sydit.se). 

It was started to help me remember things I learned by writing it down. And also with the hopes to help others build systems based on open source software.

It was started in Swedish but later I realized this was a waste of time since everyone in Sweden speaks english well. And if you're going to use open source software you pretty much **have to** understand english to a decent degree.
